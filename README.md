# Victory Academy SINTAK

Project ini adalah program web untuk VCIA dibuat untuk profil perusahaan, SINTAK manajemen siswa, guru, pembayaran dan berita.

## Getting Started

Untuk dapat memulai developing project ini pada komputer lokal Anda. Silahkan baca dan pelajari lebih lanjut penjelasan dibawah

### Prerequisites

Software dan library yang dibutuhkan dalam memulai developing project
- Jangan lupa menambahkan id_rsa.pub Anda kedalam gitlab profile untuk menarik project dari gitlab

```
git version 2.17.1

PHP 7.2.10 (cli) 

node v8.12.0

Composer version 1.7.2
```

### Installing

Setelah menginstall semua yang dibutuhkan diatas silahkan menambahkan project melalui git dengan membuat folder baru
```
mkdir victory

cd victory

git init

git remote add origin git@gitlab.com:dilangit/victory-academy.git

git pull origin master
```

### Development Local [BackEnd]
Project telah selesai ditambahkan, silahkan update composer dan node library pada project, jika Anda development BackEnd atau server, silahkan menjalankan code dibawah ini:

```
composer install
```

### Development Local [FrontEnd]
Project telah selesai ditambahkan, silahkan update composer dan node library pada project, jika Anda development BackEnd atau server, silahkan menjalankan code dibawah ini:

```
composer install

sudo apt install libpng* build-essential gcc make autoconf libtool pkg-config nasm

npm install
```

Jika Anda tidak menemui error maka Anda telah sukses untuk melanjutkan project tsb

## Environment Settings
Project Laravel memiliki environment setting pada 

```
[DIREKTORI_FOLDER_PROJECT]/.env
```

Jika belum memiliki .env maka dapat generate file .env dengan menjalankan perintah berikut

```
cd [DIREKTORI_FOLDER_PROJECT]

cp .env.example .env

php artisan key:generate
```

Buatlah database pada MySQL server local Anda, maka setting pada file .env yang harus Anda rubah antara lain :

```
DB_DATABASE=[NAMA_DATABASE_YANG_ANDA_BUAT]

DB_USERNAME=[USERNAME_MYSQL_SERVER_ANDA]

DB_PASSWORD=[PASSWORD_MYSQL_SERVER_ANDA]
```

Aplikasi ini menggunakan:
1. ARTESAOS SEO TOOLS, untuk installasi buka link di bawah ini :

```
https://github.com/artesaos/seotools
```

2. MidTrans Payment Gateway, untuk installasi buka link di bawah ini :

```
https://github.com/marprin/laravel-midtrans
```

## Running the tests

Untuk menjalankan development pada lokal Anda dapat menjalankan beberapa code pada folder project

```
php artisan migrate
```

Jika database sudah terisi dengan data yang sudah termigrasi, Anda dapat menjalankan perintah sebagai berikut untuk membersihkan data lama dengan data baru

```
php artisan migrate:rollback

php artisan migrate
```
Atau

```
php artisan migrate:refresh
```

Jika ditemukan error berupa "Migration not found", maka jalankan perinah berikut:

```
composer dump-autoload
```

Untuk mengisi database yang sudah dibuat dengan data <i>Dummy</i> jalankan perintah dibawah ini

```
php artisan db:seed
```

Jika tidak ada perubahan migration di BackEnd, maka dapat menjalankan perintah yang pintas untuk migrasi dan seeding dengan perintah berikut:

```
php artisan migrate:refresh --seed
```

Setelah database dibuat dan diisi dengan dummy, maka saatnya menjalankan program yang dibangun.

Jalankan scheduler terlebih dahulu
```
php artisan schedule:run >> /dev/null 2>&1
```
Lalu jalankan program ini
```
php artisan serve
```

Jika terjadi konflik port maka dapat menjalankan program pada port lain, default port adalah 8000

```
php artisan serve --port=[PORT_PILIHAN_ANDA]
```

Program akan dijalankan pada halaman default sebagai berikut

```
http://localhost:8000/
```

Atau

```
http://127.0.0.1:8000/
```

Jika port diganti maka akan berubah menjadi

```
http://localhost:[PORT_PILIHAN_ANDA]/
```

### Compile Resources Folder

Banyak JavaScript dan SCSS pada folder resource, sebagai <b>frontend</b> silahkan untuk menjalan npm untuk men compile semua resource didalam folder tersebut
```
npm run dev
```

## Deployment

Untuk mendeploy project ini ke public live, silahkan untuk git pull dari server, configurasi ada pada Trello

## Built With

* [Laravel](http://www.laravel.com/) - The web framework used
* [NPM](https://nodejs.org/) - Dependency Management
* [Composer](https://getcomposer.com/) - Used to generate Vendor file
* [Material Kit](https://www.creative-tim.com/product/material-kit) - Used for FrontEnd with Bootstrap 4
* [Remark] - Admin Template

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

1.0.1

## Authors

* **diLangit Corporation** - *Initial work* - [dilangit](https://dilangit.com)

See also the list of [developer](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Please code with  L O V E
* Make sure to communicate well with your team
* Don't  K I L L  your comrades with your unresponsible behaviour
* Please eat and drinks
* Check your Trello 
* Have Fun *in the hell of coding*
