<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Academic extends Model
{
    protected $table = 'academics';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'name', 
        'detail', 
        'picture',
    ];
    protected $hidden = [
        
    ];
}
