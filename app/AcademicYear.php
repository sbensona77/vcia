<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

class AcademicYear extends Model
{
    protected $table = 'academic_years';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'semester_type',
        'year',
        'semester_start',
        'semester_end',
        'status',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;

            if($model->status == 1)
                self::turnOffAllSemester();
    	});

        self::updating(function($model) {
            if($model->isDirty('status')) {
                if($model->status == 1)
                    self::turnOffAllSemester();

                if($model->status == 0)
                    self::turnOffAllTerms($model);
            }
        });

        self::deleting(function($model) {
            if($model->status == 1)
                self::turnOnLastestSemester();
        });
    }

    public function terms()
    {
        return $this->hasMany('App\AcademicYearTerm', 'academic_year_id', 'id');
    }

    public static function turnOffAllSemester()
    {
        $semesters = self::all();

        return $semesters->each(function($item) {
            $item
            ->fill(['status' => 0])
            ->save();
        });
    }

    public static function turnOffAllTerms($model)
    {
        return $model->terms()
        ->each(function($item) {
            $item->status = 0;
            $item->save();
        });
    }

    public static function turnOnLastestSemester()
    {
        $lastSemester = self::lastest()->first();
        $lastSemester->status = 1;
        return $lastSemester->save();
    }
}
