<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use App\AcademicYear;

class AcademicYearTerm extends Model
{
    protected $table = 'academic_years_terms';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'academic_year_id',
		'term_start',
		'term_end',
		'status',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;

            if($model->status == 1) {
                self::turnOffAllTerms($model);

                if($model->academic_year->status == 0)
                    self::turnOnSemester($model);
            }
    	});

        self::updating(function($model) {
            if($model->isDirty('status') && $model->status == 1) {
                self::turnOffAllTerms($model);

                if($model->academic_year->status == 0)
                    self::turnOnSemester($model);
            }
        });

        self::deleting(function($model) {
            if($model->status == 1)
                self::turnOnLastTerm();
        });
    }

    public function academic_year()
    {
    	return $this->belongsTo('App\AcademicYear', 'academic_year_id', 'id');
    }

    public static function turnOffAllTerms($model)
    {
        $terms = self::where(
            'academic_year_id', 
            $model->academic_year_id
        )
        ->where('status', 1)
        ->get();

        return $terms->each(function($item) {
            $item->update(['status' => 0]);
        });
    }

    public static function turnOnLastTerm()
    {
        $lastTerm = self::lastest()->first();
        $lastTerm->status = 1;
        return $lastTerm->save();
    }

    public static function turnOnSemester($model)
    {
        $academicYear = AcademicYear::find($model->academic_year_id);
        $academicYear->status = 1;
        return $academicYear->save();
    }
}
