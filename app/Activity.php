<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'name', 
        'status', 
        'datetime'
    ];
    protected $hidden = [
        
    ];
}
