<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

class AssignedClub extends Model
{
    protected $table = 'assigned_clubs';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'student_id',
        'club_id',
        'grade',
        'status',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});
    }

    public function club()
    {
        return $this->hasOne('App\Club', 'id', 'club_id');
    }

    public function student()
    {
        return $this->belongsTo('App\User', 'id', 'student_id');
    }
}