<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

use Auth;

use App\StudentsProfile;

use App\AssignedMaterials;

use App\Course;
use App\CoursesMaterial;

class AssignedCourses extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            //generate UUID
            $model->id = Uuid::generate()->string;

            //generate hidden value
            $model->assigner_id = Auth::user()->id;
            $model->total_pace = self::getTotalPace($model);
        });

        self::created(function($model){
            self::getAllAssignedMaterials($model);
        });

        self::updating(function($model){
            $model->total_pace = self::getTotalPace($model);

            //destroy all assigned materials
            self::destroyAllAssignedMaterials($model);
        });

        self::updated(function($model){
            self::getAllAssignedMaterials($model);
        });
    }

    protected $table = 'assigned_courses';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'student_id',

        'course_id',
		'pace_start',
		'pace_end',
    ];
    protected $hidden = [
        
    ];

    public function student()
    {
        return $this->belongsTo('App\StudentsProfile');
    }

    public function course()
    {
        return $this->hasOne('App\Course', 'id', 'course_id');
    }

    public function assigned_materials()
    {
        return $this->hasMany('App\AssignedMaterials', 'assigned_course_id', 'id');
    }

    public function material_start()
    {
        return $this->hasOne('App\CoursesMaterial', 'id', 'pace_start');
    }

    public function material_end()
    {
        return $this->hasOne('App\CoursesMaterial', 'id', 'pace_end');
    }

    protected static function createAssignedMaterials($model, $material){
        $assignedMaterial = new AssignedMaterials([
            'assigned_course_id' => $model->id,
            'student_id' => $model->student_id,

            'material_id' => $material->id,

            'grade' => null,
        ]);

        $assignedMaterial->save();
    }

    protected static function checkAssignedMaterials($course_id, $start, $end){
        $message = [];

        $isStartTrue = $course_id == $start ? true : array_push($message, 'Start PACE is not match with course assigned!');
        $isEndTrue = $course_id == $end ? true : array_push($message, 'End PACE is not match with course assigned!');

        return $message == [] ? true : false;
        //redirect()->back()->with('error', $message);
    }

    protected static function getTotalPace($model){
    	$materials = CoursesMaterial::all()->where('course_id', $model->course_id)->sortBy('name'); //limit only count the same course
    	$start = $materials->find($model->pace_start);
    	$end = $materials->find($model->pace_end);

    	$startKey = 0;
    	$endKey = 9999;

    	//get the key
    	foreach($materials as $key => $material){
            if(empty($start) || empty($end) || $start == $end){
                break;
                
                abort(500, 'Wrong User input');
                return redirect()->back()->with('error', 'The inserted PACE is not match with the course');
            }

    		if($material->id == $start->id){ //get first key and value
    			$startKey = $key;
    		}elseif($material->id == $end->id){ //get last key and value
    			$endKey = $key;

    			break;
    		}
    	}

    	return $endKey - $startKey + 1; //return how many paces stored
    }

    protected static function getAllAssignedMaterials($model){
        $materials = CoursesMaterial::all()->where('course_id', $model->course_id)->sortBy('name'); //limit only count the same course
        $start = $materials->find($model->pace_start);
        $end = $materials->find($model->pace_end);

        $assignment = [];
        $startKey = -1;

        $amountOfPACE = self::getTotalPace($model);

        //get the key
        foreach($materials as $key => $material){
            if(empty($start) || empty($end) || self::checkAssignedMaterials($model->course_id, $model->pace_start, $model->pace_end)){
                break;

                abort(500, 'Wrong User input');
                return redirect()->back()->with('error', 'The inserted PACE is not match with the course');
            }

            if($start == $end){
                array_push($assignment, $start); 
                self::createAssignedMaterials($model, $start);

                break;
            }

            if($material->id == $start->id){ //get first key and value
                $startKey = $key;

                //push to $assignment
                array_push($assignment, $material); 
                self::createAssignedMaterials($model, $material);
            }elseif($material->id == $end->id){
                //push to $assignment
                array_push($assignment, $material); 
                self::createAssignedMaterials($model, $material);

                break;
            }elseif($startKey >= 0){
                //push to $assignment
                array_push($assignment, $material); 
                self::createAssignedMaterials($model, $material);
            }
        }
        return $assignment;
    }

    protected static function destroyAllAssignedMaterials($model){
        $datas = AssignedMaterials::where('assigned_course_id', $model->id);

        return $datas->delete();
    }
}
