<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

use App\User;

use App\Course;
use App\CoursesMaterial;

use App\AssignedCourses;

class AssignedMaterials extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            // generate UUID
            $model->id = Uuid::generate()->string;
            $model->grade = null;

            // borrow one book
            self::borrowOneBook($model);
        });

        self::updating(function($model){
            if($model->isDirty('grade')){
                $model->status = 'graded';
            }
        });
    }

    protected $table = 'assigned_materials';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
    	'assigned_course_id',
        'student_id',

        'material_id',

        'grade',
    ];
    protected $hidden = [
        
    ];

    public function assigned_course()
    {
        return $this->belongsTo('App\AssignedCourses', 'id', 'assigned_course_id');
    }

    public function material()
    {
        return $this->hasOne('App\CoursesMaterial', 'id', 'material_id');
    }

    public static function borrowOneBook($model)
    {
        $material = CoursesMaterial::findOrFail($model->material_id);

        $inventoryCategory = InventoryCategory::where('name', 'like', $material->course->name.'%')->first();
        $inventory = $inventoryCategory->inventories()
        ->where('inventory_name', 'like', '%'.$material->name)
        ->first();

        $items = $inventory->items->where('status', 'available');

        if(count($items)) {
            $borrowedItem = $items->first();
            $borrowedItem->status = 'borrowed';

            $borrower = User::findOrFail($model->student_id);

            $borrowedItem->borrower = $borrower->id;

            $borrowedItem->save();
        }
    }
}
