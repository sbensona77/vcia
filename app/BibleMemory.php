<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

class BibleMemory extends Model
{
    protected $table = 'bible_memories';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'month',
        'student_id',
        'bible_verse',

    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});
    }

    public function student()
    {
        return $this->belongsTo('App\User', 'student_id', 'id');
    }
}