<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

class Club extends Model
{
    protected $table = 'clubs';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model) {
            $model->id = Uuid::generate()->string;
        });

        self::created(function($model) {
            
        });

        self::deleting(function($model) {
            
        });
    }

    protected $fillable = [
        'name', 
        'picture', 
        'detail',
    ];
    protected $hidden = [
        
    ];
}
