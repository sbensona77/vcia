<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use App\User;

class ContactForm extends Model
{
    protected $table = 'contact_form';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'name',
        'email',
        'message',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});
    }

    public function user()
    {
        $user = User::where('email', $this->email)->get()->first();

        if($user)
            return $user;
        else
            return null;
    }
}