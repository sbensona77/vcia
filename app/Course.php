<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

use App\CoursesMaterial;
use App\MaterialsUpload;

use App\InventoryCategory;
use App\Inventory;

class Course extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function($model) {
            $model->id = Uuid::generate()->string;
        });

        self::created(function($model) {
            $materials = self::create_materials($model);
            $category = self::addInventoryCategory($model);

            self::addInventory($category->id, $materials);
        });

        self::deleting(function($model) {
            $uploads = $model->materials();

            $uploads->delete();
        });
    }

    protected $table = 'courses';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'name',
        'teacher_id',
        'description',
    ];
    protected $hidden = [
        
    ];

    public function materials()
    {
        return $this->hasMany('App\CoursesMaterial', 'course_id', 'id');
    }

    public function teachers()
    {
        return $this->hasOne('App\StaffsProfile', 'teacher_id', 'teacher_id');
    }

    public function weekly_reports()
    {
        return $this->hasMany('App\WeeklyReport', 'course_id', 'id');
    }

    protected static function create_materials($model)
    {
        $createdMaterials = [];

        for($book = 1001; $book <= 1144; $book++) {
            $material = new CoursesMaterial([
                'course_id' => $model->id,
                'name' => $book,
                'enrollment_key' => $book,
                'description' => $model->name.' material',
            ]);

            $material->save();

            array_push($createdMaterials, $material);
        }

        return $createdMaterials;
    }

    protected static function addInventoryCategory($model)
    {
        $inventory = new InventoryCategory([
            'name' => $model->name.' Books',
        ]);

        $inventory->save();

        return $inventory;
    }

    protected static function addInventory($categoryID = '', $createdMaterials = [])
    {
        $inventoryAdded = [];

        foreach($createdMaterials as $key => $material) {
            $inventory = new Inventory([
                'inventory_name' => $material->course->name.' Books - '.$material->name,
                'amount' => 0,
                'category_id' => $categoryID,
            ]);

            $inventory->save();

            array_push($inventoryAdded, $inventory);
        }

        return $inventoryAdded;
    }
}
