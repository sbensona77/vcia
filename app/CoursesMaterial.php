<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

use App\Course;
use App\MaterialsUpload;

use App\WeeklyReport;

use File;

use App\PACEStock;

class CoursesMaterial extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->id = Uuid::generate()->string;
        });

        self::created(function ($model) {

        });

        self::deleting(function($model){
            self::delete_uploads($model);
        });
    }

    protected $table = 'courses_materials';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'course_id',
		'name',
        'enrollment_key',
		'description',
    ];
    protected $hidden = [
        
    ];

    public function course() {
        return $this->belongsTo('App\Course', 'course_id', 'id');
    }

    public function uploads() {
        return $this->hasMany('App\MaterialsUpload', 'material_id', 'id');
    }

    public function weekly_reports() {
        return $this->hasMany('App\WeeklyReport', 'material_id', 'id');
    }

    protected static function delete_uploads($model) {
        $course = Course::findOrFail($model->course_id);
        $path = public_path()."/file_uploads/courses_material/".$course->name.'/';

        $uploads = MaterialsUpload::all()->where('material_id', $model->id);

        try {
            foreach($uploads as $key => $upload){
                File::delete($path.$upload->uploaded_file);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
