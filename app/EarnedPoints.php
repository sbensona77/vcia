<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use App\VictoryPoints;

class EarnedPoints extends Model
{
    protected $table = 'earns_victory_points';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string; //generate UUID
        });
    }

    public $incrementing = false;

    protected $fillable = [
        'student_id',
		'point_id',
    ];
    protected $hidden = [
        
    ];

    public function student(){
        return $this->belongsTo('App\StudentsProfile', 'id', 'student_id');
    }

    public function point(){
        return $this->hasOne('App\VictoryPoints', 'id', 'point_id');
    }

    public static function getPointSum($student_id){
    	$points = self::all()->where('student_id', $student_id);
    	$values = VictoryPoints::all();

    	$sum = 0;

    	foreach($points as $key => $point){
    		$score = $values->find($point->point_id)->points;

    		$sum += $score; //add to var
    	}

    	return $sum;
    }
}
