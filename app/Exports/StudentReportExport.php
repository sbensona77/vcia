<?php

namespace App\Exports;

use Auth;
use File;

use App\User;

use App\AssignedMaterials;
use App\AssignedCourses;

use App\EarnedPoints;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StudentReportExport implements FromView
{
	protected $user_id;
	protected $startOfSemester;
	protected $endOfSemester;

	public function __construct($student_id, $semesterStart, $semesterEnd)
	{
		$this->user_id = $student_id;
		$this->startOfSemester = $semesterStart;
		$this->endOfSemester = $semesterEnd;
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $courses = 
        AssignedCourses::where('student_id', $this->user_id)
        ->where('created_at', '>=', $this->startOfSemester)
        ->where('created_at', '<=', $this->endOfSemester)
        ->get();

        //dd($courses);

        $materials = 
        AssignedMaterials::where('student_id', $this->user_id)
        ->where('created_at', '>=', $this->startOfSemester)
        ->where('created_at', '<=', $this->endOfSemester)
        ->where('status', 'graded')
        ->get();

        $points = EarnedPoints::where('student_id', $this->user_id)
        ->get();

        return view('exports.student-report', [
            'courses' => $courses,
        	'materials' => $materials,
        	'points' => $points
        ]);
    }
}
