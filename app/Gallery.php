<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'galleries';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'event_name',
        'date',
    ];
    protected $hidden = [
        
    ];

    public function photos(){
    	return $this->hasMany('GalleryPhotos', 'event_id', 'id');
    }
}
