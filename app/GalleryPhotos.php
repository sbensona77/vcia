<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Gallery;

class GalleryPhotos extends Model
{
    protected $table = 'galleries_photos';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'event_id',
        'picture',
    ];
    protected $hidden = [
        
    ];

    public function gallery(){
    	return $this->belongsTo('App\Gallery', 'id', 'event_id');
    }
}
