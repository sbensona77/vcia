<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'histories';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'year', 
        'title', 
        'content'
    ];
    protected $hidden = [
        
    ];
}
