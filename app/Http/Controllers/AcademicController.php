<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Academic;

class AcademicController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('teacher');
    }

    public function upload_photo($file_request){
        $photo_name = Carbon::now()->format('Y-m-d H:i:s').' '.$file_request->getClientOriginalName(); //get name
        $file_request->move(
            public_path('/file_uploads/academics/'), 
            $photo_name
        );

        return $photo_name;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academics = Academic::all();

        return view('dashboards.academics.index', compact('academics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboards.academics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'picture' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $data = new Academic([
                'name' => $request->input('name'),
                'detail' => $request->input('detail'),
                'picture' => $this->upload_photo($request->file('picture')),
            ]);

            $data->save();

            $type = 'success';
            $message = 'Succeeded creating new academic!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create new academic! : '.$e->getMessage();
        }

        return redirect('dashboard/academics')->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $academic = Academic::findOrFail($id);      
        } catch (Exception $e) {
            $academic = 'Error! '.$e->getMessage();    
        }

        return view('dashboards.academics.view', compact('academic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $academics = Academic::all();

        return view('dashboards.academics.edit', compact('academics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $data = Academic::findOrFail($id);

            if($request->hasFile('picture')){
                $photo = $this->upload_photo($request->file('picture'));
            }else{
                $photo = $data->picture;
            }

            //collect data
            $data->name = $request->input('name');
            $data->detail = $request->input('detail');
            $data->picture = $photo;

            $data->save();

            $type = 'success';
            $message = 'Succeeded updating academic data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update academic data! : '.$e->getMessage();
        }

        return redirect('dashboard/academics')->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = '';
        $message = '';

        try {
            $academic = Academic::findOrFail($id);

            $academic->delete();

            $type = 'success';
            $message = 'Succeeded deleting academic data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete academic data! : '.$e->getMessage();
        }

        return redirect('dashboard/academics')->with($type, $message);
    }
}
