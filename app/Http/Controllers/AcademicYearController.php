<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\AcademicYear;

class AcademicYearController extends Controller
{
    protected $viewDirectory;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->only(['store', 'update', 'destroy']);

        $this->viewDirectory = 'dashboards.academic-years';
    }

    public function turnOnSemester($semester_id)
    {
        $message = '';

        try {
            $semester = AcademicYear::findOrFail($semester_id);
            $semester->status = 1;
            $semester->save();

            $message = 'Succeeded turn on a semester!';
            $type = 'success';    
        } catch (Exception $e) {
            $message = 'Failed to turn on a semester';
        }

        return redirect()->back()->with($type, $message);
    }

    public function turnOffSemester($semester_id)
    {
        $message = '';

        try {
            $semester = AcademicYear::findOrFail($semester_id);
            $semester->status = 0;
            $semester->save();

            $message = 'Succeeded turn off a semester!';
            $type = 'success';    
        } catch (Exception $e) {
            $message = 'Failed to turn off a semester';
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $semesters = AcademicYear::all()->sortByDesc('semester_start');

        return view($this->viewDirectory.'.index', compact(['semesters']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewDirectory.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'semester_type' => 'required',
            'year' => 'required|integer',
            'semester_start' => 'required|date',
            'semester_end' => 'required|date',
            'status' => 'required',
        ]);

        $type = 'error';

        try {
            $semester = new AcademicYear($request->all());

            $semester->save();

            $type = 'success';
            $message = 'Succeeded creating a new semester!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new semester! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $semester = AcademicYear::findOrFail($id);
        $terms = $semester->terms
        ->sortByDesc('term_start');
        
        return view($this->viewDirectory.'.view', compact(['semester', 'terms']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'semester_type' => 'required',
            'year' => 'required|integer',
            'semester_start' => 'required|date',
            'semester_end' => 'required|date',
            'status' => 'required',
        ]);

        $type = 'error';

        try {
            $semester = AcademicYear::findOrFail($id);
            
            $semester->fill($request->all());
            
            $semester->save();

            $type = 'success';
            $message = 'Succeeded updating a academic year data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update a academic year data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $semester = AcademicYear::findOrFail($id);

            $semester->delete();

            $type = 'success';
            $message = 'Succeeded deleting a academic year data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a academic year data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}