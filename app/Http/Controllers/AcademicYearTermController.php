<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\AcademicYearTerm;

class AcademicYearTermController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'academic_year_id' => 'required',
            'term_start' => 'required|date',
            'term_end' => 'required|date',
            'status' => 'required',
        ]);

        $type = 'error';

        try {
            $term = new AcademicYearTerm($request->all());

            $term->save();

            $type = 'success';
            $message = 'Succeeded creating a new term!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new term! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $term = AcademicYearTerm::findOrFail($id);
        
        return view($this->viewDirectory.'.view', compact(['term']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $term = AcademicYearTerm::findOrFail($id);

        return view($this->viewDirectory.'.edit', compact(['term']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'term_start' => 'required|date',
            'term_end' => 'required|date',
            'status' => 'required',
        ]);

        $type = 'error';

        try {
            $term = AcademicYearTerm::findOrFail($id);
            
            $term->fill($request->all());
            
            $term->save();

            $type = 'success';
            $message = 'Succeeded updating a academicyearterm data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update a academicyearterm data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $term = AcademicYearTerm::findOrFail($id);

            $term->delete();

            $type = 'success';
            $message = 'Succeeded deleting a academicyearterm data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a academicyearterm data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}