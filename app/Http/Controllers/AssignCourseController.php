<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Response;

use App\User;

use App\AssignedCourses;
use App\AssignedMaterials;

use App\WeeklyReport;

use App\Course;
use App\CoursesMaterial;

class AssignCourseController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('teacher')->only(['store', 'update', 'destroy', 'fillPaceGrade']);
    }

    public function createReport($student_id, $material_id, $grade){
        $course_id = CoursesMaterial::find($material_id)->course_id;

        $report = new WeeklyReport([
            'student_id' => $student_id,
            'course_id' => $course_id,
            'material_id' => $material_id,

            'info' => User::findOrFail($student_id)->name.' has completed a PACE',
            'grade' => $grade,
        ]);

        $report->save();

        return $report;
    }

    public function getCourse($course_id)
    {
        $course = AssignedCourses::findOrFail($course_id)->course;

        return Response::json(['course' => json_encode($course)]);
    }

    public function getMaterial($material_id)
    {
        $material = CoursesMaterial::findOrFail($material_id);

        return Response::json(['material' => json_encode($material)]);
    }

    public function getAllAssignedMaterials($assign_id)
    {
        $assignedCourses = AssignedCourses::findOrFail($assign_id);
        $assigned_materials = $assignedCourses->assigned_materials;

        return Response::json(['assigned_materials' => json_encode($assigned_materials)]);
    }

    public function jumpPace($id){
        $pace = AssignedMaterials::findOrFail($id);

        $type = 'error';
        $message = 'Unknown error';

        try{
            $pace->status = 'jumped';
            $pace->save();

            $message = 'Succeeded jumping a PACE';
            $type = 'success';
        }catch(Exception $e){
            $message = 'Cannot jump a PACE! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function unJumpPace($id){
        $pace = AssignedMaterials::findOrFail($id);

        $type = 'error';
        $message = 'Unknown error';

        try{
            $pace->status = $pace->grade != null ? 'graded' : 'active';
            $pace->save();

            $message = 'Succeeded activating a PACE';
            $type = 'success';
        }catch(Exception $e){
            $message = 'Cannot activate a PACE! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function fillPaceGrade(Request $request, $id){
        $request->validate([
            'grade' => 'required|numeric|min:80|max:100',
        ]);

        $type = 'error';
        $message = 'Unknown Error!';

        try {
            $grade = AssignedMaterials::findOrFail($id);

            if($grade->grade == null)
            {
                $report = $this->createReport(
                    $grade->student_id,
                    $grade->material_id,
                    
                    $request->input('grade')
                );
            }

            $grade->update([
                'grade' => $request->input('grade'),
            ]);

            $message = 'Succeeded grading PACE';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Error inserting grade to student!';
        }

        return Response::json(['type' => $type, 'message' => $message]);
    }

    public function fillAllGrade(Request $request, $assign_id)
    {
        $type = 'error';

        try {
            $assign = AssignedCourses::findOrFail($assign_id);
            $assgnedMaterials = $assign->assigned_materials;

            foreach ($assgnedMaterials as $key => $material) {
                $grade = $request->input('grade'.'_'.$key.'_'.$material->material->name);

                if($grade) {
                    // save
                    $assignedMaterial = AssignedMaterials::findOrFail($material->id);
                    $assignedMaterial->grade = $grade;
                    $assignedMaterial->save();
                }
            }

            $message = 'Succeeded fill all grade!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to fill all grade! Error: '.$e->getMessage();
        }

        return Response::json(['type' => $type, 'message' => $message]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'student_id' => 'required',
            'course_id' => 'required',
            'pace_start' => 'required',
            'pace_end' => 'required',
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $assign = new AssignedCourses([
                'student_id' => $request->input('student_id'),
                'course_id' => $request->input('course_id'),
                'pace_start' => $request->input('pace_start'),
                'pace_end' => $request->input('pace_end'),
                'average' => 0,
            ]);

            $assign->save();

            $message = 'Succeeded to assign book to student';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to assign course to student! '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) //by user ID
    {
        $assignments = new AssignCourses;
        $assigns = $assignment->where('student_id', $id);
        
        $courses = Course::all();
        $materials = CoursesMaterial::all();

        return view('assign-course.index', compact(['assignment', 'assigns', 'courses', 'materials']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'student_id' => 'required',
            'course_id' => 'required',
            'pace_start' => 'required',
            'pace_end' => 'required',
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $assign = AssignedCourses::findOrFail($id);

            $assign->update([
                'student_id' => $request->input('student_id'),
                'course_id' => $request->input('course_id'),
                'pace_start' => $request->input('pace_start'),
                'pace_end' => $request->input('pace_end'),
                'average' => 0,
            ]);

            $message = 'Succeeded to update book assign to student';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to update assign course to student! '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assign = AssignedCourses::findOrFail($id);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $assign->delete();

            $type = 'success';
            $message = 'Succeeded to delete assignment to student!';
        } catch (Exception $e) {
            $message = 'Failed to delete assignment! '.$e->getMessage();
        }
        
        return redirect()->back()->with($type, $message);
    }
}
