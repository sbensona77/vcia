<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\AssignedClub;

class AssignedClubController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'student_id' => 'required',
            'club_id' => 'required',
        ]);
        
        $type = 'error';

        try {
            $club = new AssignedClub($request->all());

            $club->save();

            $type = 'success';
            $message = 'Succeeded to assign a new club!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to assign a new club! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'student_id' => 'required',
            'grade' => 'required',
        ]);

        $type = 'error';

        try {
            $club = AssignedClub::findOrFail($id);
            
            $club->fill($request->all());
            
            $club->save();

            $type = 'success';
            $message = 'Succeeded to assign a club data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to assign a club data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $club = AssignedClub::findOrFail($id);

            $club->delete();

            $type = 'success';
            $message = 'Succeeded deleting a club data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a club data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}