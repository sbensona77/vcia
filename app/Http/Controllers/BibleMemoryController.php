<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Carbon\Carbon;

use App\BibleMemory;

class BibleMemoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'month' => 'required',
            'student_id' => 'required',
            'bible_verse' => 'required',
        ]);

        $type = 'error';

        try {
            $bible = new BibleMemory($request->all());

            $bible->save();

            $type = 'success';
            $message = 'Succeeded creating a new bible memory!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new bible memory! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'month' => 'required',
            'student_id' => 'required',
            'bible_verse' => 'required',
        ]);

        $type = 'error';

        try {
            $bible = BibleMemory::findOrFail($id);
            
            $bible->fill($request->all());
            
            $bible->save();

            $type = 'success';
            $message = 'Succeeded updating a bible memory data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update a bible memory data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $bible = BibleMemory::findOrFail($id);

            $bible->delete();

            $type = 'success';
            $message = 'Succeeded deleting a bible memory data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a bible memory data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}