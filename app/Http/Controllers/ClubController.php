<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Club;

class ClubController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('teacher');
    }

    public function upload_photo($file_request){
        $photo_name = Carbon::now()->format('Y-m-d H:i:s').' '.$file_request->getClientOriginalName(); //get name
        $file_request->move(
            public_path('/file_uploads/clubs/'), 
            $photo_name
        );

        return $photo_name;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clubs = Club::all();

        return view('dashboards.clubs.index', compact('clubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboards.clubs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'motto' => 'required',
            'picture' => 'required',
            'detail' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $data = new Club([
                'name' => $request->input('name'),
                'motto' => $request->input('motto'),
                'picture' => $this->upload_photo($request->file('picture')),
                'detail' => $request->input('detail'),
            ]);

            $data->save();

            $type = 'success';
            $message = 'Succeeded creating new club!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create new club! : '.$e->getMessage();
        }

        return redirect('dashboard/clubs')->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $club = Club::findOrFail($id);

        return view('dashboards.clubs.view', compact('club'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboards.clubs.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $club = Club::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'motto' => 'required',
            //'picture' => 'required',
            'detail' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $data = Club::findOrFail($id);

            if($request->hasFile('picture')){
                $photo = $this->upload_photo($request->file('picture'));
            }else{
                $photo = $data->picture;
            }

            $data->name = $request->input('name');
            $data->picture = $photo;
            $data->motto = $request->input('motto');
            $data->detail = $request->input('detail');

            $data->save(); //save update

            $type = 'success';
            $message = 'Succeeded update club data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to edit club data! : '.$e->getMessage();
        }

        return redirect('dashboard/clubs')->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = '';
        $message = '';

        try {
            $club = Club::findOrFail($id);

            $club->delete();

            $type = 'success';
            $message = 'Succeeded deleting club data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to edit club data! : '.$e->getMessage();
        }

        return redirect('dashboard/clubs')->with($type, $message);
    }
}
