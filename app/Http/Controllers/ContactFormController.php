<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\ContactForm;

class ContactFormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
        $this->middleware('admin')->only(['destory']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = ContactForm::all();

        return view('dashboards.contact-form.index', compact(['contacts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
        ]);

        $type = 'error';

        try {
            $contact = new ContactForm($request->all());

            $contact->save();

            $type = 'success';
            $message = 'Succeeded creating a new contact!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new contact! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = ContactForm::findOrFail($id);
        
        return view('dashboards.contact-form.view', compact(['contact']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $contact = ContactForm::findOrFail($id);

            $contact->delete();

            $type = 'success';
            $message = 'Succeeded deleting a contact data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a contact data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}