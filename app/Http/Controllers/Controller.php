<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Auth;

use Carbon\Carbon;

use App\User;
use App\Notification as Notify;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function broadcast($message){
    	$this->middleware('teacher');

    	try{
    		$users = User::all(); //all users
    		$sender = Auth::user();

    		foreach($users as $key => $reciever){
    			if($reciever->id != $sender->id){ //not sending to yourself
    				$notif = new Notify([
	    				'announcer' => $sender->id,
	    				'detail' => $message,
						'reciever' => $reciever->id,
						'read_status' => 0,
	    			]);
	    			$notif->save();
    			}
    		}
    	}catch(Exception $e){
    		echo 'Error: '.$e->getMessage();
    	}
    }

    public function notifyOnlyThisUser($user_id, $message){
        $this->middleware('auth');

        try {
            $sender = Auth::user();
            $reciever = User::findOrFail($user_id);

            $notif = new Notify([
                'announcer' => $sender->id,
                'detail' => $message,
                'reciever' => $reciever,
                'read_status' => 0,
            ]);
            $notif->save();
        } catch (Exception $e) {
            return 'Error! '.$e->getMessage();
        }
    }

    public function readNotification($notification_id){
        $this->middleware('auth');
        
    	try {
            $notif = Notify::findOrFail($notification_id);
            
            $notif->read_status = 1;

            $notif->save(); 
        } catch (Exception $e) {
            return redirect('dashboard/home')->with('error', 'Failed to read the notification');
        }
    }
}
