<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Course;
use App\CoursesMaterial;
use App\User;
use App\EnrolledStudent;
use App\StaffsProfile;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->only(['edit', 'update', 'destory']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Course::all();
        $teachers = User::all()->where('role', 'teacher');

        return view('dashboards.courses.index', compact(['datas', 'teachers']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = User::all()->where('role', 'teacher');

        return view('dashboards.courses.create', compact('teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'name' => 'required',
            'teacher_id' => 'required',
            'description' => 'required',
        ]);

        $message = '';
        $type = '';

        $key = $request->input('enrollment_key');
        if(!$key) $key = str_random(10);

        try {
            $data = new Course([
                'name' => $request->input('name'),
                'teacher_id' => $request->input('teacher_id'),
                'description' => $request->input('description'),
            ]);

            $data->save();

            //notifications
            $this->broadcast('A new course has been added! Checkout '.$request->input('name'));

            $type = 'success';
            $message = 'Succeeded creating new course!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create new course! : '.$e->getMessage();
        }

        return redirect('dashboard/courses')->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Course::findOrFail($id);
        $materials = CoursesMaterial::where('course_id', $data->id)->paginate(12);

        $teacher = User::findOrFail($data->teacher_id);
        $profile = StaffsProfile::where('user_id', $teacher->id)->first(); 
        
        return view('dashboards.courses.view', compact(['data', 'materials', 'teacher', 'profile']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data = Course::findOrFail($id);
            $teachers = User::all()->where('role', 'teacher');
        } catch (Exception $e) {
            $data = ['No data found! Error'.$e->getMessage()];
            $teachers = ['No data found! Error'.$e->getMessage()];
        }

        return view('dashboards.courses.edit', compact('data', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'name' => 'required',
            'teacher_id' => 'required',
            'description' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $course = Course::findOrFail($id);
            $oldName = $course->name;

            //collecting data
            $course->name = $request->input('name');
            $course->teacher_id = $request->input('teacher_id');
            $course->description = $request->input('description');

            $course->save();

            //notifications
            $this->broadcast($course->name.' course has been updated! Check it out now!');

            $type = 'success';
            $message = 'Succeeded updating course data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update course data! : '.$e->getMessage();
        }

        return redirect('dashboard/courses')->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = '';
        $message = '';

        try {
            $course = Course::findOrFail($id);

            $course->delete();

            $type = 'success';
            $message = 'Succeeded deleting course data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete course data! : '.$e->getMessage();
        }

        return redirect('dashboard/courses')->with($type, $message);
    }
}
