<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;
use Carbon\Carbon;

use App\User;

use App\Course;
use App\CoursesMaterial;
use App\MaterialsUpload;

use App\StaffsProfile;

class CoursesMaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher')->except(['index', 'download_material', 'show']);
        $this->middleware('admin')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $course_id)
    {
        //validation
        $request->validate([
            'name' => 'required',
        ]);

        $message = '';
        $type = '';

        $key = $request->input('enrollment_key');
        if(!$key) $key = $request->input('name'); //same as name

        try {
            $data = new CoursesMaterial([
                'course_id' => $course_id,
                'name' => $request->input('name'),
                'enrollment_key' => $key,
                'description' => $request->input('description'),
            ]);

            $data->save();

            $type = 'success';
            $message = 'Succeeded creating new course material!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create new course material! : '.$e->getMessage();
        }

        return redirect()->route('courses.show', $course_id)->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($course_id, $id)
    {
        $material = CoursesMaterial::findOrFail($id);
        $materials = CoursesMaterial::all();
        $data = Course::findOrFail($course_id);

        $files = MaterialsUpload::all()->where('material_id', $id)->sortBy('created_at');

        $teacher = User::findOrFail($data->teacher_id);
        $profile = StaffsProfile::where('user_id', $teacher->id)->first();

        return view('dashboards.materials.view', compact([
            'material', 
            'materials',
            'teacher', 
            'profile', 
            'data', 
            'files'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $course_id, $id)
    {
        //validation
        $request->validate([
            'name' => 'required',
            'enrollment_key' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $course = CoursesMaterial::findOrFail($id);

            //collecting data
            $course->course_id = $course_id;
            $course->name = $request->input('name');
            $course->enrollment_key = $request->input('enrollment_key');
            $course->description = $request->input('description');

            $course->save();

            $type = 'success';
            $message = 'Succeeded updating course data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update course data! : '.$e->getMessage();
        }

        return redirect()->route('courses.show', $request->input('course_id'))->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($course_id, $id)
    {
        $type = 'error';
        $message = 'Unknown error';

        $data = CoursesMaterial::findOrFail($id);
        $course_id = $data->course_id;

        try {
            $data->delete();

            $type = 'success';
            $message = 'Succeeded deleting course material data!';     
        } catch (Exception $e) {
            $message = 'Failed to update course material data! : '.$e->getMessage();
        }

        return redirect('/dashboard/courses/'.$course_id)->with($type, $message);
    }
}
