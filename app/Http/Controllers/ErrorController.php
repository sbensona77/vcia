<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function redirect($type){
        $blade_file = 'errors.'.$type;

        return view($blade_file);
    }
}
