<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;

use App\News;
use App\ShortDescription;
use App\Facility;
use App\Club;
use App\Gallery;
use App\GalleryPhotos;
use App\History;
use App\VisionMission;
use App\Academic;
use App\ContactForm;

class FrontPageController extends Controller
{
    public $latest_news;
    public $lastest_news;

    public function __construct()
    {
        $this->latest_news = News::orderBy('created_at','desc')->take(1)->get();
        $this->lastest_news = News::orderBy('created_at','desc')->take(3)->get();
    }

    public function index()
    {
        SEO::setTitle('Victory Christian Academy');
        SEO::setDescription('Victory Academy Christian School has a vision of building Leaders of Faith, Knowledge & Character.');
        SEOMeta::addKeyword([
            'school',
            'academy',
            'victory',
            'christian',
            'teacher',
            'students',
            'student',
            'international',
            'semarang',
            'vcia',
            'yenny lawati',
            'semarang school',
            'semarang academy',
            'sekolah kristen',
            'victory school', 
            'victory christian academy', 
            'victory academy semarang', 
            'sekolah kristen internasional',
            'sekolah internasional semarang',
            'semarang international school',
        ]);
        SEO::opengraph()->setUrl('www.victory.sch.id/');
        SEO::setCanonical('www.victory.sch.id/');
        SEO::opengraph()->addProperty('type', 'school');
        OpenGraph::addImage('http://victory.sch.id/img/slider-2.JPG');
        SEO::twitter()->setSite('@victory_cia');

    	$latest_news = $this->latest_news;
        $clubs = Club::inRandomOrder()->take(4)->get();

    	return view('welcome', compact(['latest_news', 'clubs']));
    }

    public function profile()
    {
        SEO::setTitle('VCIA Profile');
        SEO::setDescription('Victory Christian Academy Profile');
        SEOMeta::addKeyword('profile','victory');
        SEO::opengraph()->setUrl('www.victory.sch.id/profile');
        SEO::setCanonical('www.victory.sch.id/profile');
        SEO::opengraph()->addProperty('type', 'school');
        SEO::twitter()->setSite('@victory_cia');

    	$latest_news = $this->latest_news;

    	$visions = VisionMission::all()->where('type', 'vision');
    	$missions = VisionMission::all()->where('type', 'mission');
    	$histories = History::all();

    	return view('profile', compact(
            ['visions', 'missions', 'histories', 'latest_news']
        ));
    }

    public function gallery()
    {
    	$latest_news = $this->latest_news;

    	$galleries = Gallery::all();
    	$photos = GalleryPhotos::all();

    	return view('gallery', compact(
            ['galleries', 'photos', 'latest_news']
        ));
    }

    public function showNewsBySlug($slug)
    {
        $news = News::where('slug', $slug)->get()->first();

        OpenGraph::setDescription(substr($news->content, 0, 150));
        OpenGraph::setTitle($news->title);
        OpenGraph::addImage(asset($news->picture));
        OpenGraph::setUrl(route('welcome.news.slug', $news->slug));
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'en-us');
        OpenGraph::addProperty('locale:alternate', ['id-id', 'en-us']);

        $latest_news = $this->latest_news;
        $newest_news = $this->lastest_news;

        $next = News::where('created_at', '>', $news->created_at)
        ->get()
        ->sortBy('created_at')
        ->first();
        $previous = News::where('created_at', '<', $news->created_at)
        ->get()
        ->sortBy('created_at')
        ->first();

        return view('single-news', compact(
            ['news', 'latest_news', 'newest_news', 'next', 'previous']
        ));
    }

    public function news()
    {
        SEO::setTitle('VCIA News');
        SEO::setDescription('Victory Christian Academy News');
        SEO::opengraph()->setUrl('www.victory.sch.id/news');
        SEO::setCanonical('www.victory.sch.id/news');
        SEO::opengraph()->addProperty('type', 'news');
        SEO::twitter()->setSite('@victory_cia');

        $latest_news = $this->latest_news;
        $lastest_news = $this->lastest_news;

    	$news = News::orderBy('created_at','desc')->paginate(5);

    	return view('news', compact(
            ['news', 'latest_news', 'lastest_news']
        ));
    }

    public function achievement()
    {
        SEO::setTitle('VCIA Achievements');
        SEO::setDescription('Victory Christian Academy Achievements');
        SEO::opengraph()->setUrl('www.victory.sch.id/achievement');
        SEO::setCanonical('www.victory.sch.id/achievement');
        SEO::opengraph()->addProperty('type', 'achievement');
        SEO::twitter()->setSite('@victory_cia');

    	$latest_news = $this->latest_news;

    	$achievements = News::where('category', 'achievement')->orderBy('created_at','desc')->paginate(5);

    	return view('achievement', compact(['achievements', 'latest_news']));
    }

    public function academic()
    {
        SEO::setTitle('VCIA Academics');
        SEO::setDescription('Victory Christian Academy Academics');
        SEO::opengraph()->setUrl(route('academics'));
        SEO::setCanonical(route('academics'));
        SEO::opengraph()->addProperty('type', 'academic');
        SEO::twitter()->setSite('@victory_cia');

    	$latest_news = $this->latest_news;

        $academics = Academic::all();

    	return view('academics', compact(['academics', 'latest_news']));
    }

    public function club()
    {
        SEO::setTitle('VCIA Clubs');
        SEO::setDescription('Victory Christian Academy Clubs');
        SEO::opengraph()->setUrl(route('clubs'));
        SEO::setCanonical(route('clubs'));
        SEO::opengraph()->addProperty('type', 'clubs');
        SEO::twitter()->setSite('@victory_cia');

    	$latest_news = $this->latest_news;
        
    	$clubs = Club::all();
        $short_description = ShortDescription::where('page_name', 'clubs')->first();

    	return view('clubs', compact(['clubs', 'latest_news', 'short_description']));
    }

    public function post_contact(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $data = new ContactForm([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'message' => $request->get('message'),
            ]);

            $data->save();

            $type = 'success';
            $message = 'Your message has been sent to admin! Thanks for contacting us!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Sorry, failed to send message to admin! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}
