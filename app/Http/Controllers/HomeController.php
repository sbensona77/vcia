<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('status');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('user_profile.show', Auth::user()->id);
    }

    public function faq(){
        return view('dashboards.main.faq');
    }

    public function profile(){
        return view('dashboards.main.profile');
    }

    public function payment(){
        return view('dashboards.main.payment');
    }
}
