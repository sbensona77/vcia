<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\InventoryCategory;

class InventoryCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->only(['printAllItems']);
    }

    public function printAllItems($category_id)
    {
        $category = InventoryCategory::findOrFail($category_id);
        $inventories = $category->inventories;

        return view('exports.inventories-list', compact(['category', 'inventories']));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('inventories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('inventories.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'name'
        ]);

        $type = 'error';

        try {
            $category = new InventoryCategory($request->all());

            $category->save();

            $type = 'success';
            $message = 'Succeeded creating a new inventory`s category!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new inventory`s category! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('inventories.show', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('inventories.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'name'
        ]);

        $type = 'error';

        try {
            $inventorycategory = InventoryCategory::findOrFail($id);
            
            $inventorycategory->fill($request->all());
            
            $inventorycategory->save();

            $type = 'success';
            $message = 'Succeeded updating a inventory`s category data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update a inventory`s category data! : '.$e->getMessage();
        }

        return redirect()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $category = InventoryCategory::findOrFail($id);

            $category->delete();

            $type = 'success';
            $message = 'Succeeded deleting a inventory`s category data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a inventory`s category data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}