<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Carbon\Carbon;

use App\Inventory;
use App\InventoryItem;
use App\InventoryCategory;
use App\InventoryLog;

use App\User;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
        $this->middleware('admin')->only(['destroy', 'destoryInventory']);
    }

    public function createItemsByAmount($inventory)
    {
        $now = Carbon::now()->copy();
        $year = $now->year;
        $month = $now->month;

        $lastInventory = InventoryItem::
        where('inventory_id', $inventory->id)
        ->get()->last();

        // get the last index of inserted item
        if($lastInventory) {
            $lastCode = $lastInventory->code;
            $explodedCode = explode('/', $lastCode);

            $lastYear = $explodedCode[0];
            $lastMonth = $explodedCode[1];

            if($year == $lastYear && $month == $lastMonth)
                $lastIndex = $explodedCode[2];
            else
                $lastIndex = 0;
        } else { 
            // no item saved? start from 0
            $lastIndex = 0;
        }

        // get amount added
        $amountAdded = $inventory->amount - $lastIndex;

        // loop as many as amount added
        for ($i = 1; $i <= $amountAdded; $i++) {
            $indexOfThisItem = $lastIndex + $i;

            // create the code of item
            $code = $year.'/'.$month.'/'.$indexOfThisItem;
            $item = new InventoryItem([
                'code' => $code,
                'inventory_id' => $inventory->id,
            ]);

            $item->save();
        }
    }

    public function storeInventory(Request $request)
    {
        $request->validate([
            'inventory_name' => 'required',
            'amount' => 'required|numeric|min:1',
            'category_id' => 'required'
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $inventory = new Inventory($request->only([
                'inventory_name',
                'amount',
                'category_id'
            ]));

            $inventory->save();

            // store items as many as amount
            $this->createItemsByAmount($inventory);

            $message = 'Succeeded create new inventory!';
            $type = 'succes';
        } catch (Exception $e) {
            $message = 'Failed to create new inventory';
        }

        return redirect()->route('inventories.show', $inventory->category_id)->with($type, $message);
    }

    public function updateInventory(Request $request, $id)
    {
        $request->validate([
            'inventory_name' => 'required',
            'category_id' => 'required'
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $inventory = Inventory::findOrFail($id);

            $inventory->fill($request->only([
                'inventory_name',
                'amount',
                'category_id'
            ]));

            if($inventory->amount <= $request->input('amount'))
                $this->createItemsByAmount($inventory);
            elseif($inventory->amount > $request->input('amount'))
                return redirect()->back()->with('error', 'You should decrease manually!');

            $inventory->save();

            $message = 'Succeeded update inventory!';
            $type = 'succes';
        } catch (Exception $e) {
            $message = 'Failed to update inventory!';
        }

        return redirect()->route('inventories.show', $inventory->category_id)->with($type, $message);
    }

    public function destoryInventory(Request $request, $id)
    {
        $type = 'error';
        $message = 'Unknown error!';

        try {
            $inventory = Inventory::findOrFail($id);

            $inventory->delete();

            $message = 'Succeeded delete inventory!';
            $type = 'succes';
        } catch (Exception $e) {
            $message = 'Failed to delete inventory!';
        }

        return redirect()->route('inventories.show', $inventory->category_id)->with($type, $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = InventoryCategory::all();

        return view('dashboards.inventories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $inventory = new InventoryCategory($request->all());

            $inventory->save();

            $message = 'Succeeded create new inventory`s category!';
            $type = 'succes';
        } catch (Exception $e) {
            $message = 'Failed to create new inventory`s category';
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = InventoryCategory::find($id);
        $inventories = $category->inventories()->paginate(10);
        $users = User::all();

        return view('dashboards.inventories.view', compact(['category', 'inventories', 'users']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $inventory = InventoryCategory::findOrFail($id);

            $inventory->fill($request->all());

            $inventory->save();

            $message = 'Succeeded update inventory category!';
            $type = 'succes';
        } catch (Exception $e) {
            $message = 'Failed to update inventory category';
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';
        $message = 'Unknown error!';

        try {
            $inventory = InventoryCategory::findOrFail($id);

            $inventory->delete();

            $message = 'Succeeded delete inventory category!';
            $type = 'succes';
        } catch (Exception $e) {
            $message = 'Failed to delete inventory category';
        }

        return redirect()->back()->with($type, $message);
    }
}
