<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\InventoryItem;

class InventoryItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
        $this->middleware('admin')->only(['destroy']);
    }

    public function borrow(Request $request, $user_id)
    {
        // validation
        $request->validate([
            'item' => 'required',
        ]);

        $type = 'error';

        try {
            $item = InventoryItem::findOrFail($request->input('item'));

            $item->executed_by = Auth::user()->id;
            $item->status = 'borrowed';
            $item->borrower = $user_id;

            $item->save();

            $message = 'Succeeded borrowed an item!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to borrow an item! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function borrowForUser(Request $request, $item_id)
    {
        // validation
        $request->validate([
            'borrower' => 'required',
        ]);

        $type = 'error';

        try {
            $item = InventoryItem::findOrFail($item_id);

            $item->executed_by = Auth::user()->id;
            $item->status = 'borrowed';
            $item->borrower = $request->input('borrower');

            $item->save();

            $message = 'Succeeded borrowed an item!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to borrow an item! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function return(Request $request, $user_id)
    {
        // validation
        $request->validate([
           'item' => 'required', 
        ]);

        $type = 'error';

        try {
            $item = InventoryItem::findOrFail($request->input('item'));

            $item->executed_by = Auth::user()->id;
            $item->status = 'returned';
            $item->borrower = null; // no borrower

            $item->save();

            $message = 'Succeeded returned an item!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to return an item! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function store(Request $request)
    {
        $request->validate([
            'item' => 'required',
        ]);

        $type = 'error';

        try {
            $item = new InventoryItem($request->all());

            $item->save();

            $type = 'success';
            $message = 'Succeeded creating a new item!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new item! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function destroy($id)
    {
        $type = 'error';

        try {
            $item = InventoryItem::findOrFail($id);

            $item->delete();

            $type = 'success';
            $message = 'Succeeded deleting a item data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a item data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}