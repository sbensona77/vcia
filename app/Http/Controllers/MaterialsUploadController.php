<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Course;
use App\CoursesMaterial;
use App\MaterialsUpload;

class MaterialsUploadController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('teacher')->except(['index', 'download_material', 'show']);
    }

    public function get_course_name($material_id){
        return Course::find(CoursesMaterial::find($material_id)->course_id)->name;
    }

    public function upload_material($course_name, $file_request){
        $fileName = Carbon::now()->format('Y-m-d H:i:s').' '.$file_request->getClientOriginalName(); //get name

        try {
            $file_request->move(
                public_path('/file_uploads/courses_material/'.$course_name), //use $course_name as folder name
                $fileName
            );
        } catch (Exception $e) {
            return redirect('error/500')->with('error', $e->getMessage());
        }

        return $fileName;
    }

    public function download_material($file_id){
        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        $file_material = MaterialsUpload::findOrFail($file_id);
        $material = CoursesMaterial::findOrFail($file_material->material_id);
        $course = Course::findOrFail($material->course_id);
        
        //PDF file is stored under project/public
        $directory = public_path().'/file_uploads/courses_material/'.$course->name.'/'.$file_material->uploaded_file;   

        return response()->download($directory, $file_material->uploaded_file, $headers, 'inline');
    }

    public function delete_material($course_name, $fileName){ //folder_name = course name
        $dir = public_path().'/file_uploads/courses_material/'.$course_name.'/';

        return File::delete($dir.$fileName);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'material_id' => 'required',
            'uploaded_file' => 'required|mimes:pdf|max:10000',
        ]);

        $message = '';
        $type = '';

        $course_name = $this->get_course_name($request->input('material_id'));
        $course_id = CoursesMaterial::findOrFail($request->input('material_id'))->course_id;

        try {
            $data = new MaterialsUpload([
                'material_id' => $request->input('material_id'), //hidden input

                //upload file and get the name
                'uploaded_file' => substr($this->upload_material($course_name, $request->file('uploaded_file')), 0, 40),
            ]);

            $data->save();

            $type = 'success';
            $message = 'Succeeded uploading course material!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to upload course material! : '.$e->getMessage();
        }

        return redirect()->route('materials.show', ['course_id' => $course_id, 'id' => $request->input('material_id')])->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'material_id' => 'required',
            'uploaded_file' => 'required|max:10000',
        ]);

        $message = '';
        $type = '';

        try {
            $data = MaterialsUpload::findOrFail($id);
 
            $course_name = $this->get_course_name($data->material_id);

            if($request->hasFile('uploaded_file')){
                //delete old file
                $this->delete_material($course_name, $data->uploaded_file);

                //upload file and get the name
                $data->uploaded_file = substr($this->upload_material($course_name, $request->file('uploaded_file')), 0, 40);
            }

            $data->save();

            $type = 'success';
            $message = 'Succeeded editing course material!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to edit course material! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = MaterialsUpload::findOrFail($id);

            $course_name = $this->get_course_name($data->material_id);

            //delete file
            $this->delete_material($course_name, $data->uploaded_file);

            $data->delete(); //delete

            $type = 'success';
            $message = 'Succeeded deleting course material data!'; 
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to update course material data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}
