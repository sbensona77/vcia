<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\News;

use File;
use Carbon\Carbon;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('teacher');
        $this->middleware('admin')->only(['destroy']);
    }

    public function upload_photo($file_request)
    {
        $photo_name = Carbon::now()->format('Y-m-d H:i:s').' '.$file_request->getClientOriginalName(); //get name
        $file_request->move(
            public_path('/file_uploads/news/'), 
            $photo_name
        );

        return $photo_name;
    }

    public function destroy_photo($fileName)
    {
        $directory = public_path('file_uploads/news');

        return File::delete($directory.$fileName);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->paginate(10);

        return view('dashboards.press.index', compact(['news']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboards.press.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'title' => 'required',
            'picture' => 'required',
            'content' => 'required',
            'category' => 'required',
        ]);

        try {
            $news = new News([
                'title' => $request->get('title'),
                'content' => $request->get('content'),
                'picture' => $this->upload_photo($request->file('picture')),
                'category' => $request->get('category'),
                'date' => Carbon::now(),
            ]);

            $news->save();

            $type = 'success';
            $message = 'Succeeded creating new news!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Error creating new news! '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::findOrFail($id);

        return view('dashboards.press.view', compact(['news']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);

        return view('dashboards.press.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'category' => 'required',
        ]);

        try {
            $news = News::findOrFail($id);

            // collecting data
            $news->title = $request->get('title');
            $news->content = $request->get('content');
            $news->category = $request->get('category');

            if($request->hasFile('picture'))
                $news->picture = $this->upload_photo($request->file('picture'));

            $news->save();

            $type = 'success';
            $message = 'Succeeded updating news!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Error creating new news! '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $news = News::findOrFail($id);

            $news->delete();

            $type = 'success';
            $message = 'Succeeded deleting news data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete news data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}
