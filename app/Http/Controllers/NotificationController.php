<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\User;
use App\Notification;

use App\Notification\CourseNotification;
use Illuminate\Support\Facades\Notification;

class NotificationController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
        $this->middleware('teacher')->only(['broadcast']);
    }

    public function push($reciever, $message){
    	$notification = new Notification([
    		'announcer' => Auth::user()->name, //user
    		'detail' => $message,
    		'reciever' => $reciever,
    	]);
    }

    public function broadcast($message){
    	try {
            $allUser = User::all();
            $sender = Auth::user()->name;

            foreach ($allUser as $key => $reciever) {
                $notification = new CourseNotification;
                $notification->setAttribute('From: ', $sender);
                $notification->setAttribute('To: ', $reciever);
                $notification->setAttribute('Message: ', $message);
                $notification->save();

                // send notification using the "user" model, when the user receives new message
                $reciever->notify(new CourseNotification($sender));

                // send notification using the "Notification" facade
                Notification::send($reciever, new NewMessage($fromUser));
            }
    	} catch (Exception $e) {
    		echo 'Error: '.$e;
    	}
    }

    public function read($notif_id){
    	$notification = Notification::findOrFail($notif_id);
    	
    	$notification->read_status = 1; //read
    }
}
