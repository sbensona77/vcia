<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\PACEStock;

use App\InventoryItem;

class PACEStockController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teahcer')->only(['relateMaterialToStock']);
    }

    public function relateMaterialToStock(Request $request)
    {
        $request->validate([
            'pace_id' => 'required',
            'item_id' => 'required',
        ]);

        $type = 'error';

        try {
            $relation = new PACEStock($request->all());

            $relation->save();

            $message = 'Succeeded connecting a PACE with the stock!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to connect a PACE to stock! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function updateRelation(Request $request, $id)
    {
        $request->validate([
            'pace_id' => 'required',
            'item_id' => 'required',
        ]);

        $type = 'error';

        try {
            $relation = PACEStock::findOrFail($id);

            $relation->fill($request->all());

            $relation->save();

            $message = 'Succeeded updating relation of PACE and item!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to update relation of PACE and item! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}