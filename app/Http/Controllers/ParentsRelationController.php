<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\User;
use App\ParentsRelation;

class ParentsRelationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'parent_id' => 'required',
            'student_id' => 'required',
            'relation' => 'required',
        ]);

        $message = 'Unknown error!';
        $type = 'error';

        try {
            $relation = new ParentsRelation([
                'parent_id' => $request->input('parent_id'),
                'student_id' => $request->input('student_id'),
                'relation' => $request->input('relation'),
            ]);

            $relation->save();

            $message = 'Succeeded added a relation!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to add relation! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'parent_id' => 'required',
            'student_id' => 'required',
            'relation' => 'required',
        ]);

        $message = 'Unknown error!';
        $type = 'error';

        try {
            $relation = ParentsRelation::findOrFail($id);
                
            $relation->parent_id = $request->input('parent_id');
            $relation->student_id = $request->input('student_id');
            $relation->relation = $request->input('relation');

            $relation->save();

            $message = 'Succeeded updating a relation!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to update relation! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = 'Unknown error!';
        $type = 'error';

        try {
            $relation = ParentsRelation::findOrFail($id);

            $relation->delete();

            $message = 'Succeeded deleting a relation!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to delete relation! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}
