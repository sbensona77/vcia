<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Payment;
use App\User;

class PaymentController extends Controller
{
    protected $viewDirectory = '';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->except(['index', 'show']);

        $this->viewDirectory = 'dashboards.payments';
    }

    public function amountConverter($amount)
    {
        $explodedAmount = explode('.', $amount);

        $newStringAmount = '';
        foreach ($explodedAmount as $key => $digit) {
            $newStringAmount .= $digit;    
        }

        return doubleval($newStringAmount);
    }

    public function payWithMidtrans()
    {
        
    }

    public function changeStatus(Request $request, $payment_id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $payment = Payment::findOrFail($payment_id);

        $payment->status = $request->input('status');

        $payment->save();

        return redirect()->back()->with('success', 'Succeeded updating payment status');
    }

    public function broadcastPayment(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'amount' => 'required',

            'due_date' => 'required',
        ]);

        $type = 'error';

        try {
            $students = User::all()->where('role', 'student');

            foreach ($students as $key => $student) {
                $payment = new Payment($request->all());
                $payment->amount = $this->amountConverter($request->input('amount'));
                $payment->student_id = $student->id;
                $payment->save();
            }

            $message = 'Succeeded sent payment bills to all students!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to sent payment bills to all students! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = new Payment();

        if(Auth::user()->role == 'admin')
            $payments = Payment::orderBy('status')->paginate(10);

        if(Auth::user()->role == 'student') 
        {
            $payments = $payments
            ->where('student_id', Auth::user()->id)
            ->where('status', 'unpaid')
            ->get();
        }
        elseif(Auth::user()->role == 'parent') 
        {
            $relations = Auth::user()->profile->relations;

            // store all data
            $allPayment = $payments
            ->where('status', 'unpaid')
            ->get();
            $studentsId = [];
            foreach ($relations as $key => $relation) {
                $child = $relation->child;
                $studentsId[$key] = ($child->user_id);
            }

            $payments = $allPayment->whereIn('student_id', $studentsId);
        }

        $users = User::all()->where('role', 'student');

        return view($this->viewDirectory.'.index', compact(['payments', 'users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewDirectory.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'student_id' => 'required',

            'title' => 'required',
            'description' => 'required',
            'amount' => 'required',

            'due_date' => 'required',
        ]);

        $type = 'error';

        try {
            $payment = new Payment($request->all());
            $payment->amount = $this->amountConverter($request->input('amount'));

            $payment->save();

            $type = 'success';
            $message = 'Succeeded creating a new payment!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new payment! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = Payment::findOrFail($id);
        
        return view($this->viewDirectory.'.view', compact(['payment']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = Payment::findOrFail($id);

        return view($this->viewDirectory.'.edit', compact(['payment']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'student_id' => 'required',

            'title' => 'required',
            'description' => 'required',
            'amount' => 'required',
            'status' => 'required',

            'due_date' => 'required',
        ]);

        $type = 'error';

        try {
            $payment = Payment::findOrFail($id);
            
            $payment->fill($request->all());
            
            $payment->save();

            $type = 'success';
            $message = 'Succeeded updating a payment data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update a payment data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $payment = Payment::findOrFail($id);

            $payment->delete();

            $type = 'success';
            $message = 'Succeeded deleting a payment data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a payment data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}