<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Carbon\Carbon;

use App\Payment;
use App\PaymentsInstallment;

class PaymentsInstallmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createInstallment(Request $request, $payment_id)
    {
        $request->validate([
            'months' => 'required|numeric|min:1',
            'start' => 'required',
        ]);

        $type = 'error';
        $message = '';

        $payment = Payment::findOrFail($payment_id);

        $months = $request->input('months');
        if($months <= 0 || !$months)
            $months = 1;

        $amountPerMonth = $payment->amount / $months;

        $thisMonth = Carbon::now();

        for ($i = 1; $i <= $months; $i++) { 
            try {
                $nextMonth = $thisMonth->addMonthsNoOverflow($i);
                $dueDate = $nextMonth->addMonthsNoOverflow(1);

                $installment = new PaymentsInstallment([
                    'payment_id' => $payment->id,
                    'amount' => $amountPerMonth,
                    'due_date' => $dueDate,
                ]);
                $installment->save();

                $type = 'success';
                $message .= '<li>Succeeded to create new installment!</li>';
            } catch (Exception $e) {
                $message .= '<li>Failed to create new installment!</li>';
            }
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewDirectory.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            
        ]);

        $type = 'error';

        try {
            $paymentsinstallment = new PaymentsInstallment($request->all());

            $paymentsinstallment->save();

            $type = 'success';
            $message = 'Succeeded creating a new paymentsinstallment!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new paymentsinstallment! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([

        ]);

        $type = 'error';

        try {
            $paymentsinstallment = PaymentsInstallment::findOrFail($id);
            
            $paymentsinstallment->fill($request->all());
            
            $paymentsinstallment->save();

            $type = 'success';
            $message = 'Succeeded updating a paymentsinstallment data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update a paymentsinstallment data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $paymentsinstallment = PaymentsInstallment::findOrFail($id);

            $paymentsinstallment->delete();

            $type = 'success';
            $message = 'Succeeded deleting a paymentsinstallment data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a paymentsinstallment data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}