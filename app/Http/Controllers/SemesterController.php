<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Semester;

use App\AssignedCourses;
use App\AssignedMaterials;

class SemesterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function startNewSemester(Request $request)
    {
        $request->validate([
            'semester_type' => 'required',
            'year' => 'required',
            'semester_end' => 'required',
            'status' => 'required',
        ]);

        $type = 'error';

        try {
            $semester = new Semester($request->all());
            $semester->semester_start = $request->input('semester_start');

            $semester->save();

            $message = 'Succeeded making a new semester!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to create new semester! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function closeSemester($semester_id)
    {
        $type = 'error';

        try {
            $semester = Semester::findOrFail($semester_id);
            $semester->semester_end = \Carbon\Carbon::now();
            $semester->status = '0';
            $semester->save();

            $message = 'Succeeded to close a semester!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to close semester! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}