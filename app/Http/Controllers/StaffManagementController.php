<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\StaffManagement;

use App\User;

class StaffManagementController extends Controller
{
    protected $viewDirectory;

    public function __construct()
    {
        $this->middleware('teacher');
        $this->middleware('admin')->except(['index', 'view']);

        $this->viewDirectory = 'dashboards.staffs-managements';
    }

    public function search(Request $request)
    {
        $input = $request->input('search');

        // search by title
        $managements = StaffManagement::where('title', 'like', '%'.$input)
        ->orWhere('title', 'like', $input.'%')
        ->orWhere('title', 'like', '%'.$input.'%')
        ->orWhere('title', 'like', $input)

        // search by description
        ->orWhere('description', 'like', $input.'%')
        ->orWhere('description', 'like', $input.'%')
        ->orWhere('description', 'like', '%'.$input.'%')
        ->orWhere('description', 'like', $input)
        ->get();

        $staffs = User::where('role', 'teacher')->get();

        return view($this->viewDirectory.'.index', compact([
            'managements',
            'staffs',
        ]));
    }

    public function finish($management_id)
    {
        try {
            $management = StaffManagement::findOrFail($management_id);

            $management->status = 'finish';

            $management->save();

            $message = 'Succeeded to finish job of a staff!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to finish staff job, Error: '.$e->getMessage();
            $type = 'error';
        }

        return redirect()->back()->with($type, $message);
    }

    public function cancel($management_id)
    {
        try {
            $management = StaffManagement::findOrFail($management_id);

            $management->status = 'canceled';

            $management->save();

            $message = 'Succeeded to cancel staff job!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to cancel staff job, Error: '.$e->getMessage();
            $type = 'error';
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managements = StaffManagement::all()
        ->sortBy('staff_id')
        ->sortByDesc('deadline');
        $staffs = User::where('role', 'teacher')->get();

        return view($this->viewDirectory.'.index', compact([
            'managements',
            'staffs',
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewDirectory.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'staff_id' => 'required',
            'status' => 'required',
            'deadline' => 'required',
        ]);

        $type = 'error';

        try {
            $management = new StaffManagement($request->all());

            $management->save();

            $type = 'success';
            $message = 'Succeeded creating a new staff assignment!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new staff assignment! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $management = StaffManagement::findOrFail($id);
        
        return view($this->viewDirectory.'.view', compact(['management']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $management = StaffManagement::findOrFail($id);

        return view($this->viewDirectory.'.edit', compact(['management']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'staff_id' => 'required',
            'status' => 'required',
            'deadline' => 'required',
        ]);

        $type = 'error';

        try {
            $management = StaffManagement::findOrFail($id);
            
            $management->fill($request->all());
            
            $management->save();

            $type = 'success';
            $message = 'Succeeded updating a staff assignment data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update a staff assignment data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $management = StaffManagement::findOrFail($id);

            $management->delete();

            $type = 'success';
            $message = 'Succeeded deleting a staff assignment data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a staff assignment data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}