<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Term;

class TermController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function startNewTerm(Request $request, $semester_id)
    {
        $request->validate([
            'year' => 'required',
            'term_end' => 'required',
            'status' => 'required',
        ]);

        $type = 'error';

        try {
            $term = new Term($request->all());
            $term->semester_id = $semester_id;
            $term->term_start = $request->input('term_start');

            $term->save();

            $message = 'Succeeded making a new term!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to create new term! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function closeSemester($term_id)
    {
        $type = 'error';

        try {
            $term = Term::findOrFail($term_id);
            $term->term_end = \Carbon\Carbon::now();
            $term->status = 0;
            $term->save();

            $message = 'Succeeded to close a term!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to close term! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}