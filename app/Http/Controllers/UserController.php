<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Hash;

use App\User;

use App\StudentsProfile;
use App\ParentsProfile;
use App\StaffsProfile;

use App\ParentsRelation;

use App\AcademicYear;
use App\AcademicYearTerm;
use App\AssignedCourses;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->only(['admin', 'update', 'destroy']);
    }

    public function search(Request $request)
    {
        $input = $request->input('search');

        $users = 
        // check name
        User::where('name', 'like', $input)
        ->orWhere('name', 'like', '%'.$input)
        ->orWhere('name', 'like', $input.'%')
        ->orWhere('name', 'like', '%'.$input.'%')

        // check id
        ->orWhere('id', 'like', $input)
        ->orWhere('id', 'like', '%'.$input)
        ->orWhere('id', 'like', $input.'%')
        ->orWhere('id', 'like', '%'.$input.'%')

        // check email
        ->orWhere('email', 'like', $input)
        ->orWhere('email', 'like', '%'.$input)
        ->orWhere('email', 'like', $input.'%')
        ->orWhere('email', 'like', '%'.$input.'%')

        ->get();

        if(Auth::user()->role != 'admin')
            $users = $users->whereNotIn('role', ['admin']);

        return view('dashboards.users-management.index', compact(['users']));
    }

    public function changePassword(Request $request, $user)
    {
        $request->validate([
            'password' => 'confirmed|min:6'
        ]);

        if(Auth::user()->role != 'admin'){
            // non admin should confirm old password
            $request->validate([
                'old_password'
            ]);

            if(Hash::check($user->password, $request->input('old_password')))
                $user->password = Hash::make($request->input('password'));
        }
        else
        {
            $user->password = Hash::make($request->input('password'));
        }   

        return $user->save();
    }

    public function changeEmail(Request $request, $user)
    {
        $user->email = $request->input('new_email');

        return $user->save();
    }

    public function updateAccount(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $user = User::findOrFail($id);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            if($request->input('new_email'))
                $this->changeEmail($request, $user);

            if($request->input('password'))
                $this->changePassword($request, $user);

            // change status of user
            $user->status = $request->input('status');

            $type = 'success';
            $message = 'Succeeded updating user data!';
        } catch (Exception $e) {
            $message = 'Failed to update user data! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function setStudentGrade($user_id, $grade) 
    {
        $profile = StudentsProfile::where('user_id', $user_id)
        ->get()
        ->first();

        $profile->level = $grade;

        return $profile->save();
    }

    public function getActiveTerm()
    {
        return AcademicYearTerm::where('status', 1)->get()->first();
    }

    public function getActiveSemester()
    {
        return AcademicYear::where('status', 1)->get()->first();
    }

    public function getAssignedStatus($student_id)
    {
        $activeTerm = $this->getActiveTerm();
        if($activeTerm) {
            $termStatus = AssignedCourses::where('student_id', $student_id)
            ->where('created_at', '>=', $activeTerm->term_start)
            ->where('created_at', '<=', $activeTerm->term_end)
            ->get()
            ->first();
        } else {
            $termStatus = [];
        }
        
        $activeSemester = $this->getActiveSemester();
        if($activeSemester) {
            $semesterStatus = AssignedCourses::where('student_id', $student_id)
            ->where('created_at', '>=', $activeSemester->semester_start)
            ->where('created_at', '<=', $activeSemester->semester_end)
            ->get()
            ->first();
        } else {
            $semesterStatus = [];
        }

        $statuses = [
            'term' => $termStatus !== [] ? true : false,
            'no_term' => $activeTerm !== null ? true : false,
            'semester' => $semesterStatus !== [] ? true : false,
            'no_semester' => $activeSemester !== null ? true : false,
        ];

        return $statuses;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function admin()
    {
        $users = User::where('role', 'admin')->paginate(10);

        return view('dashboards.users-management.index', compact(['users']));
    }

    public function teacher()
    {
        $users = User::where('role', 'teacher')->paginate(10);

        return view('dashboards.users-management.index', compact(['users']));
    }

    public function student()
    {
        $users = User::where('role', 'student')->paginate(10);
        $users->each(function($user) {
            $assignStatus = $this->getAssignedStatus($user->id);

            $user->term_status = $assignStatus['term'];
            $user->term_exist = $assignStatus['no_term'];
            $user->semester_status = $assignStatus['semester'];
            $user->semester_exist =  $assignStatus['no_semester'];
        });

        return view('dashboards.users-management.index', compact(['users']));
    }

    public function parent()
    {
        $users = User::where('role', 'parent')->paginate(10);

        return view('dashboards.users-management.index', compact(['users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboards.users-management.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'role' => 'required',
            'name' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'password' => 'required|min:6',
            'status' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $user = new User([
                'role' => $request->input('role'),
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'gender' => $request->input('gender'),
                'password' => Hash::make($request->input('password')),
                'status' => $request->input('status'),
            ]);

            $save = $user->save();
            if($save && $user->role == 'student')
            {
                $profile = $this->setStudentGrade($user->id, $request->input('class'));
            }

            $type = 'success';
            $message = 'Succeeded creating new user!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create new user! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('dashboards', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id); 

        return view('dashboards', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'role' => 'required',
            'name' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'password' => 'required|min:6|confirmed',
            'status' => 'required',
        ]);

        $type = '';
        $message = '';

        try {
            $user = User::findOrFail($id);

            $user->role = $request->input('role');
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->gender = $request->input('gender');
            $user->password = Hash::make($request->input('password'));
            $user->status = $request->input('status');

            $save = $user->save(); //save update

            if($user->role == 'student' && $request->input('class') !== '') {
                $profile = $this->setStudentGrade($user->id, $request->input('class'));
            }

            $type = 'success';
            $message = 'Succeeded update user data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to edit user data! : '.$e->getMessage();
        }

        return redirect('dashboard')->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = '';
        $message = '';

        try {
            $user = User::findOrFail($id);
            $role = $user->role;

            //dd($role);

            $user->delete();

            $type = 'success';
            $message = 'Succeeded deleting user data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to edit user data! : '.$e->getMessage();
        }

        return redirect()->route($role.'s.index')->with($type, $message);
    }
}