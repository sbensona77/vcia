<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;
use Response;

use Carbon\Carbon;

use App\AcademicYear;
use App\AcademicYearTerm;

use App\User;
use App\ParentsProfile;
use App\StudentsProfile;
use App\StaffsProfile;

use App\AssignedCourses;
use App\AssignedMaterials;

use App\WorkHabit;
use App\StudentsHabit;

use App\Club;
use App\AssignedClub;

use App\BibleMemory;

use App\Course;
use App\CoursesMaterial;
use App\MaterialsUpload;

use App\VictoryPoints;
use App\EarnedPoints;

use App\WeeklyReport;

use App\InventoryCategory;
use App\Inventory;
use App\InventoryItem;

use App\PrintPage;

use App\StaffManagement;

class UserProfileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
   //     $this->middleware('owner')->only(['update']);
        $this->middleware('teacher')->only(['levelUpStudent', 'levelDownStudent', 'createReport']);
        $this->middleware('admin')->only(['destroy']);
    }

    public function printUserProfile($user_id)
    {
        $user = User::findOrFail($user_id);
        $profile = $user->profile;

        return view('exports.profile-print', compact(['user', 'profile']));
    }

    public function getSemesterById($semester_id)
    {
        return Semester::findOrFail($semester_id);
    }

    public function getSemesterByDate($date_start, $date_end, $status = 1)
    {
        return Semester::where('semester_start', $date_start)
        ->where('semester_end', $semester_end)
        ->where('status', $status)
        ->get();
    }

    public function levelUpStudent($user_id)
    {
        try {
            $student = StudentsProfile::where('user_id', $user_id)
            ->get()
            ->first();

            $student->level = $student->level + 1;

            if($student->level >= 12) {
                $student->level = 13; // graduated
                $student->user->status = 0; // acount inactive
            }

            $student->save();

            $type = 'success';
            $message = 'Succeeded leveling up a student!';
        } catch(Exception $e){
            $type = 'error';
            $message = 'Failed to level up a student!';
        }

        return redirect()->back()->with($type, $message);
    }

    public function levelDownStudent($user_id)
    {
        try {
            $student = StudentsProfile::where('user_id', $user_id)
            ->get()
            ->first();

            $student->level = $student->level - 1;

            if($student->level < 1)
                $student->level = 1;

            $student->save();

            $type = 'success';
            $message = 'Succeeded updating student level!';
        } catch(Exception $e){
            $type = 'error';
            $message = 'Failed to change student of level!';
        }

        return redirect()->back()->with($type, $message);
    }

    public function upload_picture($file_request)
    {
        $fileName = Carbon::now()->format('Y-m-d H:i:s').' '.$file_request->getClientOriginalName(); //get name

        try {
            $file_request->move(
                public_path('/file_uploads/users/'), //use $course_name as folder name
                $fileName
            );
        } catch (Exception $e) {
            return redirect('error/500')->with('error', $e->getMessage());
        }

        return $fileName;
    }

    public function destroy_picture($fileName)
    {
        $dir = public_path().'/file_uploads/users';

        return File::delete($dir.$fileName);
    }

    public function printSemesterReport(Request $request, $student_id)
    {
        $request->validate(['id_semester' => 'required']);

        $semester = AcademicYear::findOrFail($request->input('id_semester'));

        $start = $semester->semester_start;
        $end = $semester->semester_end;

        $user = User::findOrFail($student_id);

        $courses = 
        AssignedCourses::where('student_id', $student_id)
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->get();

        $materials = 
        AssignedMaterials::where('student_id', $student_id)
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->where('status', 'graded')
        ->get();

        $points = 
        EarnedPoints::where('student_id', $student_id)
        ->get();

        $clubs = 
        AssignedClub::where('student_id', $student_id)
        ->get();

        $bible_memories = 
        BibleMemory::where('student_id', $student_id)
        /*->where('month', '>=', Carbon::parse($start)->month)
        ->where('month', '<=', Carbon::parse($end)->month)
        ->where('created_at', '>=', Carbon::now())*/
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->get()
        ->groupBy('month');

        $habits = WorkHabit::all()->groupBy('type');

        $student_habits = 
        StudentsHabit::where('student_id', $student_id)
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->get();

        $schoolDays = $request->input('semester_school_days') ? $request->input('semester_school_days') : 0;
        $absenceDays = $request->input('semester_absence_days') ? $request->input('semester_absence_days') : 0;

        return view('exports.student-report-national', [
            'student_id' => $student_id,
            'courses' => $courses,
            'materials' => $materials,
            'points' => $points,
            'clubs' => $clubs,
            'bible_memories' => $bible_memories,
            'habits' => $habits,
            'student_habits' => $student_habits,
            'profile' => $user->profile,
            'semester' => $semester,
            'school_days' => $schoolDays,
            'absence_days' => $absenceDays,
            'semester' => $semester,
        ]);
    }

    public function printTermReport(Request $request, $student_id)
    {
        $request->validate(['id_term' => 'required']);

        $term = AcademicYearTerm::findOrFail($request->input('id_term'));

        $start = $term->term_start;
        $end = $term->term_end;

        $user = User::findOrFail($student_id);

        $courses = 
        AssignedCourses::where('student_id', $student_id)
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->get();

        $materials = 
        AssignedMaterials::where('student_id', $student_id)
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->where('status', 'graded')
        ->get();

        $points = 
        EarnedPoints::where('student_id', $student_id)
        ->get();

        $clubs = 
        AssignedClub::where('student_id', $student_id)
        ->get();

        $bible_memories = 
        BibleMemory::where('student_id', $student_id)
        /*->where('month', '>=', Carbon::parse($start)->month)
        ->where('month', '<=', Carbon::parse($end)->month)
        ->where('created_at', '>=', Carbon::now())*/
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->get()
        ->groupBy('month');

        // dd($bible_memories);

        $habits = WorkHabit::all()->groupBy('type');

        $student_habits = 
        StudentsHabit::where('student_id', $student_id)
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->get();

        $schoolDays = $request->input('term_school_days') ? $request->input('semester_school_days') : 0;
        $absenceDays = $request->input('term_absence_days') ? $request->input('semester_absence_days') : 0;

        return view('exports.student-report', [
            'student_id' => $student_id,
            'courses' => $courses,
            'materials' => $materials,
            'points' => $points,
            'clubs' => $clubs,
            'bible_memories' => $bible_memories,
            'habits' => $habits,
            'student_habits' => $student_habits,
            'profile' => $user->profile,
            'term' => $term,
            'school_days' => $schoolDays,
            'absence_days' => $absenceDays,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $profile = $user->profile;

        return view('dashboards.user-profile.index', compact(['user', 'profile']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $students = User::all()->where('role', 'student');

        if($user->role == 'admin' && Auth::user()->role != 'admin')
        {
            return redirect('/error/503');
        }

        // courses
        $courses = Course::all();
        $materials = CoursesMaterial::all();

        // assignment
        $assigns = AssignedCourses::all()
        ->where('student_id', $id);
        $assigned_materials = AssignedMaterials::all()
        ->where('student_id', $id);

        // clubs
        $clubs = Club::all();
        $assigned_clubs = AssignedClub::all()
        ->where('student_id', $user->id);

        // profile
        $data = $user->profile;

        // points
        $points = VictoryPoints::all()
        ->sortBy('activity_name');
        $earns = EarnedPoints::all()
        ->where('student_id', $user->id)->sortBy('point_id');

        $sum = EarnedPoints::getPointSum($user->id);

        // weekly report
        $reports = WeeklyReport::all()
        ->where('student_id', $id);

        // items to be borrowed
        $inventories = InventoryCategory::all();
        $borrows = InventoryItem::where('borrower', $user->id)
        ->get();

        // students habits
        $habits = WorkHabit::all()->groupBy('type');

        // semesters and terms
        $semesters = AcademicYear::all();
        $activeSemester = $semesters->where('status', 1)->first();

        // tasks
        $tasks = StaffManagement::where('staff_id', $user->id)->get();

        // list on semester based data
        if($activeSemester) {
            $assigns = $assigns
            ->where('created_at', '>=', $activeSemester->semester_start)
            ->where('created_at', '<=', $activeSemester->semester_end);
            $assigned_materials = $assigned_materials
            ->where('created_at', '>=', $activeSemester->semester_start)
            ->where('created_at', '<=', $activeSemester->semester_end);

            $assigned_clubs = $assigned_clubs
            ->where('created_at', '>=', $activeSemester->semester_start)
            ->where('created_at', '<=', $activeSemester->semester_end);

            $earns = $earns
            ->where('created_at', '>=', $activeSemester->semester_start)
            ->where('created_at', '<=', $activeSemester->semester_end);
            
            $reports = $reports
            ->where('created_at', '>=', $activeSemester->semester_start)
            ->where('created_at', '<=', $activeSemester->semester_end);
        }

        return view('dashboards.user-profile.view', compact([
            // user
            'data',
            'user',
            'students',

            // courses and materials
            'courses', 
            'materials',

            // assignments
            'assigns',  
            'assigned_materials',

            // points
            'points', 
            'earns', 
            'sum',

            // reports
            'reports',

            // inventory
            'inventories',
            'borrows',

            // clubs
            'clubs',
            'assigned_clubs',

            // habits
            'habits',

            // semesters
            'semesters',

            // tasks
            'tasks',
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        if($user->role == 'student')
            $data = StudentsProfile::all()->where('user_id', $id)->first();
        else 
            $data = StaffsProfile::all()->where('user_id', $id)->first();

        return view('dashboards.user-profile.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        if($user->role == 'student'){
            $data = StudentsProfile::all()->where('user_id', $id)->first();

            if($request->hasFile('profile_picture')){
                $request->validate([
                    'profile_picture' => 'mimes:png,jpg,jpeg,svg|max:5000',
                ]);

                $this->destroy_picture($data->profile_picture);
                $picture = $this->upload_picture($request->file('profile_picture'));
            }else{
                $picture = $data->profile_picture;
            }

            try {
                $data->update([
                    'profile_picture' => $picture,
                    'nis' => $request->input('nis'),
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'birth_date' => $request->input('birth_date'),
                    'phone' => $request->input('phone'),
                ]);

                $type = 'success';
                $message = 'Succeeded updating profile data';
            } catch (Exception $e) {
                $message = 'Cannot update profile! '.$e->getMessage();
            }
        }elseif($user->role == 'parent'){
            $data = ParentsProfile::all()->where('user_id', $id)->first();

            if($request->hasFile('profile_picture')){
                $request->validate([
                    'profile_picture' => 'mimes:png,jpg,jpeg,svg|max:5000',
                ]);

                $this->destroy_picture($data->profile_picture);
                $picture = $this->upload_picture($request->file('profile_picture'));
            }else{
                $picture = $data->profile_picture;
            }

            try {
                $data->update([
                    'profile_picture' => $picture,
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                ]);

                $type = 'success';
                $message = 'Succeeded updating profile data';
            } catch (Exception $e) {
                $message = 'Cannot update profile! '.$e->getMessage();
            }

        }else{ 
            $data = StaffsProfile::all()->where('user_id', $id)->first();

            if($request->hasFile('profile_picture')){
                $request->validate([
                    'profile_picture' => 'mimes:png,jpg,jpeg,svg|max:5000',
                ]);

                $this->destroy_picture($data->profile_picture);
                $picture = $this->upload_picture($request->file('profile_picture'));
            }else{
                $picture = $data->profile_picture;
            }

            try {
                $data->update([
                    'profile_picture' => $picture,
                    'nip' => $request->input('nip'),
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'position' => $request->input('position'),
                    'subject' => $request->input('subject'),
                    'birth_date' => $request->input('birth_date'),
                    'phone' => $request->input('phone'),
                ]);

                $type = 'success';
                $message = 'Succeeded updating profile data';
            } catch (Exception $e) {
                $message = 'Cannot update profile! '.$e->getMessage();
            }
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect()->back();
    }

    public function isStockAvailable($material_id)
    {
        $material = CoursesMaterial::findOrFail($material_id);

        $inventoryCategory = InventoryCategory::where('name', 'like', $material->course->name.'%')->first();

        $inventory = $inventoryCategory->inventories()
        ->where('inventory_name', 'like', '%'.$material->name)
        ->first();

        if(count($inventory->items))
            $available = true;
        else
            $available = false;

        return $available;
    }

    //AJAX functions
    public function getAllMaterials($course_id)
    {
        $materials = CoursesMaterial::all()->where('course_id', $course_id);

        $materials->each(function($item) {
            if(!$this->isStockAvailable($item->id)) // not exist
                $item->name = $item->name.' [No Stock PACE!]';
        });

        return Response::json(['materials' => json_encode($materials)]);
    }

    public function getAllItems($category_id)
    {
        $items = Inventory::all()
        ->where('category_id', $category_id)
        ->where('amount', '>', 0);

        return Response::json(['items' => json_encode($items)]);
    }

    public function getAllItemsCode($inventory_id)
    {
        $codes = InventoryItem::all()
        ->where('inventory_id', $inventory_id)
        ->where('status', 'available');

        return Response::json(['codes' => json_encode($codes)]);
    }
}
