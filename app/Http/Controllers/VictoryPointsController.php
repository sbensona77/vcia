<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Response;

use App\VictoryPoints;
use App\EarnedPoints;

class VictoryPointsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('teacher');
    }

    public function grantPoint(Request $request, $user_id){
        $earn = new EarnedPoints([
            'student_id' => $user_id,
            'point_id' => $request->input('point'),
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $earn->save();

            $message = 'Succeeded grant point to student!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to grant point to student!';
        }

        return redirect()->back()->with($type, $message);
    }

    public function reducePoint(Request $request, $id){
        $earned = EarnedPoints::findOrFail($id);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $earned->delete();

            $message = 'Succeeded reducing point from a student!';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to reduce point from a student!';
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'activity_name' => 'required',
            'points' => 'required',
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $point = new VictoryPoints([
                'activity_name' => $request->input('activity_name'),
                'points' => $request->input('points'),
            ]);

            $point->save();

            $type = 'success';
            $message = 'Succeeded created new kind of victory point!';
        } catch (Exception $e) {
            $message = 'Failed to create new kind of victroy point! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $point = VictoryPoints::findOrFail($id);

        return response()->json(['point' => json_encode($point)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'activity_name' => 'required',
            'points' => 'required',
        ]);

        $type = 'error';
        $message = 'Unknown error!';

        try {
            $point = VictoryPoints::findOrFail($id);

            $point->activity_name = $request->input('activity_name');
            $point->points = $request->input('points');

            $point->save();

            $type = 'success';
            $message = 'Succeeded updating kind of victory point!';
        } catch (Exception $e) {
            $message = 'Failed to updating kind of victroy point! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';
        $message = 'Unknown error!';

        try {
            $point = VictoryPoints::findOrFail($id);

            //dd($point);

            $point->delete();

            $type = 'success';
            $message = 'Succeeded deleting kind of victory point!';
        } catch (Exception $e) {
            $message = 'Failed to deleting kind of victroy point! Error: '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}
