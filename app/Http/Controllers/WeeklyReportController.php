<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\WeeklyReport;

class WeeklyReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('parent')->only(['confirm', 'confirmAll']);
        $this->middleware('teacher')->only(['store', 'update']);
        $this->middleware('admin')->only(['destroy']);
    }

    public function confirm(Request $request, $id)
    {
        $message = 'Unknown error';
        $type = 'error';

        try {
            $reports = WeeklyReport::where('student_id', $id)->where('start_week', '>=', $request->input('start_week'))->where('end_week', '<=', $request->input('end_week'))->where('confirmed', 0);

            $reports->update([
                'confirmed' => 1,
                'confirmer' => Auth::user()->id,
                'confirmed_at' => \Carbon\Carbon::now(),
            ]);

            $message = 'Succeeded confirming weekly report';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to confirm weekly report; Error: '. $e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    public function confirmAll(Request $request, $id)
    {
        $message = 'Unknown error';
        $type = 'error';

        try {
            $reports = WeeklyReport::where('student_id', $id)->where('confirmed', 0);
            
            $reports->update([
                'confirmed' => 1,
                'confirmer' => Auth::user()->id,
                'confirmed_at' => \Carbon\Carbon::now(),
            ]);

            $message = 'Succeeded confirming all report';
            $type = 'success';
        } catch (Exception $e) {
            $message = 'Failed to confirm all report; Error: '. $e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'students_id' => 'required',
            'course_id' => 'required',
            'material_id' => 'required',

            'info' => 'required',
        ]);

        $message = 'Unknown error!';
        $type = 'error';

        try {
            $report = new Course([
                'students_id' => $request->input('students_id'),
                'course_id' => $request->input('assigned_course_id'),
                'material_id' => $request->input('material_id'),

                'info' => $request->input('info'),
                'start_week' => $request->input('start_week'),
                'end_week' => $request->input('end_week'),
            ]);

            $report->save();

            $type = 'success';
            $message = 'Succeeded added weekly report for student!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create new report! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'info' => 'required',
        ]);

        $message = 'Unknown error!';
        $type = 'error';

        try {
            $report->info = $request->input('info');

            $report->save();

            $type = 'success';
            $message = 'Succeeded editing weekly report for student!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to edit new report! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = 'Unknown error!';
        $type = 'error';

        try {
            $report = WeeklyReport::findOrFail($id);

            $report->save();

            $type = 'success';
            $message = 'Succeeded deleting weekly report for student!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to edit new report! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}
