<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\WorkHabit;

class WorkHabitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'type' => 'required',
            'habit' => 'required',
        ]);

        $type = 'error';

        try {
            $habit = new WorkHabit($request->all());

            $habit->save();

            $type = 'success';
            $message = 'Succeeded creating a new work habit!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to create a new work habit! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'type' => 'required',
            'habit' => 'required',
        ]);

        $type = 'error';

        try {
            $habit = WorkHabit::findOrFail($id);
            
            $habit->fill($request->all());
            
            $habit->save();

            $type = 'success';
            $message = 'Succeeded updating a work habit data!';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'Failed to update a work habit data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = 'error';

        try {
            $habit = WorkHabit::findOrFail($id);

            $habit->delete();

            $type = 'success';
            $message = 'Succeeded deleting a work habit data!';
        } catch (Exception $e) {
            $type = 'error';
            $message = 'Failed to delete a work habit data! : '.$e->getMessage();
        }

        return redirect()->back()->with($type, $message);
    }
}