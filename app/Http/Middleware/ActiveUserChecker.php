<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class ActiveUserChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->status == 1){
            return $next($request);
        }

        $message = 'Sorry, your account has been suspended! Please ask admin or your home room teacher for activate your account';

        return view('auth.login', compact('message'));
    }
}
