<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

use App\User;
use App\StudentsProfile;
use App\ParentsProfile;

use App\ParentsRelation;

class AuthenticatedOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestParam = $request->route()->parameters('id');
        $requestedID = $requestParam['id'];
        $user = Auth::user();

        if(Auth::user()->role == 'admin' || Auth::user()->role == 'teahcer')
        {
            return $next($request);
        }
        else if($user->role == 'student' && $user->id == $requestedID)
        {
            return $next($request);
        }
        elseif($user->role == 'parent')
        {
            $relation = ParentsRelation::all()->where('parent_id', $user->id)->where('student_id', $requestedID)->first();

            if($relation != null || $user->id == $requestedID)
            {
                return $next($request);
            }else
            {
                return redirect('/error/503'); 
            }
        }else
        {
            return redirect('/error/503');
        }
    }
}
