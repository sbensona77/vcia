<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

use App\EnrolledStudent;

class EnrollmentCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher'){
            return $next($request);
        }else{
            $user = Auth::user();
            $material_id = $request->route()->parameters('material');

            $enrollment = EnrolledStudent::where('material_id', $material_id);

            if($enrollment->where('user_id', $user->id)->first()){
                return $next($request);
            }else{
                return redirect('dashboard/courses')->with('error', 'You are not enrolled! Please ask your mentor and insert the enrollment key');
            }
        }
    }
}
