<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

use Auth;

use App\InventoryLog;

class Inventory extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string;
        });

        self::created(function($model){
        	//create log
            $activity = 'New Inventory created';

            if(Auth::check()) 
            {
                $info = Auth::user()->name.' has created a new inventory!';

                self::recordToLog($model, $activity, $info);
            }
            else
            {
                $info = 'System has created a new inventory!';
            }
        });

        self::updated(function($model){
        	//create log
            $activity = 'Inventory Updated';
            $info = Auth::user()->name.' has updated an inventory';

            if($model->isDirty('amount')){
            	$original = (int) $model->getOriginal('amount');

            	if($original > (int) $model->amount)
                {

            		$info .= ' - Amount has been decreased from '.$original.' to '.$model->amount;
            	}
                elseif($original < (int) $model->amount)
                {
            		$info .= ' - Amount has been increased from '.$original.' to '.$model->amount;
            	}
            }

            return self::recordToLog($model, $activity, $info);
        });

        self::deleting(function($model){
        	//create log
            $activity = 'Deleting Inventory';
            $info = Auth::user()->name.' is DESTROYING '.$model->inventory_name.'; Last Amount: '.$model->amount;

            return self::recordToLog($model, $activity, $info);
        });
    }

    protected $table = 'inventories';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'inventory_name',
		'amount',
        'category_id',
    ];
    protected $hidden = [
        
    ];

    public function logs()
    {
    	return $this->hasMany('App\InventoryLog', 'inventory_id', 'id');
    }

    public function category()
    {
        return $this->hasOne('App\InventoryCategory', 'id', 'category_id');
    }

    public function items()
    {
        return $this->hasMany('App\InventoryItem', 'inventory_id', 'id');
    }

    public static function recordToLog($model, $activity, $info)
    {
    	$log = new InventoryLog([
    		'inventory_id' => $model->id,
			'activity' => $activity,
			'info' => $info,
			'performer_id' => Auth::user()->id,
    	]);

        return $log->save();
    }
}
