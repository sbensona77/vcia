<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

class InventoryCategory extends Model
{
    protected $table = 'inventory_categories';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'name'
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});

    	self::created(function($model) {
    		
    	});

    	self::updating(function($model) {
    		
    	});

    	self::updated(function($model) {
    		
    	});

    	self::deleting(function($model) {
    		
    	});

    	self::deleted(function($model) {
    		
    	});
    }

    public function inventories()
    {
        return $this->hasMany('App\Inventory', 'category_id', 'id');
    }
}