<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use Auth;
use Carbon\Carbon;

use App\Inventory;
use App\InventoryLog;

class InventoryItem extends Model
{
    protected $table = 'inventories_items';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'code',
        'inventory_id',

        'status',
        'executed_by',
        'borrower'
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;

            $model->executed_by = Auth::user()->id;
    	});

    	self::updating(function($model) {
            if($model->isDirty('status')) {
                if($model->status == 'borrowed') 
                {
                    $activity = 'Purchasing';
                    $info = Auth::user()->name.' has purchasing '.$model->inventory->name.' - '.$model->code.' for '.$model->current_borrower->name;
                }
                elseif($model->status == 'returned') 
                {
                    $model->status = 'available';

                    $activity = 'Returning';
                    $info = Auth::user()->name.' has returned '.$model->inventory->name.' - '.$model->code;
                }

                self::createLog($model, $activity, $info);
            }

            $model->executed_by = Auth::user()->id;
    	});

        self::updated(function($model) {
            self::refreshAmount($model);
        });

    	self::deleting(function($model) {
            // create log
            $activity = 'Destorying';
            $info = Auth::user()->name.' has destoryed '.$model->inventory->name.' - '.$model->code;

            self::createLog($model, $activity, $info);
    	});

        self::deleted(function($model) {
            self::refreshAmount($model);
        });
    }

    public function inventory()
    {
        return $this->belongsTo('App\Inventory', 'inventory_id', 'id');
    }

    public function last_executor()
    {
        return $this->hasOne('App\User', 'id', 'executed_by');
    }

    public function current_borrower()
    {
        $borrower = $this->hasOne('App\User', 'id', 'borrower');

        if($borrower) 
            return $borrower;
        else 
            return null;
    }

    public function amount_borrowed()
    {
        $amount = self::all()
        ->where('inventory_id', self::inventory_id)
        ->where('status', 'borrowed')
        ->count();

        return $amount;
    }

    public static function getAmountOfAvailable($inventory_id)
    {
        return self::all()
        ->where('inventory_id', $inventory_id)
        ->where('status', 'available')
        ->count();
    }

    public static function refreshAmount($model)
    {
        $inventory = $model->inventory;
        
        $inventory->amount = self::getAmountOfAvailable($inventory->id);

        return ($inventory->save());
    }

    public static function createLog($model, $activity, $info)
    {
        $log = new InventoryLog([
            'inventory_id' => $model->inventory_id,
            'activity' => $activity,
            'info' => $info,
            'performer_id' => Auth::user()->id,
        ]);

        return $log->save();
    }
}