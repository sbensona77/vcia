<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

use App\Inventory;

class InventoryLog extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string;
        });
    }

    protected $table = 'inventories_logs';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'inventory_id',
		'activity',
		'info',
		'performer_id',
    ];
    protected $hidden = [
        
    ];

    public function inventory(){
    	$this->belongsTo('App\Inventory', 'id', 'inventory_id');
    }
}
