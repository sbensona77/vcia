<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

use File;
use App\CoursesMaterial;
use App\Course;

class MaterialsUpload extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string;
        });
    }

    protected $table = 'materials_uploads';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'material_id',
		'uploaded_file',
    ];
    protected $hidden = [
        
    ];

    public function material()
    {
        return $this->belongsTo('App\CoursesMaterial', 'id', 'material_id');
    }
}
