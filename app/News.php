<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class News extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->id = Uuid::generate()->string;
            $model->slug = self::generateSlug($model->title);
        });

        self::updating(function ($model) {
            if($model->isDirty('title'))
                $model->slug = self::generateSlug($model->title);
        });
    }

    protected $table = 'news';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'title', 
        'picture', 
        'content', 
        'date',
        'slug',

        'category',
    ];
    protected $hidden = [
        
    ];

    public static function generateSlug($title)
    {
        $titleLowerCase = strtolower($title);
        $replacedSpaces = str_replace(' ', '-', $titleLowerCase);

        return $replacedSpaces;
    }
}
