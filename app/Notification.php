<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Notification extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->id = Uuid::generate()->string;
        });
    }

    protected $table = 'notifications';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
		'announcer',
		'detail',
		'reciever',
		'read_status',
    ];
    protected $hidden = [
        
    ];
}
