<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\User;

class NewCourse extends Notification
{
    use Queueable;

    public $announcer;
    public $detail;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $message)
    {
        $this->announcer = $user;
        $this->detail = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('There is new course!')
                    ->action('Notification Action', url('/dashboard/courses'))
                    ->line('Please check it out');
    }

    public function toDatabase($notifiable)
    {
        return [
            'type' => 'Announcement',
            'notifiable' => $notifiable,
            'data' => $this->detail,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'Announcement',
            'notifiable' => $notifiable,
            'data' => $this->detail,
        ];
    }
}
