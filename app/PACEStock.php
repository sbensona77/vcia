<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use App\InventoryItem;

class PACEStock extends Model
{
    protected $table = 'paces_stocks';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'pace_id',
        'item_id',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});

    	self::updating(function($model) {
    		
    	});

    	self::updated(function($model) {
    		
    	});

    	self::deleting(function($model) {
    		
    	});
    }

    public function pace()
    {
        return $this->hasOne('App\CoursesMaterial', 'id', 'pace_id');
    }

    public function item()
    {
        return $this->hasOne('App\InventoryItem', 'id', 'item_id');
    }

    public function inventory()
    {
        $item = $this->item;
        $inventory = Inventory::find($item->inventory_id);

        return $inventory;
    }
}