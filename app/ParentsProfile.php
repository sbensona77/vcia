<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;
use Webpatser\Uuid\Uuid;

use App\User;

class ParentsProfile extends Model
{
    protected $table = 'parents_profile';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string; //generate UUID
        });
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'user_id',
		'profile_picture',
		'name',
		'email',
		'phone',
    ];
    protected $hidden = [
        
    ];

    public function user()
    {
    	$this->hasOne('App\User', 'id', 'user_id');
    }

    public function relations()
    {
    	return $this->hasMany('App\ParentsRelation', 'parent_id', 'user_id');
    }
}
