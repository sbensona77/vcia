<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;
use Webpatser\Uuid\Uuid;

class ParentsRelation extends Model
{
    protected $table = 'parents_relations';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string; //generate UUID
        });
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'parent_id',
		'student_id',
		'relation',
    ];
    protected $hidden = [
        
    ];

    public function parent()
    {
        return $this->hasOne('App\ParentsProfile', 'parent_id', 'user_id');
    }

    public function child()
    {
        return $this->hasOne('App\StudentsProfile', 'user_id', 'student_id');
    }
}
