<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use App\User;

class Payment extends Model
{
    protected $table = 'payments';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'student_id',

        'title',
        'description',
        'amount',
        'status',

        'due_date',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});

    	self::created(function($model) {
    		
    	});

    	self::updating(function($model) {
    		
    	});

    	self::updated(function($model) {
    		
    	});

    	self::deleting(function($model) {
    		
    	});

    	self::deleted(function($model) {
    		
    	});
    }

    public function student()
    {
        return $this->hasOne('App\User', 'id', 'student_id');
    }

    public function relation()
    {
        return $this->hasMany('App\ParentsRelation', 'student_id', 'student_id');
    }

    public function installments()
    {
        return $this->hasMany('App\PaymentsInstallments', 'payment_id', 'id');
    }

    public static function broadcast($model)
    {
        $students = User::where('role', 'student')->get();

        foreach ($students as $key => $student) {
            $payment = new Payment([
                'student_id' => $student->id,
                'title' => $model->title,
                'description' => $model->description,
                'amount' => (double) $model->amount,
                'due_date' => $model->due_date,
            ]);

            $payment->save();
        }
    }
}