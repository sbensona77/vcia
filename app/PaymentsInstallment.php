<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

class PaymentsInstallment extends Model
{
    protected $table = 'payments_installments';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'payment_id',
        'amount',
        'status',
        'due_date',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});

        self::updated(function($model) {
            if($model->isDirty('status')) {
                if(self::isAllPaymentPaid($model->payment_id))
                    self::updatePaymentStatus($model->payment_id);
            }
        });
    }

    public function payment()
    {
        return $this->hasOne('App\Payment');
    }

    public static function isAllPaymentPaid($payment_id)
    {
        $payment = Payment::findOrFail($payment_id);
        $installments = $payment->installments;

        foreach ($installments->sortBy('due_date') as $key => $installment) 
        {
            if($installment->status != 'paid') 
            {
                break;

                return false;
            }
        }

        return true;
    }

    public static function updatePaymentStatus($payment_id)
    {
        $payment = Payment::findOrFail($payment_id);

        $payment->status = 'paid'; // updating status

        return $payment->save();
    }
}