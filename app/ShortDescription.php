<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortDescription extends Model
{
    protected $table = 'short_descriptions';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'page_name',
		'title',
		'detail',
    ];
    protected $hidden = [
        
    ];
}
