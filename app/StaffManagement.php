<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

class StaffManagement extends Model
{
    protected $table = 'staffs_managements';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'title',
        'description',
        'staff_id',
        'status',
        'deadline'
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});
    }

    public function staff()
    {
        return $this->belongsTo('App\User', 'staff_id', 'id');
    }
}