<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;
use Webpatser\Uuid\Uuid;

use App\User;

class StaffsProfile extends Model
{
    protected $table = 'staffs_profile';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model) {
            $model->id = Uuid::generate()->string; //generate UUID
        });
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'user_id',

        'profile_picture',

        'nip',
        'name',
		'email',

        'position',
        'subject',

		'birth_date',
		'join_date',
		
		'phone',
    ];
    protected $hidden = [
        
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subject_name()
    {
        return $this->hasOne('App\Course', 'id', 'subject');
    }
}
