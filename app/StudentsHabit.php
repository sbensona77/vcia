<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

class StudentsHabit extends Model
{
    protected $table = 'students_habits';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'student_id',
        'habit_id',
        'grade',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});
    }
    
    public function student()
    {
        return $this->belongsTo('App\User', 'student_id', 'id');
    }

    public function habit()
    {
        return $this->hasOne('App\WorkHabit', 'id', 'habit_id');
    }
}