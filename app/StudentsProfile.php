<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;
use Webpatser\Uuid\Uuid;

use App\User;
use App\ParentsRelation;

use App\AssignedCourses;

use App\EarnedPoints;

use App\WeeklyReport;

class StudentsProfile extends Model
{
    protected $table = 'students_profile';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string; //generate UUID
            $model->join_date = \Carbon\Carbon::now();
        });

        self::updating(function($model){
            if($model->isDirty('name') || $model->isDirty('email')) 
                self::update_user($model->user_id);

            return self::isAuthorized(Auth::user()->id) ? true : false;
        });
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'user_id',

        'profile_picture',

        'nis',
        'name',
		'email',

        'level',

		'birth_date',

		'phone',
    ];
    protected $hidden = [
        
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function courses()
    {
        return $this->hasMany('App\AssignedCourses', 'student_id', 'user_id');
    }

    public function materials()
    {
        return $this->hasMany('App\AssignedMaterials', 'student_id', 'user_id');
    }

    public function points()
    {
        return $this->hasMany('App\EarnedPoints', 'student_id', 'user_id');
    }

    public function weekly_reports()
    {
        return $this->hasMany('App\WeeklyReport', 'student_id', 'user_id');
    }

    public function relations()
    {
        return $this->hasMany('App\ParentsRelation', 'student_id', 'user_id');
    }

    private static function isAuthorized($user_id)
    {
        return Auth::user()->role == 'admin' || $user_id == Auth::user()->id;
    }

    private static function update_user($user_id)
    {
        try {
            $user = User::findOrFail($user_id);

            $user->update([
                'name' => $model->name,
                'email' => $model->email,
            ]);

            $user->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
