<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Webpatser\Uuid\Uuid;

use Carbon\Carbon;

use App\StaffsProfile;
use App\ParentsProfile;
use App\StudentsProfile;

class User extends Authenticatable
{
    use Notifiable;

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string; //generate UUID
        });

        self::created(function($model){
            User::create_profile($model);
        });

        self::updating(function($model){
            if($model->isDirty('role'))
            {
                $user = User::findOrFail($model->id);

                User::delete_profile($model);
                User::create_profile($model);
            }
            else if($model->isDirty('name') || $model->isDirty('email'))
            {
                User::update_profile($model);
            }
        });

        self::deleting(function($model){
            User::delete_profile($model);
        });
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',

        'name',
        'email',

        'gender',
        
        'password',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile(){
        if($this->role == 'student') 
            return $this->hasOne('App\StudentsProfile');
        elseif($this->role == 'parent') 
            return $this->hasOne('App\ParentsProfile');
        else 
            return $this->hasOne('App\StaffsProfile');
    }

    public function clubs()
    {
        return $this->hasMany('App\AssignedClub', 'student_id', 'id');
    }

    public function bible_memories()
    {
        return $this->hasMany('App\BibleMemory', 'student_id', 'id');
    }

    public function work_habits()
    {
        return $this->hasMany('App\StudentsHabit', 'student_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment', 'id', 'student_id');
    }

    private static function create_profile($model){
        try {
            if($model->role == 'admin' || $model->role == 'teacher'){
                $profile = new StaffsProfile([
                    'user_id' => $model->id,
                    'name' => $model->name,
                    'email' => $model->email,
                    'join_date' => Carbon::now(),
                ]);
            }else if($model->role == 'student'){ //student
                $profile = new StudentsProfile([
                    'user_id' => $model->id,
                    'name' => $model->name,
                    'email' => $model->email,
                    'join_date' => Carbon::now(),
                ]);
            }else{
                $profile = new ParentsProfile([
                    'user_id' => $model->id,
                    'name' => $model->name,
                    'email' => $model->email,
                ]);
            }

            $profile->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private static function update_profile($model){
        try {
            if($model->role == 'admin' || $model->role == 'teacher')
            {
                $profile = StaffsProfile::findOrFail($model->profile->id);

                $profile->update([
                    'name' => $model->name,
                    'email' => $model->email,
                ]);
            }
            elseif($model->role == 'student')
            {
                $profile = StudentsProfile::findOrFail($model->profile->id);

                $profile->update([
                    'name' => $model->name,
                    'email' => $model->email,
                ]);
            }
            else
            {
                $profile = ParentsProfile::findOrFail($model->profile->id);

                $profile->update([
                    'name' => $model->name,
                    'email' => $model->email,
                ]);
            }

            $profile->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function delete_profile($model){
        try {
            if($model->role == 'admin' || $model->role == 'teacher') $profile = StaffsProfile::all()->where('user_id', $model->id)->first();
            elseif($model->role == 'student') $profile = StudentsProfile::all()->where('user_id', $model->id)->first();
            else $profile = ParentsProfile::all()->where('user_id', $model->id)->first();

            $profile->delete();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
