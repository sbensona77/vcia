<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use App\EarnedPoints;

class VictoryPoints extends Model
{
    protected $table = 'victory_points';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->id = Uuid::generate()->string; //generate UUID
        });
    }

    public $incrementing = false;

    protected $fillable = [
        'activity_name',
		'points',
    ];
    protected $hidden = [
        
    ];

    public function earns(){
        return $this->belongsToMany('App\EarnedPoints', 'point_id', 'id');
    }
}
