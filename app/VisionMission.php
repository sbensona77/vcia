<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisionMission extends Model
{
    protected $table = 'visions_missions';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'type',
        'content',
    ];
    protected $hidden = [
        
    ];
}
