<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

use App\StudentsProfile;
use App\AssignedCourses;

class WeeklyReport extends Model
{
    protected $table = 'weekly_report';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected static function boot() {
    	parent::boot();

    	self::creating(function($model) {
    		$model->id = Uuid::generate()->string;

    		//creating start week and end week
    		$now = Carbon::now();
    		$model->start_week = $now->copy()->startOfWeek();
    		$model->end_week = $now->copy()->endOfWeek();
    	});

        self::updating(function($model) {
            if($model->isDirty('confirmed')){
                $model->confirmer = Auth::check() ? Auth::user()->id : null;
                $model->confirmed_at = Carbon::now();
            }
        });
    }

    protected $fillable = [
    	'student_id',
    	'course_id',
    	'material_id',

		'info',
		'grade',

        'confirmer',
        'confirmed_at',
    ];
    protected $hidden = [
        
    ];

    public function student()
    {
    	return $this->belongsTo('App\StudentsProfile');
    }

    public function confirmedBy()
    {
        return $this->hasOne('App\User', 'id', 'confirmer');
    }

    public function course()
    {
    	return $this->hasOne('App\Course', 'id', 'course_id');
    }

    public function material()
    {
    	return $this->hasOne('App\CoursesMaterial', 'id', 'material_id');
    }

    protected static function destroyOldReport($model)
    {
    	$report = WeeklyReport::all()->where('student_id', $id)->where('start_week', '>=', $model->start_week)->where('end_week', '<=', $model->end_week);

    	if($report != []){
    		$report = WeeklyReport::where('student_id', $id)->where('start_week', '>=', $model->start_week)->where('end_week', '<=', $model->end_week)->delete();
    	}

    	return $report;
    }
}
