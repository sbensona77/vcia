<?php

return [
    'is_production' => env('MIDTRANS_IS_PRODUCTION', false), //true when finished
    'client_key' => env('MIDTRANS_CLIENT_KEY'),
    'server_key' => env('MIDTRANS_SERVER_KEY')
];