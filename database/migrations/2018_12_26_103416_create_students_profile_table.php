<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_profile', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('user_id')->default('');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('profile_picture')->default('img/logo.png');
            
            $table->string('name');
            $table->string('email')->unique();
            $table->string('nis')->nullable()->unique();
            
            $table->integer('level')->default(1);

            $table->date('birth_date')->nullable();
            $table->date('join_date');

            $table->string('phone')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('students_profile');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
