<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs_profile', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('user_id')->default('');
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('profile_picture')->default('img/logo.png');

            $table->string('name');
            $table->string('nip')->nullable()->unique();
            $table->string('email')->unique();

            $table->string('position')->nullable();
            $table->string('subject')->nullable();

            $table->date('birth_date')->nullable();
            $table->date('join_date');

            $table->string('phone')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs_profile');
    }
}
