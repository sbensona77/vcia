<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialsUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials_uploads', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->string('material_id');
            $table->string('uploaded_file');

            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('courses_materials')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials_uploads');
    }
}
