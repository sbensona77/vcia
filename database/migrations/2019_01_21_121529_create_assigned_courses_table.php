<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignedCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_courses', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('student_id');
            $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade');

            $table->uuid('assigner_id');
            $table->foreign('assigner_id')->references('id')->on('users')->onDelete('cascade');

            $table->uuid('course_id');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');

            $table->uuid('pace_start');
            $table->foreign('pace_start')->references('id')->on('courses_materials')->onDelete('cascade');
            $table->uuid('pace_end');
            $table->foreign('pace_end')->references('id')->on('courses_materials')->onDelete('cascade');

            $table->integer('total_pace')->default(0);
            $table->integer('average')->nullable();

            $table->string('status')->default('active');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_courses');
    }
}
