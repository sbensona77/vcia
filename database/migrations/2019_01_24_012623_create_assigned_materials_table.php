<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignedMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_materials', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('assigned_course_id');
            $table->foreign('assigned_course_id')->references('id')->on('assigned_courses')->onDelete('cascade');

            $table->uuid('student_id');
            $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade');

            $table->uuid('material_id');
            $table->foreign('material_id')->references('id')->on('courses_materials')->onDelete('cascade');

            $table->double('grade', 5, 2)->nullable();

            $table->string('status')->default('active');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_materials');
    }
}
