<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeklyReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekly_report', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            //student
            $table->uuid('student_id');
            $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade');

            //assigned course
            $table->uuid('course_id')->nullable();
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');

            //PACE code
            $table->uuid('material_id');
            $table->foreign('material_id')->references('id')->on('courses_materials')->onDelete('cascade');

            $table->double('grade', 5, 2)->nullable();

            $table->string('info')->nullable();

            $table->datetime('start_week');
            $table->datetime('end_week');

            $table->tinyInteger('confirmed')->default(0);
            $table->uuid('confirmer')->nullable();
            $table->foreign('confirmer')->references('id')->on('users')->onDelete('cascade');
            $table->datetime('confirmed_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weekly_report');
    }
}
