<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents_profile', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('user_id')->default('');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('profile_picture')->default('img/logo.png');
            
            $table->string('name');
            $table->string('email')->unique();

            $table->string('phone')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('parents_profile');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
