<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->string('inventory_name');
            $table->integer('amount');

            $table->uuid('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('inventory_categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('inventories');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
