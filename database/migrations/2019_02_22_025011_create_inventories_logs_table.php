<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories_logs', function (Blueprint $table) {
             $table->uuid('id');
            $table->primary('id');

            $table->uuid('inventory_id')->nullable();
            $table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade');

            $table->string('activity');
            $table->string('info');

            $table->uuid('performer_id')->nullable();
            $table->foreign('performer_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('inventories_logs');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
