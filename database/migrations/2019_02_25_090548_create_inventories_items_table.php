<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->string('code');

            $table->uuid('inventory_id')->nullable();
            $table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade');

            $table->string('status')->default('available');

            $table->uuid('executed_by')->nullable();
            $table->foreign('executed_by')->references('id')->on('users')->onDelete('cascade');

            $table->uuid('borrower')->nullable();
            $table->foreign('borrower')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories_items');
    }
}
