<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacesStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // relation between PACE and InventoryItem
        Schema::create('paces_stocks', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('pace_id')->unique();
            $table->foreign('pace_id')->references('id')->on('courses_materials')->onDelete('cascade');

            $table->uuid('inventory_id')->unique();
            $table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paces_stocks');
    }
}
