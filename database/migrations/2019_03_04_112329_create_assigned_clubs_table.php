<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignedClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_clubs', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('student_id');
            $table->foreign('student_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

            $table->uuid('club_id');
            $table->foreign('club_id')
            ->references('id')->
            on('clubs')->
            onDelete('cascade');

            $table->integer('grade')->nullable();

            $table->string('status')->default('inactive');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_clubs');
    }
}
