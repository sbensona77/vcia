<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicYearsTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_years_terms', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('academic_year_id');
            $table->foreign('academic_year_id')
            ->references('id')
            ->on('academic_years')
            ->onDelete('cascade');

            $table->date('term_start')->unique();
            $table->date('term_end')->unique();

            $table->tinyInteger('status')->default(0); // not active
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_years_terms');
    }
}
