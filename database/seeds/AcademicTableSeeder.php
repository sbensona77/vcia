<?php

use Illuminate\Database\Seeder;

class AcademicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academics')->insert([
            'name' => 'ABC',
            'detail' => 'This class prepares to enter P1. This class ranges from toddlers (4 years) to pre-child (5 years). This classes focus on how the children learn and play happily. Provided with comfortable and colorful learning space, the children can develop themselves to be smart and active students.',
            'picture' => 'img/abc.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);

        DB::table('academics')->insert([
            'name' => 'YOUNG WARRIOR',
            'detail' => 'This class levels start from P1 to P3. This class classifies from pre-age grouping of children (6 years) to children (7 years). Children will be introduced to the external ecosystem and get to know the world even further. In this class we educate students to learn independently, and be responsible.',
            'picture' => 'img/young-warrior.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);

        DB::table('academics')->insert([
            'name' => 'WARRIOR',
            'detail' => 'Levels of classes P4 to P6. This class is filled with an age group of children (8-9 years). Learning and increasing knowledge is our priority in this level of classroom. Children will participate in creative and sharpening skills so that the spirit of interest in learning increases.',
            'picture' => 'img/warrior.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);

        DB::table('academics')->insert([
            'name' => 'YOUNG VICTORS',
            'detail' => 'Levels of classes from S1 to S3. This class classifies from age group of children (10 years), to pre-adolescents (11 years). Classes will teach students about discipline and how to add creativity in their projects.',
            'picture' => 'img/young-victors.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);

        DB::table('academics')->insert([
            'name' => 'VICTORS',
            'detail' => 'Level grade S4 to JC2. This class ranges from pre-adolescents (15 years), to adolescents (16 years). Students will learn with an advanced education system. The students will practice their expertise more in the outside world, so they can improve their skill for the real world.',
            'picture' => 'img/victors.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
    }
}
