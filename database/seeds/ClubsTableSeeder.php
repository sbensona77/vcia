<?php

use Illuminate\Database\Seeder;

use App\Club;

class ClubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $club = new Club([
            'name' => 'Choir Club',
            'motto' => 'Improve your music values',
            'detail' => 'The Choral Music program is designed to enhance the musical, creative and expressive qualities of all students. This activity has a mission to provide education about music to students. So, all of students can imagine how music is expressed',
            'picture' => 'img/choir-cover.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Photography Club',
            'motto' => 'Capture expressions with beautiful portraits',
            'detail' => 'The mission of the Photography Club is to provide a supportive environment for interested photography students to share their creativity, knowledge and passion for photography.',
            'picture' => 'img/photography-cover.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Robotics Club',
            'motto' => 'Skills in modern technology',
            'detail' => "Robotics clubs are a great way to share your enthusiasm about robotics with others in your area. Robotics club is a gathering place for robots enthusiastic. If you're a robots enthusiastic, you can join robotics club. So, you can realize your imagination.",
            'picture' => 'img/robotic-cover.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Science Club',
            'motto' => 'Create scientific work to gain knowledge',
            'detail' => 'This activity has a mission to develop scientific attitudes, also natural phenomena with high sensitivity is based on a method that is systematic, objective, rational, and procedural. So that these activities can provide competence in self-development in life.',
            'picture' => 'img/science-cover.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Fine Art Club',
            'motto' => 'Color the world with your imagination',
            'detail' => 'Art Club is open for any student who interested with visual arts! You can work on drawing or painting. You can create your own arts project. Art Club is a great place to meet different people! Make your imagination comes true.',
            'picture' => 'img/fineart-cover.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Futsal Club',
            'motto' => 'Create your team and get the win',
            'detail' => 'Futsal is a variant of association football played on a hard court, smaller than a football pitch, and mainly indoors. It can be considered a version of five-a-side football. Futsal is played between two teams of five players each, one of whom is the goalkeeper. Unlimited substitutions are permitted. So, create your team and get the win with Victory Academy.',
            'picture' => 'img/futsal-cover.jpeg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Basketball Club',
            'motto' => 'Take your wins with your basketball team',
            'detail' => 'Basketball is a team sport in which two teams, most commonly of five players each, opposing one another on a rectangular court, put the ball in the ring and get the win with Victory Academy.',
            'picture' => 'img/basketball-cover.jpeg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Advanced Art Club',
            'motto' => 'Improve your art skill',
            'detail' => 'Advanced Art Club give a chance to improve your art skill. Make your art imagination become real with our club.',
			'picture' => 'img/advancedart-cover.jpg',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'IT Club',
            'motto' => 'Hold the world with technology',
            'detail' => 'Information technology (IT) is the use of computers to store, retrieve, transmit, and manipulate data, or information, often in the context of a business or other enterprise. IT is considered to be a subset of information and communications technology. Our Club give more education about modern technology in the world.',
            'picture' => 'img/itsenior-cover.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Public Speaking Club',
            'motto' => 'Let them hear your aspirations',
            'detail' => 'Public speaking is the process or act of performing a good speech to a live audience. This type of speech is deliberately structured with three general purposes: to inform, to persuade and to entertain. Public speaking is seen traditionally as part of the art of persuasion. Public speaking is commonly understood as formal, face-to-face speaking of a single person to a group of listeners.',
            'picture' => 'img/publicspeaking-cover.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Music Ministry Club',
            'motto' => 'Make some beautiful music melodies',
            'detail' => 'A music ministry club, also known as a music group or musical group, is a group of people who perform instrumental or vocal music, with the ensemble typically known by a distinct name. Some music ensembles consist solely of instruments, such as the jazz quartet or the orchestra. Some music ensembles consist solely of singers, such as choirs and doo wop groups. Make some beautiful music melodies with Music Ministry Club.',
            'picture' => 'img/musicministry-cover.jpeg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();

        $club = new Club([
            'name' => 'Sports Club',
            'motto' => 'Keep your body healthy',
            'detail' => 'Our sports club always trying to provide health for our students. S0, keep your body healthy with Victory Academy Sports Club',
            'picture' => 'img/sports-cover.jpg',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $club->save();
    }
}
