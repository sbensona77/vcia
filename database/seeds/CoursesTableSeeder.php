<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Course;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher = User::where('role', 'teacher')->inRandomOrder()->first();
        $math = new Course([
            'name' => 'Mathematics',
            'teacher_id' => $teacher->id,
            'description' => 'Mathematics (from Greek μάθημα máthēma, "knowledge, study, learning") includes the study of such topics as quantity, structure, space, and change. Mathematicians seek and use patterns to formulate new conjectures; they resolve the truth or falsity of conjectures by mathematical proof.',
        ]);

        $teacher = User::where('role', 'teacher')->inRandomOrder()->first();
        $english = new Course([
            'name' => 'English',
            'teacher_id' => $teacher->id,
            'description' => 'Language Studies is an interdisciplinary major offered by the Linguistics Department. It is designed to equip students with competence in one foreign language and, at the same time, provide an understanding of the general nature of human language, its structure and use in particular.',
        ]);

        $teacher = User::where('role', 'teacher')->inRandomOrder()->first();
        $social = new Course([
            'name' => 'Social Studies',
            'teacher_id' => $teacher->id,
            'description' => 'Social studies is the integrated study of multiple fields of social science and the humanities, including history, geography, and political science.',
        ]);

        $teacher = User::where('role', 'teacher')->inRandomOrder()->first();
        $science = new Course([
            'name' => 'Science',
            'teacher_id' => $teacher->id,
            'description' => 'Science is a systematic enterprise that builds and organizes knowledge in the form of testable explanations and predictions about the universe. The earliest roots of science can be traced to Ancient Egypt and Mesopotamia in around 3500 to 3000 BCE.',
        ]);

        $teacher = User::where('role', 'teacher')->inRandomOrder()->first();
        $bible = new Course([
            'name' => 'Bible Reading',
            'teacher_id' => $teacher->id,
            'description' => 'Learning about the truth and the words of God',
        ]);

        $math->saveOrFail();
        $english->saveOrFail();
        $social->saveOrFail();
        $science->saveOrFail();
        $bible->saveOrFail();
    }
}
