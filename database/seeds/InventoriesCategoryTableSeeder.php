<?php

use Illuminate\Database\Seeder;

use App\Course;

use App\Inventory;
use App\InventoryCategory;

class InventoriesCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$inventory = new InventoryCategory([
            'name' => 'Furniture'
        ]);

        $inventory->save();
    }
}
