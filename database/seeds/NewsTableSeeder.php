<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

use App\News;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $news = new News([
            'title' => 'Life Skill Cooking Class Gourmet House',
            'content' => '<b>Victory Academy</b> make an event to give knowledge about cooking skills for students. This event is held for all of students. so they can learn about cooking.',
            'picture' => 'img/slider-3.jpg',
            'date' => new DateTime,

            'category' => 'news', //hidden

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $news->save();

        $news = new News([
            'title' => 'Graduation',
            'content' => '<b>Victory Academy</b> make an event to give knowledge about cooking skills for students. This event is held for all of students. so they can learn about cooking.',
            'picture' => 'img/slider-1.JPG',
            'date' => new DateTime,

            'category' => 'achievement',

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $news->save();
    }
}
