<?php

use Illuminate\Database\Seeder;

class ShortDescriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('short_descriptions')->insert([
            'page_name' => 'profile',
            'title' => 'Victory Academy uses the International Accelerated Christian Education (ACE) Curriculum.',
            'detail' => 'Our student is given a strong foundation from the Scriptures, Bible principles, wisdom, and Christian character development. Our students is guaranteed to have high academic standards through strong mastery of each subject. Our students diploma (ICCE *) will be recognized globally.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);

        DB::table('short_descriptions')->insert([
            'page_name' => 'clubs',
            'title' => 'Come and Join Our Clubs !',
            'detail' => 'Victory Academy has provide school clubs that will educates and develops students potential. So, all of students have more potential to develop their skills.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
    }
}
