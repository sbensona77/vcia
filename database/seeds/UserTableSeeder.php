<?php

use Illuminate\Database\Seeder;

use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

use App\User;
use App\StudentsProfile;

use App\ParentsRelation;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'role' => 'admin', //admin
            'name' => 'Super User of Victory',
            'email' => 'admin@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();

        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'First Teacher Of Victory',
            'email' => 'teacher@victory.sch.id',
            'gender' => 'male',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();

        //teachers
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Nany Widjaja, B.Sc',
            'email' => 'nany@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Eng Yennylawati, S.Kom, M.Kom',
            'email' => 'yennylawati@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Yunita S. Handayani, S.Th',
            'email' => 'yunita@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Angga Setio Putro, S.Si',
            'email' => 'angga@victory.sch.id',
            'gender' => 'male',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Irenne Kusminarso, A.Md',
            'email' => 'irenne@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Chialis, S.S',
            'email' => 'chialis@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Yulius Darmawan, S.T',
            'email' => 'yulius@victory.sch.id',
            'gender' => 'male',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Elsa Paramita, S.S',
            'email' => 'elsa@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Ditya Wijayanti, A.Md',
            'email' => 'ditya@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $user = new User([
            'role' => 'teacher', //teacher
            'name' => 'Lusiana Prasetyo, S.E Akt',
            'email' => 'lusiana@victory.sch.id',
            'gender' => 'female',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        //teachers

        $user = new User([
            'role' => 'student', //student
            'name' => 'First Student of Victory',
            'email' => 'student@victory.sch.id',
            'gender' => 'male',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $student_id = $user->id;

        $user = new User([
            'role' => 'parent', //student
            'name' => 'First Parent of Victory',
            'email' => 'parent@victory.sch.id',
            'gender' => 'male',
            'password' => bcrypt('qwerty'),

            'status' => 1,

            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user->save();
        $parent_id = $user->id;

        $relation = new ParentsRelation([
            'parent_id' => $parent_id,
            'student_id' => $student_id,
            'relation' => 'Father',
        ]);

        $relation->save();
    }
}
