<?php

use Illuminate\Database\Seeder;

class VisionMissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visionmission')->insert([
            'type' => 'vision',

            'content' => 'Lorem ipsum'
        ]);

        DB::table('visionmission')->insert([
            'type' => 'mission',

            'content' => 'Dolor sit'
        ]);

        DB::table('visionmission')->insert([
            'type' => 'vision',

            'content' => 'Amet Si Vis'
        ]);

        DB::table('visionmission')->insert([
            'type' => 'vision',

            'content' => 'Las Vegas Open Virginia'
        ]);
    }
}
