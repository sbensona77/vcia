<?php

use Illuminate\Database\Seeder;

use App\WorkHabit;

class WorkHabitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $habit = new WorkHabit([
        	'type' => 'Work Habits',
        	'habit' => 'Follows directions'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Work Habits',
        	'habit' => 'Works well independently'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Work Habits',
        	'habit' => 'Does not disturb others'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Work Habits',
        	'habit' => 'Takes care of materials'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Work Habits',
        	'habit' => 'Completes work required'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Work Habits',
        	'habit' => 'Achieves computer assignments'
        ]);
        $habit->save();


		
        $habit = new WorkHabit([
        	'type' => 'Social Traits',
        	'habit' => 'Is courteous'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Social Traits',
        	'habit' => 'Gets along well with others'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Social Traits',
        	'habit' => 'Exhibits self-control'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Social Traits',
        	'habit' => 'Shows respect for authority'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Social Traits',
        	'habit' => 'Responds well to correction'
        ]);
        $habit->save();

        $habit = new WorkHabit([
        	'type' => 'Social Traits',
        	'habit' => 'Promotes school spirit'
        ]);
        $habit->save();

        
        $habit = new WorkHabit([
        	'type' => 'Personal Traits',
        	'habit' => 'Ability to establish own goals'
        ]);
        $habit->save();
        $habit = new WorkHabit([
        	'type' => 'Personal Traits',
        	'habit' => 'Successfully reaches goals'
        ]);
        $habit->save();
        $habit = new WorkHabit([
        	'type' => 'Personal Traits',
        	'habit' => 'Displays flexibility'
        ]);
        $habit->save();
        $habit = new WorkHabit([
        	'type' => 'Personal Traits',
        	'habit' => 'Creativity'
        ]);
        $habit->save();
        $habit = new WorkHabit([
        	'type' => 'Personal Traits',
        	'habit' => 'General overall progress'
        ]);
        $habit->save();
        $habit = new WorkHabit([
        	'type' => 'Personal Traits',
        	'habit' => 'Attitude toward computer learning'
        ]);
        $habit->save();
    }
}
