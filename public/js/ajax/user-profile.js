/*function checkPaceStock(material_id)
{
    var isAvailable = false;

    $.ajax({
        type: 'GET',
        url: 'materials/is_available/' + material_id,
        cache: false,
        dataType: 'JSON',
        async: false,
        success: function(data){
            // console.log('checking done!');

            var result = $.parseJSON(JSON.stringify(data));

            // console.log(result.available);
            if(result.available == "true") {
                isAvailable = true;
            }
        }
    });

    return isAvailable;
}*/

function populatePaceSelect()
{
    var course_id = $("#select_course option:selected").val();
    var course_name = $("#select_course option:selected").text();

    //clear all option
    $("#option_pace_start optgroup").hide();
    $("#option_pace_end optgroup").hide();

    console.log('Selecting...');

    $.ajax({
        type: 'GET',
        url: "ajax/" + course_id + "/all_materials",
        cache: false,
        dataType: 'JSON',
        success: function(data) {
            console.log('Populating...');

            $("#select_pace_start").empty();
            $("#select_pace_end").empty();

            var option = "<option></option>";

            $("#select_pace_start").append(option);
            $("#select_pace_end").append(option);

            var results = $.parseJSON(JSON.stringify(data));
            //console.log(result.materials);

            $.each(JSON.parse(results.materials), function(index, value) {
                var val = value.id;
                var name = value.name;

                var option = "<option " + "value=" + "'" + val + "'" + ">";
                /*if(!checkPaceStock(val))
                    option += '[ No Stock! ] ';*/
                option += course_name + " | " +  name + "</option>";

                $("#select_pace_start").append(option);
                $("#select_pace_end").append(option);
            });
        }
    });
}

function getPoint(base_url)
{
    $("#point-amount").html('');

    var pointId = $("#option_point option:selected").val();
    var result = '';

    $.ajax({
        type: 'GET',
        url: base_url + '/' + pointId,
        cache: false,
        dataType: 'JSON',
        success: function(data){
            console.log('Populating...');

            result = JSON.parse(data.point);
            point = result.points.toString();

            $("#point-amount").removeAttr('disabled');
            $("#point-amount").val(point);
            $("#inserted-grade{{ $data->id }}").prop('disabled', true);
        }
    });
}