@extends('layouts.app')

@section('content')
<div class="section-academics pt-6 pb-0 py-lg-5">
    <div class="container">
        <div class="row">
            <!-- ACADEMICS MENU -->
            <div class="col-12 col-lg-9">
                <div class="row pt-5 pb-8">
                    <div class="col-12">
                        <h2 class="title-c text-black mt-0 mb-5">ACADEMICS</h2>

                        <div class="row">
                            @foreach($academics as $key => $academic)
                            <div class="col-12 col-lg-10"> 
                                <img class="card-img-top" src="{{ asset($academic->picture) }}" width="100%" alt="news">
                            </div>

                            <div class="col-12 col-lg-10 mt-4 mb-6">
                                <h3 class="title-c mt-0 mb-3">{{ $academic->name }}</h3>

                                <p class="p-a">{{ $academic->detail }}</p>
                                <a href="mailto:victory.cia.semarang@gmail.com" target="_blank" class="btn btn-victory-b btn-round">Contact us</a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- ACADEMICS MENU -->

            <!-- RIGHT BAR NEWS -->
            <div class="col-12 col-lg-3 border-a">
                <div class="row pt-5 pb-0">
                    <div class="col-12">
                        <a href="login" target="_blank" class="btn btn-victory-b btn-round">Academic Access</a>

                        <a href="mailto:victory.cia.semarang@gmail.com" target="_blank" class="btn btn-victory-b btn-round">Send a Message</a>
                    </div>

                    <div class="col-12">
                        <h2 class="title my-4">Latest News</h2>
                    </div>

                    @foreach($latest_news as $key => $news)
                    <div class="col-12 col-md-4 col-lg-12">
                        <a href="{{ route('welcome.news.slug', $news->slug) }}">
                            <img class="card-img-top" src="{{ asset($news->picture) }}" alt="news" class="mt-5">

                            <h4 class="title mt-3 mb-1">
                                {{ $news->title }}
                            </h4>
                        </a>

                        <p class="p-a">{!! substr($news->content, 0, 100) !!} ...<br>
                            <a href="{{ route('welcome.news.slug', $news->slug) }}" class="link-b">
                                <span>View More</span>
                            </a>
                        </p>
                        <h5 class="title mt-0">{{ $news->date }}</h5>
                    </div>
                    @endforeach

                    <div class="col-12 mt-5 mt-lg-3">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fvictoryschool.semarang%2F&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" scrolling="no" allowTransparency="true" allow="encrypted-media" class="widget"></iframe>
                        
                        <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/en_GB/   sdk.js#xfbml=1&version=v3.2';
                            fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));
                        </script>
                    </div>
                </div>
            </div>
            <!-- RIGHT BAR NEWS -->
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</div>
<!-- END SECTION -->
@endsection