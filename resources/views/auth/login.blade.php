@extends('layouts.app-login')
@section('content')
<!-- Page -->
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
        <div class="panel">
            <!-- Panel Body -->
            <div class="panel-body">
                <!-- Image Header -->
                <div class="brand">
                    <img class="brand-img" src="{{ asset('img/logo.png') }}" width="100px" alt="logo">
                    <h4 class="title">Victory Academy</h4>
                </div>
                <!-- End Image Header -->
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br />
                @endif
                <!-- Form -->
                <form method="post" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" />
                        {{-- IF ERROR --}}
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        {{-- IF ERROR --}}
                    </div>
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" />
                        {{-- IF ERROR --}}
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                        {{-- IF ERROR --}}
                    </div>
                    <div class="form-group clearfix">
                        <!-- Checkbox Remember Me -->
                        <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                            <input type="checkbox" id="inputCheckbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="inputCheckbox">Remember me</label>
                        </div>
                        <!-- End Checkbox Remember Me -->
                        <!-- Button Forgot Password -->
                        {{-- <a class="float-right" href="{{ route('password.request') }}">Forgot password?</a> --}}
                        <!-- End Button Forgot Password -->
                    </div>
                    <!-- Button Sign In -->
                    <button type="submit" class="btn btn-victory-b btn-block">Sign In</button>
                    <!-- End Button Sign In -->
                </form>
                <!-- End Form -->
            </div>
            <!-- End Panel Body -->
        </div>
    </div>
</div>
<!-- End Page -->
@endsection