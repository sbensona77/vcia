@extends('layouts.app')

@section('content')
<div class="section-club pt-6 pb-0 py-lg-5">
    <div class="container">
        <div class="row py-5">
            <div class="col-12">
                <h2 class="title-c mt-0 mb-5">OUR CLUBS</h2>
                <h4 class="title mb-0">{{ $short_description->title }}</h4>

                <p class="p-a max-width-a">{{ $short_description->detail }}</p>
                <div class="row mt-5 mb-8">
                    @foreach($clubs as $key => $club)
                    <div class="col-12 col-md-6 col-xl-4">
                        <h4 class="title mb-0">{{ $club->name }}</h4>

                        <div class="grid">
                            <figure class="effect-layla">
                                <a data-toggle="modal" data-target="#modal{{ $club->id }}">
                                    <img class="card-img-top" src="{{ asset('/file_uploads/clubs/'.$club->picture) }}" width="100%" alt="club">
                                    <figcaption>
                                        <p><img src="{{ asset('img/logo.png') }}" width="100%"></p>
                                    </figcaption>
                                </a>
                            </figure>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modal{{ $club->id }}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="title text-white">{{ $club->name }}</h3>
                                    </div>
                                    <div class="modal-body">
                                        <img class="card-img-top" src="{{ asset('/file_uploads/clubs/'.$club->picture) }}" width="100%" alt="club">
                                        <h5 class="card-title">{{ $club->motto }}</h5>
                                        <p class="p-a mt-3">{{ $club->detail }}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-victory-b btn-sm" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</div>
<!-- END SECTION -->
@endsection