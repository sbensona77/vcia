@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page bg-white">
    <!-- Contacts Content -->
    <div class="page-main">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }} <br>
                @endforeach
            </div>

            <br />
        @endif

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}  
            </div>

            <br/>
        @elseif(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>

            <br/>
        @endif
   
        <!-- Contacts Content -->
        @if(Auth::user()->role == 'admin')
            <!-- Create Button -->          
            <button class="btn btn-victory-c ml-10 ml-md-30 mb-30 mt-30" type="button" data-target="#CRUDacademic" data-toggle="modal">
                <i class="icon md-plus"></i>Create
            </button>
            <!-- End Create Button -->
        @endif
        
        <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
            <!-- Contacts List Table -->
            <table class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                <thead>
                    <tr>
                        <th class="pre-cell"></th>
                        <!-- Table Header -->
                        <th class="cell-100" scope="col">Year</th>

                        <th class="cell-100" scope="col">Semester Type</th>

                        <th class="cell-100" scope="col">Semester Start</th>

                        <th class="cell-100" scope="col">Semester End</th>
                        
                        <th class="cell-30" scope="col">Status</th>

                        <th class="cell-200" scope="col">Action</th>

                        <th class="suf-cell"></th>
                        <!-- End Table Header -->
                    </tr>
                </thead>
                
                <tbody>
                    <!-- Table List -->
                    @foreach($semesters as $key => $semester)
                        <tr>
                            <td class="pre-cell"></td>
                            <!-- Name -->
                            <td class="cell-100">
                                {{ $semester->year }} / {{ $semester->year + 1 }}
                            </td>
                            <!-- End Name -->

                            <!-- Level -->
                            <td class="cell-100">
                                @if($semester->semester_type == 'odd')
                                    <span class="badge badge-primary">ODD</span>
                                @else
                                    <span class="badge badge-success">EVEN</span>
                                @endif
                            </td>
                            <!-- End Level -->

                            <!-- Level -->
                            <td class="cell-100">
                                {{ Carbon\Carbon::parse($semester->semester_start)->format('M d, Y') }}
                            </td>
                            <!-- End Level -->

                            <!-- Level -->
                            <td class="cell-100">
                                {{ Carbon\Carbon::parse($semester->semester_end)->format('M d, Y') }}
                            </td>
                            <!-- End Level -->

                            <!-- Status -->
                            <td class="cell-30">
                                @if($semester->status == 1)
                                    <span class="badge badge-primary">Active</span>
                                @else
                                    <span class="badge badge-success">Done</span>
                                @endif
                            </td>
                            <!-- End Status -->

                            <!-- Action -->
                            <td class="cell-200">
                            	<a class="btn btn-victory-c" href="{{ route('academic-years.show', $semester->id) }}">
                                	View
                                </a>

                                <button class="btn btn-victory-c" data-target="#Editacademic{{ $semester->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">Edit</button>

                                @if($semester->status == 1)
                                    <form method="POST" action="{{ route('academic-years-turn-off', $semester->id) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PATCH">

                                        <button type="submit" class="btn btn-danger mt-10">Turn Off</button>
                                    </form>
                                @else
                                    <form method="POST" action="{{ route('academic-years-turn-on', $semester->id) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PATCH">

                                        <button type="submit" class="btn btn-success mt-10">Turn On</button>
                                    </form>
                                @endif
                            </td>
                            <!-- End Action -->

                            <td class="suf-cell"></td>
                        </tr>
                    @endforeach
                    <!-- End Table List -->
                </tbody>
            </table>
            <!-- End Contacts List Table -->

            <!-- Pagination List -->
            @if($semesters instanceof \Illuminate\Pagination\LengthAwarePaginator)
                {{ $semesters->links('pagination') }}
            @endif
            <!-- End Pagination List -->
        </div>
    </div>
    <!-- End Contacts Content -->

	<!-- Create Semester Modal Boxes -->
    <div class="modal fade modal-fill-in" id="CRUDacademic" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple" style="max-width: 100%">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                    <!-- Title -->
                    <h4 class="modal-title" id="exampleModalTabs">Create</h4>
                    <!-- End Title -->
                </div>
                <!-- Modal Header -->

                <!-- Modal Body-->
                <div class="modal-body">
                    <div class="tab-content">
                        <!-- Create User Menu -->
                        <div class="tab-pane active" id="user" role="tabpanel">
                            <div class="row mt-20">
                                <div class="col-12">
                                    <!-- Create User Form -->
                                    <div class="example-wrap">
                                        <h4 class="example-title">Create</h4>
                                        <div class="example">
                                            <form id="start-semester" method="POST" action="{{ route('academic-years.store') }}" autocomplete="off">
                                                <div class="row">
                                                    {{-- CSRF TOKEN --}}
                                                    {{ csrf_field() }}
                                                    {{-- CSRF TOKEN --}}

                                                    <!-- Input Full Name -->
                                                    <div class="form-group form-material col-12">
						                                <div class="form-group form-material col-12 col-lg-6">
						                                    <label class="form-control-label">Start of Semester</label>
						                                    <input form="start-semester" class="form-control" type="date" name="semester_start" value="">
						                                </div>

						                                <div class="form-group form-material col-12 col-lg-6">
						                                    <label class="form-control-label">End of Semester</label>
						                                    <input form="start-semester" class="form-control" type="date" name="semester_end" value="">
						                                </div>
                                                    </div>
                                                    <!-- End Input Full Name -->
                                                </div>

                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Semester</label>
                                                    <select form="start-semester" name="semester_type" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="odd">Odd</option>
                                                            <option value="even">Even</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->

                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Year</label>
                                                    <select form="start-semester" name="year" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            @for($i = date('Y'); $i < date('Y') + 5; $i++)
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endfor
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->

                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Status</label>
                                                    <select form="start-semester" name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="1">Active</option>
                                                            <option value="0">Done</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->
                                                <div class="form-group form-material">
                                                    <!-- Button Sign Up -->
                                                    <button form="start-semester" type="submit" class="btn btn-victory-c">Apply</button>
                                                    <!-- End Button Sign Up -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Create User Form -->
                                </div>
                            </div>
                        </div>
                        <!-- End Create User Menu -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Create Semester Modal Boxes -->

    @foreach($semesters as $key => $semester)
    <!-- Edit Modal Boxes -->
    <div class="modal fade modal-fill-in" id="Editacademic{{ $semester->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple" style="max-width: 100%">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                    <!-- Title -->
                    <h4 class="modal-title" id="exampleModalTabs">Edit</h4>
                    <!-- End Title -->
                </div>
                <!-- Modal Header -->

                <!-- Modal Body-->
                <div class="modal-body">
                    <div class="tab-content">
                        <!-- Create User Menu -->
                        <div class="tab-pane active" id="user" role="tabpanel">
                            <div class="row mt-20">
                                <div class="col-12">
                                    <!-- Create User Form -->
                                    <div class="example-wrap">
                                        <h4 class="example-title">Create</h4>
                                        <div class="example">
                                            <form id="update-semester" method="POST" action="{{ route('academic-years.update', $semester->id) }}" autocomplete="off">
                                                <div class="row">
                                                    {{-- CSRF TOKEN --}}
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="PATCH" name="_method" />
                                                    {{-- CSRF TOKEN --}}

                                                    <!-- Input Full Name -->
                                                    <div class="form-group form-material col-12">
                                                        <div class="form-group form-material col-12 col-lg-6">
                                                            <label class="form-control-label">Start of Semester</label>
                                                            <input form="update-semester" class="form-control" type="date" name="semester_start" value="{{ $semester->semester_start }}">
                                                        </div>

                                                        <div class="form-group form-material col-12 col-lg-6">
                                                            <label class="form-control-label">End of Semester</label>
                                                            <input form="update-semester" class="form-control" type="date" name="semester_end" value="{{ $semester->semester_end }}">
                                                        </div>
                                                    </div>
                                                    <!-- End Input Full Name -->
                                                </div>

                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Semester</label>
                                                    <select form="update-semester" name="semester_type" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="odd" <?php if($semester->semester_type == 'odd') echo 'selected'; ?>>Odd</option>
                                                            <option value="even" <?php if($semester->semester_type == 'even') echo 'selected'; ?>>Even</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->

                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Year</label>
                                                    <select form="update-semester" name="year" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            @for($i = date('Y'); $i < date('Y') + 5; $i++)
                                                                <option value="{{ $i }}" <?php if($semester->year == $i) echo 'selected'; ?>>{{ $i }}</option>
                                                            @endfor
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->

                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Status</label>
                                                    <select form="update-semester" name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="1" <?php if($semester->status == 1) echo 'selected'; ?>>Active</option>
                                                            <option value="0" <?php if($semester->status == 0) echo 'selected'; ?>>Done</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->
                                                <div class="form-group form-material">
                                                    <!-- Button Sign Up -->
                                                    <button form="update-semester" type="submit" class="btn btn-victory-c">Apply</button>
                                                    <!-- End Button Sign Up -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Create User Form -->
                                </div>
                            </div>
                        </div>
                        <!-- End Create User Menu -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Edit Modal Boxes -->
    @endforeach
</div>
<!-- End Page -->
@endsection