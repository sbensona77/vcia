@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page bg-white">
    <div class="page-main">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }} <br>
                @endforeach
            </div>

            <br />
        @endif

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}  
            </div>

            <br/>
        @elseif(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>

            <br/>
        @endif
   
        @if(Auth::user()->role == 'admin')
            <!-- Create Button -->          
            <button class="btn btn-victory-c ml-10 ml-md-30 mb-30 mt-30" type="button" data-target="#CRUDacademic" data-toggle="modal">
                <i class="icon md-plus"></i>Create
            </button>
            <!-- End Create Button -->
        @endif
        
        <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
            <table class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                <thead>
                    <tr>
                        <th class="pre-cell"></th>
                        <!-- Table Header -->
                        <th class="cell-100" scope="col">Term</th>

                        <th class="cell-100" scope="col">Term Start</th>

                        <th class="cell-100" scope="col">Term End</th>

                        <th class="cell-30" scope="col">Status</th>

                        <th class="cell-200" scope="col">Action</th>

                        <th class="suf-cell"></th>
                        <!-- End Table Header -->
                    </tr>
                </thead>
                <tbody>
                    @php
                        $counter = count($terms);
                    @endphp
                    <!-- Table List -->
                    @foreach($terms as $term)
                    <tr>
                        <td class="pre-cell"></td>
                        <!-- Term -->
                        <td class="cell-100">
                            Term {{ $counter-- }}
                        </td>
                        <!-- End Term -->

                        <!-- Term -->
                        <td class="cell-100">
                            {{ Carbon\Carbon::parse($term->term_start)->format('M d, Y') }}
                        </td>
                        <!-- End Term -->

                        <!-- Term -->
                        <td class="cell-100">
                            {{ Carbon\Carbon::parse($term->term_end)->format('M d, Y') }}
                        </td>
                        <!-- End Term -->

                        <!-- Status -->
                        <td class="cell-30">
                            @if($term->status == 0)
                                <span class="badge badge-success">Done</span>
                            @else
                                <span class="badge badge-primary">Active</span>
                            @endif
                        </td>
                        <!-- End Status -->

                        <!-- Action -->
                        <td class="cell-200">
                            <button class="btn btn-victory-c" data-target="#Editacademic{{ $term->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">Edit</button>
                        </td>
                        <!-- End Action -->

                        <td class="suf-cell"></td>
                    </tr>
                    @endforeach
                    <!-- End Table List -->
                </tbody>
            </table>
        </div>
    </div>

	<!-- Create Semester Modal Boxes -->
    <div class="modal fade modal-fill-in" id="CRUDacademic" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple" style="max-width: 100%">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                    <!-- Title -->
                    <h4 class="modal-title" id="exampleModalTabs">Create</h4>
                    <!-- End Title -->
                </div>
                <!-- Modal Header -->

                <!-- Modal Body-->
                <div class="modal-body">
                    <div class="tab-content">
                        <!-- Create Menu -->
                        <div class="tab-pane active" id="user" role="tabpanel">
                            <div class="row mt-20">
                                <div class="col-12">
                                    <!-- Create Form -->
                                    <div class="example-wrap">
                                        <div class="example">
                                            <form method="POST" action="{{ route('academic-year-terms.store') }}" autocomplete="off">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="academic_year_id" value="{{ $semester->id }}">

                                                <!-- Term Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Term Start</label>
                                                    
                                                    <input class="form-control" type="date" name="term_start">
                                                </div>

                                                <div class="example">
                                                    <label class="form-control-label">Term End</label>
                                                    <input class="form-control" type="date" name="term_end">
                                                </div>
                                                <!-- End Term Option -->

                                                <!-- Status Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Status</label>
                                                    <select name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="1">Active</option>
                                                            <option value="0">Done</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Status Option -->

                                                <div class="form-group form-material">
                                                    <!-- Button Apply -->
                                                    <button type="submit" class="btn btn-victory-c">Apply</button>
                                                    <!-- End Button Apply -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Create Form -->
                                </div>
                            </div>
                        </div>
                        <!-- End Create Menu -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Create Semester Modal Boxes -->


    <!-- Edit Modal Boxes -->
    @foreach($terms as $key => $term)
        <div class="modal fade modal-fill-in" id="Editacademic{{ $term->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple" style="max-width: 100%">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <!-- Title -->
                        <h4 class="modal-title" id="exampleModalTabs">Edit</h4>
                        <!-- End Title -->
                    </div>
                    <!-- Modal Header -->

                    <!-- Modal Body-->
                    <div class="modal-body">
                        <div class="tab-content">
                            <!-- Create Menu -->
                            <div class="tab-pane active" id="user" role="tabpanel">
                                <div class="row mt-20">
                                    <div class="col-12">
                                        <!-- Create Form -->
                                        <div class="example-wrap">
                                            <h4 class="example-title">Edit</h4>
                                            <div class="example">
                                                <form method="POST" action="{{ route('academic-year-terms.update', $term->id) }}" autocomplete="off">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="PATCH">

                                                <!-- Term Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Term Start</label>
                                                    
                                                    <input class="form-control" type="date" name="term_start" value="{{ $term->term_start }}">
                                                </div>

                                                <div class="example">
                                                    <label class="form-control-label">Term End</label>
                                                    <input class="form-control" type="date" name="term_end" value="{{ $term->term_end }}">
                                                </div>
                                                <!-- End Term Option -->

                                                <!-- Status Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Status</label>
                                                    <select name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="1" <?php if($term->status == 1) echo 'selected'; ?>>Active</option>
                                                            <option value="0" <?php if($term->status == 0) echo 'selected'; ?>>Done</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Status Option -->

                                                <div class="form-group form-material">
                                                    <!-- Button Apply -->
                                                    <button type="submit" class="btn btn-victory-c">Apply</button>
                                                    <!-- End Button Apply -->
                                                </div>
                                            </form>
                                            </div>
                                        </div>
                                        <!-- End Create Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Create Menu -->
                        </div>
                    </div>
                </div>
            </div>
        	<!-- End Edit Modal Boxes -->
    	</div>
    @endforeach
<!-- End Page -->
@endsection