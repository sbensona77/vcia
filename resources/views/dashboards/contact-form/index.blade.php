@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-block-b">
                        <h4 class="card-title project-title">Feedback List</h4>
                        <!-- Feedback Menu -->
                        <div class="tab-pane" id="feedback" role="tabpanel">
                            <!-- List Table -->
                            <table class="table table-bordered table-hover table-striped mt-20" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>E-Mail</th>
                                        <th>Feedback</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($contacts as $key => $contact)
                                    <tr class="gradeA">
                                        <td>{{ $contact->name }}</td>
                                        <td>{{ $contact->email }}</td>
                                        <td>{{ $contact->message }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- End List Table -->
                        </div>
                        <!-- End Feedback Menu -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Page -->
@endsection