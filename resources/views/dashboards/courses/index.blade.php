@extends('layouts.app-dashboard')
@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content">
        <!-- Start Courses -->
        <div class="row">
            <div class="col-xxl-9 col-xl-8 col-lg-12">

                @if ($errors->any())
                    <div class="alert alert-danger first">
                        @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                        @endforeach
                    </div>

                    <br />
                @endif

                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}  
                    </div>

                    <br/>
                @elseif(session()->get('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                    
                    <br/>
                @endif

                @if(Auth::user()->role == 'admin')
                <div class="card">
                    <div class="card-block-b">
                        <!-- Icon Create (+) -->
                        <a class="panel-action icon wb-plus mr-5" data-target="#Createmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Create New" style="cursor: pointer"></a>
                        <!-- End Icon Create (+) -->

                        <!-- Create Modal -->
                        <div class="modal fade modal-fill-in" id="Createmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="exampleFillInModalTitle">Create Courses</h4>
                                    </div>
                                    <!-- End Modal Header -->

                                    <!-- Modal Body -->
                                    <div class="modal-body">
                                        <!-- Form CRUD -->
                                        <form method="POST" action="{{ route('courses.store') }}">
                                            <div class="row">
                                                {{-- CSRF TOKEN --}}
                                                {{ csrf_field() }}
                                                <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                {{-- CSRF TOKEN --}}

                                                <!-- Create Title -->
                                                <div class="col-12 form-group">
                                                    
                                                    <input type="text" class="form-control" name="name" placeholder="Courses Title">
                                                </div>
                                                <!-- End Create Title -->

                                                <!-- Create Teacher -->
                                                <div class="col-12 form-group">
                                                    <select class="form-control" name="teacher_id" required>
                                                        <option value="">Please Select</option>
                                                        <option value="">-------------</option>
                                                        @foreach($teachers as $key => $teacher)
                                                        <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <!-- End Create Teacher -->

                                                <!-- Create Description -->
                                                <div class="col-12">
                                                    <textarea class="form-control" name="description" rows="5" placeholder="Courses Description"></textarea>
                                                </div>
                                                <!-- End Create Description -->

                                                <!-- Create Button -->
                                                <div class="col-12">
                                                    <button class="btn btn-victory-c mt-20 mt-sm-10 float-right" type="submit">Create</button>
                                                </div>
                                                <!-- End Create Button -->
                                            </div>
                                        </form>
                                        <!-- End Form CRUD -->
                                    </div>
                                    <!-- End Modal Body -->
                                </div>
                            </div>
                        </div>
                        <!-- End Create Modal -->
                    </div>
                </div>
                @endif

                <!-- Courses Topic -->
                @foreach($datas as $key => $data) {{-- COURSES FOREACH --}}
                <div class="card">
                    <div class="card-block-b">
                        <!-- Courses Header-->
                        <h4 class="card-title project-title">
                            {{-- COURSE NAME --}}
                            <a class="link-c" href="{{ route('courses.show', $data->id) }}">
                            {{ $data->name }}
                            </a>
                            {{-- COURSE NAME --}}

                            @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                            <!-- Form Action Icon -->
                        
                            <a class="panel-action icon wb-trash float-right" data-target="#Delete<?php echo $data->id ?>modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer; font-size: 14px;color: gray">
                            </a>

                            <a class="panel-action icon wb-edit float-right" data-target="#Edit<?php echo $data->id ?>modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer; font-size: 14px;color: gray">
                            </a>
                            <!-- End Form Action Icon -->
                            @endif
                        </h4>

                        <!-- Description -->
                        <p class="card-text">
                            Description : {{ $data->description }}
                        </p>
                        <!-- End Description -->
                        <!-- End Courses Header -->
                    </div>
                </div>
                <!-- End Courses Topic -->

                <!-- Delete Modal  -->
                <div class="modal fade" id="Delete<?php echo $data->id ?>modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-body">
                                <h4 class="modal-title" id="exampleModalLabel">
                                    Are you sure want to delete?
                                </h4>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Cancel
                                </button>

                                <form action="{{ route('courses.destroy', $data->id) }}" method="post">
                                    {{-- CSRF TOKEN --}}
                                    {{ csrf_field() }}
                                    <input name="_method" type="hidden" value="DELETE">
                                    {{-- CSRF TOKEN --}}

                                    <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Delete Modal  -->
                @endforeach {{-- END COURSES FOREACH --}}

                @if(Auth::user()->role != 'student')

                <!-- Edit Modal -->
                @foreach($datas as $key => $data) {{-- EDIT FOREACH --}}
                <div class="modal fade modal-fill-in" id="Edit<?php echo $data->id ?>modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>

                                <!-- Title -->
                                <h4 class="modal-title" id="exampleFillInModalTitle">Edit Courses</h4>
                                <!-- End Title -->
                            </div>
                            <!-- End Modal Header -->

                            <!-- Modal Body -->
                            <div class="modal-body">
                                <!-- Form CRUD -->
                                <form method="POST" action="{{ route('courses.update', $data->id) }}">
                                    {{-- CSRF TOKEN --}}
                                    {{ csrf_field() }}
                                    <input name="_method" type="hidden" value="PATCH">
                                    {{-- CSRF TOKEN --}}

                                    <div class="row">
                                        <!-- Edit Title -->
                                        <div class="col-12 form-group">
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                            <textarea class="form-control" name="name" rows="1" placeholder="">{{ $data->name }}</textarea>
                                        </div>
                                        <!-- Edit Title -->

                                        <!-- Create Teacher -->
                                        <div class="col-12 form-group">
                                            <select class="form-control" name="teacher_id" required>
                                                <option value="">Please Select</option>
                                                <option value="">-------------</option>

                                                @foreach($teachers as $key => $teacher)
                                                <option value="{{ $teacher->id }}" <?php if($data->teacher_id == $teacher->id) echo 'selected'; ?>>{{ $teacher->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- End Create Teacher -->

                                        <!-- Edit Description -->
                                        <div class="col-xl-12">
                                            <textarea class="form-control" name="description" rows="5" placeholder="">{{ $data->description }}</textarea>
                                        </div>
                                        <!-- End Edit Description -->
                                    </div>

                                    <!-- Apply Button -->
                                    <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Apply</button>
                                    <!-- End Apply Button -->
                                </form>
                                <!-- End Form CRUD -->
                            </div>
                            <!-- End Modal Body -->
                        </div>
                    </div>
                </div>
                @endforeach{{-- EDIT FOREACH --}}
                <!-- End Edit Modal -->

                @endif
            </div>
        </div>
    </div>
    <!-- End Courses -->
</div>
<!-- End Page -->
@endsection