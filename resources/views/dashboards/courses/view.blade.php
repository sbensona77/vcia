@extends('layouts.app-dashboard')
@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content">
        <!-- Start Courses Teacher -->
        <div class="row">
            <div class="col-xxl-9 col-xl-8 col-lg-12">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br>
                        @endforeach
                    </div>

                    <br />
                @endif

                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}  
                    </div>

                    <br/>
                @elseif(session()->get('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>

                    <br/>
                @endif

                <div class="card">
                    <div class="card-block-b">
                        <!-- Breadcrumbs -->
                        <ol class="breadcrumb breadcrumb-arrow pl-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('courses.index') }}" class="link-c">
                                    Courses
                                </a>
                            </li>

                            <li class="breadcrumb-item active">Material</li>
                        </ol>
                        <!-- End Breadcrumbs -->

                        @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                        <!-- Create Icon (+) -->
                        <a class="panel-action icon wb-plus float-right" data-target="#Createmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Create New" style="cursor: pointer"></a>
                        <!-- End Create Icon (+) -->
                        <!-- Courses Title -->
                        <h4 class="card-title project-title">
                            <a class="link-c" href="{{ route('courses.show', $data->id) }}">{{ $data->name }}</a>
                        </h4>
                        <!-- End Courses Title -->
                        <!-- Create Modal -->
                        <div class="modal fade modal-fill-in" id="Createmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                        <!-- Title -->
                                        <h4 class="modal-title" id="exampleFillInModalTitle">Create Material</h4>
                                        <!-- End Title -->
                                    </div>
                                    <!-- End Modal Header -->
                                    <!-- Modal Body -->
                                    <div class="modal-body">
                                        <!-- Form CRUD -->
                                        <form method="POST" action="{{ route('materials.store', $data->id) }}">
                                            <div class="row">
                                                {{-- CSRF TOKEN --}}
                                                {{ csrf_field() }}
                                                <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                {{-- CSRF TOKEN --}}
                                                {{-- COURSE ID --}}
                                                <input type="hidden" name="course_id" value="{{ $data->id }}">
                                                {{-- COURSE ID --}}
                                                <div class="row">
                                                    <!-- Edit Title -->
                                                    <div class="col-12 form-group">
                                                        <textarea class="form-control" rows="1" name="name" placeholder="Materials Name"></textarea>
                                                    </div>
                                                    <!-- Edit Title -->
                                                    <!-- Edit Enrollment Key -->
                                                    <div class="col-12 form-group">
                                                        <textarea class="form-control" rows="1" name="enrollment_key" placeholder="Materials Enrollment Key"></textarea>
                                                    </div>
                                                    <!-- Edit Enrollment Key -->
                                                    <!-- Edit Description -->
                                                    <div class="col-12">
                                                        <textarea class="form-control" rows="5" name="description" placeholder="Materials Description"></textarea>
                                                    </div>
                                                    <!-- End Edit Description -->
                                                    <div class="col-12">
                                                        <!-- Create Button -->
                                                        <button class="btn btn-victory-c mt-20 mt-sm-10 float-right" type="submit">Create</button>
                                                        <!-- End Create Button -->
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- End Form CRUD -->
                                    </div>
                                    <!-- End Modal Body -->
                                </div>
                            </div>
                        </div>
                        <!-- End Create Modal -->
                        @endif
                    </div>
                </div>

                @foreach($materials as $key => $material) {{-- MATERIALS FOREACH --}}
                <!-- Material Topic -->
                <div class="card">
                    <div class="card-block-b">
                        <!-- Material Header -->
                        <h4 class="card-title project-title">
                            <!-- Title -->
                            <a class="link-c" href="{{ route('materials.show', ['course_id' => $data->id, 'id' => $material->id]) }}">
                            {{ $data->name }} - {{ $material->name }}
                            </a>
                            <!-- End Title -->

                            @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent') 
                            <!-- Button Remove -->
                            <a class="panel-action icon wb-trash float-right" data-target="#Delete<?php echo $data->id ?>modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">
                            </a>
                            <!-- End Button Remove -->

                            <!-- Button Edit -->
                            <a class="panel-action icon wb-edit float-right" data-target="#Edit{{ $material->id }}modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                            <!-- End Button Edit -->
                            @endif
                        </h4>

                        <!-- Description -->
                        <p class="card-text">
                            Description : {{ $material->description }}
                        </p>
                        <!-- End Description -->
                        <!-- End Material Header -->
                    </div>
                </div>
                <!-- End Material Topic -->

                <!-- Delete Modal  -->
                <div class="modal fade" id="Delete<?php echo $data->id ?>modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-body">
                                <h4 class="modal-title" id="exampleModalLabel">
                                    Are you sure want to delete?
                                </h4>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Cancel
                                </button>

                                <form action="{{ route('courses.destroy', $data->id) }}" method="post">
                                    {{-- CSRF TOKEN --}}
                                    {{ csrf_field() }}
                                    <input name="_method" type="hidden" value="DELETE">
                                    {{-- CSRF TOKEN --}}

                                    <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Delete Modal  -->
                @endforeach {{-- END COURSES FOREACH --}}

                {{ $materials->links('pagination') }}

                <!-- Edit Modal -->
                @foreach($materials as $key => $material)
                <div class="modal fade modal-fill-in" id="Edit{{ $material->id }}modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="exampleFillInModalTitle">Edit Courses</h4>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{ route('materials.update', ['id' => $material->id, 'course_id' => $data->id]) }}">
                                    {{-- CSRF TOKEN --}}
                                    {{ csrf_field() }}
                                    <input name="_method" type="hidden" value="PATCH">
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                    {{-- CSRF TOKEN --}}

                                    {{-- COURSE ID --}}
                                    <input type="hidden" name="course_id" value="{{ $data->id }}">
                                    {{-- COURSE ID --}}

                                    <div class="row">
                                        <!-- Edit Title -->
                                        <div class="col-12 form-group">
                                            <textarea class="form-control" rows="1" name="name" placeholder="Materials Name">{{ $material->name }}</textarea>
                                        </div>
                                        <!-- Edit Title -->

                                        <!-- Edit Enrollment Key -->
                                        <div class="col-12 form-group">
                                            <textarea class="form-control" rows="1" name="enrollment_key" placeholder="Materials Enrollment Key">{{ $material->enrollment_key }}</textarea>
                                        </div>
                                        <!-- Edit Enrollment Key -->

                                        <!-- Edit Description -->
                                        <div class="col-xl-12">
                                            <textarea class="form-control" rows="5" placeholder="Materials Description" name="description">{{ $material->description }}</textarea>
                                        </div>
                                        <!-- End Edit Description -->
                                    </div>

                                    <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Apply</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- End Edit Modal -->
            </div>

            <!-- Teacher Bar (Profile,Courses) -->
            <div class="col-xxl-3 col-xl-4 col-lg-12">
                <div class="example-wrap">
                    <!-- Teacher Profile -->
                    <div class="card card-shadow">
                        <div class="card-block text-center">
                            <img src="{{ asset($profile->profile_picture) }}" class="avatar avatar-lg">
                            <h4 class="profile-user">{{ $teacher->name }}</h4>
                            <p class="profile-job">{{ $profile->subject }} Teacher</p>

                            <!-- Socmed -->
                            <div class="profile-social">
                                <a href="#" data-content="{{ $profile->phone }}" data-trigger="hover" data-toggle="popover" tabindex="0" title="Phone Number">
                                    <i class="site-menu-icon-b md-phone" aria-hidden="true"></i>
                                </a>
                                
                                <a href="#" data-content="{{ $profile->email }}" data-trigger="hover" data-toggle="popover" tabindex="0" title="Email">
                                    <i class="site-menu-icon-b md-account-box-mail" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- End Socmed -->
                        </div>

                        <!-- Courses -->
                        <div class="card-block">
                            <h4 class="profile-user">Courses</h4>
                            <p class="profile-job">{{ $data->name }}</p>
                        </div>
                        <!-- End Courses -->
                    </div>
                    <!-- End Teacher Profile -->
                </div>

            </div>
            <!-- End Teacher Bar (Profile,Courses) -->
        </div>
        <!-- End Courses Teacher -->
    </div>
    <!-- End Student Bar (Profile,Socmed,Courses Class) -->
</div>
<!-- End Courses Student -->

<!-- End Page -->
@endsection