@extends('layouts.app-dashboard')

@section('content')

<div class="page">
    <div class="page-content">
        <div class="row">
        	@if ($errors->any())
                <div class="alert alert-danger first">
                    @foreach ($errors->all() as $error)
                    {{ $error }} <br>
                    @endforeach
                </div>

                <br />
            @endif

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}  
                </div>

                <br/>
            @elseif(session()->get('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
                
                <br/>
            @endif
          	<div class="col-12">
	          	<div class="card">
	            	<div class="card-block-b">
	            	<a class="panel-action icon wb-plus mr-5" data-target="#Createmodal" data-toggle="modal" aria-hidden="true" style="cursor: pointer" data-toggle="tooltip" data-placement="bottom" title="Create New"></a>

	                <!-- Create Modal -->
	                <div class="modal fade modal-fill-in" id="Createmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
	                    <div class="modal-dialog modal-simple">
	                        <div class="modal-content">
	                          	<div class="modal-header">
	                            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                              	<span aria-hidden="true">×</span>
	                            	</button>
	                            	<h4 class="modal-title" id="exampleFillInModalTitle">Create Inventory</h4>
	                          	</div>
	                          
	                          	<div class="modal-body">
	                            	<form id="create_category" action="{{ route('inventories.store') }}" method="POST">
	                            	{{ csrf_field() }}
	                            	
	                              	<div class="row">
		                                <!-- Create Title -->
		                                <div class="col-12 form-group">
		                                  <input form="create_category" type="text" class="form-control" name="name" placeholder="Inventory Category Name">
		                                </div>
		                                <!-- End Create Title -->
	                            	</div>
									<button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Create</button>
	                            	</form>
	                          	</div>
	                        </div>
	                    </div>
	                </div>
	                <!-- End Create Modal -->
	            </div>
	        	</div>

	        	@foreach($categories as $key => $category)
		        <div class="card">
		          	<div class="card-block-b">
			            <h4 class="card-title project-title float-left">
			            	<a class="link-c" href="{{ route('inventories.show', $category->id) }}">
			            		{{ $category->name }}
			        		</a>
			        	</h4>
						<div class="panel-actions">
			           		<a class="panel-action icon wb-trash float-right pl-0" data-target="#Deletemodal{{ $category->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer"></a>

			            	<a class="panel-action icon wb-edit float-right pr-0" data-target="#Editmodal{{ $category->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
			            </div>
		          	</div>
		        </div>


		        <!-- Edit Modal -->
		        <div class="modal fade modal-fill-in" id="Editmodal{{ $category->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
		          	<div class="modal-dialog modal-simple">
		            	<div class="modal-content">
		              		<div class="modal-header">
		                		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  		<span aria-hidden="true">×</span>
		                		</button>
		                		<h4 class="modal-title" id="exampleFillInModalTitle">Edit Title</h4>
		              		</div>

		              		<div class="modal-body">
		                	<form id="update_category{{ $category->id }}" method="POST" action="{{ route('inventories.update', $category->id) }}">
		                		{{ csrf_field() }}
	                            <input type="hidden" name="_method" value="PATCH">

		                  		<div class="row">
	                                <!-- Create Title -->
	                                <div class="col-12 form-group">
	                                 	<input form="update_category{{ $category->id }}" type="text" class="form-control" name="name" placeholder="Inventory Category Name" value="{{ $category->name }}">
	                                </div>
	                                <!-- End Create Title -->
                            	</div>

								<button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Apply</button>
		                  	</form>
		                	</div>
		             	</div>
		            </div>
		        </div>
		        <!-- End Edit Modal -->


		        <!-- Delete Modal  -->
	            <div class="modal fade" id="Deletemodal{{ $category->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
	                <div class="modal-dialog modal-simple">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                                <span aria-hidden="true">&times;</span>
	                            </button>
	                        </div>

	                        <div class="modal-body">
	                            <h4 class="modal-title" id="exampleModalLabel">
	                                Are you sure want to delete?
	                            </h4>
	                        </div>

	                        <div class="modal-footer">
	                            <button type="button" class="btn btn-default" data-dismiss="modal">
	                                Cancel
	                            </button>

	                            <form method="POST" action="{{ route('inventories.destroy', $category->id) }}">
	                            	{{ csrf_field() }}
	                            	<input type="hidden" name="_method" value="DELETE">

	                                <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
	                            </form>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!-- End Delete Modal  -->
	            @endforeach
  
        	</div>
      	</div>
    </div>
</div>


@endsection