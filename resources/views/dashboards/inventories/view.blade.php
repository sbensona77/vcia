@extends('layouts.app-dashboard')

@section('content')
<div class="page">
    <div class="page-content">
        <div class="row">
            <div class="col-12">

                @if ($errors->any())
                    <div class="alert alert-danger first">
                        @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                        @endforeach
                    </div>

                    <br />
                @endif

                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}  
                    </div>

                    <br/>
                @elseif(session()->get('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                    
                    <br/>
                @endif

                <div class="card">
                    <div class="card-block-b">
                        <h4 class="card-title project-title">
                            {{ $category->name }}<br>
                            <a target="_blank" href="{{ route('print-inventory', $category->id) }}" class="btn btn-victory-c mt-30">
                            <i class="icon md-print"></i>
                            Print</a>
                        </h4>

                        <ol class="breadcrumb breadcrumb-arrow pl-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('inventories.index') }}" class="link-c">
                                    Inventories
                                </a>
                            </li>
                            <li class="breadcrumb-item active">{{ $category->name }}</li>
                        </ol>

                        <a class="panel-action icon wb-plus" data-target="#Createmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Create New" style="cursor: pointer"></a>

                        <!-- Create Modal -->
                        <div class="modal fade modal-fill-in" id="Createmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                        <span aria-hidden="true">×</span>

                                        </button>
                                        <h4 class="modal-title" id="exampleFillInModalTitle">Create</h4>
                                    </div>

                                    <div class="modal-body">
                                        <form id="store-item" method="POST" action="{{ route('inventories.store-item') }}">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="category_id" value="{{ $category->id }}">

                                            <div class="row">
                                                <!-- Create Title -->
                                                <div class="col-12 col-md-6 form-group">
                                                    <input form="store-item" type="text" class="form-control" name="inventory_name" placeholder="Inventory Name">
                                                </div>
                                                <!-- End Create Title -->

                                                <!-- Create Amount of Item -->
                                                <div class="col-12 col-md-6 form-group">
                                                    <input form="store-item" type="number" class="form-control" name="amount" placeholder="Amount of Item">
                                                </div>
                                                <!-- End Create Amount of Item -->
                                            </div>

                                            <button form="store-item" class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Create</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Create Modal -->
                    </div>
                </div>

                @foreach($inventories->sortBy('inventory_name') as $key => $inventory)
                <div class="example example-well">
                    <div id="examplePanel" class="panel mb-0" data-load-callback="customRefreshCallback">
                        <div class="panel-heading-b">
                            <div class="panel-title">
                                <button class="btn btn-victory-c" data-target="#Typelist{{ $inventory->id }}" data-toggle="modal" title="View List">{{ $inventory->inventory_name }}</button>

                                <br>

                                <p class="p-a mt-10">Amount Available: {{ $inventory->amount }}</p>
                                <p class="p-a mt-10">Amount Purchased: {{ $inventory->items->count() - $inventory->amount }}</p>
                            </div>

                            <div class="panel-actions">
                                <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Collapse"></a>

                                <a class="panel-action icon wb-edit" data-target="#Editmodal{{ $inventory->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit"></a>

                                <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Fullscreen"></a>

                                <a class="panel-action icon wb-trash float-right" data-target="#Deletemodal{{ $inventory->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer"></a>
                            </div>
                        </div>

                        <div class="panel-body mt-0 pt-0">
                            <h4>Logs</h4>
                            <ul>
                            @foreach($inventory->logs->sortByDesc('created_at') as $key => $log)
                                <li>

                                    [{{ Carbon\Carbon::parse($inventory->created_at)->format('M d, Y | H:i:s') }}]

                                    {{ $log->activity }}

                                    | 
                                    {!! $log->info !!}
                                </li>
                            @endforeach
                            </ul>
                        </div>

                        <!-- List Modal  -->
                        <div class="modal fade modal-fill-in-b" id="Typelist{{ $inventory->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    
                                    <div class="modal-body">
                                        <div class="tab-content">
                                            <div class="page-content page-content-table" id="contactsContent">
                                                {{-- Table List --}}
                                                <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                                <h4>{{ $inventory->inventory_name }}</h4>
                                                    <thead>
                                                        <tr>
                                                            <th>Code</th>

                                                            <th>Status</th>
                                                            <th>Current Purchaser</th>

                                                            <th>Created at</th>

                                                            <th>Last Edited</th>
                                                            <th>Last Executor</th>

                                                            @if(Auth::user()->role != 'student' || Auth::user()->role != 'parent')
                                                                <th colspan="2">Action</th>
                                                            @endif
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach($inventory->items as $key => $item)
                                                        <tr class="gradeA">
                                                            <td>
                                                                {{ $item->code }}
                                                            </td>

                                                            <td>
                                                                {{ $item->status }}
                                                            </td>

                                                            <td>
                                                                @if($item->status == 'borrowed')
                                                                <a class="link-c" href="{{ route('user_profile.show', $item->current_borrower->id) }}">
                                                                    {{ $item->current_borrower->name }}
                                                                </a>
                                                                @endif
                                                            </td>

                                                            <td>
                                                                {{ \Carbon\Carbon::parse($item->created_at)->format('[M d, Y] H:i:s') }}
                                                            </td>
                                                            <td>
                                                                {{ \Carbon\Carbon::parse($item->updated_at)->format('[M d, Y] H:i:s') }}
                                                            </td>
                                                            <td>
                                                                <a class="link-c" href="{{ route('user_profile.show', $item->last_executor->id) }}">
                                                                    {{ $item->last_executor->name }}
                                                                </a>
                                                            </td>

                                                            @if(Auth::user()->role != 'student' || Auth::user()->role != 'parent')
                                                                <td>
                                                                    @if($item->status == 'available')
                                                                        <form method="POST" action="{{ route('give-item', $item->id) }}">
                                                                            {{-- CSRF TOKEN --}}
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" value="PATCH" name="_method" />
                                                                            {{-- CSRF TOKEN --}}

                                                                            <input type="hidden" name="borrower" value="{{ $item->id }}">

                                                                            <select name="borrower" class="form-control mb-10">
                                                                                @foreach($users->sortBy('role') as $user_key => $user)
                                                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                                                @endforeach
                                                                            </select>

                                                                            <button type="submit" class="btn btn-victory-c mb-10" style="padding: 2px 8px">
                                                                                Purchase For Other
                                                                            </button>
                                                                        </form>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($item->status == 'available')
                                                                        <form method="POST" action="{{ route('borrow', Auth::user()->id) }}">
                                                                            {{-- CSRF TOKEN --}}
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" value="PATCH" name="_method" />
                                                                            {{-- CSRF TOKEN --}}

                                                                            <input type="hidden" name="item" value="{{ $item->id }}">

                                                                            <button type="submit" class="btn btn-victory-c mb-10" style="padding: 2px 8px">
                                                                                Purchase For Me
                                                                            </button>
                                                                        </form>
                                                                    @else
                                                                        <form method="POST" action="{{ route('return', Auth::user()->id) }}">
                                                                            {{-- CSRF TOKEN --}}
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" value="PATCH" name="_method" />
                                                                            {{-- CSRF TOKEN --}}

                                                                            <input type="hidden" name="item" value="{{ $item->id }}">

                                                                            <button type="submit" class="btn btn-success mb-10" style="padding: 2px 8px">
                                                                                Return
                                                                            </button>
                                                                        </form>
                                                                    @endif

                                                                    @if(Auth::user()->role == 'admin')
                                                                        <form method="POST" action="{{ route('destroy.item', $item->id) }}">
                                                                            {{-- CSRF TOKEN --}}
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" value="DELETE" name="_method" />
                                                                            {{-- CSRF TOKEN --}}

                                                                            <button type="submit" class="btn btn-danger" style="padding: 2px 8px">
                                                                                Delete
                                                                            </button>
                                                                        </form>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- End List Table -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End List Modal  -->

                        <!-- Edit Modal -->
                        <div class="modal fade modal-fill-in" id="Editmodal{{ $inventory->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>

                                        <h4 class="modal-title" id="exampleFillInModalTitle">
                                            Edit Log Data
                                        </h4>
                                    </div>

                                    <div class="modal-body">
                                        <form id="update-item{{ $inventory->id }}" action="{{ route('inventories.update-item', $inventory->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            <input form="update-item{{ $inventory->id }}" type="hidden" name="_method" value="PATCH">

                                            <input form="update-item{{ $inventory->id }}" type="hidden" name="category_id" value="{{ $category->id }}">

                                            <div class="row">
                                                <!-- Create Title -->
                                                <div class="col-12 col-md-6 form-group">
                                                    <input form="update-item{{ $inventory->id }}" type="text" class="form-control" name="inventory_name" placeholder="Inventory Name" value="{{ $inventory->inventory_name }}">
                                                </div>
                                                <!-- End Create Title -->

                                                <!-- Create Amount of Item -->
                                                <div class="col-12 col-md-6 form-group">
                                                    <input form="update-item{{ $inventory->id }}" type="number" class="form-control" name="amount" placeholder="Amount of Item" value="{{ $inventory->amount }}">
                                                </div>
                                                <!-- End Create Amount of Item -->
                                            </div>

                                            <button form="update-item{{ $inventory->id }}" class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Update</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Edit Modal -->

                        <!-- Delete Modal  -->
                        <div class="modal fade" id="Deletemodal{{ $inventory->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        <h4 class="modal-title" id="exampleModalLabel">
                                            Are you sure want to delete?
                                        </h4>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Cancel
                                        </button>

                                        <form id="delete-item{{ $inventory->id }}" method="POST" action="{{ route('inventories.destroy-item', $inventory->id) }}">
                                            {{ csrf_field() }}
                                            <input form="delete-item{{ $inventory->id }}" type="hidden" name="_method" value="DELETE">

                                            <button form="delete-item{{ $inventory->id }}" type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete Modal  -->

    
                    </div>
                </div>
                @endforeach

                {{ $inventories->links('pagination') }}
            </div>
        </div>
    </div>
</div>
@endsection