@extends('layouts.app-dashboard')
@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-xl-3 col-md-4">
                <!-- Panel -->
                <div class="panel">
                    <div class="panel-body">
                        <div class="list-group faq-list" role="tablist">
                            <a class="list-group-item active" data-toggle="tab" href="#category-1" aria-controls="category-1"
                                role="tab">General</a>
                            <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                                role="tab">Payment</a>
                        </div>
                    </div>
                </div>
                <!-- End Panel -->
            </div>
            <div class="col-xl-9 col-md-8">
                <!-- Panel -->
                <div class="panel">
                    <div class="panel-body">
                        <div class="tab-content">
                            <!-- Categroy 1 -->
                            <div class=" tab-pane animation-fade active" id="category-1" role="tabpanel">
                                <div class="panel-group panel-group-simple panel-group-continuous" id="accordion2"
                                    aria-multiselectable="true" role="tablist">
                                    <!-- Question 1 -->
                                    <div class="panel">
                                        <div class="panel-heading-b" id="question-1" role="tab">
                                            <a class="panel-title" aria-controls="answer-1" aria-expanded="true" data-toggle="collapse"
                                                href="#answer-1" data-parent="#accordion2">
                                            What is the differences between Victory Academy Christian School with another school in Indonesia ?
                                            </a>
                                        </div>
                                        <div class="panel-collapse collapse show" id="answer-1" aria-labelledby="question-1"
                                            role="tabpanel">
                                            <div class="panel-body">
                                                Victory Academy Christian School goes accordingly Accelerated Christian Education. ACE is our school standar. School is provide with a solid foundation of Scripture, Biblical principles, wisdom, and Godly character development. School also ensure a high academic standard through a solid mastery of each subject lesson.
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Question 1 -->
                                    <!-- Question 2 -->
                                    <div class="panel">
                                        <div class="panel-heading-b" id="question-2" role="tab">
                                            <a class="panel-title" aria-controls="answer-2" aria-expanded="false" data-toggle="collapse"
                                                href="#answer-2" data-parent="#accordion2">
                                            What is vision of Victory Academy Christian School ?
                                            </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="answer-2" aria-labelledby="question-2"
                                            role="tabpanel">
                                            <div class="panel-body">
                                                Victory International Christian Academy has a vision of building Leaders of Faith, Knowledge & Character.
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Question 2 -->
                                    <!-- Question 3 -->
                                    <div class="panel">
                                        <div class="panel-heading-b" id="question-3" role="tab">
                                            <a class="panel-title" aria-controls="answer-3" aria-expanded="false" data-toggle="collapse"
                                                href="#answer-3" data-parent="#accordion2">
                                            What is the priority of Victory Academy Christian School?
                                            </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="answer-3" aria-labelledby="question-3"
                                            role="tabpanel">
                                            <div class="panel-body">
                                                With our ability as a mentor who is experienced and has a high moral attitude. We guide, teach, and motivate our students to become obedient personal values ​​of God, uphold attitude and moral values, knowledge and virtue. Practice to be honest, disciplined and humble actions. Victory Academy is there to gather world-class knowledge and even be awarded the national level ACE.
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Question 3 -->
                                </div>
                            </div>
                            <!-- End Categroy 1 -->
                            <!-- Categroy 2 -->
                            <div class="tab-pane animation-fade" id="category-2" role="tabpanel">
                                <div class="panel-group panel-group-simple panel-group-continuous" id="accordion"
                                    aria-multiselectable="true" role="tablist">
                                    <!-- Question 5 -->
                                    <div class="panel">
                                        <div class="panel-heading-b" id="question-5" role="tab">
                                            <a class="panel-title" aria-controls="answer-5" aria-expanded="true" data-toggle="collapse"
                                                href="#answer-5" data-parent="#accordion">
                                            How is the payment system of Victory Academy Christian School ?
                                            </a>
                                        </div>
                                        <div class="panel-collapse collapse show" id="answer-5" aria-labelledby="question-5"
                                            role="tabpanel">
                                            <div class="panel-body">
                                                Victory Academy Christian School will give a notification to all of students at the end of the month, and then all of students can pay at the beginning of the month.
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Question 5 -->
                                    <!-- Question 6 -->
                                    <div class="panel">
                                        <div class="panel-heading-b" id="question-6" role="tab">
                                            <a class="panel-title" aria-controls="answer-6" aria-expanded="false" data-toggle="collapse"
                                                href="#answer-6" data-parent="#accordion">
                                            How to pay ?
                                            </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="answer-6" aria-labelledby="question-6"
                                            role="tabpanel">
                                            <div class="panel-body">
                                                Payments can be made via transfer or directly in cash at school.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Categroy 4 -->
                        </div>
                    </div>
                </div>
                <!-- End Panel -->
            </div>
        </div>
    </div>
</div>
<!-- End Page -->
@endsection