@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
    <div class="page">
      <div class="page-content container-fluid">
        @if ($errors->any())
          <div class="alert alert-danger first">
            @foreach ($errors->all() as $error)
              {{ $error }} <br>
            @endforeach
          </div><br />
        @endif@extends('layouts.app-dashboard')
@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content container-fluid">
        @if ($errors->any())
        <div class="alert alert-danger first">
            @foreach ($errors->all() as $error)
            {{ $error }} <br>
            @endforeach
        </div>
        <br />
        @endif
        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}  
        </div>
        <br/>
        @elseif(session()->get('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
        <br/>
        @else
        @endif
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <!-- WIDGET -->
            <!-- Widget Teacher -->
            <div class="col-xl-4 col-md-6">
                <div class="card card-shadow" id="widgetLineareaOne">
                    <div class="card-block p-20 pt-10">
                        <div class="clearfix">
                            <!-- Title -->
                            <div class="grey-800 float-left py-10">
                                <i class="icon md-male-female grey-800 font-size-24 vertical-align-bottom mr-5"></i>STAFF
                            </div>
                            <!-- End Title -->
                            <!-- Total -->
                            <span class="float-right grey-700 font-size-30">10</span>
                            <!-- End Total -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Widget Teacher -->
            <!-- Widget Linearea Student -->
            <div class="col-xl-4 col-md-6">
                <div class="card card-shadow" id="widgetLineareaTwo">
                    <div class="card-block p-20 pt-10">
                        <div class="clearfix">
                            <!-- Title -->
                            <div class="grey-800 float-left py-10">
                                <i class="icon md-face grey-800 font-size-24 vertical-align-bottom mr-5"></i> STUDENT
                            </div>
                            <!-- End Title -->
                            <!-- Total -->
                            <span class="float-right grey-700 font-size-30">50</span>
                            <!-- ENd Total -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Widget Linearea Student -->
            <!-- Widget Linearea Courses -->
            <div class="col-xl-4 col-md-6">
                <div class="card card-shadow" id="widgetLineareaThree">
                    <div class="card-block p-20 pt-10">
                        <div class="clearfix">
                            <!-- Title -->
                            <div class="grey-800 float-left py-10">
                                <i class="icon md-book grey-800 font-size-24 vertical-align-bottom mr-5"></i> COURSES
                            </div>
                            <!-- End Title -->
                            <!-- Total -->
                            <span class="float-right grey-700 font-size-30">20</span>
                            <!-- End Total -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Widget Linearea Courses -->
            <!-- END WIDGET-->


            <!-- ADMIN HOME-->
            @if(Auth::user()->role == 'admin')
            <!-- Panel Courses -->
            <div class="col-xxl-5 col-lg-6">
                <div class="panel" id="projects">
                    <!-- Panel Header -->
                    <div class="panel-heading">
                        <!-- Title -->
                        <h3 class="panel-title text-white">Courses</h3>
                        <!-- End Title -->
                    </div>
                    <!-- End Panel Header -->
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td style="font-weight: 700">Courses</td>
                                    <td style="font-weight: 700">Teacher</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>English Debate</td>
                                    <td>Mr. Yulius</td>
                                </tr>
                                <tr>
                                    <td>Sains</td>
                                    <td>Mrs. Irenne</td>
                                </tr>
                                <tr>
                                    <td>Mathematics</td>
                                    <td>Mrs. Yennylawati</td>
                                </tr>
                                <tr>
                                    <td>Arts</td>
                                    <td>Mrs. Chialis</td>
                                </tr>
                                <tr>
                                    <td>Computer Tech</td>
                                    <td>Mrs. Nita</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End Table -->
                </div>
            </div>
            <!-- End Panel Courses -->
            @endif
            <!-- END ADMIN HOME -->


            <!-- STUDENT HOME -->
            {{-- <!-- Panel Courses Schedule -->
            <div class="col-xxl-7 col-lg-6">
                <div class="panel" id="projects-status">
                    <!-- Panel Header -->
                    <div class="panel-heading">
                        <!-- Title -->
                        <h3 class="panel-title text-white">
                            Schedule
                        </h3>
                        <!-- End Title -->
                    </div>
                    <!-- End Panel Header -->
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td style="font-weight: 700">Day</td>
                                    <td style="font-weight: 700">Courses</td>
                                    <td style="font-weight: 700">Time</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Monday</td>
                                    <td>English Debate</td>
                                    <td>07.00 AM - 08.45 AM</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Sains</td>
                                    <td>09.00 AM - 11.00 AM</td>
                                </tr>
                                <tr>
                                    <td>Tuesday</td>
                                    <td>Mathematics</td>
                                    <td>07.00 AM - 08.45 AM</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>English Debate</td>
                                    <td>09.00 AM - 11.00 AM</td>
                                </tr>
                                <tr>
                                    <td>Wednesday</td>
                                    <td>Arts</td>
                                    <td>07.00 AM - 10.00 AM</td>
                                </tr>
                                <tr>
                                    <td>Thursday</td>
                                    <td>Computer Tech</td>
                                    <td>07.00 AM - 08.40 AM</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Sains</td>
                                    <td>09.00 AM - 10.20 AM</td>
                                </tr>
                                <tr>
                                    <td>Friday</td>
                                    <td>Sports Education</td>
                                    <td>07.00 AM - 08.40 AM</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End Table -->
                </div>
            </div>
            <!-- End Panel Courses Schedule -->
            <!-- Panel Courses Done-->
            <div class="col-xxl-5 col-lg-6">
                <div class="panel" id="projects">
                    <!-- Panel Header -->
                    <div class="panel-heading">
                        <!-- Title -->
                        <h3 class="panel-title text-white">
                            Courses
                            <!-- Notif Total -->
                            <span class="badge badge-pill badge-warning">3</span>
                            <!-- End Notif Total -->
                        </h3>
                        <!-- End Title -->
                    </div>
                    <!-- End Panel Header -->
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td style="font-weight: 700">Courses</td>
                                    <td style="font-weight: 700">Topic</td>
                                    <td style="font-weight: 700">Deadline</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>English Debate</td>
                                    <td>Pronoun</td>
                                    <td>Feb 5, 2019</td>
                                </tr>
                                <tr>
                                    <td>Arts</td>
                                    <td>Modification</td>
                                    <td>Jan 3, 2019</td>
                                </tr>
                                <tr>
                                    <td>Sains</td>
                                    <td>Human Resources</td>
                                    <td>Jan 19, 2019</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End Table -->
                </div>
            </div>
            <!-- End Panel Courses Done--> --}}
            {{-- <!-- Panel Payment -->
            <div class="col-xxl-8 col-lg-7">
                <div class="panel" id="projects">
                    <!-- Panel Header -->
                    <div class="panel-heading">
                        <!-- Title -->
                        <h3 class="panel-title text-white">Log Payment
                        </h3>
                        <!-- End Title -->
                    </div>
                    <!-- End Panel Header -->
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td style="font-weight: 700">Description</td>
                                    <td style="font-weight: 700">Deadline</td>
                                    <td style="font-weight: 700">Payment</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>School Facilities</td>
                                    <td>Jan 1, 2019</td>
                                    <td>Rp. 2.500.000,00</td>
                                </tr>
                                <tr>
                                    <td>Book and Modules</td>
                                    <td>Dec 20, 2018</td>
                                    <td>Rp. 1.250.000,00</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td>Rp. 3.750.000,00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End Table -->
                </div>
            </div>
            <!-- End Panel Payment --> --}}
            <!-- END STUDENT HOME -->
        </div>
    </div>
</div>
<!-- End Page -->
@endsection

        @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div><br/>
        @elseif(session()->get('error'))
          <div class="alert alert-danger">
            {{ session()->get('error') }}
          </div><br/>
        @else
        <div class="alert alert-warning">
            <strong>Warning!</strong> This system is under construction!
          </div><br/>
        @endif
        <div class="row" data-plugin="matchHeight" data-by-row="true">
          <!-- WIDGET -->
          <!-- Widget Teacher -->
          <div class="col-xl-4 col-md-6">
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <!-- Title -->
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-male-female grey-800 font-size-24 vertical-align-bottom mr-5"></i>STAFF
                  </div>
                  <!-- End Title -->

                  <!-- Total -->
                  <span class="float-right grey-700 font-size-30">10</span>
                  <!-- End Total -->
                </div>
              </div>
            </div>
          </div>
          <!-- End Widget Teacher -->

          
          <!-- Widget Linearea Student -->
          <div class="col-xl-4 col-md-6">
            <div class="card card-shadow" id="widgetLineareaTwo">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <!-- Title -->
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-face grey-800 font-size-24 vertical-align-bottom mr-5"></i> STUDENT
                  </div>
                  <!-- End Title -->

                  <!-- Total -->
                  <span class="float-right grey-700 font-size-30">50</span>
                  <!-- ENd Total -->
                </div>
              </div>
            </div>
          </div>
          <!-- End Widget Linearea Student -->

          
          <!-- Widget Linearea Courses -->
          <div class="col-xl-4 col-md-6">
            <div class="card card-shadow" id="widgetLineareaThree">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <!-- Title -->
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-book grey-800 font-size-24 vertical-align-bottom mr-5"></i> COURSES
                  </div>
                  <!-- End Title -->

                  <!-- Total -->
                  <span class="float-right grey-700 font-size-30">20</span>
                  <!-- End Total -->
                </div>
              </div>
            </div>
          </div>
          <!-- End Widget Linearea Courses -->
          <!-- END WIDGET-->



          <!-- ADMIN HOME-->
          @if(Auth::user()->role == 'admin')
          <!-- Panel Courses -->
          <div class="col-xxl-5 col-lg-6">
            <div class="panel" id="projects">
              <!-- Panel Header -->
              <div class="panel-heading">
                <!-- Title -->
                <h3 class="panel-title text-white">Courses</h3>
                <!-- End Title -->
              </div>
              <!-- End Panel Header -->
              <!-- Table -->
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <td style="font-weight: 700">Courses</td>
                      <td style="font-weight: 700">Teacher</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>English Debate</td>
                      <td>Mr. Yulius</td>
                    </tr>
                    <tr>
                      <td>Sains</td>
                      <td>Mrs. Irenne</td>
                    </tr>
                    <tr>
                      <td>Mathematics</td>
                      <td>Mrs. Yennylawati</td>
                    </tr>
                    <tr>
                      <td>Arts</td>
                      <td>Mrs. Chialis</td>
                    </tr>
                    <tr>
                      <td>Computer Tech</td>
                      <td>Mrs. Nita</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- End Table -->
            </div>
          </div>
          <!-- End Panel Courses -->
          @endif
          <!-- END ADMIN HOME -->


          <!-- STUDENT HOME -->
          <!-- Panel Payment -->
          <div class="col-xxl-8 col-lg-7">
            <div class="panel" id="projects">
              <!-- Panel Header -->
              <div class="panel-heading">
                <!-- Title -->
                <h3 class="panel-title text-white">Log Payment
                </h3>
                <!-- End Title -->
              </div>
              <!-- End Panel Header -->
              <!-- Table -->
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <td style="font-weight: 700">Description</td>
                      <td style="font-weight: 700">Deadline</td>
                      <td style="font-weight: 700">Payment</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>School Facilities</td>
                      <td>Jan 1, 2019</td>
                      <td>Rp. 2.500.000,00</td>
                    </tr>
                    <tr>
                      <td>Book and Modules</td>
                      <td>Dec 20, 2018</td>
                      <td>Rp. 1.250.000,00</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Total</td>
                      <td></td>
                      <td>Rp. 3.750.000,00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- End Table -->
            </div>
          </div>
          <!-- End Panel Payment -->
          <!-- END STUDENT HOME -->

        </div>
      </div>
    </div>
    <!-- End Page -->

@endsection