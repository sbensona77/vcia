@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content">
        <!-- Panel Payment -->
        <div class="panel">
            <div class="panel-body container-fluid">
                <!-- Header invoice -->
                <div class="row">
                    <div class="col-lg-3">
                        <img src="{{ asset('img/logo.png') }}" width="100px">

                        <h4 class-"mt-0">Victory Academy</h4>

                        <address>
                            Jl. Bromo No.6, Gajahmungkur <br>
                            Kota Semarang, Jawa Tengah 50232 <br>
                            <abbr title="Mail">Email:</abbr>&nbsp;&nbsp;victory.cia.semarang@gmail.com
                            <br>
                            <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;(024) 831-2819
                        </address>
                    </div>

                    <!-- Invoice info -->
                    <div class="col-lg-3 offset-lg-6 text-right">
                        <h4>Invoice Info</h4>
                        <p>
                            <a class="font-size-20" href="javascript:void(0)">#42-02</a>
                            <br> To:
                            <br>
                            <span class="font-size-20">Ferguso</span>
                        </p>
                        <address>
                            Puri Anjasmoro
                            <br>Kota Semarang, Jawa Tengah, 50412
                            <br>
                            <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;(123) 456-7890
                            <br>
                        </address>
                        <span>Invoice Date: December 10, 2018</span>
                        <br>
                        <span>Due Date: December 17, 2018</span>
                    </div>
                    <!-- End Invoice info -->
                </div>
                <!-- End Header invoice -->

                <!-- Invoice Details -->
                <div class="page-invoice-table table-responsive">
                    <table class="table table-hover text-right">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Description</th>
                                <th class="text-right">Unit Cost</th>
                                <th class="text-right">Total</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="text-center">
                                    1
                                </td>

                                <td class="text-left">
                                    Extrakurikuler
                                </td>

                                <td>
                                    Rp. 550.000
                                </td>

                                <td>
                                    Rp. 550.000
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    2
                                </td>
                                <td class="text-left">
                                    Robotic
                                </td>
                                <td>
                                    Rp. 1.750.000
                                </td>
                                <td>
                                    Rp. 1.750.000
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">
                                    3
                                </td>
                                <td class="text-left">
                                    Books and Modules
                                </td>
                                <td>
                                    Rp. 400.000
                                </td>
                                <td>
                                    Rp. 400.000
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- End Invoice Details -->

                <!-- Total -->
                <div class="text-right clearfix">
                    <div class="float-right">
                        <p class="page-invoice-amount">Grand Total:
                            <span>Rp. 2.700.000</span>
                        </p>
                    </div>
                </div>
                <!-- End Total -->

                <!-- Payment Option -->
                <div class="text-right">
                    <button type="submit" class="btn btn-animate btn-animate-side btn-victory-b">
                        <span>
                            <i class="icon md-shopping-cart" aria-hidden="true"></i> Proceed to payment
                        </span>
                    </button>

                    <button type="button" class="btn btn-animate btn-animate-side btn-victory-b" onclick="javascript:window.print();">
                        <span>
                            <i class="icon md-print" aria-hidden="true"></i> Print
                        </span>
                    </button>
                </div>
                <!-- End Payment Option -->
            </div>
        </div>
        <!-- End Panel Payment-->
    </div>
</div>
<!-- End Page -->
@endsection