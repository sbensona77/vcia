{{-- @extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page">
  <div class="page-content">
      <!-- Start Courses Teacher -->
      @if(Auth::user()->role != 'student')
      <div class="row">
        <div class="col-xxl-9 col-xl-8 col-lg-12">
          @if ($errors->any())
            <div class="alert alert-danger">
              @foreach ($errors->all() as $error)
                {{ $error }} <br>
              @endforeach
            </div><br />
          @endif

          @if(session()->get('success'))
            <div class="alert alert-success">
              {{ session()->get('success') }}  
            </div><br/>
          @elseif(session()->get('error'))
            <div class="alert alert-danger">
              {{ session()->get('error') }}
            </div><br/>
          @endif

          <div class="card">
            <div class="card-block-b">
              <!-- Breadcrumbs -->
              <ol class="breadcrumb breadcrumb-arrow pl-0">
                <li class="breadcrumb-item"><a href="./courses/index.blade.php" class="link-c">Courses</a></li>
                <li class="breadcrumb-item active">Material</li>
              </ol>
              <!-- End Breadcrumbs -->
            
            @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
            <!-- Create Icon (+) -->
            <a class="panel-action icon wb-plus float-right" data-target="#Createmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Create New" style="cursor: pointer"></a>
            <!-- End Create Icon (+) -->
            
            <!-- Courses Title -->
            <h4 class="card-title project-title">
              <a class="link-c" href="{{ route('courses.show', $data->id) }}">{{ $data->name }}</a>
            <!-- End Courses Title -->
              
            <!-- Create Modal -->
            <div class="modal fade modal-fill-in" id="Createmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
              <div class="modal-dialog modal-simple">
                <div class="modal-content">
                  <!-- Modal Header -->
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                    <!-- Title -->
                    <h4 class="modal-title" id="exampleFillInModalTitle">Create Material</h4>
                    <!-- End Title -->
                  </div>
                  <!-- End Modal Header -->
                  

                  <!-- Modal Body -->
                  <div class="modal-body">
                    <!-- Form CRUD -->
                    <form method="POST" action="{{ route('materials.store') }}">
                      <div class="row">
                        {{-- CSRF TOKEN --}}
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                        {{-- CSRF TOKEN --}}

                        {{-- COURSE ID --}}
                        <input type="hidden" name="course_id" value="{{ $data->id }}">
                        {{-- COURSE ID --}}

                        <!-- Create Course Material Name -->
                        <div class="col-12 form-group">
                          <input type="text" class="form-control" name="name" placeholder="Material Name">
                        </div>
                        <!-- End Create Course Material Name -->

                        <!-- Create Course Enrollment Key -->
                        <div class="col-12 form-group">
                          <input type="text" class="form-control" name="enrollment_key" placeholder="Enrollment Key">
                        </div>
                        <!-- End Create Course Enrollment Key -->
                                
                        <!-- Create Description -->
                        <div class="col-xl-12">
                          <textarea class="form-control" name="description" rows="5" placeholder="Material Description"></textarea>
                        </div>
                         <!-- End Create Description -->
                      </div>
                        
                      <!-- Create Button -->
                      <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Create</button>
                      <!-- End Create Button -->
                    </form>
                    <!-- End Form CRUD -->
                  </div>
                  <!-- End Modal Body -->
                </div>
              </div>
            </div>
            <!-- End Create Modal -->
            @endif
          </div>
        </div>
        @endif
          
          @foreach($materials as $key => $material)
          <!-- Courses Topic -->
          <div class="card">
            <div class="card-block-b">
                <!-- Courses Header -->
                <!-- Title -->
                <h4 class="card-title project-title">
                  <a class="link-c" href="{{ route('materials.show', $material->id) }}">
                    {{ $data->name }} - {{ $material->name }}
                  </a>
                <!-- End Title -->
                
                <!-- Button Remove -->
                <a class="panel-action icon wb-trash float-right" data-toggle="panel-close" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Remove" style="cursor: pointer"></a>
                <!-- End Button Remove -->
                
                <!-- Button Edit -->
                <a class="panel-action icon wb-edit float-right" data-target="#Edit{{ $material->id }}modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                </h4>
                <!-- End Button Edit -->
                
                <!-- Description -->
                <p class="card-text">
                  Description : {{ $material->description }}
                </p>
                <!-- End Description -->
                <!-- End Courses Header -->


                <!-- Teacher & Student avatar -->
                <ul class="project-team-items clearfix">
                  <li class="team-item mt-0 mt-sm-10">
                    <a class="avatar avatar-sm my-5">
                      <img src="{{ asset('img-dashboard/pp-7.jpg') }}" />
                    </a>
                  </li>
                  <li class="team-item item-divider mt-0 mt-sm-10">
                    <i class="icon md-chevron-right mr-0"></i>
                  </li>
                  <li class="team-item mt-0 mt-sm-10">
                    <a class="avatar avatar-sm my-5 mr-5" data-member-id="m_1">
                      <img src="{{ asset('img-dashboard/pp-student-1.jpg') }}" />
                    </a>
                    <a class="avatar avatar-sm my-5 mr-5" data-member-id="m_2">
                      <img src="{{ asset('img-dashboard/pp-student-2.jpg') }}" />
                    </a>
                    <a class="avatar avatar-sm my-5 mr-5" data-member-id="m_3">
                      <img src="{{ asset('img-dashboard/pp-student-3.jpg') }}" />
                    </a>
                    <a class="avatar avatar-sm my-5 mr-5" data-member-id="m_4">
                      <img src="{{ asset('img-dashboard/pp-student-4.jpg') }}" />
                    </a>
                    <a class="avatar avatar-sm my-5 mr-5" data-member-id="m_5">
                      <img src="{{ asset('img-dashboard/pp-student-5.jpg') }}" />
                    </a>
                    <a class="avatar avatar-sm my-5 mr-5" data-member-id="m_6">
                      <img src="{{ asset('img-dashboard/pp-student-6.jpg') }}" />
                    </a>
                  </li>

                  <button class="btn btn-victory-c float-right mt-20 mt-sm-10" data-target="#Key{{ $material->id }}modal" data-toggle="modal" type="submit">Generate Key</button>
                </ul>
                <!-- End Teacher & Student avatar -->
              </div>
            </div>
            <!-- End Courses Topic -->
            @endforeach
          </div>
         


          <!-- Edit Modal -->
          @foreach($materials as $key => $material)
          <div class="modal fade modal-fill-in" id="Edit{{ $material->id }}modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="exampleFillInModalTitle">Edit Courses</h4>
                </div>
                <div class="modal-body">
                  <form method="POST" action="{{ route('materials.update', $material->id) }}">
                    {{-- CSRF TOKEN --}}
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PATCH">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                    {{-- CSRF TOKEN --}}

                    {{-- COURSE ID --}}
                    <input type="hidden" name="course_id" value="{{ $data->id }}">
                    {{-- COURSE ID --}}

                    <div class="row">
                      <!-- Edit Title -->
                      <div class="col-12 form-group">
                        <textarea class="form-control" rows="1" name="name" placeholder="Materials Name">{{ $material->name }}</textarea>
                      </div>
                      <!-- Edit Title -->

                      <!-- Edit Enrollment Key -->
                      <div class="col-12 form-group">
                        <textarea class="form-control" rows="1" name="enrollment_key" placeholder="Materials Enrollment Key">{{ $material->enrollment_key }}</textarea>
                      </div>
                      <!-- Edit Enrollment Key -->
                                
                      <!-- Edit Description -->
                      <div class="col-xl-12">
                        <textarea class="form-control" rows="5" placeholder="Materials Description">{{ $material->description }}</textarea>
                      </div>
                      <!-- End Edit Description -->
                    </div>
                    
                    <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Apply</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          <!-- End Edit Modal -->
          

          <!-- Teacher Bar (Profile,Courses) -->
          <div class="col-xxl-3 col-xl-4 col-lg-12">
            @if ($errors->any())
              <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                  {{ $error }} <br>
                @endforeach
              </div><br />
            @endif

            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br/>
            @elseif(session()->get('error'))
              <div class="alert alert-danger">
                {{ session()->get('error') }}
              </div><br/>
            @endif

            <div class="example-wrap">
              <div class="card card-shadow">

                <!-- Teacher Profile -->
                <div class="card-block text-center">
                  <img src="{{ asset('img-dashboard/pp-7.jpg') }}" class="avatar avatar-lg">
                <h4 class="profile-user">Mrs. Yennylawati</h4>
                <p class="profile-job">Teacher</p>

                <!-- Socmed -->
                <div class="profile-social">
                  <a href="#" data-content="087 731 XXX XXX" data-trigger="hover" data-toggle="popover" tabindex="0" title="Phone Number">
                    <i class="site-menu-icon-b md-phone" aria-hidden="true"></i>
                  </a>

                  <a href="#" data-content="blabla123@gmail.com" data-trigger="hover" data-toggle="popover" tabindex="0" title="Email">
                    <i class="site-menu-icon-b md-account-box-mail" aria-hidden="true"></i>
                  </a>
                </div>
                <!-- End Socmed -->
                </div>

                <!-- Courses -->
                <div class="card-block">
                  <h4 class="profile-user">Courses</h4>
                  <p class="profile-job">Mathematics</p>
                </div>
                <!-- End Courses -->
                <!-- End Teacher Profile -->
              </div>

          
              <!-- Enrollment Key Before Generated Modal Boxes -->
              <div class="modal fade" id="Keymodal" aria-hidden="true" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-simple modal-sidebar modal-sm">
                  <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <!-- Title -->
                      <h4 class="modal-title">Generate Enrollment Key</h4>
                      <!-- End Title -->
                    </div>
                    <!-- End Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body">
                      <div class="input-search">
                        <!-- Button Generate -->
                        <button type="submit" class="input-search-btn" style="cursor:pointer"><i class="icon md-key" aria-hidden="true"></i></button>
                        <!-- End Button Generate -->
                        
                        <!-- Input Key -->
                        <input type="text" class="form-control border-danger" name="site-search" placeholder="Generate Key">
                        <!-- End Input Key -->
                      </div>
                    </div>


                    @if(Auth::user()->role == 'student')
                    <!-- Form Generate Key Student -->
                    <form method="POST" action="{{ route('change-material-enrollment-key', $material->id) }}">
                      {{-- CSRF TOKEN --}}
                      {{ csrf_field() }}
                      <input name="_method" type="hidden" value="PUT">
                      <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                      {{-- CSRF TOKEN --}}

                      <input type="text" class="form-control border-danger" name="enrollment_key" placeholder="Generate Key">
                      <!-- End Input Key -->
                      
                      <!-- Generate Button -->
                      <button type="submit" class="input-search-btn">
                        <i class="icon md-key" aria-hidden="true"></i>
                      </button>
                      <!-- End Generate Button -->
                    </form>
                    <!-- End Form Generate Key Student -->
                    @endif

                    <!-- End Modal Body -->
                  </div>
                </div>
              </div>
              <!-- End Enrollment Key Modal Boxes -->


              <!-- Enrollment Key After Generated Modal Boxes -->
              <div class="modal fade" id="Keymodalsuccess" aria-hidden="true" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-simple modal-sidebar modal-sm">
                  <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <!-- Title -->
                      <h4 class="modal-title">Enrollment Key Generated</h4>
                      <!-- End Title -->
                    </div>
                    <!-- End Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body">
                      <!-- Key Generated Text -->
                      <h4 class="text-success"><i class="icon md-key" aria-hidden="true"></i>GHI-800</h4>
                      <!-- End Key Generated Text -->
                    </div>
                    <!-- End Modal Body -->
                  </div>
                </div>
              </div>
              <!-- End Enrollment Key After Generated Modal Boxes -->
            </div>

                      
            <!-- Students list -->
            <div class="card card-shadow">
              <div class="card-block">
                <h4 class="profile-user">Students</h4> 
                  <img src="{{ asset('img-dashboard/pp-student-1.jpg') }}" class="avatar avatar-md mt-10">
                  <img src="{{ asset('img-dashboard/pp-student-2.jpg') }}" class="avatar avatar-md mt-10">
                  <img src="{{ asset('img-dashboard/pp-student-3.jpg') }}" class="avatar avatar-md mt-10">
                  <img src="{{ asset('img-dashboard/pp-student-4.jpg') }}" class="avatar avatar-md mt-10">
                  <img src="{{ asset('img-dashboard/pp-student-5.jpg') }}" class="avatar avatar-md mt-10">
                  <img src="{{ asset('img-dashboard/pp-student-6.jpg') }}" class="avatar avatar-md mt-10">
                </div>
              </div>
              <!-- End Students list -->         
          </div>
          <!-- End Teacher Bar (Profile,Courses) -->
        </div>
        <!-- End Courses Teacher -->
                                    
      </div>
      <!-- End Student Bar (Profile,Socmed,Courses Class) -->
    </div>
    <!-- End Courses Student -->
  </div>
</div>
<!-- End Page -->
@endsection --}}