@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content">
        <!-- Start Full Courses -->
        <div class="row">
            <div class="col-xxl-9 col-xl-8 col-lg-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                        @endforeach
                    </div>
                    <br />
                @endif
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}  
                    </div>

                    <br/>
                @elseif(session()->get('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>

                    <br/>
                @endif
                <div class="card">
                    <div class="card-block-b">
                        <!-- Courses Title -->
                        <h4 class="card-title project-title">
                            <a href="{{ route('courses.show', $data->id) }}" class="link-c">
                            {{ $data->name }}
                            </a>
                        </h4>
                        <!-- End Courses Title -->
                        <!-- Breadcrumbs -->
                        <ol class="breadcrumb breadcrumb-arrow pl-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('courses.index') }}" class="link-c">Courses</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('courses.show', ['course_id' => $data->id]) }}" class="link-c">Material</a>
                            </li>
                            {{-- ACTIVE --}}
                            <li class="breadcrumb-item active">Full</li>
                            {{-- ACTIVE --}}
                        </ol>
                        <!-- End Breadcrumbs -->
                    </div>
                </div>

                <!-- Topic Menu -->
                <div class="example example-well">
                    <div id="examplePanel" class="panel mb-0" data-load-callback="customRefreshCallback">
                        <!-- Panel Heading -->
                        <div class="panel-heading-b">
                            <!-- Title -->
                            <h3 class="panel-title">{{ $data->name }} - {{ $material->name }}</h3>
                            <!-- End Title -->
                            @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                            <!-- Panel Button Action -->
                            <div class="panel-actions">
                                <!-- Button Remove -->
                                <button type="submit" id="userDeleteButton" class="panel-action icon wb-trash float-right" data-entity="user" data-toggle="tooltip" data-placement="bottom" title="Remove" style="cursor: pointer;background-color: transparent;border: none"></button>
                                <!-- End Button Remove -->
                                <!-- Icon Create (+) -->
                                <a class="panel-action icon wb-plus" data-target="#Createmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Create"></a>
                                <!-- End Icon Create (+) -->
                            </div>
                            <!-- End Panel Button Action -->
                            @endif
                        </div>
                        <!-- End Panel Heading -->


                        <!-- Create Modal -->
                        <div class="modal fade modal-fill-in" id="Createmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                        <!-- Title -->
                                        <h4 class="modal-title" id="exampleFillInModalTitle">Create Materials</h4>
                                        <!-- End Title -->
                                    </div>
                                    <!-- End Modal Header -->
                                    <!-- Modal Body -->
                                    <div class="modal-body">
                                        <!-- Form -->
                                        <form method="POST" action="{{ route('upload-material', $data->id) }}" enctype="multipart/form-data">
                                            <div class="row">
                                                {{-- CSRF TOKEN --}}
                                                {{ csrf_field() }}
                                                <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                {{-- CSRF TOKEN --}}
                                                <div class="col-12">
                                                    <!-- Upload Files Boxes -->
                                                    <div class="example-wrap">
                                                        <h3 class="panel-title text-center">
                                                            Upload your file here
                                                            <i class="icon md-long-arrow-down ml-10" aria-hidden="true"></i>
                                                        </h3>
                                                        <!-- Upload File Icon -->
                                                        <p class="text-center">
                                                            <i class="fas fa-file-pdf fa-2x mt-10 mr-10" data-placement="bottom" data-toggle="tooltip" data-original-title="Upload PDF"></i>
                                                        </p>
                                                        <!-- End Upload File Icon -->
                                                        <!-- Input File -->
                                                        <div class="example">
                                                            <input type="hidden" name="material_id" value="{{ $material->id }}">
                                                            <input type="file" id="input-file-now" name="uploaded_file" data-plugin="dropify" data-default-file=""/>
                                                        </div>
                                                        <!-- End Input File -->
                                                        <!-- Upload File Button -->
                                                        <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">
                                                        Upload Files
                                                        </button>
                                                        <!-- End Upload File Button -->
                                                    </div>
                                                    <!-- End Upload Files Boxes --> 
                                                </div>
                                            </div>
                                        </form>
                                        <!-- End Form -->
                                    </div>
                                    <!-- End Modal Body -->
                                </div>
                            </div>
                        </div>
                        <!-- End Create Modal --> 

                        <!-- Delete Modal  -->
                        <div class="modal fade" id="genericDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="modal-title" id="exampleModalLabel">Are you sure want to delete?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <form action="{{ route('courses.destroy', $data->id) }}" method="POST">
                                            {{-- CSRF TOKEN --}}
                                            {{ csrf_field() }}
                                            <input name="_method" type="hidden" value="DELETE">
                                            {{-- CSRF TOKEN --}}
                                            <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete Modal  --> 

                        <!-- Panel Body -->
                        <div class="panel-body">
                            <!-- Courses Description -->
                            {{ $material->description }}
                            <!-- End Courses Description -->
                            <div class="row pt-20">
                                <div class="col-12">
                                    @foreach($files as $key => $file)
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                        <!-- DOWNLOAD MATERI -->
                                        <a href="{{ route('download_material', ['course_id' => $data->id, 'material_id' => $material->id, 'file_id' => $file->id]) }}" data-placement="top" data-toggle="tooltip" data-original-title="Download PDF" target="_blank">

                                            <h5 class="title mb-10 mb-md-30">
                                                <i class="fas fa-file-pdf fa-2x mr-10"></i>
                                                {{ $material->name }} - {{ substr($file->uploaded_file, 20) }}
                                                <br>
                                                <br>
                                                <i class="site-menu-icon md-time"></i>
                                                {{ \Carbon\Carbon::parse($file->updated_at)->diffForHumans() }}
                                            </h5>
                                        </a>
                                        <!-- DOWNLOAD MATERI -->
                                    </div>
                                    
                                    <div class="col-12 col-md-6">
                                        <!-- Button Remove -->
                                        <a class="panel-action icon wb-trash mr-10 mb-30 mb-md-0 float-md-right" data-target="#Delete<?php echo $data->id ?>modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">
                                        </a>
                                        <!-- End Button Remove -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Panel Body -->


                        <!-- Delete Modal  -->
                        <div class="modal fade" id="Delete<?php echo $data->id ?>modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1" style="background-color: transparent;">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="modal-title" id="exampleModalLabel">
                                            Are you sure want to delete?
                                        </h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Cancel
                                        </button>
                                        <form action="{{ route('courses.destroy', $data->id) }}" method="post">
                                            {{-- CSRF TOKEN --}}
                                            {{ csrf_field() }}
                                            <input name="_method" type="hidden" value="DELETE">
                                            {{-- CSRF TOKEN --}}
                                            <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Delete Modal  -->
                        @endforeach {{-- END COURSES FOREACH --}}


                        @if(Auth::user()->role != 'student')
                        <!-- Edit Modal -->
                        <div class="modal fade modal-fill-in" id="Edit<?php $material->id ?>modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                        <!-- Title -->
                                        <h4 class="modal-title" id="exampleFillInModalTitle">Edit Courses</h4>
                                        <!-- End Title -->
                                    </div>
                                    <!-- End Modal Header -->
                                    <!-- Modal Body -->
                                    <div class="modal-body">
                                        <!-- Form CRUD -->
                                        <form method="POST" action="">
                                            {{-- CSRF TOKEN --}}
                                            {{ csrf_field() }}
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                            {{-- CSRF TOKEN --}}
                                            <div class="row">
                                                <!-- Edit Title -->
                                                <div class="col-12 form-group">
                                                    <textarea class="form-control" rows="1" placeholder="">{{ $material->name }}</textarea>
                                                </div>
                                                <!-- Edit Title -->
                                                <!-- Edit Description -->
                                                <div class="col-xl-12">
                                                    <textarea class="form-control" rows="5" placeholder="">{{ $material->description }}</textarea>
                                                </div>
                                                <!-- End Edit Description -->
                                            </div>
                                            <!-- Apply Button -->
                                            <button class="btn btn-victory-c float-right mt-20 mt-sm-10" data-target="#examplePositionSidebar" data-toggle="modal" type="submit">Apply</button>
                                            <!-- End Apply Button -->
                                        </form>
                                        <!-- End Form CRUD -->
                                    </div>
                                    <!-- Modal Body -->
                                </div>
                            </div>
                        </div>
                        <!-- End Edit Modal -->
                        @endif

                        @if(Auth::user()->role == 'student')
                        <!-- Upload Files Boxes -->
                        <div class="example-wrap">
                            <h3 class="panel-title text-center">Upload your file here<i class="icon md-long-arrow-down ml-10" aria-hidden="true"></i></h3>
                            <!-- Upload File Icon -->
                            <p class="text-center">
                                <i class="fas fa-file-pdf fa-2x mt-10 mr-10" data-placement="bottom" data-toggle="tooltip" data-original-title="Upload PDF"></i>
                                <i class="fas fa-file-powerpoint fa-2x mt-10 mr-10" data-placement="bottom" data-toggle="tooltip" data-original-title="Upload MS PowerPoint"></i>
                                <i class="fas fa-file-word fa-2x mt-10" data-placement="bottom" data-toggle="tooltip" data-original-title="Upload MS Word"></i>
                            </p>
                            <!-- End Upload File Icon -->
                            <!-- Input File -->
                            <div class="example">
                                <input type="file" id="input-file-now" data-plugin="dropify" data-default-file=""/>
                            </div>
                            <!-- End Input File -->
                            <!-- Upload File Button -->
                            <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Upload Files</button>
                            <!-- End Upload File Button -->
                        </div>
                        <!-- End Upload Files Boxes -->
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- End Full Courses -->
    </div>
</div>
<!-- End Page -->
@endsection