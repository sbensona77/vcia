@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content">
        @if ($errors->any())
            <div class="alert alert-danger first">
                @foreach ($errors->all() as $error)
                {{ $error }} <br>
                @endforeach
            </div>

            <br />
        @endif

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}  
            </div>

            <br/>
        @elseif(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>

            <br/>
        @endif

        @if(Auth::user()->role == 'admin')
            <!-- Create Button -->          
            <button class="btn btn-victory-c ml-10 ml-md-30 mb-30 mt-30" type="button" data-target="#Createmodal" data-toggle="modal">
                <i class="icon md-plus"></i>Create
            </button>
            <!-- End Create Button -->

            <!-- Broadcast Button -->          
            <button class="btn btn-victory-c ml-10 mb-30 mt-30" type="button" data-target="#Broadcastmodal" data-toggle="modal">
                <i class="icon md-plus"></i>Broadcast
            </button>
            <!-- End Broadcast Button -->

            <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
                <!-- Contacts List Table -->
                <table class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                    <thead>
                        <tr>
                            <th class="pre-cell"></th>
                            <!-- Table Header -->
                            <th class="cell-30" scope="col">No</th>
                            <th class="cell-300" scope="col">Full Name</th>

                            {{-- @if(Route::is('students.index')) --}}
                            <th class="cell-200" scope="col">Amount</th>
                            {{-- @endif --}}

                            <th class="cell-100" scope="col">Status</th>
                            <th class="cell-200" scope="col">Description</th>
                            <th class="cell-300" scope="col">Action</th>
                            <!-- End Table Header -->
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Table List -->
                        @foreach($payments as $key => $payment)
                        <tr>
                            <td class="pre-cell"></td>
                            {{-- No --}}
                            <td class="cell-30">
                                {{ $key + 1 }}
                            </td>
                            {{-- End No --}}

                            <!-- Name -->
                            <td class="cell-300">
                                {{ $payment->student->name }}
                            </td>
                            <!-- End Name -->

                            <!-- Amount -->
                            <td class="cell-200">
                                Rp. {{ number_format($payment->amount, 2, ',', '.') }}
                            </td>
                            <!-- End Amount -->
                            
                            <!-- Status -->
                            <td class="cell-100">
                                @if($payment->status == 'paid')
                                    <span class="badge badge-success">
                                        Paid
                                    </span>
                                @else
                                    <span class="badge badge-danger">
                                        Unpaid
                                    </span>
                                @endif
                            </td>
                            <!-- End Status -->

                             <!-- Details -->
                            <td class="cell-200">
                                {{ $payment->description }}
                            </td>
                            <!-- End Details -->

                            <td class="cell-300">
                                @if(Auth::user()->role == 'admin')

                                @if($payment->status == 'unpaid')
                                    <form action="{{ route('payments.change-status', $payment->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PATCH">

                                        <input type="hidden" name="status" value="paid">

                                        <button type="submit" class="btn btn-success">Paid</button>
                                    </form>
                                @elseif($payment->status == 'paid')
                                    <form action="{{ route('payments.change-status', $payment->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PATCH">

                                        <input type="hidden" name="status" value="unpaid">

                                        <button type="submit" class="btn btn-danger">Unpaid</button>
                                    </form>
                                @endif
                                <br>

                                {{-- Remove Button --}}
                                <button class="btn btn-victory-c" data-target="#Deletemodal{{ $payment->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">Delete</button>
                                {{-- End Remove Button --}}
                                
                                {{-- Edit Button --}}
                                <button class="btn btn-victory-c" data-target="#Editmodal{{ $payment->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">Edit</button>
                                {{-- End Edit Button --}}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        <!-- End Table List -->
                    </tbody>
                </table>
                <!-- End Contacts List Table -->

                <!-- Pagination List -->
                @if($payments instanceof \Illuminate\Pagination\LengthAwarePaginator)
                    {{ $payments->links('pagination') }}
                @endif
                <!-- End Pagination List -->
            </div>
        @else
            @foreach($payments as $key => $payment)
                <!-- Panel Payment -->
                <div class="panel">
                    <div class="panel-body container-fluid">
                        <!-- Header invoice -->
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="{{ asset('img/logo.png') }}" width="100px">

                                <h4 class="mt-0">Victory Academy</h4>

                                <address>
                                    Jl. Bromo No.6, Gajahmungkur <br>
                                    Kota Semarang, Jawa Tengah 50232 <br>
                                    <abbr title="Mail">Email:</abbr>&nbsp;&nbsp;victory.cia.semarang@gmail.com
                                    <br>
                                    <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;(024) 831-2819
                                </address>
                            </div>

                            <!-- Invoice info -->
                            <div class="col-lg-3 offset-lg-6 text-right">
                                <h4>Invoice Info</h4>
                                <p>
                                    <a class="font-size-20" href="javascript:void(0)"></a>
                                    <br> To:
                                    <br>
                                    <span class="font-size-20">{{ $payment->student->name }}</span>
                                </p>

                                <address>
                                    <abbr title="Phone">Phone:</abbr>
                                    &nbsp;&nbsp;{{ $payment->student->profile->phone }}
                                    <br>
                                </address>

                                <span>
                                    Invoice Date: {{ Carbon\Carbon::parse($payment->created_at)->format('M d, Y') }}
                                </span>
                                
                                <br>

                                <span>
                                    Due Date: {{ Carbon\Carbon::parse($payment->due_date)->format('M d, Y') }}
                                </span>
                            </div>
                            <!-- End Invoice info -->
                        </div>
                        <!-- End Header invoice -->

                        <!-- Invoice Details -->
                        <div class="page-invoice-table table-responsive">
                            <table class="table table-hover text-right">
                                <thead>
                                    <tr>
                                        <th class="text-left">Description</th>

                                        <th class="text-right">Total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td class="text-left">
                                            {{ $payment->description }}
                                        </td>

                                        <td>
                                            Rp. {{ number_format($payment->amount, 2, ',', '.') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- End Invoice Details -->

                        <!-- Total -->
                        <div class="text-right clearfix">
                            <div class="float-right">
                                <p class="page-invoice-amount">Grand Total:
                                    <span>Rp. {{ number_format($payment->amount, 2, ',', '.') }}</span>
                                </p>
                            </div>
                        </div>
                        <!-- End Total -->

                        <!-- Payment Option -->
                        <div class="text-right">
                            {{-- <button type="submit" class="btn btn-victory-b">
                                <span>
                                    Proceed to payment
                                </span>
                            </button> --}}

                            <button type="button" class="btn btn-victory-b" onclick="javascript:window.print();">
                                <span>
                                    Print
                                </span>
                            </button>
                        </div>
                        <!-- End Payment Option -->
                    </div>
                </div>
                <!-- End Panel Payment-->
            @endforeach

            <!-- Pagination List -->
            @if($payments instanceof \Illuminate\Pagination\LengthAwarePaginator)
                {{ $payments->links('pagination') }}
            @endif
            <!-- End Pagination List -->
        @endif

        
        <!-- Create Modal Boxes -->
        <div class="modal fade modal-fill-in" id="Createmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple" style="max-width: 100%">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <!-- Title -->
                        <h4 class="modal-title" id="exampleModalTabs">Create</h4>
                        <!-- End Title -->
                    </div>
                    <!-- Modal Header -->

                    <!-- Modal Body-->
                    <div class="modal-body">
                        <div class="tab-content">
                            <!-- Create User Menu -->
                            <div class="tab-pane active" id="user" role="tabpanel">
                                <div class="row mt-20">
                                    <div class="col-12">
                                        <!-- Create User Form -->
                                        <div class="example-wrap">
                                            <div class="example">
                                                <form method="POST" action="{{ route('payments.store') }}" autocomplete="off">
                                                    <div class="row">
                                                        {{-- CSRF TOKEN --}}
                                                        {{ csrf_field() }}
                                                        <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                        {{-- CSRF TOKEN --}}

                                                        <!-- Input Full Name -->
                                                        <div class="form-group form-material col-12">
                                                            <label class="form-control-label" for="inputBasicFirstName">
                                                                User
                                                            </label>

                                                            <select name="student_id" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                                <option></option>
                                                                <optgroup label="Please Select">
                                                                    @foreach($users as $key => $user)
                                                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                                    @endforeach
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                            <!-- End Input Full Name -->
                                                    </div>
                                                    <!-- Input Title -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicEmail">Title</label>

                                                        <input type="text" class="form-control" id="inputBasicEmail" name="title" placeholder="Title" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Title -->

                                                    <!-- Input Description -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicPassword">Description</label>

                                                        <input type="text" class="form-control" id="inputBasicPassword" name="description" placeholder="Description" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Description -->

                                                    <!-- Input Amount -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicPassword">Amount</label>

                                                        <input type="text" class="form-control" id="amount" pattern="^\d+(\.|\,)\d{2}$" value="" name="amount" placeholder="Amount" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Amount -->

                                                    <!-- Status Option -->
                                                    <div class="example">
                                                        <label class="form-control-label">Status</label>
                                                        <select name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                            <option></option>
                                                            <optgroup label="Please Select">
                                                                <option value="paid">Paid</option>
                                                                <option value="unpaid">Not Paid</option>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                    <!-- End Status Option -->

                                                    <!-- Input Date -->
                                                    <div class="form-group form-material">

                                                        <label class="form-control-label" for="inputBasicPassword">Due Date</label>
                                                        <input type="date" class="form-control" id="inputBasicPassword" name="due_date" placeholder="Date" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Date -->

                                                    <div class="form-group form-material">
                                                        <!-- Button Apply-->
                                                        <button type="submit" class="btn btn-victory-c">Apply</button>
                                                        <!-- End Button Apply-->
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Create User Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Create User Menu -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Create Modal Boxes -->
        
        <!-- Broadcast Modal Boxes -->
        <div class="modal fade modal-fill-in" id="Broadcastmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple" style="max-width: 100%">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <!-- Title -->
                        <h4 class="modal-title" id="exampleModalTabs">Broadcast</h4>
                        <!-- End Title -->
                    </div>
                    <!-- Modal Header -->

                    <!-- Modal Body-->
                    <div class="modal-body">
                        <div class="tab-content">
                            <!-- Create User Menu -->
                            <div class="tab-pane active" id="user" role="tabpanel">
                                <div class="row mt-20">
                                    <div class="col-12">
                                        <!-- Create User Form -->
                                        <div class="example-wrap">
                                            <div class="example">
                                                <form method="POST" action="{{ route('payments.broadcast') }}" autocomplete="off">
                                                    <div class="row">
                                                        {{-- CSRF TOKEN --}}
                                                        {{ csrf_field() }}
                                                        <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                        {{-- CSRF TOKEN --}}
                                                    </div>
                                                    <!-- Input Title -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicEmail">Title</label>

                                                        <input type="text" class="form-control" id="inputBasicEmail" name="title" placeholder="Title" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Title -->

                                                    <!-- Input Description -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicPassword">Description</label>

                                                        <input type="text" class="form-control" id="inputBasicPassword" name="description" placeholder="Description" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Description -->

                                                    <!-- Input Amount -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicPassword">Amount</label>

                                                        <input type="text" class="form-control" id="amount-broadcast" pattern="^\d+(\.|\,)\d{2}$" value="" name="amount" placeholder="Amount" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Amount -->

                                                    <!-- Status Option -->
                                                    <div class="example">
                                                        <label class="form-control-label">Status</label>
                                                        <select name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                            <option></option>
                                                            <optgroup label="Please Select">
                                                                <option value="paid">Paid</option>
                                                                <option value="unpaid">Not Paid</option>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                    <!-- End Status Option -->

                                                    <!-- Input Date -->
                                                    <div class="form-group form-material">

                                                        <label class="form-control-label" for="inputBasicPassword">Due Date</label>
                                                        <input type="date" class="form-control" id="inputBasicPassword" name="due_date" placeholder="Date" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Date -->

                                                    <div class="form-group form-material">
                                                        <!-- Button Apply-->
                                                        <button type="submit" class="btn btn-victory-c">Apply</button>
                                                        <!-- End Button Apply-->
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Create User Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Create User Menu -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Broadcast Modal Boxes -->

        @foreach($payments as $key => $payment)
        <!-- Edit Modal Boxes -->
        <div class="modal fade modal-fill-in" id="Editmodal{{ $payment->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple" style="max-width: 100%">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <!-- Title -->
                        <h4 class="modal-title" id="exampleModalTabs">Edit</h4>
                        <!-- End Title -->
                    </div>
                    <!-- Modal Header -->

                    <!-- Modal Body-->
                    <div class="modal-body">
                        <div class="tab-content">
                            <!-- Create User Menu -->
                            <div class="tab-pane active" id="user" role="tabpanel">
                                <div class="row mt-20">
                                    <div class="col-12">
                                        <!-- Create User Form -->
                                        <div class="example-wrap">
                                            <div class="example">
                                                <form id="editpayment{{ $payment->id }}" method="POST" action="{{ route('payments.update', $payment->id) }}" autocomplete="off">
                                                    <div class="row">
                                                        {{-- CSRF TOKEN --}}
                                                        {{ csrf_field() }}
                                                        <input type="hidden" value="PATCH" name="_method" />
                                                        {{-- CSRF TOKEN --}}

                                                        <!-- Input Full Name -->
                                                        <div class="form-group form-material col-12">
                                                            <label class="form-control-label" for="inputBasicFirstName">
                                                                User
                                                            </label>

                                                            <select form="editpayment{{ $payment->id }}" name="student_id" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                                <option></option>
                                                                <optgroup label="Please Select">
                                                                    @foreach($users as $key => $user)
                                                                        <option value="{{ $user->id }}" <?php if($payment->student_id == $user->id) echo 'selected' ?>>{{ $user->name }}</option>
                                                                    @endforeach
                                                                </optgroup>
                                                            </select>
                                                            </div>
                                                            <!-- End Input Full Name -->
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- Input Title -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicEmail">Title</label>

                                                        <input form="editpayment{{ $payment->id }}" type="text" class="form-control" id="inputBasicEmail" name="title" placeholder="Title" autocomplete="off" value="{{ $payment->title }}" />
                                                    </div>
                                                    <!-- End Input Title -->

                                                    <!-- Input Description -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicPassword">Description</label>

                                                        <input form="editpayment{{ $payment->id }}" type="text" class="form-control" id="inputBasicPassword" name="description" placeholder="Description" autocomplete="off" value="{{ $payment->description }}" />
                                                    </div>
                                                    <!-- End Input Description -->

                                                    <!-- Input Amount -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicPassword">Amount</label>

                                                        <input type="text" class="form-control" id="amount" pattern="^\d+(\.|\,)\d{2}$" value="" name="amount" placeholder="Amount" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Amount -->

                                                    <!-- Status Option -->
                                                    <div class="example">
                                                        <label class="form-control-label">Status</label>
                                                        <select form="editpayment{{ $payment->id }}" name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                            <option></option>
                                                            <optgroup label="Please Select">
                                                                <option value="paid" <?php if($payment->status == 'paid') echo 'selected' ?>>Paid</option>
                                                                <option value="unpaid" <?php if($payment->status == 'unpaid') echo 'selected' ?>>Not Paid</option>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                    <!-- End Status Option -->

                                                    <!-- Input Date -->
                                                    <div class="form-group form-material">
                                                        <label class="form-control-label" for="inputBasicPassword">Due Date</label>

                                                        <input form="editpayment{{ $payment->id }}" type="date" class="form-control" id="inputBasicPassword" name="due_date" placeholder="Date" value="{{ Carbon\Carbon::parse($payment->due_date)->format('Y-m-d') }}" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Date -->

                                                    <div class="form-group form-material">
                                                        <!-- Button Apply-->
                                                        <button form="editpayment{{ $payment->id }}" type="submit" class="btn btn-victory-c">Apply</button>
                                                        <!-- End Button Apply-->
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Create User Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Create User Menu -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Edit Modal Boxes -->


        <!-- Delete Modal  -->
        <div class="modal fade" id="Deletemodal{{ $payment->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <h4 class="modal-title" id="exampleModalLabel">
                            Are you sure want to delete?
                        </h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>

                        <form id="deletepayment{{ $payment->id }}" action="{{ route('payments.destroy', $payment->id) }}" method="post">
                            {{-- CSRF TOKEN --}}
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="DELETE">
                            {{-- CSRF TOKEN --}}

                            <button form="deletepayment{{ $payment->id }}" type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Delete Modal  -->
        @endforeach
    </div>
</div>
<!-- End Page -->
@endsection