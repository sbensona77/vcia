@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page bg-white">
    <!-- Contacts Content -->
    <div class="page-main">
        <!-- Contacts Content Header -->
        <div class="page-header">
            @if ($errors->any())
                <div class="alert alert-danger first">
                    @foreach ($errors->all() as $error)
                    {{ $error }} <br>
                    @endforeach
                </div>

                <br />
            @endif
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}  
                </div>

                <br/>
            @elseif(session()->get('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>

                <br/>
            @endif
            <div class="page-header-actions">
                <!-- Form -->
                <form>
                    <div class="input-search input-search-dark mt-30">
                        <i class="input-search-icon md-search" aria-hidden="true"></i>
                        <!-- Input Search -->
                        <input type="text" class="form-control" name="" placeholder="Search...">
                        <!-- End Input Search -->
                    </div>
                </form>
                <!-- End Form -->
            </div>
        </div>
        <!-- End Contacts Content Header -->

        <!-- Contacts Content -->
        @if(Auth::user()->role == 'admin')
        <!-- Create Button -->          
        <button class="btn btn-victory-c ml-10 ml-md-30 mb-30 mt-30" type="button" data-target="#CRUDuser" data-toggle="modal" ">
            <i class="icon md-plus"></i>Create
        </button>
        <!-- End Create Button -->
        @endif

        @foreach($news as $key => $new)
        <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
            <!-- Contacts List Table -->
            <table class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                <tbody>
                    <!-- Table List -->
                    <tr>
                        <td class="pre-cell"></td>

                        <td class="cell-300">
                            <!-- Title -->
                            <h3 class="mt-0">{{ $new->title }}</h3>
                            <!-- End Title -->
                            <p>{!! substr($new->content, 0, 150) !!} ...</p>
                        </td>

                        <!-- Action -->
                        <td class="cell-50">
                            <!-- Button Remove -->
                            <a class="panel-action icon wb-trash" data-target="#Deletemodal{{ $new->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">
                            </a>
                            <!-- End Button Remove -->
                            <!-- Button Edit -->
                            <a class="panel-action icon wb-edit" data-target="#Editmodal{{ $new->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                            <!-- End Button Edit -->
                        </td>
                        <!-- End Action -->

                        <!-- Date -->
                        <td class="cell-300">
                            <p>{{ \Carbon\Carbon::parse($new->created_at)->diffForHumans() }}</p>
                        </td>
                        <!-- End Date -->

                        <td class="cell-100">
                            <a href="{{ route('news.show', $new->id) }}">
                                <button class="btn btn-victory-c">View</button>
                            </a>
                        </td>

                        <td class="suf-cell"></td>
                    </tr>
                    {{-- @endforeach --}}
                    <!-- End Table List -->
                </tbody>
            </table>
            <!-- End Contacts List Table -->

            <!-- Pagination List -->
            @if($news instanceof \Illuminate\Pagination\LengthAwarePaginator)
                {{ $news->links('pagination') }}
            @endif
            <!-- End Pagination List -->
        </div>
        @endforeach
    </div>
    <!-- End Contacts Content -->

    <!-- Create News Modal Boxes -->
    <div class="modal fade modal-fill-in" id="CRUDuser" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple" style="max-width: 100%">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                    <!-- Title -->
                    <h4 class="modal-title" id="exampleModalTabs">Create</h4>
                    <!-- End Title -->
                </div>
                <!-- Modal Header -->

                <!-- Modal Body-->
                <div class="modal-body">
                    <div class="tab-content">
                        <!-- Create Menu -->
                        <div class="tab-pane active" id="user" role="tabpanel">
                            <div class="row mt-20">
                                <div class="col-12">
                                    <!-- Create Form -->
                                    <div class="example-wrap">
                                        <div class="example">
                                            <form method="POST" action="{{ route('news.store') }}" autocomplete="off" enctype="multipart/form-data">
                                                <div class="row">
                                                    {{-- CSRF TOKEN --}}
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                    {{-- CSRF TOKEN --}}

                                                    <!-- Input Full Name -->
                                                    <div class="form-group form-material col-12">
                                                        <div class="example mb-20">
                                                            <input type="file" name="picture" id="input-file-now" data-plugin="dropify" data-default-file="Upload Images"/>
                                                        </div>
                                                        <label class="form-control-label" for="inputTitle">Title</label>
                                                        <input type="text" class="form-control" id="inputTitle" name="title" placeholder="Title" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Full Name -->
                                                </div>

                                                <!-- Input Content -->
                                                <div class="form-group form-material">
                                                    <label class="form-control-label" for="inputBasicEmail">Content</label>
                                                    <textarea rows="6" class="form-control" id="inputBasicEmail" name="content" placeholder="Content" autocomplete="off"></textarea>
                                                </div>
                                                <!-- End Input Content -->

                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Categories</label>
                                                    <select name="category" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="admin">News</option>
                                                            <option value="teacher">Achievement</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->

                                                <div class="form-group form-material">
                                                    <!-- Button Create -->
                                                    <button type="submit" class="btn btn-victory-c">Create</button>
                                                    <!-- End Button Create -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Create News Form -->
                                </div>
                            </div>
                        </div>
                        <!-- End Create News Menu -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Create News Modal Boxes -->
    
    {{ $news->links('pagination') }}

    @foreach($news as $key => $new)
    <!-- Delete Modal  -->
    <div class="modal fade" id="Deletemodal{{ $new->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <h4 class="modal-title" id="exampleModalLabel">
                        Are you sure want to delete?
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancel
                    </button>
                    <form action="{{ route('news.destroy', $new->id) }}" method="post">
                        {{-- CSRF TOKEN --}}
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="DELETE">
                        {{-- CSRF TOKEN --}}

                        <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete Modal  -->

    <!-- Edit Modal -->
    <div class="modal fade modal-fill-in" id="Editmodal{{ $new->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="exampleFillInModalTitle">Edit</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('news.update', $new->id) }}">
                        {{-- CSRF TOKEN --}}
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PATCH">
                        {{-- CSRF TOKEN --}}
                        <div class="row">
                            <!-- Input Full Name -->
                            <div class="form-group form-material col-12">
                                <div class="example mb-20">
                                    <input type="file" name="picture" id="input-file-now" data-plugin="dropify" data-default-file="{{ asset('/file_uploads/news/'.$new->picture) }}"/>
                                </div>

                                <label class="form-control-label" for="inputTitle">Title</label>

                                <input type="text" class="form-control" id="inputTitle" name="title" placeholder="Title" autocomplete="off" value="{{ $new->title }}" />
                            </div>
                            <!-- End Input Full Name -->
                        </div>

                        <!-- Input Content -->
                        <div class="form-group form-material">
                            <label class="form-control-label" for="inputBasicEmail">Content</label>

                            <textarea rows="6" class="form-control" id="inputBasicEmail" name="content" placeholder="Content" autocomplete="off">{{ $new->content }}</textarea>
                        </div>
                        <!-- End Input Content -->
                        <!-- Categories Option -->
                        <div class="example">
                            <label class="form-control-label">Categories</label>

                            <select name="category" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                <option></option>
                                <optgroup label="Please Select">
                                    <option value="news" <?php if($new->category == 'news') echo 'selected' ?>>News</option>
                                    <option value="achievement" <?php if($new->category == 'achievement') echo 'selected' ?>>Achievement</option>
                                </optgroup>
                            </select>
                        </div>
                        <!-- End Categories Option -->
                        <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Apply</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Edit Modal -->
    @endforeach
</div>
<!-- End Page -->
@endsection