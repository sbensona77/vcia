@extends('layouts.app-dashboard')

@section('content')
<!-- Edit Modal -->
    <div class="page">
          <div class="page-content container-fluid">
            @if ($errors->any())
                        <div class="alert alert-danger first">
                            @foreach ($errors->all() as $error)
                            {{ $error }} <br>
                            @endforeach
                        </div>

                        <br />
                    @endif

                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}  
                        </div>

                        <br/>
                    @elseif(session()->get('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                        
                        <br/>
                    @endif
              <div class="row">
                    <div class="col-12">
                        <form id="edit-form" method="POST" action="{{ route('news.update', $news->id) }}" enctype="multipart/form-data">
                            {{-- CSRF TOKEN --}}
                            <input form="edit-form" name="_token" type="hidden" value="{{ csrf_token() }}">
                            <input form="edit-form" name="_method" type="hidden" value="PATCH">
                            {{-- CSRF TOKEN --}}

                            <div class="row">
                                <div class="form-group form-material col-12">
                                    <div class="example mb-20">
                                        <input id="image_input" class="dropify" form="edit-form" type="file" name="picture" id="input-file-now" data-plugin="dropify" data-default-file="{{ asset('/file_uploads/news/'.$news->picture) }}" />
                                    </div>

                                    <label class="form-control-label" for="inputTitle">Title</label>

                                    <input form="edit-form" type="text" class="form-control" id="inputTitle" name="title" placeholder="Title" autocomplete="off" value="{{ $news->title }}" />
                                </div>
                            </div>

                            <!-- Input Content -->
                            <div class="form-group form-material">
                                <label class="form-control-label" for="inputBasicEmail">Content</label>

                                <textarea form="edit-form" rows="6" class="form-control" id="inputBasicEmail" name="content" placeholder="Content" autocomplete="off">{{ $news->content }}</textarea>
                            </div>
                            <!-- End Input Content -->

                            <!-- Categories Option -->
                            <div class="example">
                                <label class="form-control-label">Categories</label>

                                <select form="edit-form" name="category" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                    <option></option>
                                    <optgroup label="Please Select">
                                        <option value="news" <?php if($news->category == 'news') echo 'selected' ?>>News</option>
                                        <option value="achievement" <?php if($news->category == 'achievement') echo 'selected' ?>>Achievement</option>
                                    </optgroup>
                                </select>
                            </div>
                            <!-- End Categories Option -->

                            <button form="edit-form" class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Apply</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!-- End Edit Modal -->

@endsection