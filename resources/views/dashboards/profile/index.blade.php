@extends('layouts.app-dashboard')
@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-block-b">
                        <!-- Title List-->
                        <h4 class="card-title project-title">
                            <a class="link-c" href="dashboard-courses-full.html">
                            Profile - Vision Mision</a>
                            <!-- End Title List-->
                            <a class="panel-action icon wb-trash float-right" data-toggle="panel-close" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Remove" style="cursor: pointer"></a>
                            <a class="panel-action icon wb-edit float-right" data-target="#Editmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                        </h4>
                        <!-- Description List -->
                        <p class="card-text">
                            Description : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada dictum nulla eu commodo. Ut in nisl turpis. Nulla pellentesque faucibus arcu, rutrum fringilla urna eleifend a. Curabitur bibendum, quam in suscipit consequat, dolor velit pellentesque sapien, ut gravida ligula ipsum vitae libero.
                        </p>
                        <!-- End Description List-->
                        <!-- Button to view -->
                        <a href="view.blade.php" class="btn btn-victory-c">View</a>
                        <!-- End Button to view -->
                        <!-- Upload Date -->
                        <h5 class="title float-right"><i class="site-menu-icon md-time"></i>
                            <b>05/12/2018</b>
                        </h5>
                        <!-- End Upload Date -->
                    </div>
                </div>

                <div class="card">
                    <div class="card-block-b">
                        <!-- Title List-->
                        <h4 class="card-title project-title">
                            <a class="link-c" href="dashboard-courses-full.html">
                            Profile - Our Focus</a>
                            <!-- End Title List-->
                            <a class="panel-action icon wb-trash float-right" data-toggle="panel-close" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Remove" style="cursor: pointer"></a>
                            <a class="panel-action icon wb-edit float-right" data-target="#Editmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                        </h4>
                        <!-- Description List -->
                        <p class="card-text">
                            Description : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada dictum nulla eu commodo. Ut in nisl turpis. Nulla pellentesque faucibus arcu, rutrum fringilla urna eleifend a. Curabitur bibendum, quam in suscipit consequat, dolor velit pellentesque sapien, ut gravida ligula ipsum vitae libero.
                        </p>
                        <!-- End Description List-->
                        <!-- Button to view -->
                        <a href="view.blade.php" class="btn btn-victory-c">View</a>
                        <!-- End Button to view -->
                        <!-- Upload Date -->
                        <h5 class="title float-right"><i class="site-menu-icon md-time"></i>
                            <b>05/12/2018</b>
                        </h5>
                        <!-- End Upload Date -->
                    </div>
                </div>

                <div class="card">
                    <div class="card-block-b">
                        <!-- Title List-->
                        <h4 class="card-title project-title">
                            <a class="link-c" href="dashboard-courses-full.html">
                            Profile - Extras</a>
                            <!-- End Title List-->
                            <a class="panel-action icon wb-trash float-right" data-toggle="panel-close" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Remove" style="cursor: pointer"></a>
                            <a class="panel-action icon wb-edit float-right" data-target="#Editmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                        </h4>
                        <!-- Description List -->
                        <p class="card-text">
                            Description : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada dictum nulla eu commodo. Ut in nisl turpis. Nulla pellentesque faucibus arcu, rutrum fringilla urna eleifend a. Curabitur bibendum, quam in suscipit consequat, dolor velit pellentesque sapien, ut gravida ligula ipsum vitae libero.</a>
                        </p>
                        <!-- End Description List-->
                        <!-- Button to view -->
                        <a href="view.blade.php" class="btn btn-victory-c">View</a>
                        <!-- End Button to view -->
                        <!-- Upload Date -->
                        <h5 class="title float-right"><i class="site-menu-icon md-time"></i>
                            <b>05/12/2018</b>
                        </h5>
                        <!-- End Upload Date -->
                    </div>
                </div>
                
                <!-- Edit Modal -->
                <div class="modal fade modal-fill-in" id="Editmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="exampleFillInModalTitle">Edit</h4>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="row">
                                        <!-- Edit Title -->
                                        <div class="col-12 form-group">
                                            <textarea class="form-control" rows="1" placeholder="">Vision Mision</textarea>
                                        </div>
                                        <!-- Edit Title -->
                                        <!-- Edit Description -->
                                        <div class="col-xl-12">
                                            <textarea class="form-control" rows="5" placeholder="">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada dictum nulla eu commodo. Ut in nisl turpis. Nulla pellentesque faucibus arcu, rutrum fringilla urna eleifend a. Curabitur bibendum, quam in suscipit consequat, dolor velit pellentesque sapien, ut gravida ligula ipsum vitae libero.</textarea>
                                        </div>
                                        <!-- End Edit Description -->
                                    </div>
                                    <button class="btn btn-victory-c float-right mt-20 mt-sm-10" data-target="#examplePositionSidebar" data-toggle="modal" type="button">Apply</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Edit Modal -->
            </div>
        </div>
    </div>
</div>
@endsection