@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page">
  <div class="page-content container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-block-b">
          <!-- Title -->
          <h4 class="card-title project-title"><a class="link-c" href="dashboard-list.html">Profile</a> - Vision Mision
          <!-- End Title -->
                
          <a class="panel-action icon wb-trash float-right" data-toggle="panel-close" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Remove" style="cursor: pointer"></a>

          <a class="panel-action icon wb-edit float-right" data-target="#Editmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
          </h4>
           
          <!-- Full Description -->
          <p class="card-text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam magna orci, mollis aliquam placerat ac, imperdiet sit amet dolor. Maecenas metus eros, tempor quis libero in, ullamcorper aliquam est. In sollicitudin at ipsum lobortis suscipit. Phasellus a augue ligula. Donec dictum suscipit odio eu facilisis. Maecenas pellentesque, orci vulputate eleifend elementum, odio metus lobortis lectus, et rutrum odio enim ut nisi. Curabitur ut nisl at orci molestie suscipit sed ut elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed bibendum mauris felis, a laoreet ipsum auctor eget. In hac habitasse platea dictumst. Quisque at libero malesuada, dapibus lorem at, malesuada quam. Mauris ante est, vestibulum nec auctor sit amet, lacinia at lectus. Quisque efficitur, purus nec consectetur rutrum, lorem augue elementum justo, eu placerat nisl quam sed est. Fusce efficitur libero magna, at vulputate mi faucibus at.
          </p>
          <!-- End Full Description -->

          <!-- Upload Date -->
          <h5 class="title float-right"><i class="site-menu-icon md-time"></i>
          <b>05/12/2018</b></h5>
          <!-- End Upload Date -->
        </div>
      </div>
    </div>
    

    <!-- Edit Modal -->
    <div class="modal fade modal-fill-in" id="Editmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-simple">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="exampleFillInModalTitle">Edit</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="row">
                <!-- Edit Title -->
                <div class="col-12 form-group">
                  <textarea class="form-control" rows="1" placeholder="">Vision Mision</textarea>
                </div>
                <!-- Edit Title -->
                                
                <!-- Edit Description -->
                <div class="col-xl-12">
                  <textarea class="form-control" rows="5" placeholder="">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada dictum nulla eu commodo. Ut in nisl turpis. Nulla pellentesque faucibus arcu, rutrum fringilla urna eleifend a. Curabitur bibendum, quam in suscipit consequat, dolor velit pellentesque sapien, ut gravida ligula ipsum vitae libero.</textarea>
                </div>
                <!-- End Edit Description -->
              </div>

              <button class="btn btn-victory-c float-right mt-20 mt-sm-10" data-target="#examplePositionSidebar" data-toggle="modal" type="button">Apply</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- End Edit Modal -->
    </div>
  </div>
</div>
@endsection