@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page bg-white">
    <!-- Contacts Content -->
    <div class="page-main">
        <!-- Contacts Content Header -->
        <div class="page-header">
            @if ($errors->any())
                <div class="alert alert-danger first">
                    @foreach ($errors->all() as $error)
                    {{ $error }} <br>
                    @endforeach
                </div>

                <br />
            @endif
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}  
                </div>

                <br/>
            @elseif(session()->get('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>

                <br/>
            @endif
            <div class="page-header-actions">

                <!-- Form -->
                <form method="GET" action="{{ route('staffs-managements.search') }}">
                    <div class="input-search input-search-dark mt-50">
                        {{ csrf_field() }}

                        <!-- Input Search -->
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <!-- End Input Search -->
                    </div>
                </form>
                <!-- End Form -->

            </div>
        </div>
        <!-- End Contacts Content Header -->

        <!-- Contacts Content -->
        @if(Auth::user()->role == 'admin' && Route::is('staffs-managements.index'))
            <!-- Create Button -->          
            <button class="btn btn-victory-c ml-10 ml-md-30 mb-30 mt-30" type="button" data-target="#CRUDuser" data-toggle="modal">
            <i class="icon md-plus"></i>Create
            </button>
            <!-- End Create Button -->
        @endif
        
        <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
            <!-- Contacts List Table -->
            <table class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                <thead>
                    <tr>
                        <th class="pre-cell"></th>
                        <!-- Table Header -->
                        <th class="cell-300" scope="col">Staff</th>

                        <th class="cell-150" scope="col">Title</th>

                        <th class="cell-300" scope="col">Description</th>
                        
                        <th class="cell-100" scope="col">Deadline</th>

                        <th class="cell-100" scope="col">Status</th>

                        @if(Auth::user()->role == 'admin' && Route::is('staffs-managements.index'))
                            <th class="cell-100" scope="col">Action</th>
                        @endif
                        
                        <th class="suf-cell"></th>
                        <!-- End Table Header -->
                    </tr>
                </thead>
                <tbody>
                    <!-- Table List -->
                    @foreach($managements as $key => $management)
                    <tr>
                        <td class="pre-cell"></td>
                        <!-- Name -->
                        <td class="cell-300">
                            <a class="link-c" href="{{ route('user_profile.show', $management->staff->id) }}" target="_blank">
                                {{ $management->staff->name }}
                            </a>
                        </td>
                        <!-- End Name -->

                        <!-- Title -->
                        <td class="cell-150">
                            {{ $management->title }}
                        </td>
                        <!-- End Title -->

                        <!-- Description -->
                        <td class="cell-300">
                           {!! $management->description !!}
                        </td>
                        <!-- End Description -->

                        <!-- Deadline -->
                        <td class="cell-100">
                            {{ Carbon\Carbon::parse($management->deadline)->format('M d, Y') }}
                        </td>
                        <!-- End Deadline -->

                        <!-- Status -->
                        <td class="cell-100">
                            @if($management->status == 'ongoing')
                                <span class="badge badge-info">On Going</span>

                                @if($management->deadline <= Carbon\Carbon::now())
                                    <span class="badge badge-warning">Overdue</span>
                                @endif
                            @elseif($management->status == 'done' || $management->status == 'finish')
                                <span class="badge badge-success">Finished</span>
                            @else
                                <span class="badge badge-danger">Canceled</span>
                            @endif
                        </td>
                        <!-- End Status -->

                        <!-- Action -->
                        @if(Auth::user()->role == 'admin' && Route::is('staffs-managements.index'))
                        <td class="cell-100">
                            <a class="panel-action icon wb-trash" data-target="#Deletemodal{{ $management->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer"></a>

                            <a class="panel-action icon wb-edit" data-target="#Editmodal{{ $management->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                        </td>
                        @endif
                        <!-- End Action -->

                        <td class="suf-cell"></td>
                    </tr>
                    @endforeach
                    <!-- End Table List -->
                </tbody>
            </table>
            <!-- End Contacts List Table -->
        </div>
    </div>
    <!-- End Contacts Content -->
    
    @if(Auth::user()->role == 'admin' && Route::is('staffs-managements.index'))
        <!-- Create User Modal Boxes -->
        <div class="modal fade modal-fill-in" id="CRUDuser" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple" style="max-width: 100%">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <!-- Title -->
                        <h4 class="modal-title" id="exampleModalTabs">Create</h4>
                        <!-- End Title -->
                    </div>
                    <!-- Modal Header -->

                    <!-- Modal Body-->
                    <div class="modal-body">
                        <div class="tab-content">
                            <!-- Create User Menu -->
                            <div class="tab-pane active" id="user" role="tabpanel">
                                <div class="row mt-20">
                                    <div class="col-12">
                                        <!-- Create User Form -->
                                        <div class="example-wrap">
                                            <div class="example">
                                                <form method="POST" action="{{ route('staffs-managements.store') }}">
                                                {{ csrf_field() }}

                                                <div class="row">
                                                    <!-- Create Title -->
                                                    <div class="col-12 form-group">
                                                        Staff Name

                                                        <select name="staff_id" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                            <option></option>
                                                            <optgroup label="Please Select">
                                                                @foreach($staffs as $key => $staff)
                                                                <option value="{{ $staff->id }}">{{ $staff->name }}</option>
                                                                @endforeach
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                    <!-- End Create Title -->
                                                    
                                                    <div class="col-12 form-group">
                                                        Job Title

                                                        <input type="text" class="form-control" name="title" placeholder="Title" value="">
                                                    </div>

                                                    <div class="col-12 form-group">
                                                        Description

                                                        <textarea class="form-control" name="description" placeholder="Description" value="" style="height: 120px"></textarea>
                                                    </div>

                                                    <div class="col-12 col-md-6 form-group">
                                                        Deadline

                                                        <input type="date" class="form-control" name="deadline" placeholder="Deadline" value="">
                                                    </div>

                                                    <div class="col-12 col-md-6 form-group">
                                                        Status

                                                        <select name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                            <option></option>
                                                            <optgroup label="Please Select">
                                                                <option value="ongoing" selected>On Going</option>
                                                                <option value="canceled">Canceled</option>
                                                                <option value="finish">Finish</option>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>

                                                <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Apply</button>
                                            </form>
                                            </div>
                                        </div>
                                        <!-- End Create User Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Create User Menu -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Create User Modal Boxes -->

        @foreach($managements as $management)
            <!-- Edit Modal -->
            <div class="modal fade modal-fill-in" id="Editmodal{{ $management->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-simple">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFillInModalTitle">Edit</h4>
                        </div>

                        <div class="modal-body">
                            <form method="POST" action="{{ route('staffs-managements.update', $management->id) }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PATCH">

                                <div class="row">
                                    <!-- Create Title -->
                                    <div class="col-12 form-group">
                                        Staff Name

                                        <select name="staff_id" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                            <option></option>
                                            <optgroup label="Please Select">
                                                @foreach($staffs as $key => $staff)
                                                <option value="{{ $staff->id }}" <?php if($staff->id == $management->staff_id) echo 'selected'; ?>>{{ $staff->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                    <!-- End Create Title -->
                                    
                                    <div class="col-12 form-group">
                                        Job Title

                                        <input type="text" class="form-control" name="title" placeholder="Title" value="{{ $management->title }}">
                                    </div>

                                    <div class="col-12 form-group">
                                        Description

                                        <textarea class="form-control" name="description" placeholder="Description" style="height: 120px">{{ $management->description }}</textarea>
                                    </div>

                                    <div class="col-12 col-md-6 form-group">
                                        Deadline

                                        <input type="date" class="form-control" name="deadline" placeholder="Deadline" value="{{ \Carbon\Carbon::parse($management->deadline)->format('Y-m-d') }}">
                                    </div>

                                    <div class="col-12 col-md-6 form-group">
                                        Status

                                        <select name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                            <option></option>
                                            <optgroup label="Please Select">
                                                <option value="ongoing" <?php if($management->status == 'ongoing') echo 'selected'; ?>>On Going</option>
                                                <option value="canceled" <?php if($management->status == 'canceled') echo 'selected'; ?>>Canceled</option>
                                                <option value="finish" <?php if($management->status == 'finish') echo 'selected'; ?>>Finish</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <button class="btn btn-victory-c float-right mt-20 mt-sm-10" type="submit">Apply</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Edit Modal -->

            <!-- Delete Modal  -->
            <div class="modal fade" id="Deletemodal{{ $management->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-simple">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <h4 class="modal-title" id="exampleModalLabel">
                                Are you sure want to delete?
                            </h4>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Cancel
                            </button>

                            <form method="POST" action="{{ route('staffs-managements.destroy', $management->id) }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">

                                <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Delete Modal  -->
        @endforeach
    @endif
</div>
<!-- End Page -->
@endsection