@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page">
    <div class="page-content container-fluid">
        @if ($errors->any())
            <div class="alert alert-danger first">
                @foreach ($errors->all() as $error)
                {{ $error }} <br>
                @endforeach
            </div>

            <br />
        @endif

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}  
            </div>

            <br/>
        @elseif(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>

            <br/>
        @endif

        <div class="row">
            <div class="col-12">
                @php
                    $nis = $data->nis;
                    $nip = $data->nip;

                    $phone = $data->phone;
                    $birth_date = $data->birth_date;
                @endphp

                @if(Auth::user()->role == 'admin' || Auth::user()->id == $user->id)
                <!-- Edit Profile Button -->            
                <button class="btn btn-victory-c mb-30" type="button" data-target="#EditProfile" data-toggle="modal">
                    <i class="icon md-plus"></i>
                    Edit Profile
                </button>
                <!-- End Edit Profile Button -->

                <a target="_blank" href="{{ route('print-user-profile', $user->id) }}" class="btn btn-victory-c mb-30">
                    <i class="icon md-print"></i>
                    Print
                </a>
                @endif
            </div>

            <!-- Page Widget -->
            <div class="col-lg-3">
                <!-- Profile Timeline (Photos,Socmed) -->
                <div class="card card-shadow text-center">
                    <div class="card-block-b">
                        <a class="avatar avatar-lg" href="javascript:void(0)">
                            <img src="{{ asset('file_uploads/users/'.$data->profile_picture) }}">
                        </a>

                        <h4 class="profile-user">{{ $user->name }}</h4>

                        @if($user->role == 'student')
                        <p>
                            @if($data->level < 2)  ABC
                            @elseif($data->level <= 3) Young Warrior
                            @elseif($data->level <= 6) Warrior
                            @elseif($data->level <= 9) Young Victors
                            @else Victors 
                            @endif

                            - {{ $data->level }}
                        </p>
                        @endif

                        @if($user->role != 'student' && $user->role != 'parent')
                            @if($courses->find($data->subject))
                            <p class="profile-job">{{ $data->subject_name->name }} Teacher</p>
                            @else
                            <p class="profile-job">Teacher</p>
                            @endif

                            <p class="profile-job">{{ $data->nip }}</p>
                            <p class="profile-job">{{ \Carbon\Carbon::parse($data->birth_date)->format('M d, Y') }}</p>
                        @else
                            <p class="profile-job">{{ $data->nis }}</p>
                            <p class="profile-job">{{ \Carbon\Carbon::parse($data->birth_date)->format('M d, Y') }}</p>
                        @endif

                        <div class="profile-social">
                            <a href="#" data-content="{{ $data->phone }}" data-trigger="hover" data-toggle="popover" tabindex="0" title="Phone Number">
                                <i class="site-menu-icon-b md-phone" aria-hidden="true"></i>
                            </a>

                            <a href="#" data-content="{{ $user->email }}" data-trigger="hover" data-toggle="popover" tabindex="0" title="Email">
                                <i class="site-menu-icon-b md-account-box-mail" aria-hidden="true"></i>
                            </a>
                        </div>
                        
                        @if($user->role == 'student' && Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                        {{-- LEVEL UP BUTTON --}}
                        <form method="POST" action="{{ route('students.level_up', $user->id) }}">
                            {{-- CSRF TOKEN --}}
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" value="PATCH" name="_method" />
                            {{-- CSRF TOKEN --}}

                            <button class="btn btn-success mt-20" type="submit">Level Up</button>
                        </form>
                        {{-- END LEVEL UP BUTTON --}}

                        {{-- LEVEL DOWN BUTTON --}}
                        <form method="POST" action="{{ route('students.level_down', $user->id) }}">
                            {{-- CSRF TOKEN --}}
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" value="PATCH" name="_method" />
                            {{-- CSRF TOKEN --}}

                            <button class="btn btn-danger mt-10" type="submit">Level Down</button>
                        </form>
                        {{-- END LEVEL DOWN BUTTON --}}
                        @endif

                        
                        @if(Auth::user()->role == 'admin')
                        <!-- Button Remove -->
                        <button class="btn btn-victory-c mt-10" data-target="#Deletemodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">Delete User
                        </button>
                        <!-- End Button Remove -->
                        @endif
                    </div>
                </div>
                <!-- Profile Timeline (Photos,Socmed) -->
            </div>
            <!-- End Page Widget -->
            

            @if($user->role == 'parent')
            <div class="col-lg-9">
                <div class="panel">
                    <div class="panel-body">
                        <!-- Contacts Content Header -->
                        <div class="page-header">
                            <!-- Title-->
                            <h1 class="page-title">Child List</h1>
                            <!-- End Title -->
                        </div>
                        <!-- End Contacts Content Header -->
                        
                        @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                        <!-- Create Button -->          
                        <button class="btn btn-victory-c ml-30 mb-30" type="button" data-target="#CRUDuser" data-toggle="modal">
                        <i class="icon md-plus"></i>Add
                        </button>
                        <!-- End Create Button -->
                        @endif
                    
                        <!-- Contacts Content -->            
                        <div id="contactsContent" class="page-content page-content-table" data-plugin="selectable">
                            <!-- Contacts List Table -->
                            <table class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <thead>
                                    <tr>
                                        <th class="pre-cell"></th>
                                        <!-- Table Header -->
                                        <th class="cell-300" scope="col">Name</th>

                                        @if(Route::is('students.index'))
                                        <th class="cell-30" scope="col">Level</th>
                                        @endif

                                        <th class="cell-300" scope="col">Email</th>
                                        <th class="cell-30" scope="col">Status</th>

                                        @if(Route::is('students.index'))
                                        <th class="cell-30" scope="col">Book</th>
                                        @endif

                                        <th class="cell-30" scope="col"></th>

                                        <th class="suf-cell"></th>
                                        <!-- End Table Header -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Table List -->
                                    @foreach($data->relations as $key => $relation)
                                    <tr>
                                        <td class="pre-cell"></td>
                                        <!-- Name -->
                                        <td class="cell-300">
                                            <a class="avatar" href="javascript:void(0)" style="vertical-align: middle">
                                            <img class="img-fluid" src="{{ asset('file_uploads/users/'.$relation->child->profile_picture) }}">
                                            </a>
                                            {{ $relation->child->name }}
                                        </td>
                                        <!-- End Name -->

                                        @if(Route::is('students.index'))
                                        <!-- Level -->
                                        <td class="cell-30">
                                            @if($user->profile->level <= 2) ABC
                                            @elseif($user->profile->level <= 3) Young Warrior
                                            @elseif($user->profile->level <= 6) Warrior
                                            @elseif($user->profile->level <= 9) Young Victors
                                            @elseif($user->profile->level == 13) Graduated
                                            @else Victors 
                                            @endif

                                            -

                                            {{ $relation->child->profile->level }}
                                        </td>
                                        <!-- End Level -->
                                        @endif

                                        <!-- Email -->
                                        <td class="cell-300">
                                            {{ $relation->child->email }}
                                        </td>
                                        <!-- End Email -->

                                        <!-- Status -->
                                        <td class="cell-30">
                                            @if($relation->child->user->role == 'admin') Admin
                                            @elseif($relation->child->user->role == 'teacher') Teacher
                                            @else Student
                                            @endif
                                        </td>
                                        <!-- End Status -->

                                        @if(Route::is('students.index'))
                                        <!-- Book -->
                                        <td class="cell-30">
                                            <span class="badge badge-success">Assigned</span>
                                        </td>
                                        <!-- Book -->
                                        @endif

                                        <td class="cell-30">
                                            <a href="{{ route('user_profile.show', $relation->child->user_id) }}">
                                                <button class="btn btn-victory-c">Profile</button>
                                            </a>
                                        </td>
                                        <td class="suf-cell"></td>
                                    </tr>
                                    @endforeach
                                    <!-- End Table List -->
                                </tbody>
                            </table>
                            <!-- End Contacts List Table -->
                        </div>
                        <!-- End Contacts Content -->
                    </div>
                </div>
            </div>
            @endif

            <!-- Panel -->
            <div class="col-lg-9">

            @if($user->role == 'student')
            <!-- Activities Timeline -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">

                    {{-- Student Assigned PACE --}}
                    @if($user->role == 'student')
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" role="tab">Assigned PACE</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="page-content page-content-table tab-pane animation-slide-left active" id="contactsContent" role="tabpanel">
                            {{-- Table List --}}
                            <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <thead>
                                    <tr>
                                        <th>Courses</th>
                                        <th>PACE Start</th>
                                        <th>PACE Finish</th>

                                        @if(Auth::user()->role != 'parent')
                                        <th>Actions</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                                    <tr class="gradeA">
                                        <form id="inputForm" method="POST" action="{{ route('assign-material.store') }}">
                                            {{-- CSRF TOKEN --}}
                                            {{ csrf_field() }}
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                            {{-- CSRF TOKEN --}}

                                            <input type="hidden" name="student_id" value="{{ $user->id }}">
                                            <td>
                                                {{-- Select Option --}}
                                                <select onchange="populatePaceSelect()" id="select_course" name="course_id" form="inputForm" class="form-control" data-plugin="select2" data-placeholder="Please Select" data-allow-clear="true">
                                                    <option></option>

                                                    <optgroup id="option_course" form="inputForm" label="Please Select">
                                                        @foreach($courses as $key => $course)
                                                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                </select>
                                                {{-- End Select Option --}}
                                            </td>

                                            <td>
                                                {{-- Select Option --}}
                                                <select id="select_pace_start" name="pace_start" form="inputForm" class="form-control" data-plugin="select2" data-placeholder="Please Select" data-allow-clear="true">
                                                    <option></option>
                                                </select>
                                                {{-- End Select Option --}}
                                            </td>

                                            <td>
                                                {{-- Select Option --}}
                                                <select id="select_pace_end" name="pace_end" form="inputForm" class="form-control" data-plugin="select2" data-placeholder="Please Select" data-allow-clear="true">
                                                    <option></option>
                                                </select>
                                                {{-- End Select Option --}}
                                            </td>
                                            
                                            @if($user->role != 'parent')
                                            <td class="actions">
                                                {{-- Assigned Button --}}
                                                <button form="inputForm" class="btn btn-victory-c" type="submit">
                                                    <i class="icon md-plus" aria-hidden="true"></i> Assign
                                                </button>
                                                {{-- End Assigned Button --}}
                                            </td>
                                            @endif
                                        </form>
                                    </tr>
                                    @endif

                                    {{-- RESULTS --}}
                                    @foreach($assigns as $key => $assign)
                                    <tr class="gradeA">
                                        <td>
                                            {{ $assign->course->name }}
                                        </td>
                                        <td>
                                            {{ $assign->material_start->name }}
                                        </td>
                                        <td>
                                            {{ $assign->material_end->name }}
                                        </td>
                                        
                                        @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                                        <td class="actions">
                                           
                                            <!-- Button Edit -->
                                            <a class="panel-action icon wb-edit mr-5" data-target="#Edit{{ $assign->id }}modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                                            <!-- End Button Edit -->
                                           
                                            <!-- Button Remove -->
                                            <a class="panel-action icon wb-trash mr-5" data-target="#Delete<?php echo $assign->id ?>modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">
                                            </a>
                                            <!-- End Button Remove -->
                                            
                                            <!-- Button List -->
                                            <a class="panel-action icon wb-pencil" data-target="#List{{ $assign->id }}modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Score" style="cursor: pointer">
                                            </a>
                                            <!-- End Button List -->
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    {{-- RESULTS --}}
                                </tbody>
                            </table>
                            {{-- Table List --}}
                        </div>
                    </div>
                    @endif
                    {{-- End Student Assigned PACE --}}
                </div>
            </div>
            <!-- Activities Timeline -->
            @endif
            

            @if(Auth::user()->role != 'parent' && $user->role != 'parent')
            <!-- Purchased Items -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">

                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" role="tab">Purchased Items</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="page-content page-content-table tab-pane animation-slide-left active" id="contactsContent" role="tabpanel">
                            {{-- Table List --}}
                             <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <thead>
                                    <tr>
                                        <th>Item Category</th>
                                        <th>Item Name</th>
                                        <th>Item Code</th>
                                        <th>Last Updated</th>

                                        @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                            <th>Actions</th>
                                        @endif
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                    <tr class="gradeA">
                                        <form id="borrow-item" method="POST" action="{{ route('borrow', $user->id) }}">
                                            {{-- CSRF TOKEN --}}
                                            {{ csrf_field() }}
                                            <input type="hidden" value="PATCH" name="_method" />
                                            {{-- CSRF TOKEN --}}

                                            <script type="text/javascript">
                                                function populateItem()
                                                {
                                                    var id = $('#borrow-item-select option:selected').val();

                                                    $.ajax({
                                                        url: '/dashboard/inventories/' + id + '/get-items',
                                                        type: 'GET',
                                                        cache: false,
                                                        dataType: 'JSON',
                                                        success: function(data) {
                                                            console.log('populating...');

                                                            var results = $.parseJSON(JSON.stringify(data));

                                                            $("#borrowable-items").empty();

                                                            var openingOption = "<option>Please Select</option>" + "<option>-------------------------------</option>";

                                                            $("#borrowable-items").append(openingOption);

                                                            $.each($.parseJSON(results.items), function(index, value) {
                                                                var itemID = value.id;
                                                                var itemName = value.inventory_name;

                                                                var option = "<option " + "value=" + "'" + itemID + "'" + ">" +  itemName + "</option>";

                                                                $("#borrowable-items").append(option);
                                                            });
                                                        }
                                                    });
                                                }

                                                function populateCode()
                                                {
                                                    var inventory_id = $('#borrowable-items option:selected').val();

                                                    $.ajax({
                                                        url: '/dashboard/inventories/' + inventory_id + '/get-items-code',
                                                        type: 'GET',
                                                        cache: false,
                                                        dataType: 'JSON',
                                                        success: function(data) {
                                                            console.log('populating...');

                                                            console.log(data);

                                                            var results = $.parseJSON(JSON.stringify(data));

                                                            console.log(results);

                                                            $("#borrowable-items-code").empty();

                                                            var openingOption = "<option>Please Select</option>" + "<option>-------------------------------</option>";

                                                            $("#borrowable-items-code").append(openingOption);

                                                            $.each($.parseJSON(results.codes), function(index, value) {
                                                                var itemID = value.id;
                                                                var itemName = value.code;

                                                                var option = "<option " + "value=" + "'" + itemID + "'" + ">" +  itemName + "</option>";

                                                                console.log(option);

                                                                $("#borrowable-items-code").append(option);
                                                            });
                                                        }
                                                    });
                                                }
                                            </script>

                                            <td>
                                                {{-- Select Option --}}
                                                <select form="borrow-item" name="inventory_categories" onchange="populateItem()" id="borrow-item-select" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                    <option>Please Select</option>
                                                    <option>-------------</option>
                                                    
                                                    @foreach($inventories as $key => $inventory)
                                                        <option value="{{ $inventory->id }}">
                                                            {{ $inventory->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                {{-- End Select Option --}}
                                            </td>
                                            
                                            {{-- Item Code --}}
                                            <td>
                                                {{-- Select Option --}}
                                                <select form="borrow-item" name="item_name" id="borrowable-items" onchange="populateCode()" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                    <option>Please Select</option>
                                                    <option>-------------</option>
                                                    
                                                </select>
                                                {{-- End Select Option --}}
                                            </td>
                                            {{-- End Item Code --}}

                                            <td>
                                                {{-- Select Option --}}
                                                <select form="borrow-item" name="item" id="borrowable-items-code" onchange="" form="grant-point-form" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                    
                                                </select>
                                                {{-- End Select Option --}}
                                            </td>

                                            <td>
                                                
                                            </td>
                                            
                                            <td class="actions">
                                                {{-- Borrow Button --}}
                                                <button form="borrow-item" class="btn btn-victory-c" type="submit">
                                                Borrow
                                                </button>
                                                {{-- End Borrow Button --}}
                                            </td>
                                        </form>
                                    </tr>
                                    @endif

                                    {{-- RESULTS --}}
                                    @foreach($borrows as $key => $borrow)
                                    <tr>
                                        <form id="return-item{{ $borrow->id }}" action="{{ route('return', $user->id) }}" method="POST">
                                            {{-- CSRF TOKEN --}}
                                            {{ csrf_field() }}
                                            <input type="hidden" value="PATCH" name="_method" />
                                            {{-- CSRF TOKEN --}}

                                            <td>
                                                {{ $borrow->inventory->category->name }}
                                            </td>

                                            <td>
                                                {{ $borrow->inventory->inventory_name }}
                                            </td>

                                            <td>
                                                {{ $borrow->code }}
                                            </td>

                                            <td>
                                                {{ \Carbon\Carbon::parse($borrow->updated_at)->format('[M d, Y] H:i:s') }}
                                            </td>

                                            @if(Auth::user()->role != 'student' || Auth::user()->role != 'parent')
                                                <td class="actions">
                                                    {{-- Borrow Button --}}
                                                    <button form="return-item{{ $borrow->id }}" class="btn btn-victory-c" type="submit">
                                                    Return
                                                    </button>
                                                    {{-- End Borrow Button --}}
                                                </td>
                                            @endif
                                        </form>
                                    </tr>
                                    @endforeach
                                    {{-- RESULTS --}}
                                </tbody>
                            </table>
                            {{-- Table List --}}
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <!-- End Borrowed Items -->


            @if($user->role != 'student' && $user->role != 'parent')
            <!-- Job List -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">

                    {{-- Job List --}}
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" role="tab">Job List</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="page-content page-content-table tab-pane animation-slide-left active" id="contactsContent" role="tabpanel">
                            {{-- Table List --}}
                             <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <thead>
                                    <tr>
                                        <th>Job Title</th>
                                        <th>Job Description</th>
                                        <th>Status</th>
                                        <th>Deadline</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($tasks as $task)
                                    <tr>
                                        <td>
                                            {{ $task->title }}
                                        </td>
                                        <td>
                                            {{ $task->description }}
                                        </td>
                                        <!-- Status -->
                                        <td class="cell-100">
                                            @if($task->status == 'ongoing')
                                                <span class="badge badge-info">On Going</span>

                                                @if($task->deadline <= Carbon\Carbon::now())
                                                    <span class="badge badge-warning">Overdue</span>
                                                @endif
                                            @elseif($task->status == 'done' || $task->status == 'finish')
                                                <span class="badge badge-success">Finished</span>
                                            @else
                                                <span class="badge badge-danger">Canceled</span>
                                            @endif
                                        </td>
                                        <!-- End Status -->
                                        <td>
                                            {{ \Carbon\Carbon::parse($task->deadline)->format('M d, Y') }}
                                        </td> 
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if($user->role == 'student')
            <!-- Weekly Report -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">

                    {{-- Student Weekly Achievement--}}
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" role="tab">Weekly Report</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="page-content page-content-table tab-pane animation-slide-left active" id="contactsContent" role="tabpanel">
                            <button class="btn btn-victory-c mb-20" data-target="#Historymodal" data-toggle="modal">History</button>

                            @php
                                //creating start week and end week
                                $now = Carbon\Carbon::now();
                                $start_week = $now->copy()->startOfWeek();
                                $end_week = $now->copy()->endOfWeek();    
                            @endphp

                            <h5>Week Start : {{ $start_week->format('M d, Y') }}</h5>
                            <h5>Week End : {{ $end_week->format('M d, Y') }}</h5>

                            {{-- Table List --}}
                            <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <thead>
                                    <tr>
                                        <th>Courses</th>
                                        <th>PACE</th>
                                        <th>Grade</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(Auth::user()->role !='student')
                                        @foreach($reports->where('start_week', $start_week)->where('end_week', $end_week)->where('confirmed', 0)->sortBy('course_id') as $key => $report)
                                            <tr class="gradeA">
                                                <td>
                                                    {{ $report->course->name }}
                                                </td>

                                                <td>
                                                    {{ $report->material->name }}
                                                </td>

                                                <td>
                                                   {{ $report->grade }}
                                                </td>

                                                <td>
                                                   @if($report->confirmed == 1) CONFIRMED
                                                   @elseif($report->confirmed == 0) NOT CONFIRMED
                                                   @else LATE CONFIRMATION
                                                   @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            {{-- Table List --}}
                            
                            @if(Auth::user()->role != 'student')
                            <form id="confirmForm" action="{{ route('confirm-report', $user->id) }}" method="POST">
                                {{-- CSRF TOKEN --}}
                                {{ csrf_field() }}
                                <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                <input type="hidden" value="PATCH" name="_method" />
                                {{-- CSRF TOKEN --}}

                                <input form="confirmForm" type="hidden" name="start_week" value="{{ $start_week }}">

                                <input form="confirmForm" type="hidden" name="end_week" value="{{ $end_week }}">
                            </form>

                                <button form="confirmForm" class="btn btn-victory-c ml-10 mb-20 float-right">Confirm</button>
                            @endif
                        </div>
                    </div> 
                    {{-- End Student Assigned PACE --}}
                </div>
            </div>
            <!-- End Weekly Report -->


            @if($user->role == 'student')
            <!-- Extracurricular Report -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                    
                    {{-- Student Weekly Achievement--}}
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" role="tab">Extracurricular Report</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="page-content page-content-table tab-pane animation-slide-left active" id="contactsContent" role="tabpanel">
                            {{-- Table List --}}
                            <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <thead>
                                    <tr>
                                        <th>Extracurricular</th>
                                        <th>Grade</th>

                                        @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                        <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(Auth::user()->role != 'parent')
                                        <form id="assign_club_form" method="POST" action="{{ route('assigned-club.store') }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <input type="hidden" name="student_id" value="{{ $user->id }}">

                                            <tr class="gradeA">
                                                <td>
                                                    {{-- Select Option --}}
                                                    <select form="assign_club_form" name="club_id" class="form-control" data-plugin="select2" data-placeholder="Please Select" data-allow-clear="true">
                                                        <option></option>

                                                        @foreach($clubs as $key => $club)
                                                            <option value="{{ $club->id }}">
                                                                {{ $club->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>   
                                                    {{-- End Select Option --}}
                                                </td>

                                                <td></td>
                                                
                                                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                                <td class="actions">
                                                    {{-- Assigned Button --}}
                                                    <button form="assign_club_form" class="btn btn-victory-c" type="submit">
                                                        <i class="icon md-plus" aria-hidden="true"></i> Assign
                                                    </button>
                                                    {{-- End Assigned Button --}}
                                                </td>
                                                @endif
                                            </tr>
                                        </form>
                                    @endif

                                    @foreach($assigned_clubs as $key => $assigned_club)
                                    <tr class="gradeA">
                                        <form method="POST" action="{{ route('assigned-club.update', $assigned_club->id) }}">

                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="PATCH">

                                            <input type="hidden" name="student_id" value="{{ $user->id }}">

                                            <td>
                                                {{ $assigned_club->club->name }}
                                            </td>

                                            <td>
                                                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                                    @if($assigned_club->grade)
                                                        <input type="number" name="grade" class="form-control" value="{{ $assigned_club->grade }}" placeholder="Grade">
                                                    @else
                                                        <input type="number" name="grade" class="form-control" placeholder="Grade">
                                                    @endif
                                                @else
                                                    @if($assigned_club->grade)
                                                        {{ $assigned_club->grade }}
                                                    @endif
                                                @endif
                                            </td>
                                            
                                            @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                                            <td>
                                                {{-- Add Button --}}
                                                <button class="btn btn-victory-c mr-10" type="submit">
                                                    Apply
                                                </button>
                                                {{-- End Add Button --}}
                                               
                                                <!-- Button Remove -->
                                                <a class="panel-action icon wb-trash mr-5" data-target="#DeleteExtramodal{{ $assigned_club->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">
                                                </a>
                                                <!-- End Button Remove -->
                                            </td>
                                            @endif
                                        </form>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- Table List --}}
                        </div>
                    </div> 
                    {{-- End Student Assigned PACE --}}
                </div>
            </div>
            <!-- End Extracurricular Report -->
            @endif
            

            @if($user->role == 'student')
            <!-- Bible Memory Report -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                    
                    {{-- Student Weekly Achievement--}}
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" role="tab">Bible Memory</a>
                        </li>
                    </ul>

                    @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                        <button class="btn btn-victory-c mb-20" data-target="#Biblemodal" data-toggle="modal">Create New</button>
                    @endif

                    <div class="tab-content">
                        <div class="page-content page-content-table tab-pane animation-slide-left active" id="contactsContent" role="tabpanel">
                            {{-- Table List --}}
                            <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <thead>
                                    <tr>
                                        <th>Month</th>
                                        <th>Bible Verse</th>

                                        @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                                            <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(Auth::user()->role != 'parent')
                                        <form method="POST" action="{{ route('bible-memories.store') }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            <input type="hidden" name="student_id" value="{{ $user->id }}">

                                            <tr class="gradeA">
                                                <td class="cell-150">
                                                    {{-- Select Option --}}
                                                    <select name="month" class="form-control" data-plugin="select2" data-placeholder="Please Select" data-allow-clear="true">
                                                        <option></option>

                                                        @for($monthNum = 1; $monthNum <= 12; $monthNum++)
                                                            <option value="{{ $monthNum }}">
                                                                {{ \Carbon\Carbon::createFromFormat('m', $monthNum)->format('M') }}
                                                            </option>
                                                        @endfor
                                                    </select>   
                                                    {{-- End Select Option --}}
                                                </td>

                                                <td class="cell-300">
                                                    <input type="text" name="bible_verse" class="form-control">
                                                </td>
                                                
                                                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                                <td class="actions">
                                                    {{-- Assigned Button --}}
                                                    <button class="btn btn-victory-c" type="submit">
                                                        <i class="icon md-plus" aria-hidden="true"></i> Assign
                                                    </button>
                                                    {{-- End Assigned Button --}}
                                                </td>
                                                @endif
                                            </tr>
                                        </form>
                                    @endif

                                    @foreach($user->bible_memories as $key => $memory)
                                        <tr>
                                            <td>
                                                {{ \Carbon\Carbon::createFromFormat('m', $memory->month)->format('M') }}
                                            </td>

                                            <td>
                                                {{ $memory->bible_verse }}
                                            </td>
                                            
                                            @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                                            <td>
                                                <!-- Button Edit -->
                                                <a class="panel-action icon wb-edit mr-5" data-target="#EditBiblemodal{{ $memory->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Edit" style="cursor: pointer"></a>
                                                <!-- End Button Edit -->
                                            
                                                <!-- Button Remove -->
                                                <a class="panel-action icon wb-trash mr-5" data-target="#DeleteBiblemodal{{ $memory->id }}" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">
                                                </a>
                                                <!-- End Button Remove -->
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Bible Memory Report -->
            @endif


            @if($user->role == 'student')
            <!-- Habits Report -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                    
                    {{-- Student Weekly Achievement--}}
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" data-toggle="tab" role="tab">Desirable Habits and Traits Report</a>
                        </li>
                    </ul>

                    @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                        <button class="btn btn-victory-c mb-20" data-target="#Habitsmodal" data-toggle="modal">Create New</button>
                    @endif

                    <div class="tab-content">
                        <div class="page-content page-content-table tab-pane animation-slide-left active" id="contactsContent" role="tabpanel">
                            {{-- Table List --}}
                            <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <tbody>
                                    @foreach($habits as $habit_key => $habit)
                                        @foreach($habit as $name_key => $work_habits)
                                            @if($name_key == 0)
                                                <tr>
                                                    <th colspan="5">
                                                        <h4>{{ $work_habits->type }}</h4>
                                                    </th>
                                                </tr>
                                            @endif

                                            <tr class="gradeA">
                                                <td>
                                                    {{ $work_habits->habit }}
                                                </td>

                                                @php
                                                    $loopCounter = 0;    
                                                @endphp

                                                @foreach($user->work_habits->where('habit_id', $work_habits->id) as $grade_key => $habit_grade)
                                                    <td>
                                                        {{ $habit_grade->grade }}
                                                    </td>
                                                    @php
                                                        $loopCounter++;
                                                    @endphp
                                                @endforeach

                                                @for($i = $loopCounter; $i < 4; $i++)
                                                    <td>
                                                        NOT GRADED
                                                    </td>
                                                @endfor
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Habits Report -->
            @endif

            <!-- Victory Point -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">

                    {{-- Student Assigned PACE --}}
                    @if($user->role == 'student')
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" role="tab">Victory Point</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="page-content page-content-table tab-pane animation-slide-left active" id="contactsContent" role="tabpanel">

                            @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                            <button class="btn btn-victory-c mb-20" data-target="#Pointmodal" data-toggle="modal">Create New</button>
                            @endif
                             <!-- Button List -->
                                <a class="panel-action icon wb-list ml-10 mb-20" data-target="#ListPointmodal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="List" style="cursor: pointer;vertical-align: middle;">
                                </a>
                            <!-- End Button List -->

                            {{-- Table List --}}
                            <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                <thead>
                                    <tr>
                                        <th>Activity</th>
                                        <th>Points</th>

                                        @if(Auth::user()->role != 'parent')
                                        <th>Actions</th>
                                        @endif
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
                                        @if($user->role == 'student')
                                        <tr class="gradeA">
                                            <form id="grant-point-form" method="POST" action="{{ route('victory-points.grant', $user->id) }}">
                                                {{-- CSRF TOKEN --}}
                                                <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                {{-- CSRF TOKEN --}}
                                                <td>
                                                    {{-- Select Option --}}
                                                    <select id="select_point" onchange="getPoint('{{ route('victory-points.index') }}')" name="point" form="grant-point-form" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup id="option_point" form="grant-point-form" label="Please Select" name="point">
                                                            @foreach($points as $key => $point)
                                                                <option value="{{ $point->id }}">
                                                                    {{ $point->activity_name }}
                                                                </option>
                                                            @endforeach
                                                        </optgroup>
                                                    </select>
                                                    {{-- End Select Option --}}
                                                </td>

                                                <td>
                                                    <b>
                                                        <input class="form-control-plaintext" id="point-amount" disabled> 
                                                    </b>
                                                </td>
                                                
                                                <td class="actions">
                                                    {{-- Assigned Button --}}
                                                    <button class="btn btn-victory-c" type="submit">
                                                        <i class="icon md-plus" aria-hidden="true"></i> Add
                                                    </button>
                                                    {{-- End Assigned Button --}}
                                                </td>
                                            </form>
                                        </tr>
                                        @endif
                                    @endif

                                    {{-- RESULTS --}}
                                    @foreach($earns as $key => $earn)
                                    <tr class="gradeA">
                                        <td>
                                            {{ $earn->point->activity_name }}
                                        </td>
                                        <td>
                                            {{ $earn->point->points }}
                                        </td>
                                        <td class="actions">
                                            @if(Auth::user()->role == 'admin')
                                                <!-- Button Remove -->
                                                <a class="panel-action icon wb-trash" data-target="#DeletePoints<?php echo $earn->id ?>modal" data-toggle="modal" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Delete" style="cursor: pointer">
                                                </a>
                                                <!-- End Button Remove -->
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    {{-- RESULTS --}}
                                </tbody>
                            </table>
                            {{-- Table List --}}

                            <h5>Total Points : {{ $sum }}</h5>
                        </div>
                    </div>
                    @endif
                    {{-- End Student Assigned PACE --}}
                </div>
            </div>
            <!-- Victory Point -->

            @if(Auth::user()->role != 'student' && Auth::user()->role != 'parent')
            <!-- Form Print Report -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" data-toggle="tab" href="#SchoolReport" aria-controls="SchoolReport" role="tab">School Report</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" data-toggle="tab" href="#NationalReport" aria-controls="NationalReport" role="tab">National Report</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="SchoolReport">
                            <h5>Print Term Report</h5>
                            <form method="POST" target="_blank" action="{{ route('exports.student-report', $user->id) }}">
                                {{ csrf_field() }}
                            
                                <input class="form-control" type="number" name="term_absence_days" placeholder="Absence Days">

                                <br>

                                <input class="form-control" type="number" name="term_school_days" placeholder="School Days">

                                <br>
                                    
                                <select name="id_term" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                    <option></option>
                                    @foreach($semesters as $semester_key => $semester)
                                        @php
                                            $counter = 1;
                                        @endphp
                                        <optgroup label="{{ $semester->year }}/{{ $semester->year + 1 }} | {{ strtoupper($semester->semester_type) }}">
                                            @foreach($semester->terms->sortBy('term_start') as $term_key => $term)
                                                <option value="{{ $term->id }}">
                                                    Term {{ $counter++ }} | [{{ Carbon\Carbon::parse($term->term_start)->format('M d, Y') }} - {{ Carbon\Carbon::parse($term->term_end)->format('M d, Y') }}]
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>

                                <button type="submit" class="btn btn-victory-c mt-10"><i class="icon md-print"></i>
                                Print School Report</button>
                            </form>
                        </div>

                        <div class="tab-pane" role="tabpanel" id="NationalReport">
                            <h5>Print Semester Report</h5>
                            <form method="POST" target="_blank" action="{{ route('exports.student-national-report', $user->id) }}">
                                {{ csrf_field() }}

                                <input class="form-control" type="number" name="semester_absence_days" placeholder="Absence Days">

                                <br>

                                <input class="form-control" type="number" name="semester_school_days" placeholder="School Days">

                                <br>
                                
                                <select name="id_semester" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                    @foreach($semesters as $key => $semester)
                                        <option value="{{ $semester->id }}">
                                            {{ $semester->year }}/{{ $semester->year + 1 }} | {{ strtoupper($semester->semester_type) }}
                                        </option>
                                    @endforeach
                                </select>

                                <button type="submit" class="btn btn-victory-c mt-10"><i class="icon md-print"></i>
                                Print National Report</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Print Report -->
            @endif
            @endif
            </div>
            <!-- End Panel -->
        </div>

        @foreach($assigns->sortBy('course_id') as $key => $assign)
        <!-- Delete Modal  -->
        <div class="modal fade" id="Delete<?php echo $assign->id ?>modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <h4 class="modal-title" id="exampleModalLabel">
                            Are you sure want to delete?
                        </h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>

                        <form action="{{ route('assign-material.destroy', $assign->id) }}" method="post">
                            {{-- CSRF TOKEN --}}
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="DELETE">
                            {{-- CSRF TOKEN --}}

                            <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Delete Modal  -->
        
        <!-- Edit Modal -->
        <div class="modal fade modal-fill-in" id="Edit{{ $assign->id }}modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="exampleFillInModalTitle">Edit PACE</h4>
                    </div>
                    <div class="modal-body">
                        <tr class="gradeA">
                            <form id="edit{{ $assign->id }}Form" method="POST" action="{{ route('assign-material.update', $assign->id) }}">
                                {{-- CSRF TOKEN --}}
                                {{ csrf_field() }}
                                <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                <input type="hidden" value="PATCH" name="_method" />
                                {{-- CSRF TOKEN --}}

                                <input form="edit{{ $assign->id }}Form" type="hidden" name="student_id" value="{{ $assign->student_id }}">
                                <input form="edit{{ $assign->id }}Form" type="hidden" name="course_id" value="{{ $assign->course_id }}">

                                <td>
                                    <h5 class="modal-title">Start PACE</h5>
                                    {{-- Select Option --}}
                                    <select form="edit{{ $assign->id }}Form" name="pace_start" form="edit{{ $assign->id }}Form" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                        <option></option>
                                        <optgroup form="edit{{ $assign->id }}Form" label="Please Select" name="pace_start">
                                            @foreach($materials->where('course_id', $assign->course_id) as $key => $material)
                                                <option value="{{ $material->id }}" <?php if($assign->pace_start == $material->id) echo 'selected'; ?>>{{ $material->course->name }} | {{ $material->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                    {{-- End Select Option --}}
                                </td>

                                <td>
                                    <h5 class="modal-title">Finish PACE</h5>
                                    {{-- Select Option --}}
                                    <select form="edit{{ $assign->id }}Form" name="pace_end" form="edit{{ $assign->id }}Form" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                        <option></option>
                                        <optgroup form="edit{{ $assign->id }}Form" label="Please Select" name="pace_end">
                                            @foreach($materials->where('course_id', $assign->course_id) as $key => $material)
                                                <option value="{{ $material->id }}" <?php if($assign->pace_end == $material->id) echo 'selected'; ?>>{{ $material->course->name }} | {{ $material->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                    {{-- End Select Option --}}
                                </td>
                                <button type="submit" class="btn btn-victory-c float-right mt-20">Apply</button>
                            </form>
                        </tr>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Edit Modal -->

        <!-- List Modal -->
        <div class="modal fade modal-fill-in" id="List{{ $assign->id }}modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header pb-60">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="exampleFillInModalTitle">Input Score</h4>
                    </div>

                    <div class="modal-body">
                        <div id="report-session{{ $assign->id }}" style="display: none;" class="alert">
                            
                        </div>

                        @foreach($assign->assigned_materials as $key => $data)
                        @php
                            $explode = explode(' ', $data->material->course->name);
                            $courseName = $explode[0];

                            for($i = 0; $i < count($explode); $i++){
                                if(empty($explode[$i]))
                                    $courseName .= $explode[$i];
                            }
                        @endphp

                        <tbody>
                            <form id="input-grade-form{{ $data->id }}" method="POST" action="{{ route('assign-material.grade', $data->id) }}"></form>
                            <form id="jump-material-form{{ $data->id }}" method="POST" action="{{ route('assign-material.jump', $data->id) }}"></form>
                            <form id="unjump-material-form{{ $data->id }}" method="POST" action="{{ route('assign-material.unjump', $data->id) }}"></form>
                            <tr>
                                <td>
                                    {{ $data->material->course->name }} - {{ $data->material->name }}
                                </td>
                            </tr>
                            <br />
                            <tr>
                                <td class="pr-30">
                                    <input id="inserted-grade{{ $data->id }}" type="number" class="mr-0 mr-md-10" value="{{ $data->grade }}" disabled>
                                </td>
                                
                                @if(Auth::user()->role != 'student') 
                                {{-- INPUT GRADE --}}
                                <td class="pr-30">
                                    {{-- HIDDEN INPUT --}}
                                    <input id="material_id{{ $data->id }}" form="input-grade-form{{ $data->id }}" type="hidden" name="material_id" value="{{ $data->material_id }}">
                                    {{-- HIDDEN INPUT --}}

                                    @if($data->status != 'jumped')
                                        <input id="grade_{{ $courseName }}_{{ $data->material->name }}" form="input-grade-form{{ $data->id }}" type="number" name="grade" class="mr-0 mr-md-20" value="{{ $data->grade }}">
                                    @else
                                        <input form="input-grade-form{{ $data->id }}" type="text" class="mr-0 mr-md-20" value="JUMPED">
                                    @endif
                                </td>
                                {{-- INPUT GRADE --}}
                                
                                <td>
                                    <button id="button-input-grade-form{{ $data->id }}" form="input-grade-form{{ $data->id }}" class="btn btn-victory-c">Apply</button>
                                </td>
                                
                                @if($data->status != 'jumped')
                                    <td>
                                        {{-- CSRF TOKEN --}}
                                        {{ csrf_field() }}
                                        <input form="jump-material-form{{ $data->id }}" type="hidden" value="{{ csrf_token() }}" name="_token" />
                                        <input form="jump-material-form{{ $data->id }}" type="hidden" value="PATCH" name="_method" />
                                        {{-- CSRF TOKEN --}}
                                        <button form="jump-material-form{{ $data->id }}" type="submit" class="btn btn-warning">Jump</button>
                                    </td>
                                @else
                                    <td>
                                        {{-- CSRF TOKEN --}}
                                        {{ csrf_field() }}
                                        <input form="unjump-material-form{{ $data->id }}" type="hidden" value="{{ csrf_token() }}" name="_token" />
                                        <input form="unjump-material-form{{ $data->id }}" type="hidden" value="PATCH" name="_method" />
                                        {{-- CSRF TOKEN --}}
                                        <button form="unjump-material-form{{ $data->id }}" type="submit" class="btn btn-success">Activate</button>
                                    </td>
                                @endif
                                @endif

                                <script type="text/javascript">
                                    $("#input-grade-form{{ $data->id }}").submit(function(event){
                                        event.preventDefault();

                                        var grade_{{ $courseName }}_{{ $data->material->name }} = $('#grade_{{ $courseName }}_{{ $data->material->name }}').val();

                                        console.log(grade_{{ $courseName }}_{{ $data->material->name }});

                                        //clear
                                        $("#report-session{{ $assign->id }}").html('');
                                        $("#report-session{{ $assign->id }}").removeClass();

                                        $.ajax({
                                            url: "{{ route('assign-material.grade', $data->id) }}",
                                            type: 'POST',
                                            cache: false,
                                            data: {
                                                _token: '{{ csrf_token() }}',
                                                _method: 'PATCH',

                                                grade: grade_{{ $courseName }}_{{ $data->material->name }},
                                            },
                                            dataType: 'JSON',
                                            success: function (data) {
                                                $("#grade_{{ $courseName }}_{{ $data->material->name }}").append(grade_{{ $courseName }}_{{ $data->material->name }});

                                                $("#inserted-grade{{ $data->id }}").removeAttr('disabled');
                                                $("#inserted-grade{{ $data->id }}").val(grade_{{ $courseName }}_{{ $data->material->name }});
                                                $("#inserted-grade{{ $data->id }}").prop('disabled', true);

                                                //clear
                                                $("#report-session{{ $assign->id }}").html('');
                                                $("#report-session{{ $assign->id }}").removeClass();

                                                $("#report-session{{ $assign->id }}").css('display', '');
                                                $("#report-session{{ $assign->id }}").addClass('alert alert-' + data.type);
                                                $("#report-session{{ $assign->id }}").append(data.message + ' ' + '{{ $data->material->course->name }}');
                                            },
                                            error: function(jqXHR, exception){
                                                $("#report-session{{ $assign->id }}").css('display', '');
                                                $("#report-session{{ $assign->id }}").addClass('alert alert-' + 'danger');

                                                $("#report-session{{ $assign->id }}").append('Wrong input data, the grade should be equal or between 80 to 100');
                                                // $("#report-session{{ $assign->id }}").append(jqXHR);
                                            }
                                        });

                                        return false;
                                    });
                                </script>
                            </tr>
                        </tbody>

                        <br/>
                        @endforeach

                        <form id="fill-all-grade{{ $assign->id }}" method="POST" action="{{ route('fill-all-grade', $assign->id) }}">
                            <button type="submit" class="btn btn-victory-c mt-30">Apply All</button>
                        </form>

                        <script type="text/javascript">
                            $("#fill-all-grade{{ $assign->id }}").submit(function(event){
                                event.preventDefault();

                                @foreach($assign->assigned_materials as $key => $assignedMaterial)
                                    @php
                                        $explode = explode(' ', $assignedMaterial->material->course->name);

                                        for($i = 0; $i < count($explode); $i++) {
                                            if(empty($explode[$i]))
                                                $courseName .= $explode[$i];
                                        }
                                    @endphp

                                    if($("#grade_{{ $courseName }}_{{ $assignedMaterial->material->name }}").val()) {
                                        console.log($("#grade_{{ $courseName }}_{{ $assignedMaterial->material->name }}").val());
                                        $("#button-input-grade-form{{ $assignedMaterial->id }}").click();
                                    }
                                @endforeach

                                //clear
                                $("#report-session{{ $assign->id }}").html('');
                                $("#report-session{{ $assign->id }}").removeClass();

                                $("#report-session{{ $assign->id }}").css('display', '');
                                $("#report-session{{ $assign->id }}").addClass('alert alert-' + data.type);
                                $("#report-session{{ $assign->id }}").append('Succeeded input massively');
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <!-- End List Modal -->
        @endforeach

        @foreach($earns as $key => $earn)
        <!-- Delete Modal  -->
        <div class="modal fade" id="DeletePoints<?php echo $earn->id ?>modal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <h4 class="modal-title" id="exampleModalLabel">
                            Are you sure want to delete?
                        </h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>

                        <form action="{{ route('victory-points.reduce', $earn->id) }}" method="post">
                            {{-- CSRF TOKEN --}}
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="DELETE">
                            {{-- CSRF TOKEN --}}

                            <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Delete Modal  -->
        @endforeach

        <!-- Point Modal -->
        <div class="modal fade modal-fill-in" id="Pointmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="exampleFillInModalTitle">Create New Point</h4>
                    </div>
                    <div class="modal-body">
                        <tr class="gradeA">
                            <form id="create-point" method="POST" action="{{ route('victory-points.store') }}">
                                {{-- CSRF TOKEN --}}
                                {{ csrf_field() }}
                                {{-- CSRF TOKEN --}}

                                <div class="row">
                                    <div class="col-12 col-md-7">
                                        <h5 class="modal-title">Activity Name</h5>

                                        <input form="create-point" type="text" class="form-control" name="activity_name" placeholder="Activity Name" autocomplete="off" />
                                    </div>
                                    
                                    <div class="col-12 col-md-5">
                                        <h5 class="modal-title">Point</h5>
                                        
                                        <input form="create-point" type="number" class="form-control" name="points" placeholder="Point" autocomplete="off" />
                                    </div>    
                                </div>

                                <button form="create-point" type="submit" class="btn btn-victory-c float-right mt-20">Apply</button>
                            </form>
                        </tr>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Point Modal -->

        <!-- Point List Modal -->
        <div class="modal fade modal-fill-in" id="ListPointmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="exampleFillInModalTitle">Point List</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
                            <thead>
                                <tr>
                                    <th>Activity</th>
                                    <th>Point</th>

                                    @if(Auth::user()->role != 'parent')
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($points as $key => $point)
                                <tr class="gradeA">
                                    <td>{{ $point->activity_name }}</td>
                                    <td>{{ $point->points }}</td>

                                    <td>
                                        <form id="destroyPoint{{ $point->id }}" action="{{ route('victory-points.destroy', $point->id) }}" method="POST">
                                            {{-- CSRF TOKEN --}}
                                            {{ csrf_field() }}
                                            <input form="destroyPoint{{ $point->id }}" name="_method" type="hidden" value="DELETE">
                                            {{-- CSRF TOKEN --}}

                                            <button form="destroyPoint{{ $point->id }}" type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Point List Modal -->

        @foreach($assigned_clubs as $assigned_club)
        <!-- Delete Modal  -->
        <div class="modal fade" id="DeleteExtramodal{{ $assigned_club->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <h4 class="modal-title" id="exampleModalLabel">
                            Are you sure want to delete?
                        </h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>

                        <form action="{{ route('assigned-clubs.destroy', $assigned_club->id) }}" method="post">
                            {{-- CSRF TOKEN --}}
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="DELETE">
                            {{-- CSRF TOKEN --}}

                            <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach


        <!-- Create Bible Modal -->
        <div class="modal fade modal-fill-in" id="Biblemodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="exampleFillInModalTitle">Create New Bible Memory</h4>
                    </div>
                    <div class="modal-body">
                        <tr class="gradeA">
                            <form id="create-bible-memory" method="POST" action="{{ route('victory-points.store') }}">
                                {{-- CSRF TOKEN --}}
                                {{ csrf_field() }}
                                {{-- CSRF TOKEN --}}
                                
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="modal-title">Bible Memory Verse</h5>
                                        <input form="create-bible-memory" type="text" class="form-control" name="activity_name" placeholder="Bible Memory Verse" autocomplete="off" />
                                    </div>
                                </div>      

                                <button form="create-bible-memory" type="submit" class="btn btn-victory-c float-right mt-20">Apply</button>
                            </form>
                        </tr>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Create Bible Modal -->

        @foreach($user->bible_memories as $key => $bible_memory)
            <!-- Edit Bible Modal -->
            <div class="modal fade modal-fill-in" id="EditBiblemodal{{ $bible_memory->id }}" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-simple">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFillInModalTitle">Edit Bible Memory</h4>
                        </div>
                        <div class="modal-body">
                            <tr class="gradeA">
                                <form method="POST" action="{{ route('bible-memories.update', $bible_memory->id) }}">
                                    {{-- CSRF TOKEN --}}
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PATCH">
                                    {{-- CSRF TOKEN --}}
                                    
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="modal-title">Bible Memory Verse</h5>

                                            <input type="hidden" name="student_id" value="{{ $user->id }}">

                                            <select name="month" class="form-control" data-plugin="select2" data-placeholder="Please Select" data-allow-clear="true">
                                                <option></option>

                                                @for($monthNum = 1; $monthNum <= 12; $monthNum++)
                                                    <option value="{{ $monthNum }}" <?php if($monthNum == $bible_memory->month) echo 'selected'; ?>>
                                                        {{ \Carbon\Carbon::createFromFormat('m', $monthNum)->format('M') }}
                                                    </option>
                                                @endfor
                                            </select> 

                                            <input type="text" class="form-control" name="bible_verse" placeholder="Bible Memory Verse" autocomplete="off" value="{{ $bible_memory->bible_verse }}" />
                                        </div>
                                    </div>      

                                    <button type="submit" class="btn btn-victory-c float-right mt-20">Apply</button>
                                </form>
                            </tr>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Edit Bible Modal -->
        @endforeach

        <!-- Habits Modal -->
        <div class="modal fade modal-fill-in" id="Habitsmodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <tr class="gradeA">
                            <form method="POST" action="{{ route('students-habit.store') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="modal-title">Desirable Habits and Traits</h5>
                                        <div class="tab-content">
                                            {{-- Table List --}}
                                            <table class="table table-bordered table-hover table-striped is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                                                <tbody>
                                                    <input type="hidden" name="student_id" value="{{ $user->id }}">

                                                    <tr class="gradeA">
                                                        <td class="150">
                                                            <select name="habit_id" class="form-control" data-plugin="select2" data-placeholder="Please Select" data-allow-clear="true">
                                                                <option>Please Select</option>

                                                                @foreach($habits as $type => $habit)
                                                                    <optgroup label="{{ $type }}">
                                                                        @foreach($habit as $key => $habit_name)
                                                                            <option value="{{ $habit_name->id }}">
                                                                                {{ $habit_name->habit }}
                                                                            </option>
                                                                        @endforeach
                                                                    </optgroup>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td class="cell-150">
                                                            {{-- Select Option --}}
                                                            <select name="grade" class="form-control" data-plugin="select2" data-placeholder="Please Select" data-allow-clear="true">
                                                                <option value="E">Excellent</option>
                                                                <option value="VG">Very Good</option>
                                                                <option value="G">Good</option>
                                                                <option value="NI">Need Improvement</option>
                                                            </select>
                                                            {{-- End Select Option --}}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>      

                                <button type="submit" class="btn btn-victory-c float-right mt-20">Apply</button>
                            </form>
                        </tr>


                    </div>
                </div>
            </div>
        </div>
        <!-- End Habits Modal -->


        <!-- Edit Profile Modal -->
        <div class="modal fade modal-fill-in" id="EditProfile" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content" style="max-width: 100%;">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <!-- Title -->
                        <h4 class="modal-title" id="exampleModalTabs">Edit Profile</h4>
                        <!-- End Title -->
                    </div>
                    <!-- End Modal Header -->

                    <!-- Modal Body-->
                    <div class="modal-body">
                        <div class="tab-content">
                            <!-- Edit User Menu -->
                            <div class="tab-pane active" id="user" role="tabpanel">
                                <div class="row mt-20">
                                    <div class="col-12">
                                        <!-- Edit User Form -->
                                        <div class="example-wrap">
                                            <div class="example">
                                                <form method="POST" action="{{ route('user_profile.update', $user->id) }}" enctype="multipart/form-data" autocomplete="off">
                                                    <div class="row">
                                                        {{-- CSRF TOKEN --}}
                                                        {{ csrf_field() }}
                                                        <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                        <input name="_method" type="hidden" value="PATCH">
                                                        {{-- CSRF TOKEN --}}

                                                        <!-- Photo Upload -->
                                                        <div class="example">
                                                            <input type="file" name="profile_picture" id="input-file-now" data-plugin="dropify" data-default-file="{{ asset('file_uploads/users/'.$data->profile_picture) }}"/>
                                                        </div>
                                                        <!-- End Photo Upload -->

                                                        <!-- Input Full Name -->
                                                        <div class="form-group form-material col-12">
                                                            <label class="form-control-label" for="inputBasicFirstName">Full Name</label>
                                                            <input type="text" class="form-control" id="inputBasicFirstName" name="name" placeholder="Full Name" autocomplete="off" value="{{ $user->name }}"/>
                                                        </div>
                                                        <!-- End Input Full Name -->

                                                        @if($user->role == 'student')
                                                        <!-- Input NIS -->
                                                        <div class="form-group form-material col-12 col-lg-6">
                                                            <label class="form-control-label" for="inputBasicFirstName">NIS</label>
                                                            <input type="text" class="form-control" id="inputBasicFirstName" name="nis" placeholder="NIS" autocomplete="off" value="{{ $nis }}" />
                                                        </div>
                                                        <!-- End Input NIS -->
                                                        @elseif($user->role != 'parent')
                                                        <!-- Input NIP -->
                                                        <div class="form-group form-material col-12 col-lg-6">
                                                            <label class="form-control-label" for="inputBasicFirstName">NIP</label>
                                                            <input type="text" class="form-control" id="inputBasicFirstName" name="nip" placeholder="NIP" autocomplete="off" value="{{ $nip }}" />
                                                        </div>
                                                        <!-- End Input NIP -->
                                                        @endif

                                                        <!-- Input Email -->
                                                        <div class="form-group form-material col-12 col-lg-6">
                                                            <label class="form-control-label" for="inputBasicFirstName">Email</label>
                                                            <input type="text" class="form-control" id="inputBasicFirstName" name="email" placeholder="Email" autocomplete="off" value="{{ $user->email }}" />
                                                        </div>
                                                        <!-- End Input Email -->

                                                        <!-- Input Phone Number -->
                                                        <div class="form-group form-material col-12">
                                                            <label class="form-control-label" for="inputBasicFirstName">Phone Number</label>
                                                            <input type="text" class="form-control" id="inputBasicFirstName" name="phone" placeholder="Phone Number" autocomplete="off" value="{{ $phone }}" />
                                                        </div>
                                                        <!-- End Input Phone Number -->

                                                        @if($user->role != 'parent')
                                                        <!-- Input Date -->
                                                        <div class="form-group form-material col-12">
                                                            <label class="form-control-label" for="inputBasicFirstName">Birth Date</label>
                                                            <input type="date" class="form-control" id="inputBasicFirstName" name="birth_date" placeholder="Birth Date" autocomplete="off" value="{{ $birth_date }}" />
                                                        </div>
                                                        <!-- End Input Date -->
                                                        @endif
                                                    </div>
                                                    
                                                    @if($user->role != 'student' && $user->role != 'parent')
                                                    <!-- Position Option -->
                                                    <div class="example">
                                                        <label class="form-control-label">Position</label>
                                                        <select name="position" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                            <option value="headmaster">Headmaster</option>
                                                            <option value="teacher">Teacher</option>
                                                        </select>
                                                    </div>
                                                    <!-- End Position Option -->

                                                    <!-- Subject Option -->
                                                    <div class="example">
                                                        <label class="form-control-label">Subject</label>
                                                        <select name="subject" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                            @foreach($courses as $key => $course)
                                                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <!-- End Subject Option -->
                                                    @endif

                                                    <div class="form-group form-material">
                                                        <!-- Button Sign Up -->
                                                        <button type="submit" class="btn btn-victory-c">Save</button>
                                                        <!-- End Button Sign Up -->
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Edit User Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Edit User Menu -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Edit Profile Page Modal -->


        <!-- Weekly Report Modal -->
        <div class="modal fade modal-fill-in" id="Historymodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>

                        <h4 class="modal-title" id="exampleFillInModalTitle">
                            Weekly Reports History
                        </h4>
                    </div>

                    <div class="modal-body">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
                            <thead>
                                <tr>
                                    <th>Course</th>
                                    <th>PACE</th>
                                    <th>Grade</th>
                                    <th>Status</th>
                                    <th>Confirmed By</th>
                                    <th>Confirmed At</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($reports as $key => $report)
                                <tr class="gradeA">
                                    <td>
                                        {{ $report->course->name }}
                                    </td>

                                    <td>
                                        {{ $report->material->name }}
                                    </td>

                                    <td>
                                       {{ $report->grade }}
                                    </td>

                                    <td>
                                       @if($report->confirmed == 1) CONFIRMED
                                       @elseif($report->confirmed == 0) NOT CONFIRMED
                                       @else LATE CONFIRMATION
                                       @endif
                                    </td>

                                    <td>
                                        @if($report->confirmer)
                                        {{ $report->confirmedBy->name }}
                                        @endif
                                    </td>

                                    <td>
                                        {{ \Carbon\Carbon::parse($report->confirmed_at)->diffForHumans() }}
                                        -
                                        {{ \Carbon\Carbon::parse($report->confirmed_at)->format('M d, Y | H:i:s') }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if(Auth::user()->role != 'student')
                        <form id="confirmAllForm" action="{{ route('confirm-all-report', $user->id) }}" method="POST">
                            {{-- CSRF TOKEN --}}
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" value="PATCH" name="_method" />
                            {{-- CSRF TOKEN --}}
                        </form>

                            <button form="confirmAllForm" class="btn btn-victory-c mb-20 float-right">Confirm</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- End Weekly Report Modal -->


        
        @if($user->role == 'parent')
        <!-- Create User Modal Boxes -->
        <div class="modal fade modal-fill-in" id="CRUDuser" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple" style="max-width: 100%">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <!-- Modal Header -->

                    <!-- Modal Body-->
                    <div class="modal-body">
                        <div class="tab-content">
                            <!-- Create User Menu -->
                            <div class="tab-pane active" id="user" role="tabpanel">
                                <div class="row mt-20">
                                    <div class="col-12">
                                        <!-- Create User Form -->
                                        <div class="example-wrap">
                                            <h4 class="example-title">Add Student</h4>
                                            <div class="example">
                                                <form method="POST" action="{{ route('parent-relation.store') }}" autocomplete="off">

                                                    {{-- CSRF TOKEN --}}
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                    {{-- CSRF TOKEN --}}

                                                    <input type="hidden" name="parent_id" value="{{ $user->id }}">

                                                    <!-- Child List -->
                                                    <div class="example">
                                                        <label class="form-control-label">Student List</label>
                                                        <select name="student_id" class="form-control" data-placeholder="Select" data-allow-clear="true">
                                                            <option></option>
                                                            <optgroup label="Please Select">
                                                                @foreach($students as $key => $student)
                                                                @if(! $student->profile->relations->where('parent_id', $user->id)->first())
                                                                <option value="{{ $student->id }}">
                                                                    @if($student->profile->nis)
                                                                    {{ $student->profile->nis }} | 
                                                                    @endif

                                                                    {{ $student->name }}
                                                                </option>
                                                                @endif
                                                                @endforeach
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                    <!-- End Child List -->

                                                    <!-- Categories Option -->
                                                    <div class="example">
                                                        <label class="form-control-label">Relation</label>
                                                        <select name="relation" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                            <option></option>
                                                            <optgroup label="Please Select">
                                                                <option value="Father">Father</option>
                                                                <option value="Mother">Mother</option>
                                                                <option value="Others">Others</option>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                    <!-- End Categories Option -->

                                                    <div class="form-group form-material">
                                                        <!-- Button Sign Up -->
                                                        <button type="submit" class="btn btn-victory-c">Apply</button>
                                                        <!-- End Button Sign Up -->
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Create User Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Create User Menu -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Create User Modal Boxes -->
        @endif

        <!-- Delete Modal  -->
        <div class="modal fade" id="Deletemodal" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <h4 class="modal-title" id="exampleModalLabel">
                            Are you sure want to delete?
                        </h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>

                        <form action="{{ route('users.destroy', $user->id) }}" method="post">
                            {{-- CSRF TOKEN --}}
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="DELETE">
                            {{-- CSRF TOKEN --}}

                            <button type="submit" id="confirmDeleteButton" class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Delete Modal  -->
    </div>
</div>
<!-- End Page -->
@endsection