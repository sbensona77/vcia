@extends('layouts.app-dashboard')

@section('content')
<!-- Page -->
<div class="page bg-white">
    <!-- Contacts Content -->
    <div class="page-main">
        @if ($errors->any())
            <div class="alert alert-danger first">
                @foreach ($errors->all() as $error)
                {{ $error }} <br>
                @endforeach
            </div>

            <br />
        @endif

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}  
            </div>

            <br/>
        @elseif(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>

            <br/>
        @endif
        <!-- Contacts Content Header -->
        <div class="page-header">
            <div class="page-header-actions">

                <!-- Form -->
                <form method="POST" action="{{ route('users.search') }}">
                    <div class="input-search input-search-dark mt-30">
                        {{ csrf_field() }}
                        <!-- Input Search -->
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <!-- End Input Search -->
                    </div>
                </form>
                <!-- End Form -->

            </div>
        </div>
        <!-- End Contacts Content Header -->

        <!-- Contacts Content -->
        @if(Auth::user()->role == 'admin')
        <!-- Create Button -->          
        <button class="btn btn-victory-c ml-10 ml-md-30 mb-30 mt-30" type="button" data-target="#CRUDuser" data-toggle="modal">
        <i class="icon md-plus"></i>Create
        </button>
        <!-- End Create Button -->
        @endif
        
        <div id="contactsContent" class="page-content page-content-table px-3" data-plugin="selectable">
            <!-- Contacts List Table -->
            <table class="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr" data-selectable="selectable">
                <thead>
                    <tr>
                        <th class="pre-cell"></th>
                        <!-- Table Header -->
                        <th class="cell-300" scope="col">Name</th>

                        @if(Route::is('students.index'))
                        <th class="cell-100" scope="col">Level</th>
                        @endif

                        <th class="cell-100" scope="col">Email</th>
                        <th class="cell-30" scope="col">Status</th>

                        @if(Route::is('students.index'))
                        <th class="cell-30" scope="col">Semester</th>
                        <th class="cell-30" scope="col">Term</th>
                        @endif

                        <th class="cell-30" scope="col"></th>

                        <th class="suf-cell"></th>
                        <!-- End Table Header -->
                    </tr>
                </thead>
                    
                <tbody>
                    <!-- Table List -->
                    @foreach($users as $key => $user)
                    <tr>
                        <td class="pre-cell"></td>
                        <!-- Name -->
                        <td class="cell-300">
                            <a class="avatar" href="javascript:void(0)" style="vertical-align: middle">
                                <img class="img-fluid" src="{{ asset('file_uploads/users/'.$user->profile->profile_picture) }}">
                            </a>

                            {{ $user->name }}
                        </td>
                        <!-- End Name -->

                        @if(Route::is('students.index'))
                        <!-- Level -->
                        <td class="cell-100">
                            @if($user->profile->level <= 2) ABC
                            @elseif($user->profile->level <= 3) Young Warrior
                            @elseif($user->profile->level <= 6) Warrior
                            @elseif($user->profile->level <= 9) Young Victors
                            @elseif($user->profile->level == 13) Graduated
                            @else Victors 
                            @endif

                            -

                            {{ $user->profile->level }}
                        </td>
                        <!-- End Level -->
                        @endif

                        <td class="cell-100">
                            {{ $user->email }}
                        </td>
                        <!-- End Email -->

                        <!-- Status -->
                        <td class="cell-30">
                            {{ ucfirst($user->role) }}
                        </td>
                        <!-- End Status -->

                        @if(Route::is('students.index'))
                        <!-- Semester -->
                        <td class="cell-30">
                            @if(!$user->term_exist)
                                <span class="badge badge-danger">No Term Active</span>
                            @endif

                            @if($user->term_status)
                                <span class="badge badge-success">Assigned</span>
                            @else
                                <span class="badge badge-warning">Not Assigned</span>
                            @endif
                        </td>
                        <!-- Semester -->

                        <!-- Term -->
                        <td class="cell-30">
                            @if(!$user->semester_exist)
                                <span class="badge badge-danger">No Term Active</span>
                            @endif

                            @if($user->semester_status)
                                <span class="badge badge-success">Assigned</span>
                            @else
                                <span class="badge badge-warning">Not Assigned</span>
                            @endif
                        </td>
                        <!-- Term -->
                        @endif

                        <td class="cell-30">
                            <a href="{{ route('user_profile.show', $user->id) }}">
                                <button class="btn btn-victory-c">Profile</button>
                            </a>
                        </td>
                        <td class="suf-cell"></td>
                    </tr>
                    @endforeach
                    <!-- End Table List -->
                </tbody>
            </table>
            <!-- End Contacts List Table -->

            <!-- Pagination List -->
            @if($users instanceof \Illuminate\Pagination\LengthAwarePaginator)
                {{ $users->links('pagination') }}
            @endif
            <!-- End Pagination List -->
        </div>
    </div>
    <!-- End Contacts Content -->
    
    <!-- Create User Modal Boxes -->
    <div class="modal fade modal-fill-in" id="CRUDuser" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple" style="max-width: 100%">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                    <!-- Title -->
                    <h4 class="modal-title" id="exampleModalTabs">Create User</h4>
                    <!-- End Title -->
                </div>
                <!-- Modal Header -->

                <!-- Modal Body-->
                <div class="modal-body">
                    <div class="tab-content">
                        <!-- Create User Menu -->
                        <div class="tab-pane active" id="user" role="tabpanel">
                            <div class="row mt-20">
                                <div class="col-12">
                                    <!-- Create User Form -->
                                    <div class="example-wrap">
                                        <h4 class="example-title">Create User</h4>
                                        <div class="example">
                                            <form method="POST" action="{{ route('users.store') }}" autocomplete="off">
                                                <div class="row">
                                                    {{-- CSRF TOKEN --}}
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                    {{-- CSRF TOKEN --}}
                                                    <!-- Input Full Name -->
                                                    <div class="form-group form-material col-12">
                                                        <label class="form-control-label" for="inputBasicFirstName">Full Name</label>
                                                        <input type="text" class="form-control" id="inputBasicFirstName" name="name"placeholder="Full Name" autocomplete="off" />
                                                    </div>
                                                    <!-- End Input Full Name -->
                                                </div>
                                                <!-- Gender Option -->
                                                <div class="form-group form-material">
                                                    <label class="form-control-label">Gender</label>
                                                    <div>
                                                        <div class="radio-custom radio-default radio-inline">
                                                            <input type="radio" id="inputBasicMale" name="gender" />
                                                            <label for="inputBasicMale">Male</label>
                                                        </div>
                                                        <div class="radio-custom radio-default radio-inline">
                                                            <input type="radio" id="inputBasicFemale" name="gender" />
                                                            <label for="inputBasicFemale">Female</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Gender Option -->
                                                <!-- Input Email -->
                                                <div class="form-group form-material">
                                                    <label class="form-control-label" for="inputBasicEmail">Email Address</label>
                                                    <input type="email" class="form-control" id="inputBasicEmail" name="email" placeholder="Email Address" autocomplete="off" />
                                                </div>
                                                <!-- End Input Email -->
                                                <!-- Input Password -->
                                                <div class="form-group form-material">
                                                    <label class="form-control-label" for="inputBasicPassword">Password</label>
                                                    <input type="password" class="form-control" id="inputBasicPassword" name="password" placeholder="Password" autocomplete="off" />
                                                </div>
                                                <!-- End Input Password -->
                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Access</label>
                                                    <select name="role" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="admin" <?php if(Route::is('admins.index')) echo 'selected'; ?>>Admin</option>
                                                            <option value="teacher" <?php if(Route::is('teachers.index')) echo 'selected'; ?>>Teacher</option>
                                                            <option value="student" <?php if(Route::is('students.index')) echo 'selected'; ?>>Student</option>
                                                            <option value="parent" <?php if(Route::is('parents.index')) echo 'selected'; ?>>Parent</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->

                                                @if(Route::is('students.index'))
                                                <!-- Class Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Class</label>
                                                    <select name="class" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="1">ABC - 1</option>
                                                            <option value="2">Young Warrior - 2</option>
                                                            <option value="3">Young Warrior - 3</option>
                                                            <option value="4">Warrior - 4</option>
                                                            <option value="5">Warrior - 5</option>
                                                            <option value="6">Warrior - 6</option>
                                                            <option value="7">Young Victors - 7</option>
                                                            <option value="8">Young Victors - 8</option>
                                                            <option value="9">Young Victors - 9</option>
                                                            <option value="10">Victors - 10</option>
                                                            <option value="11">Victors - 11</option>
                                                            <option value="12">Victors - 12</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Class Categories Option -->
                                                @endif

                                                <!-- Categories Option -->
                                                <div class="example">
                                                    <label class="form-control-label">Status</label>
                                                    <select name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                        <option></option>
                                                        <optgroup label="Please Select">
                                                            <option value="1" selected>Active</option>
                                                            <option value="0">Inactive</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <!-- End Categories Option -->
                                                <div class="form-group form-material">
                                                    <!-- Button List -->
                                                   {{--  <a href="{{ route('users.index') }}" class="btn btn-victory-c">List</a> --}}
                                                    <!-- End Button List -->

                                                    <!-- Button Sign Up -->
                                                    <button type="submit" class="btn btn-victory-c">Sign Up</button>
                                                    <!-- End Button Sign Up -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Create User Form -->
                                </div>
                            </div>
                        </div>
                        <!-- End Create User Menu -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Create User Modal Boxes -->
</div>
<!-- End Page -->
@endsection