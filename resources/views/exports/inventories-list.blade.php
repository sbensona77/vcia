@extends('layouts.app-printable')

@section('content')
<div class="page">
    <div class="page-content container-fluid">
    	<button class="btn btn-victory-c no-print mb-50" onclick="javascript:window.print();">
    		<i class="icon md-print"></i>
			Print
		</button>

    	@foreach($inventories as $key => $inventory)
		<h3 class="modal-title">
		   	{{ $inventory->inventory_name }}
		</h3>

		@if($inventory->items)
		<div class="row mb-50" style="border-bottom: 2px solid #D0D0D0">
			@php
				$counter = 1;
				$arrayLength = count($inventory->items);
			@endphp

			@foreach($inventory->items as $key => $item)
			@if($counter == 1)
			<div class="col-12 col-md-6 col-lg-4">
				<table class="table table-bordered">
					<tr>
				        <th>
				            <h5>
				            	<img src="{{ asset('img/logo.png') }}" class="mr-5" alt="logo" style="width: 25px; vertical-align: middle;">
				            	{{ $item->code }}
				            </h5>
				        </th>
				    </tr>
			@endif
				    <tr>
				    	<td>
				    		<img src="{{ asset('img/logo.png') }}" class="mr-5" alt="logo" style="width: 25px; vertical-align: middle;">{{ $inventory->inventory_name }} / {{ $item->code }}
				    	</td>
				    </tr>

					@if($counter == 10)
							@php
								$counter = 1;
							@endphp
							</table>
						</div>
					@elseif($key == $arrayLength - 1)
							</table>
						</div>
						
					@else
						@php
							$counter++;
						@endphp
					@endif
			@endforeach
		</div>
		@endif
		@endforeach
	</div>
</div>
@endsection