@extends('layouts.app-printable')

@section('content')

<div class="page">
    <div class="page-content container-fluid">
        <button class="btn btn-victory-c no-print mb-50" onclick="javascript:window.print();">
            <i class="icon md-print"></i>Print
        </button>

        <div class="row mb-20">
            <div class="col-12 col-sm-3 text-center">
                <a class="avatar avatar-lg" href="javascript:void(0)">
                    <img src="{{ asset('file_uploads/users/'.$profile->profile_picture) }}">
                </a>
            </div>

            <div class="col-12 col-sm-9 mt-40 mt-sm-0">
                <table class="table table-bordered">
					
                    <tr>
                        <td class="cell-200">Name</td>
                        <td>
                            {{ $profile->name }}
                        </td>
                    </tr>
					
					@if($user->role != 'parent')
                    <tr>
                        <td class="cell-200">ID</td>
                        <td>
                        	@if($user->role == "student")
                            {{ $profile->nis }}
                            @elseif($user->role == 'teacher' || $user->role == 'admin')
                            {{ $profile->nip }}
                            @endif
                        </td>
                    </tr>
                    @endif

                    <tr>
                        <td class="cell-200">Date of Entry in VCIA</td>
                        <td>
                            {{ Carbon\Carbon::parse($profile->created_at)->format('M d, Y') }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection