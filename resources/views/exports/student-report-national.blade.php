@extends('layouts.app-printable')

@section('content')

<div class="page">
    <div class="page-content container-fluid">
        <button class="btn btn-victory-c no-print mb-50" onclick="javascript:window.print();">
            <i class="icon md-print"></i>Print
        </button>

        <div class="row mb-50">
            <div class="col-12 col-sm-3">
                <a class="avatar avatar-lg" href="javascript:void(0)">
                    <img src="{{ asset('file_uploads/users/'.$profile->profile_picture) }}">
                </a>
            </div>

            <div class="col-12 col-sm-9 mt-20 mt-sm-0">
                <table class="table table-bordered">
                    <tr>
                        <td class="cell-200">Academic Year</td>
                        <td>{{ $semester->year }} | <?php if($semester->semester_type == 'odd') echo 'ODD'; else 'EVEN' ?></td>
                    </tr>

                    <tr>
                        <td class="cell-200">Student's Name</td>
                        <td>
                            {{ $profile->name }}
                        </td>
                    </tr>

                    <tr>
                        <td class="cell-200">Student No</td>
                        <td>
                            {{ $profile->nis }}
                        </td>
                    </tr>

                    <tr>
                        <td class="cell-200">Date of Entry in VCIA</td>
                        <td>
                            {{ Carbon\Carbon::parse($profile->created_at)->format('M d, Y') }}
                        </td>
                    </tr>

                    <tr>
                        <td class="cell-200">Date of Progress Report</td>
                        <td>{{ Carbon\Carbon::parse($semester->semester_start)->format('M d, Y') }} - {{ Carbon\Carbon::parse($semester->semester_end)->format('M d, Y') }}</td>
                    </tr>
                </table>
            </div>
        </div>

        @php
            $totalMaterials = 0;
            $sumOfGrade = 0;
            $average = 0;

            $sumOfCourses = 0;
            $sumOfAverages = 0;
        @endphp

        @foreach($courses as $key => $course)
            @php
                foreach($materials->where('assigned_course_id', $course->id) as $material_key => $assigned) 
                {
                    $sumOfGrade += $assigned->grade;
                }
                $totalMaterials = $material_key + 1;
                $average = round(($sumOfGrade / $totalMaterials), 2);

                $sumOfCourses++;
                $sumOfAverages += $average;
            @endphp
        @endforeach


        {{-- SUMMARY OF TERM --}}
        <h4 class="modal-title mt-80">
            SUMMARY OF SEMESTER
        </h4>

        <table class="table table-bordered">
            <tr>
                <th class="cell-50">
                    <h5>#</h5>
                </th>
                
                <th>
                    <h5>Subject</h5>
                </th>
                
                <th>
                    <h5># of PACEs Completed</h5>
                </th>
                
                <th>
                    <h5>Average</h5>
                </th>
                
                <th>
                    <h5>Grade</h5>
                </th>
            </tr>

            <tr>
                <td class="cell-50">1</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            @php
                if($sumOfCourses != 0)
                    $termAverage = round(($sumOfAverages / $sumOfCourses), 2);
                else
                    $termAverage = 0;


                $GPA = round(($termAverage / 25), 2);
                $termGrade = '';

                if($GPA >= 4.00)
                    $termGrade = 'A';
                else if($GPA >= 3.70)
                    $termGrade = 'A-';
                else if($GPA >= 3.30)
                    $termGrade = 'B+';
                else if($GPA >= 3.00)
                    $termGrade = 'B';
                else if($GPA >= 2.70)
                    $termGrade = 'B-';
                else if($GPA >= 2.30)
                    $termGrade = 'C+';
                else if($GPA >= 2.00)
                    $termGrade = 'C';
                else
                    $termGrade = 'E';
            @endphp

            <tr>
                
            </tr>

            <tr>
                <td colspan="2" class="cell-100">
                    <h5>Total PACEs Completed</h5>
                </td>

                <td>{{ $totalMaterials }}</td>
                <td>
                    
                </td>
                <td>
                    
                </td>            
            </tr>

            <tr>
                <td colspan="2" class="cell-100">
                    <h5>Term Average</h5>
                </td>

                <td>{{ $termAverage }}</td>
                <td>
                    
                </td>
                <td>
                    
                </td>               
            </tr>

            <tr>
                <td colspan="2" class="cell-100">
                    <h5>Term Grade</h5>
                </td>

                <td>{{ $termGrade }}</td>
                <td>
                    
                </td>
                <td>
                    
                </td>               
            </tr>

            <tr>
                <td colspan="2" class="cell-100">
                    <h5>No of School Days</h5>
                </td>

                <td>
                    {{ $school_days }}
                </td>
                <td>
                    
                </td> 
                <td>
                    
                </td>              
            </tr>
            <tr>
                <td colspan="2" class="cell-100">
                    <h5>Days Absent</h5>
                </td>

                <td>
                    {{ $absence_days }}
                </td>

                <td>
                    
                </td>

                <td>
                    
                </td>               
            </tr>
        </table>
        {{-- END SUMMARY OF TERM --}}


        {{-- PROGRESS REPORT --}}
        <h4 class="modal-title mt-80">
            PROGRESS REPORT
        </h4>

        <table class="table table-bordered">
            <tr>
                <th class="cell-50">
                    <h5>#</h5>
                </th>

                <th>
                    <h5>ExtraCurricular</h5>
                </th>

                <th>
                    <h5>Grade</h5>
                </th>
            </tr>

            @foreach($clubs as $key => $club)
                <tr>
                    <td class="cell-50">
                        {{ $key }}
                    </td>

                    <td>
                        {{ $club->club->name }}
                    </td>

                    <td>
                        {{ $club->grade }}
                    </td>
                </tr>
            @endforeach
        </table>
        {{-- END PROGRESS REPORT --}}

        
        <div class="row my-80">
            <div class="col-12 text-center">
                <hr style="background-color:black;height: 2px; width: 100px;">
                <h5>LC Supervisor</h5>
            </div>

            <div class="col-6 text-center mt-80">
                <hr style="background-color:black;height: 2px; width: 100px;">
                <h5>Parent's Signature</h5>
            </div>

            <div class="col-6 text-center mt-80">
                <hr style="background-color:black;height: 2px; width: 100px;">
                <h5>VCIA Principal</h5>
            </div>

            <div class="col-12 text-center mt-80">
                <h5 class="text-uppercase">Victory Christian International Academy</h5>
                <h5>Jl. Bromo No. 6 Semarang 50232</h5>
                <h5>(024) 831 2819</h5>
                <h6>victory.cia.semarang@gmail.com</h6>
            </div>  
        </div>
    </div>
</div>

@endsection