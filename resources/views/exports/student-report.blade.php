@extends('layouts.app-printable')

@section('content')

<div class="page">
    <div class="page-content container-fluid">
        <button class="btn btn-victory-c no-print mb-50" onclick="javascript:window.print();">
            <i class="icon md-print"></i>Print
        </button>

        <div class="row mb-20">
            <div class="col-12 col-sm-3 text-center">
                <a class="avatar avatar-lg" href="javascript:void(0)">
                    <img src="{{ asset('file_uploads/users/'.$profile->profile_picture) }}">
                </a>
            </div>

            <div class="col-12 col-sm-9 mt-40 mt-sm-0">
                <table class="table table-bordered">
                    <tr>
                        <td class="cell-200">Academic Year</td>
                        <td>{{ $term->academic_year->year }} | <?php if($term->academic_year->semester_type == 'odd') echo 'ODD'; else 'EVEN' ?></td>
                    </tr>

                    <tr>
                        <td class="cell-200">Student's Name</td>
                        <td>
                            {{ $profile->name }}
                        </td>
                    </tr>

                    <tr>
                        <td class="cell-200">Student No</td>
                        <td>
                            {{ $profile->nis }}
                        </td>
                    </tr>

                    <tr>
                        <td class="cell-200">Date of Entry in VCIA</td>
                        <td>
                            {{ Carbon\Carbon::parse($profile->created_at)->format('M d, Y') }}
                        </td>
                    </tr>

                    <tr>
                        <td class="cell-200">Date of Progress Report</td>
                        <td>{{ Carbon\Carbon::parse($term->term_start)->format('M d, Y') }} - {{ Carbon\Carbon::parse($term->term_end)->format('M d, Y') }}</td>
                    </tr>
                </table>
            </div>
        </div>

        {{-- SCORE REPORT --}}
        <h4 class="modal-title">
            SCORE REPORT
        </h4>
        <table class="table table-bordered">
            <tr>
                <th rowspan="2">
                    <h5>Subject</h5>
                </th>

                <th colspan="11">
                    <h5>Scores</h5>
                </th>

                <th>
                    <h5>Total PACEs</h5>
                </th>

                <th>
                    <h5>AVG.</h5>
                </th>
            </tr>

            <tr>
                <td colspan="11">
                    <h5>Term</h5>
                </td>

                <td>

                </td>

                <td>

                </td>
            </tr>

            @php
                $totalMaterials = 0;
                $sumOfGrade = 0;
                $average = 0;

                $sumOfCourses = 0;
                $sumOfAverages = 0;
            @endphp

            @foreach($courses as $course_key => $course)
                <tr>
                    <td rowspan="2">{{ $course->course->name }}</td>
                    <td>PACE</td>

                    @foreach($materials->where('assigned_course_id', $course->id) as $material_key => $assigned)
                        @for($i = 0; $i < 1; $i++)
                            <td>
                                {{ $assigned->material->name }}
                            </td>
                        @endfor   
                    @endforeach
                     
                    @php
                        $totalMaterials = $material_key + 1;
                    @endphp       
                </tr>

                <tr>
                    <td>Score</td>
                    @foreach($materials->where('assigned_course_id', $course->id) as $key => $assigned)
                        <td>
                            {{ $assigned->grade }}
                        </td>

                        @php
                            $sumOfGrade += $assigned->grade;
                        @endphp
                    @endforeach
                </tr>

                @php
                    $average = round(($sumOfGrade / $totalMaterials), 2);
                @endphp
                
                <tr></tr>

                <tr>
                    <td colspan="12">
                        <h5>Total PACEs/PACEs Average</h5>
                    </td>
                    
                    <td>{{ $totalMaterials }}</td>
                    
                    <td>{{ $average }}</td>          
                </tr>
                
                @php
                    $sumOfCourses++;
                    $sumOfAverages += $average;
                @endphp
            @endforeach
        </table>
        {{-- END SCORE REPORT --}}


        {{-- SUMMARY OF TERM --}}
        <h4 class="modal-title mt-40">
            SUMMARY OF TERM
        </h4>

        <table class="table table-bordered">
            <tr>
                <th class="cell-50">
                    <h5>#</h5>
                </th>
                
                <th>
                    <h5>Subject</h5>
                </th>
                
                <th>
                    <h5># of PACEs Completed</h5>
                </th>
            </tr>

            <tr>
                <td class="cell-50">1</td>
                <td></td>
                <td></td>
            </tr>

            @php
                if($sumOfCourses != 0)
                    $termAverage = round(($sumOfAverages / $sumOfCourses), 2);
                else
                    $termAverage = 0;

                $GPA = round(($termAverage / 25), 2);
                $termGrade = '';

                if($GPA >= 4.00)
                    $termGrade = 'A';
                else if($GPA >= 3.70)
                    $termGrade = 'A-';
                else if($GPA >= 3.30)
                    $termGrade = 'B+';
                else if($GPA >= 3.00)
                    $termGrade = 'B';
                else if($GPA >= 2.70)
                    $termGrade = 'B-';
                else if($GPA >= 2.30)
                    $termGrade = 'C+';
                else if($GPA >= 2.00)
                    $termGrade = 'C';
                else
                    $termGrade = 'E';
            @endphp

            <tr>
                
            </tr>

            <tr>
                <td colspan="2" class="cell-100">
                    <h5>Total PACEs Completed</h5>
                </td>

                <td>{{ $totalMaterials }}</td>          
            </tr>

            <tr>
                <td colspan="2" class="cell-100">
                    <h5>Term Average</h5>
                </td>

                <td>{{ $termAverage }}</td>             
            </tr>

            <tr>
                <td colspan="2" class="cell-100">
                    <h5>Term Grade</h5>
                </td>

                <td>{{ $termGrade }}</td>              
            </tr>

            <tr>
                <td colspan="2" class="cell-100">
                    <h5>No of School Days</h5>
                </td>

                <td>
                    {{ $school_days }}
                </td>

                <td>
                    
                </td>
                 
                <td>
                    
                </td>              
            </tr>
            <tr>
                <td colspan="2" class="cell-100">
                    <h5>Days Absent</h5>
                </td>

                <td>
                    {{ $absence_days }}
                </td>

                <td>
                    
                </td>

                <td>
                    
                </td>               
            </tr>
        </table>
        {{-- END SUMMARY OF TERM --}}

        
        {{-- PROGRESS REPORT --}}
        <h4 class="modal-title mt-40">
            PROGRESS REPORT
        </h4>

        <table class="table table-bordered">
            <tr>
                <th class="cell-50">
                    <h5>#</h5>
                </th>

                <th>
                    <h5>ExtraCurricular</h5>
                </th>

                <th>
                    <h5>Grade</h5>
                </th>
            </tr>

            @foreach($clubs as $key => $club)
                <tr>
                    <td class="cell-50">
                        {{ $key + 1 }}
                    </td>

                    <td>
                        {{ $club->club->name }}
                    </td>

                    <td>
                        {{ $club->grade }}
                    </td>
                </tr>
            @endforeach
        </table>
        {{-- END PROGRESS REPORT --}}
            
        
        {{-- DESIRABLE HABITS AND TRAITS --}}
        <h4 class="modal-title mt-40">
            DESIRABLE HABITS AND TRAITS
        </h4>

        <table class="table table-bordered">
            @foreach($habits as $habit_key => $habit)
                <tr>
                    <th>
                        <h5>{{ $habit_key }}</h5>
                    </th>
                    
                    <th colspan="5">
                        <h5>Grade</h5>
                    </th>
                </tr>

                @foreach($habit as $name_key => $habit_name)
                <tr>
                    <td class="cell-250">
                        {{ $habit_name->habit }}
                    </td>

                    @php
                        $loopCounter = 0;
                    @endphp

                    @foreach($student_habits->where('habit_id', $habit_name->id) as $grade_key => $habit_grade)
                        <td>{{ $habit_grade->grade }}</td>

                        @php
                            $loopCounter++;
                        @endphp
                    @endforeach

                    @for($i = $loopCounter; $i < 4; $i++)
                        <td></td>
                    @endfor
                </tr>
                @endforeach
            @endforeach
        </table>
        {{-- END DESIRABLE HABITS AND TRAITS --}}
    
        
        {{-- BIBLE MEMORY --}}
        <h4 class="modal-title mt-40">
            BIBLE MEMORY
        </h4>

        <table class="table table-bordered">
            <tr>
                <th colspan="3"><h5>Bible Memory</h5></th>
            </tr>

            @foreach($bible_memories as $key => $memory)
            <tr>
                <td class="cell-100">
                    {{ \Carbon\Carbon::createFromFormat('m', $key)->format('M') }}
                </td>

                @foreach($memory as $key => $memory_items)
                <td>
                    {{ $memory_items->bible_verse }}
                </td>
                @endforeach
            </tr>
            @endforeach
        </table>
        {{-- END BIBLE MEMORY --}}

        <div class="row my-80">
            <div class="col-12 text-center">
                <hr style="background-color:black;height: 2px; width: 100px;">
                <h5>LC Supervisor</h5>
            </div>

            <div class="col-6 text-center mt-40">
                <hr style="background-color:black;height: 2px; width: 100px;">
                <h5>Parent's Signature</h5>
            </div>

            <div class="col-6 text-center mt-40">
                <hr style="background-color:black;height: 2px; width: 100px;">
                <h5>VCIA Principal</h5>
            </div>

            <div class="col-12 text-center mt-40">
                <h5 class="text-uppercase">Victory Christian International Academy</h5>
                <h5>Jl. Bromo No. 6 Semarang 50232</h5>
                <h5>(024) 831 2819</h5>
                <h6>victory.cia.semarang@gmail.com</h6>
            </div>  
        </div>
    </div>
</div>

@endsection