@extends('layouts.app')
@section('content')
<!-- GALLERY -->
<div class="section-gallery pt-6 pb-0 py-lg-5">
    <div class="container">
        <div class="row py-5">
            <div class="col-12">
                <h2 class="title-c mt-0 mb-5">OUR GALLERY</h2>
                <h4 class="title mb-0">Look at our school activities gallery !</h4>
                <p class="p-a max-width-a">Victory Academy always carry out useful and educational activities for all of students, and always make an events that have elements of nationalism.</p>
                <div class="row py-5">
                    <div class="col-12 col-md-6 col-xl-4">
                        <h4 class="title mb-0">Graduation (June 2018)</h4>
                        <div class="grid">
                            <figure class="effect-layla">
                                <a data-toggle="modal" data-target="#modal1">
                                    <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0077.JPG') }}" width="100%" alt="gallery">
                                    <figcaption>
                                        <p><img src="{{ asset('img/logo.png') }}" width="100%"></p>
                                    </figcaption>
                                </a>
                            </figure>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="title text-white">Graduation</h3>
                                        <h3 class="title text-white">June 2018</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{asset ('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0023.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0023.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0026.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0026.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0029.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0029.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0032.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0032.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0045.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0045.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0057.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0057.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0067.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0067.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0077.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0077.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0084.JPG') }}" data-fancybox="graduation">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Graduation_June 2018/NDK_0084.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                        </div>
                                        <p class="p-a mt-3">2018 Graduation Day in Victory Academy Christian School</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-victory-b btn-sm" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-xl-4">
                        <h4 class="title mb-0">Kartini Day (April 2018)</h4>
                        <div class="grid">
                            <figure class="effect-layla">
                                <a data-toggle="modal" data-target="#modal2">
                                    <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070933-b.jpg') }}" width="100%" alt="gallery">
                                    <figcaption>
                                        <p><img src="{{ asset('img/logo.png') }}"" width="100%"></p>
                                    </figcaption>
                                </a>
                            </figure>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="title text-white">Kartini Day</h3>
                                        <h3 class="title text-white">April 2018</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070928.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070928.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070929.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070929.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070930.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070930.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070931.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070931.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070932.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070932.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070935.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070935.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070951.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070951.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070955.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070955.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                                <a href="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070971.JPG') }}" data-fancybox="kartini">
                                                <img class="card-img-top" src="{{ asset('img/galeri/Januari - Juni 2018/Kartini Day_April 2018/P1070971.JPG') }}" width="100%" alt="gallery"></a>
                                            </div>
                                        </div>
                                        <p class="p-a mt-3">Kartini Day in Victory Academy Christian School, Our school always uphold traditional values.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-victory-b btn-sm" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- GALLERY -->       
            </div>
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</div>
<!-- END SECTION -->
@endsection