<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <link rel="apple-touch-icon" sizes="76x76" href="./asset/img/apple-icon.png">
        <link rel="icon" type="image/jpg" href="{{ asset('img/logo.png') }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dashboard | Victory Academy Christian School</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon.png') }}">
        <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
        <!-- Stylesheets -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700|Material+Icons" />
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap-extend.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/site.css') }}">
        <!-- Dashboard CSS Files -->
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/asScrollable.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/switchery.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/introjs.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/slidePanel.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/waves.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/v1.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/profile.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/project.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/jquery.fileupload.css') }}">
        {{-- <link rel="stylesheet" href="{{ asset('css-dashboard/dropify.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/summernote.css') }}"> --}}
        <link rel="stylesheet" href="{{ asset('css-dashboard/faq.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/invoice.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/modals.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/footable.core.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/footable.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dataTables.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dataTables.fixedheader.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dataTables.fixedcolumns.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dataTables.rowgroup.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dataTables.scroller.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dataTables.select.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dataTables.responsive.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dataTables.buttons.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/datatable.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/formValidation.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/validation.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/layouts.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/select2.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap-tokenfield.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap-tagsinput.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap-select.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/icheck.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/multi-select.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/typeahead.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/advanced.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/select2.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap-tokenfield.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap-tagsinput.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap-select.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/icheck.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/multi-select.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/typeahead.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/advanced.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/contacts.css') }}">
        {{-- <link rel="stylesheet" href="{{ asset('css/dashboard/jquery-tabledit.css') }}"> --}}
        <link rel="stylesheet" href="{{ asset('css-dashboard/summernote/summernote.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/material-design/material-design.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/brand-icons/brand-icons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/web-icons/web-icons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/dropify-icons/dropify.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/fontawesome-all.min.css') }}">

        {{-- AJAX --}}
        <script type="text/javascript" src="<?php echo asset('assets/js/jquery.js'); ?>"></script>
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

        <script type="text/javascript" src="{{ asset('js/ajax/user-profile.js') }}"></script>
        {{-- AJAX --}}

        {{-- SEO --}}
        {!! SEO::generate(true) !!}
        {{-- SEO --}}
    </head>

    <body class="fade-in three page-aside-left">
        <div id="app">
            <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                        data-toggle="menubar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="hamburger-bar"></span>
                    </button>
                    <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                        data-toggle="collapse">
                    <i class="icon md-more" aria-hidden="true"></i>
                    </button>
                    <div class="navbar-brand navbar-brand-center">
                        <img class="navbar-brand-logo" src="{{ asset('img/logo.png') }}">
                        <span class="navbar-brand-text hidden-xs-down"> Victory Academy</span>
                    </div>
                </div>

                <div class="navbar-container container-fluid">
                    <!-- Navbar Collapse -->
                    <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                        <!-- Navbar Toolbar -->
                        <ul class="nav navbar-toolbar">
                            <li class="nav-item hidden-float" id="toggleMenubar">
                                <a class="nav-link" data-toggle="menubar" href="#" role="button">
                                    <i class="icon hamburger hamburger-arrow-left">
                                        <span class="sr-only">
                                            Toggle menubar
                                        </span>
                                        <span class="hamburger-bar"></span>
                                    </i>
                                </a>
                            </li>
                            <!-- Create button (+) -->
                            {{-- @if(Auth::user()->role == 'admin')
                            <li class="nav-item" id="toggleFullscreen">
                                <a class="nav-link" data-target="#CRUDmodal-main" data-toggle="modal" data-toggle="tooltip" data-placement="bottom" title="Management Main Website">
                                    <h5 class="mt-5">
                                        <i class="icon md-plus"></i>
                                        Main Website
                                    </h5>
                                </a>
                            </li>
                            @endif --}}
                            <!-- End Create Button (+) -->
                            <li class="nav-item dropdown dropdown-fw dropdown-mega"></li>
                        </ul>
                        <!-- End Navbar Toolbar -->

                        <!-- Navbar Toolbar Right -->
                        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                            <li class="nav-item dropdown">
                                <!-- Profil photo user -->
                                <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                                    data-animation="scale-up" role="button">
                                    <span class="avatar avatar-online">
                                        <img src="{{ asset('file_uploads/users/'.Auth::user()->profile->profile_picture) }}">

                                        <i></i>
                                    </span>
                                </a>
                                <!-- Profil photo user -->

                                <div class="dropdown-menu" role="menu">
                                    <!-- menu profile settings user -->
                                    <a class="dropdown-item" href="{{ route('user_profile.show', Auth::user()->id) }}" role="menuitem">
                                        <i class="icon md-account" aria-hidden="true"></i> Profile
                                    </a>
                                    <!-- menu profile settings user -->

                                    <!-- settings menu -->
                                    <a class="dropdown-item" data-target="#EditAccount" data-toggle="modal"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
                                    <!-- settings menu -->

                                    <!-- logout menu -->
                                    <div class="dropdown-divider" role="presentation"></div>

                                    <a class="dropdown-item" href="{{ route('logout') }}" role="menuitem">
                                        <i class="icon md-power" aria-hidden="true"></i> Logout
                                    </a>
                                    <!-- logout menu -->
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <!-- notification system -->
                                {{-- <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Notifications"
                                    aria-expanded="false" data-animation="scale-up" role="button">
                                    <i class="icon md-notifications" aria-hidden="true"></i>
                                    <span class="badge badge-pill badge-danger up">5</span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                                    <div class="dropdown-menu-header">
                                        <h5>NOTIFICATIONS</h5>
                                        <span class="badge badge-round badge-danger">New 5</span>
                                    </div>

                                    <!-- Notification -->
                                    <div class="list-group">
                                        <div data-role="container">
                                            <div data-role="content">
                                                <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                                    <div class="media">
                                                        <div class="pr-10">
                                                            <i class="icon md-receipt bg-red-600 white icon-circle" aria-hidden="true"></i>
                                                        </div>

                                                        <div class="media-body">
                                                            <h6 class="media-heading">
                                                                A new quiz has been uploaded
                                                            </h6>

                                                            <time class="media-meta" datetime="2017-06-12T20:50:48+08:00">1 hours ago</time>
                                                        </div>
                                                    </div>
                                                </a>

                                                <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                                    <div class="media">
                                                        <div class="pr-10">
                                                            <i class="icon md-account bg-green-600 white icon-circle" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading">Benson completed the task</h6>
                                                            <time class="media-meta" datetime="2017-06-11T18:29:20+08:00">5 hours ago</time>
                                                        </div>
                                                    </div>
                                                </a>

                                                <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                                    <div class="media">
                                                        <div class="pr-10">
                                                            <i class="icon md-settings bg-red-600 white icon-circle" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading">Settings updated</h6>
                                                            <time class="media-meta" datetime="2017-06-11T14:05:00+08:00">6 days ago</time>
                                                        </div>
                                                    </div>
                                                </a>

                                                <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                                    <div class="media">
                                                        <div class="pr-10">
                                                            <i class="icon md-account bg-green-600 white icon-circle" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading">Chandra missed the task</h6>
                                                            <time class="media-meta" datetime="2017-06-11T18:29:20+08:00">1 week ago</time>
                                                        </div>
                                                    </div>
                                                </a>

                                                <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                                                    <div class="media">
                                                        <div class="pr-10">
                                                            <i class="icon md-comment bg-orange-600 white icon-circle" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading">Message received</h6>
                                                            <time class="media-meta" datetime="2017-06-10T12:34:48+08:00">3 days ago</time>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <!-- End Notification -->
                                </div>
                            </li>
                        </ul>
                        <!-- End Navbar Toolbar Right -->
                    </div>
                    <!-- notification system -->
                    <!-- End Navbar Collapse -->
                </div>
            </nav>

            @if(Auth::user()->role == 'admin')
            <!-- Management Main Page Modal -->
            <div class="modal fade modal-fill-in" id="CRUDmodal-main" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-simple">
                    <div class="modal-content" style="max-width: 100%;">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <!-- Title -->
                            <h4 class="modal-title" id="exampleModalTabs">Management Main Website</h4>
                            <!-- End Title -->
                        </div>
                        <!-- End Modal Header -->

                        <!-- Menu Tab -->
                        <ul class="nav nav-tabs nav-tabs-line pb-70 pb-lg-30" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" data-toggle="tab" href="#profile" aria-controls="list" role="tab">Profile</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#profile" aria-controls="list" role="tab">Gallery</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#profile" aria-controls="list" role="tab">Clubs</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#profile" aria-controls="list" role="tab">Academics</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#news" aria-controls="create" role="tab">News & Achievement</a>
                            </li>
                        </ul>
                        <!-- End Menu Tab -->

                        <div class="modal-body">
                            <div class="tab-content">
                                <!-- Create Form Profile -->
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <form class="form-horizontal pt-30" id="exampleSummaryForm" autocomplete="off">
                                        <div class="summary-errors alert alert-danger alert-dismissible">
                                            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                                                <span aria-hidden="true">×</span>
                                            </button>

                                            <p>Errors list below: </p>
                                            <ul></ul>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Title
                                                <span class="required">*</span>
                                            </label>

                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="title" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Description</label>
                                            <!-- Panel Editor -->
                                            <div class="panel-body py-10 pr-xl-10 pl-xl-50">
                                                <div id="summernote" data-plugin="summernote">
                                                    <h2>WYSIWYG Editor</h2>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper sapien non nisl facilisis bibendum in quis tellus. Duis in urna bibendum turpis pretium fringilla.</p>
                                                    <h4>Lacinia</h4>
                                                </div>
                                            </div>
                                            <!-- End Panel Editor -->
                                        </div>
                                        <div class="text-right">
                                            <a href="./dashboard/profile/index.blade.php" class="btn btn-victory-c">List</a>
                                            <button type="submit" class="btn btn-victory-c" id="validateButton3">Create</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- End Create Form Profile -->
                                <!-- Create Form Gallery -->
                                <div class="tab-pane" id="gallery" role="tabpanel">
                                    <form class="form-horizontal pt-30" id="exampleSummaryForm" autocomplete="off">
                                        <div class="summary-errors alert alert-danger alert-dismissible">
                                            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                                            <span aria-hidden="true">×</span>
                                            </button>
                                            <p>Errors list below: </p>
                                            <ul>
                                            </ul>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Title
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="title" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Description</label>
                                            <!-- Panel Editor -->
                                            <div class="panel-body py-10 pr-xl-10 pl-xl-50">
                                                <div id="summernote" data-plugin="summernote">
                                                    <h2>WYSIWYG Editor</h2>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper sapien non nisl facilisis bibendum in quis tellus. Duis in urna bibendum turpis pretium fringilla.</p>
                                                    <h4>Lacinia</h4>
                                                </div>
                                            </div>
                                            <!-- End Panel Editor -->
                                        </div>
                                        <div class="text-right">
                                            <a href="#" class="btn btn-victory-c">List</a>
                                            <button type="submit" class="btn btn-victory-c" id="validateButton3">Create</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- End Create Form Gallery -->

                                <!-- Create Form Clubs -->
                                <div class="tab-pane" id="clubs" role="tabpanel">
                                    <form class="form-horizontal pt-30" id="exampleSummaryForm" autocomplete="off">
                                        <div class="summary-errors alert alert-danger alert-dismissible">
                                            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                                            <span aria-hidden="true">×</span>
                                            </button>
                                            <p>Errors list below: </p>
                                            <ul>
                                            </ul>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Title
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="title" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Description</label>
                                            <!-- Panel Editor -->
                                            <div class="panel-body py-10 pr-xl-10 pl-xl-50">
                                                <div id="summernote" data-plugin="summernote">
                                                    <h2>WYSIWYG Editor</h2>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper sapien non nisl facilisis bibendum in quis tellus. Duis in urna bibendum turpis pretium fringilla.</p>
                                                    <h4>Lacinia</h4>
                                                </div>
                                            </div>
                                            <!-- End Panel Editor -->
                                        </div>
                                        <div class="text-right">
                                            <a href="#" class="btn btn-victory-c">List</a>
                                            <button type="submit" class="btn btn-victory-c" id="validateButton3">Create</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- End Create Form Clubs -->
                                <!-- Create Form Academics -->
                                <div class="tab-pane" id="academics" role="tabpanel">
                                    <form method="POST" action="{{ route('academics.store') }}" class="form-horizontal pt-30" id="exampleSummaryForm" autocomplete="off">
                                        <div class="summary-errors alert alert-danger alert-dismissible">
                                            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                                            <span aria-hidden="true">×</span>
                                            </button>
                                            <p>Errors list below: </p>
                                            <ul>
                                            </ul>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Title
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="name" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Description</label>
                                            <!-- Panel Editor -->
                                            <div class="panel-body py-10 pr-xl-10 pl-xl-50">
                                                <div id="summernote" data-plugin="summernote">
                                                    <h2>WYSIWYG Editor</h2>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper sapien non nisl facilisis bibendum in quis tellus. Duis in urna bibendum turpis pretium fringilla.</p>
                                                    <h4>Lacinia</h4>
                                                </div>
                                            </div>
                                            <!-- End Panel Editor -->
                                        </div>
                                        <div class="text-right">
                                            <a href="#" class="btn btn-victory-c">List</a>
                                            <button type="submit" class="btn btn-victory-c" id="validateButton3">Create</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- End Create Form Academics -->
                                <!-- Create Form News -->
                                <div class="tab-pane" id="news" role="tabpanel">
                                    <form class="form-horizontal pt-30" id="exampleSummaryForm" autocomplete="off">
                                        <div class="summary-errors alert alert-danger alert-dismissible">
                                            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                                            <span aria-hidden="true">×</span>
                                            </button>
                                            <p>Errors list below: </p>
                                            <ul>
                                            </ul>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Title
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="title" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Category
                                            <span class="required">*</span>
                                            </label>
                                            <div class="col-md-10">
                                                <select class="form-control" id="category" name="category" required="">
                                                    <option value="">Choose a Category</option>
                                                    <option value="yahoo">News</option>
                                                    <option value="microsoft">Achievement</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row form-material">
                                            <label class="col-md-2 form-control-label">Description</label>
                                            <!-- Panel Editor -->
                                            <div class="panel-body py-10 pr-xl-10 pl-xl-50">
                                                <div id="summernote" data-plugin="summernote">
                                                    <h2>WYSIWYG Editor</h2>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper sapien non nisl facilisis bibendum in quis tellus. Duis in urna bibendum turpis pretium fringilla.</p>
                                                    <h4>Lacinia</h4>
                                                </div>
                                            </div>
                                            <!-- End Panel Editor -->
                                        </div>
                                        <div class="text-right">
                                            <a href="#" class="btn btn-victory-c">List</a>
                                            <button type="submit" class="btn btn-victory-c" id="validateButton3">Create</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- End Create Form News -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Management Main Page Modal -->
            @endif
            
            <!-- Site Menu Bar -->
            <div class="site-menubar">
                <div class="site-menubar-body">
                    <div>
                        <div>
                            <ul class="site-menu" data-plugin="menu">
                                <li class="site-menu-category">General</li>
                                <!-- Profile -->
                                <li class="site-menu-item">
                                    <a href="{{ route('user_profile.show', Auth::user()->id) }}">
                                    <i class="site-menu-icon md-home" aria-hidden="true"></i>
                                    <span class="site-menu-title">Home</span>
                                    </a>
                                </li>
                                <!-- End Profile -->

                                <!-- FAQ -->
                                <li class="site-menu-item">
                                    <a href="{{ route('faq') }}">
                                    <i class="site-menu-icon md-pin-help" aria-hidden="true"></i>
                                    <span class="site-menu-title">FAQ</span>
                                    </a>
                                </li>
                                <!-- End FAQ -->

                                <!-- Site Bar 2 -->
                                <li class="site-menu-category">Management</li>
                                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                <!-- Teacher Access -->
                                <li class="site-menu-item">
                                    <a href="{{ route('teachers.index') }}">
                                    <i class="site-menu-icon md-male-female" aria-hidden="true"></i>
                                    <span class="site-menu-title">Staff</span>
                                    </a>
                                </li>
                                <!-- End Teacher Access -->
                                @endif
                                
                                @if(Auth::user()->role == 'admin')
                                <!-- Staff Management Access -->
                                <li class="site-menu-item">
                                    <a href="{{ route('staffs-managements.index') }}">
                                    <i class="site-menu-icon md-folder-person" aria-hidden="true"></i>
                                    <span class="site-menu-title">Staff Management</span>
                                    </a>
                                </li>
                                <!-- End Staff Management Access -->
                                @endif

                                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                <!-- Student Access -->
                                <li class="site-menu-item">
                                    <a href="{{ route('students.index') }}">
                                    <i class="site-menu-icon md-face" aria-hidden="true"></i>
                                    <span class="site-menu-title">Student</span>
                                    </a>
                                </li>
                                <!-- End Student Access -->
                                @endif
                                
                                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'teacher')
                                <!-- Parent Access -->
                                <li class="site-menu-item">
                                    <a href="{{ route('parents.index') }}">
                                    <i class="site-menu-icon md-male" aria-hidden="true"></i>
                                    <span class="site-menu-title">Parent</span>
                                    </a>
                                </li>
                                <!-- End Parent Access -->
                                @endif
                                
                                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'student')
                                <!-- Courses Access -->
                                <li class="site-menu-item">
                                    <a href="{{ route('courses.index') }}">
                                    <i class="site-menu-icon md-book" aria-hidden="true"></i>
                                    <span class="site-menu-title">Book</span>
                                    </a>
                                </li>
                                <!-- End Courses Access -->
                                @endif
                                
                                @if(Auth::user()->role == 'admin')
                                <!-- Academic Year Access -->
                                <li class="site-menu-item">
                                    <a href="{{ route('academic-years.index') }}">
                                    <i class="site-menu-icon md-calendar" aria-hidden="true"></i>
                                    <span class="site-menu-title">Academic</span>
                                    </a>
                                </li>
                                <!-- End Academic Year Access -->
                                @endif
                                
                                @if(Auth::user()->role != 'teacher')
                                <!-- Payment Access -->
                                <li class="site-menu-item">
                                    <a href="{{ route('payments.index') }}">
                                    <i class="site-menu-icon md-money" aria-hidden="true"></i>
                                    <span class="site-menu-title">Payment</span>
                                    </a>
                                </li>
                                <!-- End Payment Access -->
                                @endif
                                

                                @if(Auth::user()->role == 'admin')
                                <!-- Inventory Access -->
                                <li class="site-menu-item">
                                    <a href="{{ route('inventories.index') }}">
                                    <i class="site-menu-icon md-folder" aria-hidden="true"></i>
                                    <span class="site-menu-title">Inventory</span>
                                    </a>
                                </li>
                                <!-- End Inventory Access -->
                                <!-- End Site Bar 2 -->

                                <!-- Site Bar 3 -->
                                <li class="site-menu-category">Management Press</li>
                                <!-- News (run to website) -->
                                <li class="site-menu-item">
                                    <a href="{{ route('news.index') }}">
                                    <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                                    <span class="site-menu-title">News</span>
                                    </a>
                                </li>
                                <!-- End News (run to website) -->

                                <!-- Feedback (from contact form website) -->
                                <li class="site-menu-item">
                                    <a href="{{ route('contact-forms.index') }}">
                                    <i class="site-menu-icon md-comment" aria-hidden="true"></i>
                                    <span class="site-menu-title">Feedback</span>
                                    </a>
                                </li>
                                <!-- End Feedback (from contact form website) -->
                                <!-- End Site Bar 3 -->
                                @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Site Bar Footer (Logout) -->
            <div class="site-menubar-footer">
                <a href="{{ route('logout') }}" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
                <span class="icon md-power" aria-hidden="true"></span>
                </a>
            </div>
            <!-- End Site Menu Bar -->



            <!-- Edit Profile Modal -->
            <div class="modal fade modal-fill-in" id="EditAccount" aria-hidden="false" aria-labelledby="exampleFillIn" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-simple">
                    <div class="modal-content" style="max-width: 100%;">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>

                            <!-- Title -->
                            <h4 class="modal-title" id="exampleModalTabs">Settings</h4>
                            <!-- End Title -->
                        </div>
                        <!-- End Modal Header -->
                        <!-- Modal Body-->
                        <div class="modal-body">
                            <div class="tab-content">
                                <!-- Edit User Menu -->
                                <div class="tab-pane active" id="user" role="tabpanel">
                                    <div class="row mt-20">
                                        <div class="col-12">
                                            <!-- Edit User Form -->
                                            <div class="example-wrap">
                                                <div class="example">
                                                    <form method="POST" action="{{ route('account-update', Auth::user()->id) }}" enctype="multipart/form-data" autocomplete="off">
                                                        <div class="row">
                                                            {{-- CSRF TOKEN --}}
                                                            {{ csrf_field() }}
                                                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                                                            <input name="_method" type="hidden" value="PATCH">
                                                            {{-- CSRF TOKEN --}}


                                                            <!-- Change Full Name -->
                                                            <div class="form-group form-material col-12">
                                                                <label class="form-control-label" for="inputBasicFirstName">Full Name</label>

                                                                <input type="text" class="form-control" id="inputBasicFirstName" name="name" placeholder="Change Full Name" autocomplete="off" value="{{ Auth::user()->name }}" />
                                                            </div>
                                                            <!-- End Change Full Name -->

                                                            @if(Auth::user()->role == 'admin')
                                                            <!-- Change Email -->
                                                            <div class="form-group form-material col-12 col-lg-6">
                                                                <label class="form-control-label" for="inputBasicFirstName">Old Email</label>

                                                                <input type="text" class="form-control" id="inputBasicFirstName" name="old_email" placeholder="Old Email" autocomplete="off" value="{{ Auth::user()->email }}" />
                                                            </div>
                                                            <div class="form-group form-material col-12 col-lg-6">
                                                                <label class="form-control-label" for="inputBasicFirstName">New Email</label>

                                                                <input type="text" class="form-control" id="inputBasicFirstName" name="new_email" placeholder="New Email" autocomplete="off" value="" />
                                                            </div>
                                                            <!-- End Change Email -->
                                                            @endif

                                                            <!-- Change Password -->
                                                            @if(Auth::user()->role != 'admin')
                                                            <div class="form-group form-material col-12 col-lg-6">
                                                                <label class="form-control-label" for="inputBasicFirstName">Old Password</label>

                                                                <input type="password" class="form-control" id="inputBasicFirstName" name="old_password" placeholder="Old Password" autocomplete="off" />
                                                            </div>
                                                            @endif

                                                            <div class="form-group form-material col-12 col-lg-6">
                                                                <label class="form-control-label" for="inputBasicFirstName">New Password</label>

                                                                <input type="password" class="form-control" id="inputBasicFirstName" name="password" placeholder="New Password" autocomplete="off" value="" />
                                                            </div>

                                                            <div class="form-group form-material col-12 col-lg-6">
                                                                <label class="form-control-label" for="inputBasicFirstName">Confirm Password</label>

                                                                <input type="password" class="form-control" id="inputBasicFirstName" name="password_confirmation" placeholder="Confirm New Password" autocomplete="off" value="" />
                                                            </div>
                                                            <!-- End Change Password -->
                                                        </div>
                                                        
                                                        @if(Auth::user()->role == 'admin')
                                                        <!-- Subject Option -->
                                                        <div class="example">
                                                            <label class="form-control-label">Status</label>
                                                            <select name="status" class="form-control" data-plugin="select2" data-placeholder="Select" data-allow-clear="true">
                                                                <option value="1">Active</option>
                                                                <option value="0">Inactive</option>
                                                                <option value="2">Suspended</option>
                                                            </select>
                                                        </div>
                                                        <!-- End Subject Option -->
                                                        @endif

                                                        <div class="form-group form-material">
                                                            <!-- Button Sign Up -->
                                                            <button type="submit" class="btn btn-victory-c">Save</button>
                                                            <!-- End Button Sign Up -->
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- End Edit User Form -->
                                        </div>
                                    </div>
                                </div>
                                <!-- End Edit User Menu -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Edit Profile Page Modal -->


            {{-- CONTENT --}}
            @yield('content')
            {{-- CONTENT --}}
        </div>

        <!-- Core  -->
        <script src="{{ asset('js-dashboard/babel-external-helpers.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-ui.js') }}"></script>
        <script src="{{ asset('js-dashboard/tmpl.js') }}"></script>
        <script src="{{ asset('js-dashboard/canvas-to-blob.js') }}"></script>
        <script src="{{ asset('js-dashboard/load-image.all.min.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/jquery.fileupload.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.fileupload-process.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.fileupload-image.js') }}"></script> --}}
        {{-- <script src="{{ asset('js-dashboard/jquery.fileupload-audio.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.fileupload-video.js') }}"></script> --}}
        {{-- <script src="{{ asset('js-dashboard/jquery.fileupload-validate.js') }}"></script> --}}
        {{-- <script src="{{ asset('js-dashboard/jquery.fileupload-ui.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/jquery.js') }}"></script>
        <script src="{{ asset('js-dashboard/popper.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootstrap.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.mousewheel.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-asScrollbar.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-asScrollable.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-asHoverScroll.js') }}"></script>
        <script src="{{ asset('js-dashboard/waves.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/project.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/dropify.js') }}"></script>
        <script src="{{ asset('js-dashboard/summernote.js') }}"></script>
        <!-- Plugins -->
       {{--  <script src="{{ asset('js-dashboard/switchery.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/intro.js') }}"></script>
        <script src="{{ asset('js-dashboard/screenfull.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-slidePanel.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.matchHeight-min.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.peity.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-asProgress.js') }}"></script>
        <script src="{{ asset('js-dashboard/moment.min.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/footable.min.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/jquery-asBreadcrumbs.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('js-dashboard/dataTables.bootstrap4.js') }}"></script>
        <script src="{{ asset('js-dashboard/dataTables.fixedHeader.js') }}"></script>
        <script src="{{ asset('js-dashboard/dataTables.fixedColumns.js') }}"></script>
        <script src="{{ asset('js-dashboard/dataTables.rowGroup.js') }}"></script>
        <script src="{{ asset('js-dashboard/dataTables.scroller.js') }}"></script>
        <script src="{{ asset('js-dashboard/dataTables.responsive.js') }}"></script>
        <script src="{{ asset('js-dashboard/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('js-dashboard/dataTables.buttons.js') }}"></script>
        <script src="{{ asset('js-dashboard/buttons.html5.js') }}"></script>
        <script src="{{ asset('js-dashboard/buttons.flash.js') }}"></script>
        <script src="{{ asset('js-dashboard/buttons.print.js') }}"></script>
        <script src="{{ asset('js-dashboard/buttons.colVis.js') }}"></script>
        <script src="{{ asset('js-dashboard/buttons.bootstrap4.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-asRange.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootbox.js') }}"></script>
        <script src="{{ asset('js-dashboard/formValidation.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootstrap4.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.placeholder.js') }}"></script>
        <script src="{{ asset('js-dashboard/select2.full.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootstrap-tokenfield.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootstrap-select.js') }}"></script>
        <script src="{{ asset('js-dashboard/icheck.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.multi-select.js') }}"></script>
        <script src="{{ asset('js-dashboard/bloodhound.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/typeahead.jquery.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/summernote.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-asPaginator.min.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery.tabledit.min.js') }}"></script>

        <!-- Scripts -->
        <script src="{{ asset('js-dashboard/Component.js') }}"></script>
        <script src="{{ asset('js-dashboard/Plugin.js') }}"></script>
        <script src="{{ asset('js-dashboard/Base.js') }}"></script>
        <script src="{{ asset('js-dashboard/Config.js') }}"></script>
        <script src="{{ asset('js-dashboard/breakpoints.js') }}"></script>
        <script>
            Breakpoints();
        </script>
        <script src="{{ asset('js-dashboard/Menubar.js') }}"></script>
        <script src="{{ asset('js-dashboard/GridMenu.js') }}"></script>
        <script src="{{ asset('js-dashboard/Sidebar.js') }}"></script>
        <script src="{{ asset('js-dashboard/PageAside.js') }}"></script>
        <script src="{{ asset('js-dashboard/menu.js') }}"></script>
        <script src="{{ asset('js-dashboard/colors.js') }}"></script>
        <script src="{{ asset('js-dashboard/tour.js') }}"></script>
        <script>Config.set('assets', '../assets');</script>

        <!-- Page -->
        {{-- <script src="{{ asset('js-dashboard/faq.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/datatables.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/datatable.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/icon.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/validation.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/jquery-placeholder.js') }}"></script>
        <script src="{{ asset('js-dashboard/select2.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootstrap-tokenfield.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootstrap-tagsinput.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootstrap-select.js') }}"></script>
        <script src="{{ asset('js-dashboard/icheck.js') }}"></script>
        <script src="{{ asset('js-dashboard/multi-select.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/advanced.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/summernote.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/editor-summernote.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/sticky-header.js') }}"></script>
        <script src="{{ asset('js-dashboard/action-btn.js') }}"></script>
        <script src="{{ asset('js-dashboard/asselectable.js') }}"></script>
        <script src="{{ asset('js-dashboard/editlist.js') }}"></script>
        <script src="{{ asset('js-dashboard/aspaginator.js') }}"></script>
        <script src="{{ asset('js-dashboard/animate-list.js') }}"></script>
        <script src="{{ asset('js-dashboard/jquery-placeholder.js') }}"></script>
        <script src="{{ asset('js-dashboard/material.js') }}"></script>
        <script src="{{ asset('js-dashboard/selectable.js') }}"></script>
        <script src="{{ asset('js-dashboard/bootbox.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/BaseApp.js') }}"></script> --}}
        {{-- <script src="{{ asset('js-dashboard/Contacts.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/contacts.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/jsjqtabledit.js') }}"></script> --}}
        {{--  <script src="{{ asset('js-dashboard/v1.js') }}"></script> --}}
        <script src="{{ asset('js/delete-modal.js') }}"></script>
        <script src="{{ asset('js-dashboard/Site.js') }}"></script>
        <script src="{{ asset('js-dashboard/asscrollable.js') }}"></script>
        <script src="{{ asset('js-dashboard/slidepanel.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/switchery.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/matchheight.js') }}"></script>
        <script src="{{ asset('js-dashboard/peity.js') }}"></script>
        <script src="{{ asset('js-dashboard/panel.js') }}"></script>
        <script src="{{ asset('js-dashboard/panel-actions.js') }}"></script>
        {{-- <script src="{{ asset('js-dashboard/footable.js') }}"></script> --}}
        <script src="{{ asset('js-dashboard/asbreadcrumbs.js') }}"></script>
        <script src="{{ asset('js-dashboard/fontawesome-all.min.js') }}"></script>
        <script>
            (function(document, window, $){
              'use strict';
            
              var Site = window.Site;
              $(document).ready(function(){
                Site.run();
              });
            })(document, window, jQuery);
        </script>
        <script>
        
        var rupiah = document.getElementById('amount');
        rupiah.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah.value = formatRupiah(this.value, '');
        });
 
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
 
            rupiah = split[3] != undefined ? rupiah + ',' + split[3] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
        </script>


        <script>
        var rupiahBroadcast = document.getElementById('amount-broadcast');
        rupiahBroadcast.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiahBroadcast() untuk mengubah angka yang di ketik menjadi format angka
            rupiahBroadcast.value = formatRupiahBroadcast(this.value, '');
        });
 
        /* Fungsi formatRupiahBroadcast */
        function formatRupiahBroadcast(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiahBroadcast          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiahBroadcast += separator + ribuan.join('.');
            }
 
            rupiahBroadcast = split[3] != undefined ? rupiahBroadcast + ',' + split[3] : rupiahBroadcast;
            return prefix == undefined ? rupiahBroadcast : (rupiahBroadcast ? '' + rupiahBroadcast : '');
        }
        </script>
    </body>
</html>