<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./asset/img/apple-icon.png">
        <link rel="icon" type="image/jpg" href="{{ asset('img/logo.png') }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Victory Academy Christian School - Login</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!-- Stylesheet  -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('css-login/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/bootstrap-extend.css') }}">
        <!-- Login CSS Files -->
        <link rel="stylesheet" href="{{ asset('css-login/site.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/animsition/animsition.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/asscrollable/asScrollable.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/switchery/switchery.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/intro-js/introjs.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/slidepanel/slidePanel.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/waves/waves.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/login-v3.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/material-design/material-design.css') }}">
        <link rel="stylesheet" href="{{ asset('css-login/brand-icons/brand-icons.min.css') }}">
        {{-- SEO --}}
        {!! SEO::generate(true) !!}
        {{-- SEO --}}
    </head>
    <body class="animsition page-login-v3 layout-full">
        <div id="app">
            {{-- CONTENT --}}
            @yield('content')
            {{-- CONTENT --}}
        </div>
        <!--   Login JS Files   -->
        <script src="{{ asset('js-login/babel-external-helpers.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/jquery.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/popper.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/bootstrap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/animsition/animsition.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/mousewheel/jquery.mousewheel.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/asscrollbar/jquery-asScrollbar.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/asscrollable/jquery-asScrollable.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/ashoverscroll/jquery-asHoverScroll.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/waves/waves.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js-login/Site.js') }}" type="text/javascript"></script>
        <script>
            (function(document, window, $){
              'use strict';
            
              var Site = window.Site;
              $(document).ready(function(){
                Site.run();
              });
            })(document, window, jQuery);
        </script>
    </body>
</html>