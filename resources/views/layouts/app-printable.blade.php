<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <link rel="apple-touch-icon" sizes="76x76" href="./asset/img/apple-icon.png">
        <link rel="icon" type="image/jpg" href="{{ asset('img/logo.png') }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dashboard | Victory Academy Christian School</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon.png') }}">
        <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
        <!-- Stylesheets -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700|Material+Icons" />
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/bootstrap-extend.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/site.css') }}">
        <link rel="stylesheet" href="{{ asset('css-dashboard/material-design/material-design.css') }}">
    </head>

    <body class="index-page sidebar-collapse animated fadeIn">
        <div id="app">
            <div class="container-fluid mb-50">
                <div class="row pb-20" style="border-bottom-style: double; border-color: black;border-width: 3px;">
                    <div class="col-4 text-right">
                        <img src="{{ asset('img/logo.png') }}" alt="logo" style="width: 130px">
                    </div>
                    <div class="col-8 text-left">
                        <h3 class="text-uppercase mt-0">Victory Christian Academy</h3>
                        <h4>Address : Jl. Bromo No.6 Semarang 50232</h4>
                        <h4>Phone : (024) 831 2819</h4>
                        <h5>Email : victory.cia.semarang@gmail.com</h5>
                    </div>
                </div>
            </div>

            {{-- CONTENT --}}
            @yield('content')
            {{-- CONTENT --}}
        
        </div>
    
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/core/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/core/popper.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
    
    </body>
</html>