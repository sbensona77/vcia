<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135379830-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        
        function gtag(){
            dataLayer.push(arguments);
        }
        
        gtag('js', new Date());
        gtag('config', 'UA-135379830-2');
    </script>

    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="./asset/img/apple-icon.png">
    <link rel="icon" type="image/jpg" href="{{ asset('img/logo.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  
    <!-- CSS Files -->
    <link href="{{ asset('css/material-kit.css?v=2.0.4') }}" rel="stylesheet" />
    <link href="{{ asset('css/section.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/slide-animation.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/hover-effect-b.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/image-animation.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/jquery.fancybox.css') }}" rel="stylesheet" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    {{-- SEO --}}
    {!! SEO::generate(true) !!}
    {{-- SEO --}}
</head>

<body class="index-page sidebar-collapse animated fadeIn">
    <div id="app">
        <nav class="navbar navbar-first navbar-expand-xl pt-3 pt-lg-4">
            <div class="container">
                <div class="navbar-translate">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('img/logo.png') }}" class="logo d-inline-block align-top" alt="logo">
                        Victory Academy
                    </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon"></span>
                        <span class="navbar-toggler-icon"></span>
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item-right">
                            <!-- LOGIN -->
                            @if(!Auth::check())
                                <a class="nav-link" href="{{ route('login') }}" target="_blank">
                                    <i class="material-icons mr-1">people</i> Login
                                </a>
                            @else
                                <a class="nav-link" href="{{ route('dashboard') }}" target="_blank">
                                    <i class="material-icons mr-1">people</i> Hello {{ Auth::user()->name }}!
                                </a>
                            @endif
                            <!-- LOGIN -->
                        </li>

                        <li class="nav-item-right">
                            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/victoryschool.semarang" target="_blank" data-original-title="Like us on Facebook">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </li>

                        <li class="nav-item-right">
                            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/victory_cia" target="_blank" data-original-title="Follow us on Instagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>    
            </div>
        </nav>

        <!-- NAVBAR -->
        <nav class="navbar navbar-second fixed-top navbar-expand-xl">
            <div class="container" style="background-color: #047CA4">
                <div class="collapse navbar-collapse" style="background-color:#047CA4">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('/') ? 'active' : null }}" href="{{ route('welcome') }}">
                                Home
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle {{ Request::is('profile') ? 'active' : null }} {{ Request::is('gallery') ? 'active' : null }}" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                About
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" {{ Request::is('profile') ? 'active' : null }} rel="tooltip" title="" data-placement="bottom" href="{{ route('profile') }}">Profile</a>

                                <a class="dropdown-item" {{ Request::is('gallery') ? 'active' : null }} rel="tooltip" title="" data-placement="bottom" href="{{ route('gallery') }}">Gallery</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('news') ? 'active' : null }}" rel="tooltip" title="" data-placement="bottom" href="{{ route('welcome.news') }}">
                                News
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('clubs') ? 'active' : null }}" rel="tooltip" title="" data-placement="bottom" href="{{ route('clubs') }}">
                                Clubs
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('academics') ? 'active' : null }}" rel="tooltip" title="" data-placement="bottom" href="{{ route('academics') }}">
                                Academics
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('achievement') ? 'active' : null }}" rel="tooltip" title="" data-placement="bottom" href="{{ route('welcome.achievement') }}">
                                Achievement
                            </a>
                        </li>
                    </ul>


                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item-right">
                            <a class="nav-link" href="mailto:victory.cia.semarang@gmail.com" target="_blank" rel="tooltip" title="" data-placement="bottom">
                                <i class="material-icons mr-2 float-left" style="line-height: 2em">
                                    email
                                </i>

                                <h6 class="float-lg-right mt-0" style="font-family: 'Lato'; font-size: 16px; text-transform: none; line-height: 1.5em">victory.cia.semarang@gmail.com/<br>halo@victory.sch.id</h6>
                            </a>
                        </li>

                        <li class="nav-item-right">
                            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Call us">
                                <i class="material-icons mr-1">
                                    phone
                                </i>
                                024-8312819
                            </a>
                        </li>

                        <!-- MENU FOR SMALL SCREEN -->
                        <li class="nav-item-right d-block d-xl-none">
                            @if(!Auth::check())
                                <a class="nav-link" href="{{ route('login') }}">
                                    <i class="material-icons mr-1">people</i> 
                                    Login
                                </a>
                            @else
                                <a class="nav-link" href="{{ route('dashboard') }}">
                                    <i class="material-icons mr-1"">people</i> Hello {{ Auth::user()->name }}!
                                </a>
                            @endif
                        </li>

                        <li class="nav-item-right d-block d-xl-none text-center">
                            <a href="https://www.facebook.com/victoryschool.semarang" target="_blank">
                                <i class="fa fa-facebook-square 2x text-white mr-3 mt-3"></i>
                            </a>

                            <a href="https://www.instagram.com/victory_cia" target="_blank">
                                <i class="fa fa-instagram 2x text-white mr-3 mt-3"></i>
                            </a>
                        </li>
                        <!-- MENU FOR SMALL SCREEN -->

                    </ul>
                </div>
            </div>
        </nav>
        <!-- NAVBAR -->

        {{-- CONTENT --}}
        @yield('content')
        {{-- CONTENT --}}
    </div>

    <!-- SOCMED -->
    <div class="section-socmed py-6">
        <div class="container">
            <div class="row">
                <div class="col-8 offset-2 col-md-6 offset-md-0 col-lg-3 mt-5 mt-lg-0 text-center text-md-left">
                    <a href="https://www.facebook.com/victoryschool.semarang" target="_blank">
                        <i class="fa fa-facebook-square fa-3x text-white"></i>
                        <h3 class="title text-white mt-3 mb-1">Facebook</h3>

                        <p class="p-a text-white">
                            Follow and like our official facebook page
                        </p>
                    </a>
                </div>
                <div class="col-8 offset-2 col-md-6 offset-md-0 col-lg-3 mt-5 mt-lg-0 text-center text-md-left">
                    <a href="https://www.instagram.com/victory_cia" target="_blank">
                        <i class="fa fa-instagram fa-3x text-white"></i>
                        <h3 class="title text-white mt-3 mb-1">Instagram</h3>

                        <p class="p-a text-white">
                            Like and share our post in instagram, don't forget to follow us
                        </p>
                    </a>
                </div>

                <div class="col-8 offset-2 col-md-6 offset-md-0 col-lg-3 mt-5 mt-lg-0 text-center text-md-left">
                    <a href="#">
                        <i class="fa fa-envelope fa-3x text-white"></i>
                        <h3 class="title text-white mt-3 mb-1">Email</h3>

                        <p class="p-a text-white">
                            Ask us anything via email
                        </p>
                    </a>
                </div>

                <div class="col-8 offset-2 col-md-6 offset-md-0 col-lg-3 mt-5 mt-lg-0 text-center text-md-left">
                    <a href="#">
                        <i class="fa fa-phone fa-3x text-white"></i>
                        <h3 class="title text-white mt-3 mb-1">Phone</h3>

                        <p class="p-a text-white">
                           Call us to ask something via phone 
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- SOCMED -->
    
    <!-- FOOTER -->
    <footer class="footer">
        <div class="container">
            <div class="row pb-5" style="border-bottom: 2px solid  #047CA4;">
                <div class="col-12 col-md-6 col-lg-4">
                    <img src="{{ asset('img/logo.png') }}" width="80px;" alt="logo">
                    <h4 class="title text-white mt-2">VICTORY ACADEMY</h4>

                    <ul>
                        <li>
                            Jl. Bromo No.6, Gajahmungkur,
                            <br>
                            Kota Semarang, Jawa Tengah 50232
                        </li>

                        <a class="btn btn-victory btn-sm mt-2 mb-3" href="https://www.google.co.id/maps/place/Victory+Academy/@-7.0145883,110.414056,17z/data=!3m1!4b1!4m5!3m4!1s0x2e708b64fb0593bd:0xca6bc801d5a4ba90!8m2!3d-7.0145936!4d110.4162447?hl=id&authuser=0" target="_blank">Direction</a>

                        <li>
                            <a href="mailto:victory.cia.semarang@gmail.com" target="_blank">
                                <i class="material-icons mr-2">
                                    email
                                </i>
                                victory.cia.semarang@gmail.com/halo@victory.sch.id
                            </a>
                        </li>

                        <li>
                            <a>
                                <i class="material-icons mr-2">
                                    phone
                                </i>
                                024-8312819
                            </a>
                        </li>

                        <h4 class="title text-white mt-5 mb-0">
                            Follow us on
                        </h4>
                        <a href="https://www.facebook.com/victoryschool.semarang" target="_blank">
                            <i class="fa fa-facebook-square 2x text-white mr-3"></i>
                        </a>
                        <a href="https://www.instagram.com/victory_cia" target="_blank">
                            <i class="fa fa-instagram 2x text-white mr-3"></i>
                        </a>
                    </ul>

                    <h5 class="title text-white d-none d-lg-block">© 2018 Victory Academy Christian School</h5>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <h4 class="title text-white mt-5 mb-3 mt-md-6">
                        QUICK LINKS
                    </h4>

                    <ul>
                        <li>
                            <a class="{{ Request::is('profile') ? 'active' : null }}" href="{{ route('profile') }}">PROFILE</a>
                        </li>
                        <li>
                            <a class="{{ Request::is('gallery') ? 'active' : null }}" href="{{ route('gallery') }}">GALLERY</a>
                        </li>
                        <li>
                            <a class="{{ Request::is('clubs') ? 'active' : null }}" href="{{ route('clubs') }}">CLUBS</a>
                        </li>
                        <li>
                            <a class="{{ Request::is('achievement') ? 'active' : null }}" href="{{ route('welcome.achievement') }}">ACHIEVEMENT</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <h4 class="title text-white mt-5 mb-3 mt-md-6">
                        ACCESS
                    </h4>

                    <ul>
                        <li>
                            <a href="{{ route('dashboard') }}" target="_blank">STAFF</a>
                        </li>

                        <li>
                            <a href="{{ route('dashboard') }}" target="_blank">STUDENT</a>
                        </li>

                        <li>
                            <a href="{{ route('courses.index') }}" target="_blank">COURSES</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <h4 class="title text-white mt-5 mb-3 mt-md-6">
                        NEWS
                    </h4>

                    <ul>
                        <li>
                            @foreach($latest_news as $key => $latest_new)
                            <a href="{{ route('welcome.news.slug', $latest_new->slug) }}">
                                <img class="card-img-top" src="{{ asset('file_uploads/news/'.$latest_new->picture) }}" alt="news">
              
                                <h4 class="title text-white mt-3 mb-1">
                                    {{ $latest_new->title }}
                                </h4>
                                <p class="p-a">
                                    {!! substr($latest_new->content, 0, 150) !!}
                                </p>
                            </a>
                            @endforeach
                        </li>
                    </ul>
                </div>
        
                <div class="col-12 d-block d-lg-none">
                    <h5 class="title text-white text-center mt-7">© 2018 Victory Academy Christian School</h5>
                </div>
            </div>
            
            {{-- TRADEMARK --}}
            <div class="row">
                <div class="col-12">
                    <h5 class="title text-white text-center">
                        Designed by <a class="link-a" href="https://dilangit.com/" target="_blank">diLangit Corporation</a> 
                    </h5>
                </div>
            </div>
            {{-- TRADEMARK --}}
        </div>
    </footer>
    <!-- FOOTER -->



    <!--   Core JS Files   -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="{{ asset('js/core/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/core/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/plugins/moment.min.js') }}"></script>
    <!--  Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="{{ asset('js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ asset('js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
    <!--  Plugin for Sharrre btn -->
    <script src="{{ asset('js/plugins/jquery.sharrre.js') }}" type="text/javascript"></script>
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('js/material-kit.js?v=2.0.4') }}" type="text/javascript"></script>

    <script src="{{ asset('js/slide-animation.js') }}"></script>

    <script src="{{ asset('js/jquery.fancybox.js') }}" type="text/javascript"></script>

    <script>
        AOS.init();

        $(document).ready(function() {
          //init DateTimePickers
          materialKit.initFormExtendedDatetimepickers();

          // Sliders Init
          materialKit.initSliders();
        });


        function scrollToDownload() {
          if ($('.section-download').length != 0) {
            $("html, body").animate({
              scrollTop: $('.section-download').offset().top
            }, 1000);
          }
        }


        $(document).ready(function() {

          $('#facebook').sharrre({
            share: {
              facebook: true
            },
            enableHover: false,
            enableTracking: false,
            enableCounter: false,
            click: function(api, options) {
              api.simulateClick();
              api.openPopup('facebook');
            },
            template: '<i class="fab fa-facebook-f"></i> Facebook',
            url: 'https://demos.creative-tim.com/material-kit/index'
          });

          $('#googlePlus').sharrre({
            share: {
              googlePlus: true
            },
            enableCounter: false,
            enableHover: false,
            enableTracking: true,
            click: function(api, options) {
              api.simulateClick();
              api.openPopup('googlePlus');
            },
            template: '<i class="fab fa-google-plus"></i> Google',
            url: 'https://demos.creative-tim.com/material-kit/index'
          });

          $('#twitter').sharrre({
            share: {
              twitter: true
            },
            enableHover: false,
            enableTracking: false,
            enableCounter: false,
            buttons: {
              twitter: {
                via: 'CreativeTim'
              }
            },
            click: function(api, options) {
              api.simulateClick();
              api.openPopup('twitter');
            },
            template: '<i class="fab fa-twitter"></i> Twitter',
            url: 'https://demos.creative-tim.com/material-kit/index'
          });

        });
    </script>
</body>
</html>
