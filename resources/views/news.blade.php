@extends('layouts.app')

@section('content')
<div class="section-news pt-6 pb-0 py-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-9">
                <div class="row py-5" style="border-bottom:3px solid #F2F2F2">
                    <div class="col-12">
                        <h2 class="title-c text-black mt-0 mb-5">LATEST NEWS</h2>
                    </div>

                    <!-- NEWS -->
                    @foreach($news as $key => $new)
                    <div class="col-12 col-md-6">
                        <div class="grid">
                            <figure class="effect-layla">
                                <a href="{{ route('welcome.news.slug', ['slug' => $new->slug]) }}">
                                    <img class="card-img-top" src="{{ asset('/file_uploads/news/'.$new->picture) }}" width="100%" alt="news">

                                    <figcaption>
                                        <p>
                                            <!-- DEFAULT LOGO -->
                                            <img src="{{ asset('img/logo.png') }}" width="100%">
                                            <!-- DEFAULT LOGO -->
                                        </p>
                                    </figcaption>
                                </a>
                            </figure>
                        </div>

                        <span class="label-victory">
                            {{ \Carbon\Carbon::parse($new->date)->formatLocalized('%b %d, %Y') }}
                        </span>
                    </div>

                    <div class="col-12 col-md-6">
                        <!-- TITLE -->
                        <h3 class="title mt-0">
                            <a href="{{ route('welcome.news.slug', ['slug' => $new->slug]) }}" class="link-c">{{ $new->title }}</a>
                        </h3>
                        <!-- TITLE -->

                        <!-- CONTENT -->
                        <p class="p-a my-3">
                            {!! substr($new->content, 0, 150) !!} ...
                        </p>
                        <!-- CONTENT -->

                        <!-- READ MORE BUTTON -->
                        <a href="{{ route('welcome.news.slug', ['slug' => $new->slug]) }}" class="btn btn-victory-b btn-round">
                        Read More
                        </a>
                        <!-- READ MORE BUTTON -->

                        <h5 class="title text-black mt-3">
                            by Admin
                            <br>
                            {{ \Carbon\Carbon::parse($new->created_at)->diffForHumans() }}
                        </h5>
                    </div>
                    @endforeach
                </div>
                <!-- NEWS -->

                <!-- PAGINATION -->
                <nav aria-label="Page navigation" class="mt-3">
                    {{ $news->links('pagination') }} 
                </nav>
                <!-- PAGINATION -->
            </div>
            <!-- END ROW -->

            <!-- RIGHT BAR NEWS -->
            <div class="col-12 col-lg-3 border-a">
                <div class="row pt-5 pb-0">
                    <div class="col-12">
                        <a href="login" target="_blank" class="btn btn-victory-b btn-round">Academic Access</a>
                        
                        <a href="mailto:victory.cia.semarang@gmail.com" target="_blank" class="btn btn-victory-b btn-round">Send a Message</a>
                    </div>

                    <div class="col-12">
                        <h2 class="title my-4">Latest News</h2>
                    </div>

                    <!-- LATEST NEWS -->
                    @foreach($latest_news as $key => $news)
                    <div class="col-12 col-md-4 col-lg-12">
                        <a href="{{ route('welcome.news.slug', $news->slug) }}">
                            <img class="card-img-top" src="{{ asset('/file_uploads/news/'.$news->picture) }}" alt="news" class="mt-5">

                            <h4 class="title mt-3 mb-1">
                                {{ $news->title }}
                            </h4>
                        </a>

                        <p class="p-a">{!! substr($news->content, 0, 100) !!} ...<br>
                            <a href="{{ route('welcome.news.slug', $news->slug) }}" class="link-b">
                                <span>View More</span>
                            </a>
                        </p>

                        <h5 class="title mt-0">{{ $news->date }}</h5>
                    </div>
                    @endforeach
                    <!-- LATEST NEWS -->

                    <div class="col-12 mt-5 mt-lg-3">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fvictoryschool.semarang%2F&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" scrolling="no" allowTransparency="true" allow="encrypted-media" class="widget"></iframe>
                        <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/en_GB/   sdk.js#xfbml=1&version=v3.2';
                            fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));
                        </script>
                    </div>
                </div>
            </div>
            <!-- RIGHT BAR NEWS -->
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</div>
<!-- END SECTION -->
@endsection