@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled">
                <a class="page-link">
                    <span>&laquo;</span>
                </a>
            </li>
        @else
            <li>
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a>
            </li>
        @endif

        @if($paginator->currentPage() > 3)
            <li class="hidden-xs">
                <a class="page-link" href="{{ $paginator->url(1) }}">1</a>
            </li>
        @endif
        @if($paginator->currentPage() > 4)
            <li>
                <a class="page-link active">....</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach (range(1, $paginator->lastPage()) as $i)
            @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                @if ($i == $paginator->currentPage())
                    <li class="active">
                        <a class="page-link">
                            <span>{{ $i }}</span>
                        </a>
                    </li>
                @else
                    <li>
                        <a class="page-link" href="{{ $paginator->url($i) }}">
                            {{ $i }}
                        </a>
                    </li>
                @endif
            @endif
        @endforeach

        @if($paginator->currentPage() < $paginator->lastPage() - 3)
            <li>
                <span>
                    <a class="page-link">...</a>
                </span>
            </li>
        @endif
        @if($paginator->currentPage() < $paginator->lastPage() - 2)
            <li class="hidden-xs">
                <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a>
            </li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li>
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a>
            </li>
        @else
            <li class="page-link disabled">
                <span>&raquo;</span>
            </li>
        @endif
    </ul>
@endif
