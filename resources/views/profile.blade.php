@extends('layouts.app')

@section('content')
<div class="section-profile pt-6 pb-0 py-lg-5">
    <div class="container">
        <div class="row">
            <!-- PROFILE -->
            <div class="col-12 col-lg-9">
                <div class="row py-5" style="border-bottom:3px solid #F2F2F2">
                    <div class="col-12">
                        <h2 class="title-c mt-0 mb-5">OUR PROFILE</h2>

                        <img src="{{ asset('img/logo-b.png') }}" width="150px">

                        <h2 class="title mt-0">Victory Academy</h2>

                        <p class="p-c max-width-a">
                            Victory Academy uses the International Accelerated Christian Education (ACE) Curriculum.
                        </p>

                        <p class="p-a max-width-a">
                            Our student is given a strong foundation from the Scriptures, Bible principles, wisdom, and Christian character development. Our students is guaranteed to have high academic standards through strong mastery of each subject. Our students diploma (ICCE *) will be recognized globally.
                        </p>
                    </div>
                </div>

                <div class="row py-4" style="border-bottom:3px solid #F2F2F2">
                    <div class="col-12 col-md-6">
                        <h3 class="title">VISION</h3>

                        <p class="p-a max-width-a">
                            Victory International Christian Academy has a vision of building Leaders of Faith, Knowledge & Character.
                        </p>
                    </div>

                    <div class="col-12 col-md-6 pl-md-4">
                        <h3 class="title">CURRICULUM INCORPORATES</h3>

                        <ul class="p-0">
                            <p class="p-a max-width-a">This Curriculum Incorporates. Individualized learning, every child is able to learn at his own rate. Mastery Learning, Each child progresses only when he is academically prepared for the next step. Character building, 60 Character traits with definitions and scripture are used throughtout each level of the curriculum. Leadership training, Student Convention and Educators Conferences are held to present new ways to organize, develop, and motivate school staff and students. Diagnostic testing, A new student is given diagnostic tests to determine his educational performance level and identity and tackle learning gaps for holistic education.</p>
                        </ul>
                    </div>
                </div>

                <div class="row py-5">
                    <div class="col-12">
                        <h2 class="title-c mt-0 mb-5">OUR PRIORITY</h2>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="card-img-top" src="{{ asset('img/slider-2.JPG') }}" width="100%" alt="profile">
                            </div>

                            <div class="col-12 col-md-6">
                                <p class="p-c text-white py-4 px-4 bg-victory">
                                    Victory Academy always strives for maximum results and always prioritizes the success of all the students
                                </p>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-12 col-md-10">
                                <h3 class="title mt-1 mb-0">Victory Academy has mission to develop interest in learning, reasoning skills, and discipline character in every child at an early age.</h3>
                                <p class="p-a mt-3">With our ability as a mentor who is experienced and has a high moral attitude. We guide, teach, and motivate our students to become obedient personal values ​​of God, uphold attitude and moral values, knowledge and virtue. Practice to be honest, disciplined and humble actions. Victory Academy is there to gather world-class knowledge and even be awarded the national level ACE. The development of our education system will always attempted to improve. So, our students become as expected by their parents and all of mentors.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PROFILE -->

            <!-- RIGHT BAR NEWS -->
            <div class="col-12 col-lg-3 border-a">
                <div class="row pt-5 pb-0">
                    <div class="col-12">
                        <a href="{{ route('login') }}" target="_blank" class="btn btn-victory-b btn-round">Academic Access</a>

                        <a href="mailto:victory.cia.semarang@gmail.com" target="_blank" class="btn btn-victory-b btn-round">Send a Message</a>
                    </div>

                    <div class="col-12">
                        <h2 class="title my-4">Latest News</h2>
                    </div>

                    @foreach($latest_news as $key => $news)
                    <div class="col-12 col-md-4 col-lg-12">
                        <a href="{{ route('welcome.news.slug', $news->slug) }}">
                            <img class="card-img-top" src="{{ asset('/file_uploads/news/'.$news->picture) }}" alt="news" class="mt-5">

                            <h4 class="title mt-3 mb-1">{{ $news->title }}
                            </h4>
                        </a>

                        <p class="p-a">{!! substr($news->content, 0, 100) !!} ...<br>
                            <a href="{{ route('welcome.news.slug', $news->slug) }}" class="link-b"><span>View More</span>
                            </a>
                        </p>

                        <h5 class="title mt-0">{{ $news->date }}</h5>
                    </div>
                    @endforeach

                    <div class="col-12 mt-5 mt-lg-3">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fvictoryschool.semarang%2F&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" scrolling="no" allowTransparency="true" allow="encrypted-media" class="widget"></iframe>
                        <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/en_GB/   sdk.js#xfbml=1&version=v3.2';
                            fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));
                        </script>
                    </div>
                </div>
            </div>
            <!-- RIGHT BAR NEWS -->
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</div>
@endsection