@extends('layouts.app')

@section('content')
<!-- NEWS -->
<!-- BACKEND AUTO UPDATE NEWS -->
<div class="section-news pt-6 pb-0 py-lg-5">
    <!-- SHARE ICON -->
    {{-- <div class="icon-bar">
        <a href="#" class="facebook" data-toggle="tooltip" data-original-title="Share on Facebook">
            <i class="fa fa-facebook"></i>
        </a>

        <a href="#" class="instagram" data-toggle="tooltip" data-original-title="Share on Instagram">
            <i class="fa fa-instagram"></i>
        </a>

        <a href="#" class="twitter" data-toggle="tooltip" data-original-title="Share on Twitter">
            <i class="fa fa-twitter"></i>
        </a>
    </div> --}}
    <!-- SHARE ICON -->

    <div class="container">
        <div class="row">
            <!-- SINGLE NEWS -->
            <div class="col-12 col-lg-9">
                <div class="row py-5" style="border-bottom:3px solid #F2F2F2">
                    <div class="col-12">
                        <!-- TITLE -->
                        <h2 class="title-c text-black mt-5 mb-5 mt-lg-0">{{ $achievement->title }}</h2>
                        <!-- TITLE -->

                        <!-- IMAGE -->
                        <img class="card-img-top" src="{{ asset($achievement->picture) }}" width="100%" alt="news">
                        <!-- IMAGE -->

                        <h3 class="title-b text-center">Victory Academy has graduate all of students with Accelerated Christian Education and International Curriculum.</h3>
                        <p class="p-a text-justify text-black mt-4"><b><i>SEMARANG, 9/6/2018 – Victory Academy has graduate all of students with Accelerated Christian Education and International Curriculum.</i></b></p>
                        <p class="p-a text-justify">{!! $achievement->content !!}</p>
                        <h5 class="title text-black mt-5 mb-0">by Admin<br>{{ $achievement->created_at }}</h5>
                    </div>
                </div>
                
                <div class="row py-5">
                    <!-- PREV BUTTON -->
                    <div class="col-12 col-md-5 col-xl-4">
                        <a href="single-news-2" class="link-prev btn btn-victory-b">
                            Previous<br>
                            <h4 class="title-b">Victory Academy Graduation</h4>
                            <i class="fa fa-arrow-left pb-3 hvr-icon"></i>
                        </a>
                    </div>
                    <!-- PREV BUTTON -->

                    <!-- NEXT BUTTON -->
                    <div class="col-12 col-md-5 col-xl-4">
                        <a href="single-news-2" class="link-next btn btn-victory-b">
                            Previous<br>
                            <h4 class="title-b">Victory Academy Graduation</h4>
                            <i class="fa fa-arrow-right pb-3 hvr-icon"></i>
                        </a>
                    </div>
            
                    <!-- NEXT BUTTON -->
                </div>
            </div>
            <!-- SINGLE NEWS -->

            <!-- RIGHT BAR NEWS -->
            <div class="col-12 col-lg-3 border-a">
                <div class="row pt-5 pb-0">
                    <div class="col-12">
                        <a href="login" target="_blank" class="btn btn-victory-b btn-round">
                            Academic Access
                        </a>

                        <a href="mailto:victory.cia.semarang@gmail.com" target="_blank" class="btn btn-victory-b btn-round">Send a Message</a>
                    </div>

                    <div class="col-12">
                        <h2 class="title my-4">Latest News</h2>
                    </div>
                    <div class="col-12 col-md-4 col-lg-12">
                        <a href="single-news-2">
                            <img class="card-img-top" src="{{ asset('img/slider-1.JPG') }}" alt="news" class="mt-5">
                            <h4 class="title mt-3 mb-1">Victory Academy Graduation</h4>
                        </a>

                        <p class="p-a">
                            Victory Academy has graduate all of students with ACE Curriculum.<br>

                            <a href="single-news-2" class="link-b">
                                <span>View More</span>
                            </a>
                        </p>
                        
                        <h5 class="title mt-0">9 June 2018</h5>
                    </div>
                    <div class="col-12 mt-5 mt-lg-3">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fvictoryschool.semarang%2F&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" scrolling="no" allowTransparency="true" allow="encrypted-media" class="widget"></iframe>
                        <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/en_GB/   sdk.js#xfbml=1&version=v3.2';
                            fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));
                        </script>
                    </div>
                </div>
            </div>
            <!-- RIGHT BAR NEWS -->
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</div>
<!-- END SECTION -->
@endsection