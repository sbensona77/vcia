@extends('layouts.app')

@section('content')
<!-- NEWS -->
<div class="section-news pt-6 pb-0 py-lg-5">
    <!-- SHARE ICON -->
    {{-- <div class="icon-bar">
        <a href="#" class="facebook" data-toggle="tooltip" data-original-title="Share on Facebook">
            <i class="fa fa-facebook"></i>
        </a> 

        <a href="#" class="instagram" data-toggle="tooltip" data-original-title="Share on Instagram">
            <i class="fa fa-instagram"></i>
        </a> 

        <a href="#" class="twitter" data-toggle="tooltip" data-original-title="Share on Twitter">
            <i class="fa fa-twitter"></i>
        </a>
    </div> --}}
    <!-- SHARE ICON -->
    
    <!-- SINGLE NEWS -->
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-9">
                <div class="row py-5" style="border-bottom:3px solid #F2F2F2">
                    <div class="col-12">
                        <!-- TITLE -->
                        <h2 class="title-c text-black mt-5 mb-5 mt-lg-0">
                            {{ $news->title }}
                        </h2>
                        <!-- TITLE -->

                        <!-- PICTURE -->
                        <img class="card-img-top" src="{{ asset('file_uploads/news/'.$news->picture) }}" width="100%" alt="news">
                        <!-- PICTURE -->

                        <!-- CONTENT -->
                        <p class="p-a text-justify text-black mt-4">
                            {{-- RUNABLE SCRIPT CONTENT --}}
                            {!! $news->content !!}
                            {{-- RUNABLE SCRIPT CONTENT --}}
                        </p>
                        <!-- CONTENT -->

                        <h5 class="title text-black mt-5 mb-0">
                            by Admin
                            <br>
                            {{ $news->date }}
                        </h5>
                    </div>
                </div>

                <div class="row py-5">
                    <!-- PREV BUTTON -->
                    @if(isset($previous))
                    <div class="col-12 col-md-5 col-xl-4">
                        <a href="{{ route('welcome.news.slug', $previous['slug']) }}" class="link-prev btn btn-victory-b">
                            Previous
                            <br>
                            <h4 class="title-b">
                                {{ $previous['title'] }}
                            </h4>
                            <i class="fa fa-arrow-left pb-3 hvr-icon"></i>
                        </a>
                    </div>
                    @endif
                    <!-- PREV BUTTON -->

                    <!-- NEXT BUTTON -->
                    @if(isset($next))
                    <div class="col-12 col-md-5 col-xl-4">
                        <a href="{{ route('welcome.news.slug', $next['slug']) }}" class="link-next btn btn-victory-b">
                            Next
                            <br>
                            <h4 class="title-b">
                                {{ $next['title'] }}
                            </h4>
                            <i class="fa fa-arrow-right pb-3 hvr-icon"></i>
                        </a>
                    </div>
                    @endif
                    <!-- NEXT BUTTON -->
                </div>
            </div>
            <!-- SINGLE NEWS -->

            <!-- RIGHT BAR NEWS -->
            <div class="col-12 col-lg-3 border-a">
                <div class="row pt-5 pb-0">
                    <div class="col-12">
                        <a href="login" target="_blank" class="btn btn-victory-b btn-round">Academic Access</a>

                        <a href="mailto:victory.cia.semarang@gmail.com" target="_blank" class="btn btn-victory-b btn-round">Send a Message</a>
                    </div>

                    <div class="col-12">
                        <h2 class="title my-4">Latest News</h2>
                    </div>

                    @foreach($latest_news as $key => $late_news)
                    <div class="col-12 col-md-4 col-lg-12">
                        <a href="{{ route('welcome.news.slug', $late_news->slug) }}">
                            <img class="card-img-top" src="{{ asset('file_uploads/news/'.$late_news->picture) }}" alt="news" class="mt-5">

                            <h4 class="title mt-3 mb-1">{{ $late_news->title }}</h4>
                        </a>
                        <p class="p-a">
                            {!! substr($late_news->content, 0, 150) !!} ...<br>

                            <a href="{{ route('welcome.news.slug', $late_news->slug) }}" class="link-b"><span>View More</span></a>
                        </p>
                        <h5 class="title mt-0">{{ \Carbon\Carbon::parse($late_news->date)->formatLocalized('%b %d, %Y') }}</h5>
                    </div>
                    @endforeach

                    <div class="col-12 mt-5 mt-lg-3">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fvictoryschool.semarang%2F&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" scrolling="no" allowTransparency="true" allow="encrypted-media" class="widget"></iframe>
                        <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/en_GB/   sdk.js#xfbml=1&version=v3.2';
                            fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));
                        </script>
                    </div>
                </div>
            </div>
            <!-- RIGHT BAR NEWS -->
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</div>
<!-- END SECTION -->
@endsection