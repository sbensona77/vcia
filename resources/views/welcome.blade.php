@extends('layouts.app')

@section('content')
<!-- CAROUSEL SLIDE -->
<div class="card card-raised card-carousel m-0 pt-5 pt-lg-6 pt-xl-0">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="10000">
        <div class="carousel-inner">
            {{-- FIRST SLIDE --}}            
            <div class="carousel-item active animated fadeIn">
                <img class="carousel-img d-none d-sm-block" src="{{ asset('img/bg-update1.jpg') }}" alt="First slide">

                <img class="carousel-img d-block d-sm-none" src="{{ asset('img/bg-update1-square.jpg') }}" alt="First slide">

                <div class="carousel-caption animated fadeInLeft">
                    <div class="content">
                        <h1 class="animated fadeInLeft delay-1">
                            Victory Academy Semarang
                        </h1>

                        <h3 class="title-c text-white animated fadeInLeft delay-2">
                            Victory Academy Christian School has a vision of building Leaders of Faith, Knowledge & Character. 
                        </h3>
                    </div>
                </div>
            </div>
            {{-- FIRST SLIDE --}}

            {{-- SECOND SLIDE --}}
            <div class="carousel-item animated fadeIn">
                <img class="carousel-img d-none d-sm-block" src="{{ asset('img/bg-update2.jpg') }}" alt="Second slide">

                <img class="carousel-img d-block d-sm-none" src="{{ asset('img/bg-update2-square.jpg') }}" alt="Second slide">

                <div class="carousel-caption animated fadeInLeft">
                    <div class="content">
                        <h2 class="animated fadeInLeft delay-1">
                            Accelerated Christian Education
                        </h2>

                        <p class="animated fadeInLeft delay-2">
                            Accelerated Christian Education (ACE) Curriculum is our school standards. Our school provides a solid foundation of Scriptures, Biblical principles and wisdoms, Christian morals as well as divine characters development. Our school also ensure high academic standards through a solid mastery of each subject lesson.
                        </p>
                    </div>
                </div>
            </div>
            {{-- SECOND SLIDE --}} 

            {{-- THIRD SLIDE --}}
            <div class="carousel-item animated fadeIn">
                <img class="carousel-img d-none d-sm-block" src="{{ asset('img/bg-update3.jpg') }}" alt="Third slide">

                <img class="carousel-img d-block d-sm-none" src="{{ asset('img/bg-update3-square.jpg') }}" alt="Third slide">

                <div class="carousel-caption animated fadeInLeft">
                    <div class="content">
                        <h2 class="animated fadeInLeft delay-1">
                            Student Convention
                        </h2>

                        <p class="animated fadeInLeft delay-2">
                            To discover gifts in your children and provide them with a competitive advantage, annual Student Conventions are held nationally (in Indonesia), regionally (for all South Pacific), and internationally (in the USA) allowing young people to compete in over 140 events ranging from music, arts, speech, athletics, and academic categories.
                        </p>
                    </div>
                </div>
            </div>
            {{-- THIRD SLIDE --}}
        </div>
        {{-- CAROUSEL BUTTON --}}

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <i class="material-icons">keyboard_arrow_left</i>

            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <i class="material-icons">keyboard_arrow_right</i>

            <span class="sr-only">Next</span>
        </a>
        {{-- CAROUSEL BUTTON --}}
    </div>
</div>
<!-- CAROUSEL SLIDE -->

<!-- PARALLAX -->
<div class="parallax py-5 py-sm-6 px-0 px-md-10 py-lg-8">
    <div class="container">
        <div class="col-12 col-parallax py-3 px-3 py-md-5 text-center">
            <h2 class="title text-white">
                <q>COME & SEE OUR FACILITES</q>
            </h2>

            <p class="p-b text-white mb-4">
                Victory Academy uses the Accelerated Christian Education (ACE) Curriculum.
            </p>

            <a href="{{ route('profile') }}" class="btn btn-victory-b btn-round">
                View Profile
            </a>
        </div>
    </div>
</div>
<!-- PARALLAX -->

<!-- PROMOTION -->
<div class="section-promo py-6">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 text-center text-md-left mt-4 mt-md-0 slideanim">
                <h2 class="title text-white mb-2">Our Clubs</h2>

                <p class="p-a text-white">
                    Our clubs always give a chance to all of students for improve their talent
                </p>

                <a href="{{ url('/clubs') }}" class="btn btn-victory btn-raised btn-round" data-toggle="tooltip" data-original-title="Look at our clubs">See More</a>
            </div>

            <div class="col-12 col-md-4 text-center text-md-left mt-4 mt-md-0 slideanim">
                <h2 class="title text-white mb-2">Our Profile</h2>
                
                <p class="p-a text-white">
                    Our school always do the best to make all of students get what they need
                </p>

                <a href="{{ url('/profile') }}" class="btn btn-victory btn-raised btn-round" data-toggle="tooltip" data-original-title="Look at our profile">See More</a>
            </div>

            <div class="col-12 col-md-4 text-center text-md-left mt-4 mt-md-0 slideanim">
                <h2 class="title text-white mb-2">Our Achievement</h2>

                <p class="p-a text-white">
                    Our school gets some achievements because of our education system
                </p>

                <a href="{{ url('/achievement') }}" class="btn btn-victory btn-raised btn-round" data-toggle="tooltip" data-original-title="Look at our achievement">See More</a>
            </div>
        </div>
    </div>
</div>
<!-- PROMOTION -->

<!-- FACILITIES -->
<div class="section-club py-6 py-md-8">
    <div class="container">
        <h2 class="title-b text-center text-md-left">
            OUR CLUBS
        </h2>

        <hr class="hr-a d-block d-md-none">

        <div class="row">
            @foreach($clubs as $key => $club)
            <div class="col-12 col-md-6 offset-md-0 slideanim">
                <div class="card">
                    <div class="card-body" style="height:600px">
                        <h4 class="title mb-0">{{ $club->name }}</h4>
                        <div class="hover08 column">
                            <a href="clubs">
                                <figure>
                                    <img class="card-img-top" src="{{ asset($club->picture) }}" width="100%" alt="club">
                                </figure>
                            </a>
                        </div>
                        <h5 class="card-title">{{ $club->motto }}</h5>
                        <p class="p-a card-text">{!! substr($club->detail, 0, 300) !!}...</p>
                        <a href="{{ route('clubs') }}" class="btn btn-victory-b btn-round">View More</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

</div>
</div>
<!-- CLUB -->

<!-- FEEDS -->
<div class="section-feeds py-5 py-md-6">
    <div class="container">
        <h2 class="title-b text-center text-md-left">
            Follow Us on Instagram
        </h2>

        <hr class="hr-a d-block d-md-none">

        <div class="row">
            <div class="col-12 mt-3 slideanim">
                <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>

                <iframe src="//lightwidget.com/widgets/9cbe9bd970ae5443b21f7c42e7166df0.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
            </div>

            <div class="col-12 text-center">
                <a href="https://www.instagram.com/victory_cia/" class="btn btn-victory-b btn-round">More</a>
            </div>
        </div>
    </div>
</div>
<!-- FEEDS -->

<!-- CONTACT FORM -->
<div class="section-contact py-7">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-4 offset-lg-0 slideanim">
                <div class="card card-login">
                    <form class="form" method="post" action="{{ route('post-contact') }}">
                        <input name="_token" value="{{ csrf_token() }}" type="hidden">

                        <div class="card-header card-header-victory text-center">
                            <h4 class="card-title">Ask A Question</h4>
                            <div class="social-line">
                                <a href="https://www.facebook.com/victoryschool.semarang" target="_blank" class="btn btn-just-icon btn-link">
                                    <i class="fa fa-facebook-square"></i>
                                </a>

                                <a href="https://www.instagram.com/victory_cia" target="_blank" class="btn btn-just-icon btn-link">
                                    <i class="fa fa-instagram"></i>
                                </a>

                                <a href="#" class="btn btn-just-icon btn-link">
                                    <i class="fa fa-phone"></i>
                                </a>
                            </div>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                {{ $error }} <br>
                                @endforeach
                            </div>
                            <br />
                        @endif
                        @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}  
                            </div>

                            <br/>
                        @elseif(session()->get('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>

                            <br/>
                        @endif
                        
                        <div class="card-body">
                            <span class="bmd-form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">face</i>
                                        </span>
                                    </div>

                                    <input name="name" type="text" class="form-control" placeholder="Name...">
                                </div>
                            </span>

                            <span class="bmd-form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">mail</i>
                                        </span>
                                    </div>

                                    <input name="email" type="email" class="form-control" placeholder="Email...">
                                </div>
                            </span>
                            <span class="bmd-form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">comment</i>
                                        </span>
                                    </div>

                                    <textarea name="message" class="form-control" rows="5" placeholder="Comments..."></textarea>
                                </div>
                            </span>
                        </div>

                        <div class="footer text-center">
                            <button type="submit" class="btn btn-victory-b">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-12 col-lg-8">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1177.3021341652452!2d110.41621049241573!3d-7.014389746256571!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708b64fb0593bd%3A0xca6bc801d5a4ba90!2sVictory+Academy!5e0!3m2!1sid!2sid!4v1543730136664" class="maps" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<!-- CONTACT FORM -->
@endsection