<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('error/{type}', 'ErrorController@redirect');

Route::get('/', 'FrontPageController@index')->name('welcome');
Route::get('profile', 'FrontPageController@profile')->name('profile');
Route::get('gallery', 'FrontPageController@gallery')->name('gallery');
Route::get('achievement', 'FrontPageController@achievement')->name('welcome.achievement');
Route::get('achievement/{id}', 'FrontPageController@show_achievement')->name('welcome.achievement.view');
Route::get('news', 'FrontPageController@news')->name('welcome.news');
Route::get('clubs', 'FrontPageController@club')->name('clubs');
Route::get('academics', 'FrontPageController@academic')->name('academics');
Route::get('news/show/{slug}', 'FrontPageController@showNewsBySlug')->name('welcome.news.slug');

Route::post('/', 'FrontPageController@post_contact')->middleware('web')->name('post-contact');

// redirects
Route::redirect('/home', '/dashboard', 301)->name('home');

// in dashboards
Route::group([
    'prefix' => 'dashboard', 
    'middleware' => [
        'web', 
        'auth', 
        'status'
    ]
], 
    function() {
    // users
    Route::get('users/students', 'UserController@student')->name('students.index');
    Route::get('users/teachers', 'UserController@teacher')->name('teachers.index');
    Route::post('users/search/', 'UserController@search')->name('users.search');

    // all users
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::get('faq', 'HomeController@faq')->name('faq');

    // user profile
    Route::get('profile/{id}', 'UserProfileController@show')->name('user_profile.show');
    Route::match(['put', 'patch'], 'profile/{id}/update', 'UserProfileController@update')->name('user_profile.update');

    //download material
    Route::get('download_material/{file_id}', 'MaterialsUploadController@download_material')->name('download_material')->middleware('enroll');

    // enroll
    Route::post('enroll-course/{course_id}', 'CourseController@enroll')->name('enroll-course');
    Route::post('enroll-material/{material_id}', 'CourseController@enroll')->name('enroll-material');

    // courses
    Route::resources([
        'courses' => 'CourseController',
        'courses/{course_id}/materials' => 'CoursesMaterialController',
        'payments' => 'PaymentController',
    ]);

    // parent
    Route::get('user/parent', 'UserController@parent')->name('parents.index');

    // account setting
    Route::match(['put', 'patch'], 'profile/{id}/update-account', 'UserController@updateAccount')->name('account-update');

    Route::group(['middleware' => ['parent']], function() {
        // confirm weekly report
        Route::match(['put', 'patch'], 'confirm/{id}', 'WeeklyReportController@confirm')->name('confirm-report');
        Route::match(['put', 'patch'], 'confirm/all/{id}', 'WeeklyReportController@confirmAll')->name('confirm-all-report');
    });

    // inventory
    Route::match(['put', 'patch'], 'inventories/return/{user_id}', 'InventoryItemController@return')->name('return'); // return item

    Route::group(['middleware' => ['teacher']], function() {
        Route::get('get-material/{material_id}', 'AssignCourseController@getMaterial')->name('get-material');
        Route::get('get-course/{course_id}', 'AssignCourseController@getCourse')->name('get-course');
        Route::get('get-all-pace-assigned/{assign_id}', 'AssignCourseController@getAllAssignedMaterials')->name('get-all-pace-assigned');
        Route::match(['put', 'patch'], 'fill-grade/{id}', 'AssignCourseController@fillPaceGrade')->name('assign-material.grade');
        Route::match(['put', 'patch'], 'jump-pace/{id}', 'AssignCourseController@jumpPace')->name('assign-material.jump');
        Route::match(['put', 'patch'], 'unjump-pace/{id}', 'AssignCourseController@unJumpPace')->name('assign-material.unjump');
        Route::match(['put', 'patch'], 'fill-all-grade/{assign_id}', 'AssignCourseController@fillAllGrade')->name('fill-all-grade');

        // upload materials
        Route::post('courses/materials/upload_file', 'MaterialsUploadController@store')->name('upload-material');

        // level up and down
        Route::match(['put', 'patch'], 'students/leveling/{user_id}/up', 'UserProfileController@levelUpStudent')->name('students.level_up');
        Route::match(['put', 'patch'], 'students/leveling/{user_id}/down', 'UserProfileController@levelDownStudent')->name('students.level_down');

        // Victory Points
        Route::post('victory-points/{user_id}/grant', 'VictoryPointsController@grantPoint')->name('victory-points.grant');
        Route::delete('victory-points/{id}/reduce', 'VictoryPointsController@reducePoint')->name('victory-points.reduce');
        Route::delete('victory-points/{id}/destroy', 'VictoryPointsController@destroy')->name('victory-points.delete');

        // Students Report Download
        Route::post('profile/{student_id}/print_report', 'UserProfileController@createReport')->name('profile.print-report');

        // Inventories
        Route::post('inventories/store-item', 'InventoryController@storeInventory')->name('inventories.store-item');
        Route::match(['put', 'patch'], 'inventories/update-item/{id}', 'InventoryController@updateInventory')->name('inventories.update-item'); // update item
        Route::match(['put', 'patch'], 'inventories/borrow/{user_id}', 'InventoryItemController@borrow')->name('borrow'); // borrow
        Route::match(['put', 'patch'], 'inventories/give-item/{item_id}', 'InventoryItemController@borrowForUser')->name('give-item');
        Route::post('inventories/store-one-item', 'InventoryItemController@store')->name('store.one');
        // get all items
        Route::get('inventories/{id}/get-items', 'UserProfileController@getAllItems')->name('get-all-items');

        // staff managements
        Route::get('staffs-managements/search/', 'StaffManagementController@search')->name('staffs-managements.search');
        Route::match(['put', 'patch'], 'staffs-managements/{management_id}/finish', 'InventoryItemController@finish')->name('staffs-managements.finish');
        Route::match(['put', 'patch'], 'staffs-managements/{management_id}/cancel', 'InventoryItemController@cancel')->name('staffs-managements.cancel');

        Route::resources([
            'academics' => 'AcademicController',
            'news' => 'NewsController',
            'achievement' => 'AchievementController',
            'clubs' => 'ClubController',
            'assigned-club' => 'AssignedClubController',
            'contact-forms' => 'ContactFormController',
            'assign-material' => 'AssignCourseController',
            'victory-points' => 'VictoryPointsController',
            'weekly_report' => 'WeeklyReportController',
            'parent-relation' => 'ParentsRelationController',
            'inventories' => 'InventoryController',
            'students-habit' => 'StudentsHabitController',
            'bible-memories' => 'BibleMemoryController',
            'staffs-managements' => 'StaffManagementController',
            'assigned-clubs' => 'AssignedClubController',
        ]);

        // AJAX
        Route::get('profile/ajax/{course_id}/all_materials', 'UserProfileController@getAllMaterials')->name('ajax-get-all-materials');
        Route::get('inventories/{id}/get-items', 'UserProfileController@getAllItems')->name('get-all-items');
        Route::get('inventories/{id}/get-items-code', 'UserProfileController@getAllItemsCode')->name('get-all-items-code');

        // print report
        Route::post('print/student-report/{student_id}', 'UserProfileController@printTermReport')->name('exports.student-report');
        Route::post('print/student-national-report/{student_id}', 'UserProfileController@printSemesterReport')->name('exports.student-national-report');
    });

    Route::group(['middleware' => ['admin']], function() {
        // admins
        Route::get('users/admins', 'UserController@admin')->name('admins.index');

        // inventories
        Route::delete('inventories/destroy-item/{id}', 'InventoryController@destoryInventory')->name('inventories.destroy-item');
        Route::delete('inventories/destroy-one-item/{id}', 'InventoryItemController@destroy')->name('destroy.item');

        // payment
        Route::post('payments/broadcast', 'PaymentController@broadcastPayment')->name('payments.broadcast');

        // turn on/off semestera and terms
        Route::match(['put', 'patch'], 'academic-years/turn-on/{semester_id}', 'AcademicYearController@turnOnSemester')->name('academic-years-turn-on');
        Route::match(['put', 'patch'], 'academic-years/turn-off/{semester_id}', 'AcademicYearController@turnOffSemester')->name('academic-years-turn-off');

        // print inventory lists
        Route::get('/inventories/{id}/print-list', 'InventoryCategoryController@printAllItems')->name('print-inventory');

        // print profile
        Route::get('/profile/{id}/print', 'UserProfileController@printUserProfile')->name('print-user-profile');

        // change payment status
        Route::match(['put', 'patch'], 'payments/{payment_id}/change_status', 'PaymentController@changeStatus')->name('payments.change-status');

        Route::resources([
            // user management
            'users' => 'UserController',

            // front page management
            'news' => 'NewsController',

            // academic year
            'academic-years' => 'AcademicYearController',
            'academic-year-terms' => 'AcademicYearTermController',
        ]);
    });
});

